


<%@page import="com.mollatech.axiom.v2.core.rss.AxiomStatus"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page import="org.json.JSONObject"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<%
    try {
        AxiomWrapper axWrapper = new AxiomWrapper();
        String host = request.getContextPath().toLowerCase();

        String sessionId = null;
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Token self destroy successful!!!";

        if (session.getAttribute("_apsession") == null) {

            json.put("_result", "error");
            json.put("_message", "Invalid Session!!!");

            String callbackstrr = request.getParameter("jsoncallback");
            String strresponser = callbackstrr + "(" + json.toString() + ")";
            response.setContentType("application/json");
            out.print(strresponser);
            return;

        }
        if (session.getAttribute("_rssDemoUserCerdentials") == null) {

            json.put("_result", "error");
            json.put("_message", "User Not Found!!!");
            String callbackstrr = request.getParameter("jsoncallback");
            String strresponser = callbackstrr + "(" + json.toString() + ")";
            response.setContentType("application/json");
            out.print(strresponser);
            return;

        }

        sessionId = (String) session.getAttribute("_apsession");

        RssUserCerdentials _userObj = (RssUserCerdentials) session.getAttribute("_rssDemoUserCerdentials");

        if (_userObj != null) {
            // Array.Clear(_userObj.tokenDetails, 0, _userObj.tokenDetails.Length);

            AxiomCredentialDetails axiomCredentialDetails = new AxiomCredentialDetails();
            axiomCredentialDetails.setCategory(2);// 2 - sw token
            axiomCredentialDetails.setSubcategory(1);//1 - web token
            axiomCredentialDetails.setStatus(-2);;
            _userObj.getTokenDetails().add(axiomCredentialDetails);
            AxiomStatus aStatus = axWrapper.ChangeTokenStatus(sessionId, _userObj.getRssUser().getUserId(), null, 2, null);

            if (aStatus.getErrorcode() == 0) {//success
                json.put("_result", result);
                json.put("_message", message);
                session.setAttribute("_apsession", null);
                session.setAttribute("_apuserObj", null);
                response.sendRedirect("tokenDestroyed.jsp");

            } else {
                result = "error";
                message = "Failed to destroy token!!!";
                json.put("error", result);
                json.put("_message", message);
                session.setAttribute("_apsession", null);
                session.setAttribute("_apuserObj", null);

                response.setContentType("application/json");
                out.print(json);

                return;
            }
        }
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>