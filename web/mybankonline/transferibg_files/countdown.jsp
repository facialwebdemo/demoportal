














var timer0 = 300;
var timer = timer0;
var has_reset = false;
var logoutUrl = '/personal/login/logout.do';
var popped; 

function countDown1() {
	window.status = 'MyBankonline  ' + (this.timer);
	if (timer == 30) {
		var url = '/personal/timeOut_msg.jsp';
		popped = window.open(url,'popupmsg','toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,width=275,height=210');
	}
	if (timer == 0) {
		doOnUnload();
		window.location = logoutUrl;
		return;
	}
    timer--; 
	setTimeout(countDown1, 1000);
}

function resetTimer() {
	if (has_reset) {
		timer = timer0;  
		has_reset = false;
	}
}

function doOnUnload() {
	if (typeof(popped) != 'undefined') {
		popped.close();
	}
}

countDown1();

(function() {
	// Disable right click on the page
	document.oncontextmenu = function () {
		return false;
	}
})();
