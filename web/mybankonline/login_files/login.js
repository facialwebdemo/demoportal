
var ilocation = "Not Available";
var ibrowser = "Not Available";
var agent = "Not Available";
var googleAddress = "Not Available";
function strcmp(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4DemoPortal(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}
function strcmpDemoPortal(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function get_time_zone_offset() {
    var current_date = new Date();
    return -current_date.getTimezoneOffset() / 60;
}

function Alert4login(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result === false) {
        } else {
            //end here
        }
    });
}


function TellTimezone() {
    var s = './timezone?_gmt=' + get_time_zone_offset();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //alert(data._result);
        }
    });
}

function RefreshforgotPassword() {
    window.location.href = "./index.jsp";
}

function forgotpassword() {
    var s = './forgotpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if (strcmp(data._result, "success") === 0) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshforgotPassword, 5000);
            }
        }
    });
}

function changeoperatorpasswordAfter1stLogin() {
    var s = './changeoprpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ChangePasswordform").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
                $('#Password-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#_oldPassword').val("");
                $('#_newPassword').val("");
                $('#_confirmPassword').val("");
                //ClearAddOperatorForm();
            }
            else if (strcmp(data._result, "success") === 0) {
                $('#Password-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                $('#passwordChange').attr("disabled", true);
                window.setTimeout(RefreshLogout, 5000);
            }
        }
    });
}



function Login(latitude, longitude, xCoordinate, yCoordinate) {
    if (authType === '6') {
        if (xCoordinate === '') {
            alert("Please Select Sweet Spot");
            return;
        }
    }
    var s = '../login?_lattitude=' + latitude + '&_longitude=' + longitude + '&_xCoordinate=' + xCoordinate + '&_yCoordinate=' + yCoordinate;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
               // alert(data._message);
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "success") === 0) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}

function refreshOTPverify() {

}

function Login1(latitude, longitude, xCoordinate, yCoordinate) {
//    if (xCoordinate === '') {
//        alert("Please Select Sweet Spot");
//        return;
//    }
    var s = './../login1?_lattitude=' + latitude + '&_longitude=' + longitude + '&_xCoordinate=' + xCoordinate + '&_yCoordinate=' + yCoordinate;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
               // alert("Error");
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>" + data._message1);
                //Alert4DemoPortal("<span><font color=blue>" + data._message + "</font></span></br>"+ data._message1+ "</font></span></br>"+data._message2+"</font></span>");
            } else if (strcmp(data._result, "success") === 0) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>" + data._message1 + "</font></span>" + data._message2 + "</font></span>");
//                Alert4DemoPortal("<span><font color=blue>" + data._message + "</font></span></br>"+ data._message1+ "</font></span></br>"+data._message2+"</font></span>");
//                window.setTimeout(window.location.href = data._url,5000);
                window.location.href = data._url;
            }
        }
    });
}

function loginpass(latitude, longitude, xCoordinate, yCoordinate) {
//    if (xCoordinate === '') {
//        alert("Please Select Sweet Spot");
//        return;
//    }
    var s = '../loginpass?_lattitude=' + latitude + '&_longitude=' + longitude;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
               // alert(data._message);
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "success") === 0) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}

function RefreshLogout() {
    window.location.href = "./signout.jsp";
}
function fnClear() {

}

function geologins() {
//    alert("hi");
//    var xCoordinate = document.getElementById('x').innerHTML;
//    var yCoordinate = document.getElementById('y').innerHTML;
    var authType = document.getElementById('authType').value;
    var xCoordinate = 0;
    var yCoordinate = 0;
    if (authType === '6') {
//        alert(authType);
        xCoordinate = document.getElementById('x').innerHTML;
        yCoordinate = document.getElementById('y').innerHTML;
    }
//      alert(xCoordinate);
//      alert(yCoordinate);
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        Login("latitude", "longitude", xCoordinate, yCoordinate);
        if (authType === '7') {
            Login1("latitude", "longitude", xCoordinate, yCoordinate);
        } else {
            Login("latitude", "longitude", xCoordinate, yCoordinate);
        }

//        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        if (authType === '7') {
            Login1(latitude, longitude, xCoordinate, yCoordinate);
        } else {
            Login(latitude, longitude, xCoordinate, yCoordinate);
        }
    }
    function error(error) {
        Login("latitude", "longitude", xCoordinate, yCoordinate);

//        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}

function geoSendSignatureOOB() {
//    alert("in geo send soob");
//alert(ttype);
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        sendSignatureOOB(latitude, longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}

function sendSignatureOOB(latitude, longitude, ttype) {
//    alert(ttype);
    var s = './../../sendSignatureOOB?_lattitude=' + latitude + '&_longitude=' + longitude;

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#transactionauth").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
                $('#sotpresult').html("<span><font color=blue>" + data._message + "</font></span>");
            } else if (strcmp(data._result, "success") === 0) {
                $('#sotpresult').html("<span><font color=blue>" + data._message + "</font></span>");
//                
//                window.location.href = data._url;                
            }
        }
    });
}

function validategeo() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        validate("latitude", "longitude");
//        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        validate(latitude, longitude);
    }
    function error(error) {
        validate("latitude", "longitude");
//        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}

function validate(latitude, longitude) {
    var s = './secondfactorauth?_lattitude=' + latitude + '&_longitude=' + longitude;
//    var s = './login';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#secondfactor").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "success") === 0) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}




function transdetails(latitude, longitude) {

    var s = './transactiondetails?_lattitude=' + latitude + '&_longitude=' + longitude;
//    var s = './login';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#transactiondetails").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
                $('#savecrtsettings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "success") === 0) {
                $('#savecrtsettings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}



function transauthentication(latitude, longitude) {
    var s = './transactionauth?_lattitude=' + latitude + '&_longitude=' + longitude;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#transactionauth").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
                $('#savecrtsettings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "success") === 0) {
                $('#savecrtsettings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}

function geoSendOOB() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        transdetails(latitude, longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}

function sendOOB() {
    var s = './sendOOB?_lattitude=' + latitude + '&_longitude=' + longitude;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#transactionauth").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
                $('#savecrtsettings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "success") === 0) {
                $('#savecrtsettings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}


function geoTransaction() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        transdetails(latitude, longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}

function geoTransactionValidation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        transauthentication(latitude, longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}


function Transauthentication() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        transauthentication(latitude, longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}


//nilesh
//function geoGetSecurePhrase() {
//    
//       var username = document.getElementById('username').value;
//     
//    if (navigator.geolocation) {
//        navigator.geolocation.getCurrentPosition(success, error, geo_options);
//    } else { 
////        alert("hi");
//        GetSecurePhrase(username,"latitude","longitude");
//        
////        alert("Geolocation services are not supported by your web browser.");
//    }
//    function success(position) {
//          alert(username);
//        var latitude = position.coords.latitude;
//        var longitude = position.coords.longitude;
//        var altitude = position.coords.altitude;
//        var accuracy = position.coords.accuracy;
//     GetSecurePhrase(username,latitude,longitude);
//    }
//    function error(error) { 
//         GetSecurePhrase(username,"latitude","longitude");
//        
////        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
//    }
//    ;
//    var geo_options = {
//        enableHighAccuracy: true,
//        maximumAge: 30000,
//        timeout: 27000
//    };
//}  

//function GetSecurePhrase(username,latitude,longitude){ 
////    alert(latitude);
//    var s = '../getSecurePhrase?_lattitude=' + latitude + '&_longitude=' + longitude+'&username='+username;
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#securePhraseForm").serialize(),
//        success: function(data) {
////            SetImage();
//            if ( strcmp(data._result,"error") === 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                  window.location.href = data._url;     
//            }else if( strcmp(data._result,"success") === 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;                
//            }
//        }
//    }); 
//}
function SetImage() {


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
//        GetSecurePhrase(username,"latitude","longitude");
        var latitude = "latitude";
        var longitude = "longitude";
        var imagelink = "<image src='../ShowImage?_lattitude=" + latitude + "&_longitude=" + longitude + "'/>";
        $('#showimageEAD').html(imagelink);
    }
    function success(position) {
//          alert(username);
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
//     GetSecurePhrase(username,latitude,longitude);
        var imagelink = "<image src='../ShowImage?_lattitude=" + latitude + "&_longitude=" + longitude + "'/>";
//        alert(imagelink);
        $('#showimageEAD').html(imagelink);
    }
    function error(error) {
//         GetSecurePhrase(username,"latitude","longitude");
        var latitude = "latitude";
        var longitude = "longitude";
        var imagelink = "<image src='../ShowImage?_lattitude=" + latitude + "&_longitude=" + longitude + "'/>";
        $('#showimageEAD').html(imagelink);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };


}

//nilesh
var loginType = 0;
function geoGetSecurePhrase(type) {
  
    loginType = type;
    var username = document.getElementById('username').value;
    if (type === 3) {
        var emailid = document.getElementById('emailid').value;
        var phoneno = document.getElementById('phoneno').value;
    }
    if (navigator.geolocation) {
       // alert("if navigator.geolocation");
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
       // alert("else navigator.geolocation");
        GetSecurePhrase(username, "latitude", "longitude", ilocation, ibrowser);
    }
    function success(position) {
       
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        displayLocation(latitude, longitude, username);

    }
    function error(error) {

        GetSecurePhrase(username, "latitude", "longitude");
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
    function displayLocation(latitude, longitude, username) {
        var request = new XMLHttpRequest();
        var method = 'GET';
        var url;
        if (strcmp(window.location.protocol, "https:") !== 0) {
            url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true';
        } else {
            url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true';
        }
        var async = true;
        request.open(method, url, async);
        request.onreadystatechange = function() {
            if (request.readyState === 4 && request.status === 200) {
                var data = JSON.parse(request.responseText);
                var address = data.results[0];
                ilocation = address.formatted_address;
                googleAddress = address;
//                var len = address.address_components.length;
//                for (i = 0; i < len; i++) {
//                    var listsize = address.address_components[i].types.length;
//                    for (j = 0; j < listsize; j++) {
//                        if (address.address_components[i].types[j] === 'administrative_area_level_2') {
//                            console.log(address.address_components[i].long_name);
//                        }
//                        if (address.address_components[i].types[j] === 'country') {
//                            console.log(address.address_components[i].long_name);
//                        }                 
//                    }
//                }
//                alert(" : " + address.address_components[len - 1].types);
                getLocationbyLatLong(latitude, longitude, username);
            }
        };
        request.send();
    }
    ;
}
function GetSecurePhrase(username, latitude, longitude, loc, brow) {
//    alert("GetSecurePhrase");
    var s = '';
    if (loginType === 1) {
        s = '../isRegisteredUser';
    }
    if (loginType === 2) {
      //  alert("isRegisteredUserWithoutPush");
        s = '../isRegisteredUserWithoutPushWeb';
    }
    var _locationChunk = document.getElementById("_locationChunk").value;
    var jsondata = {
        "_locationChunk": _locationChunk,
        "_lattitude": latitude,
        "_longitude": longitude,
        "username": username,
        "location": loc,
        "browser": brow,
        "agent": agent,
        "_googleAddress": JSON.stringify(googleAddress)
    };
//    alert(loc);    
    ilocation = "Not Available";
    agent = "Not Available";
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: jsondata,
        success: function(data) {
           
            if (strcmp(data._result, "error") === 0) {
                alert(data._message+" :: data._message");
            } else {
                if (typeof (data._serverResponse) !== "undefined" && strcmpDemoPortal(data._serverResponse, "") != 0) {
                     // alert("2");
                    document.getElementById("_serverResponse").innerHTML = data._serverResponse;
                    document.getElementById("EasyloginResponse").style.display = "block";
                    window.setTimeout(function() {
                    }, 7000);
                }
                var cookies = getCookie(data._cookieString);
              //  alert("cookies "+cookies);
                //var cookiesLength = getCookie(data._cookieString).value.trim().length;
                if (cookies === null || cookies === "") {
                 //   alert("1");
                    
                    window.location.href = data._url;
                  //  alert("data._url "+data._url);
                }else {
                  // alert("Check user called");
                    CheckUser(username,latitude, longitude, data._cookieString);
                }
            }
        }
    });
}
function createuser() {
    var s = '';
    var username = document.getElementById('username').value;
    var emailid = document.getElementById('emailid').value;
    var phoneno = document.getElementById('phoneno').value;
//    alert(phoneno);
    s = '../createuser';
    var _locationChunk = document.getElementById("_locationChunk").value;
    var jsondata = {
        "_locationChunk": _locationChunk,
        "username": username,
        "emailid": emailid,
        "phoneno": phoneno,
        "agent": agent
    };
    ilocation = "Not Available";
    agent = "Not Available";
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: jsondata,
        success: function(data) {

            if (strcmp(data._result, "error") == 0) {
//                 window.location.href = data._url; 
               // alert(data._message);
            } else {
//                var cookies = getCookie(data._cookieString);
//                if (cookies == null) {
//                    window.location.href = data._url;
//                } else {
//                    CheckUser(latitude, longitude, data._cookieString);
//                }
               // alert(data._message);
                window.location.href = data._url;
            }
        }
    });
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username1");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username1", user, 30);
        }
    }
}


var _osnametoSAVE = "";
var _osArchitecttoSAVE = "";
var _screenHeighttoSAVE = "";
var _screenwidthtoSAVE = "";
var _colordepthtoSAVE = "";
var _userlangtoSAVE = "";
var _timezonetoSAVE = "";
var dpid1 = "";



var DeviceProfile = {
    dpid: dpid1,
    osdetails: _osnametoSAVE,
    osarchitect: _osArchitecttoSAVE,
    screenheight: _screenHeighttoSAVE,
    screenwidth: _screenwidthtoSAVE,
    colordepth: _colordepthtoSAVE,
    userlanguage: _userlangtoSAVE,
    timezone: _timezonetoSAVE,
    ip: "localhost",
    getInfo: function () {
        return {"dpid": this.dpid, "osdetails": this.osdetails, "osarchitect": this.osarchitect, "screenheight": this.screenheight, "screenwidth": this.screenwidth, "colordepth": this.colordepth, "userlanguage": this.userlanguage, "timezone": this.timezone, "ip": this.ip};
    }
};

//change for Device binding by pramod




var deviceIdToUsef = "";
function getDeviceDetailstoSave(userid) {
   
    if (navigator.userAgent.indexOf("WOW64") !== -1 ||
            navigator.userAgent.indexOf("Win64") !== -1) {
        _osArchitecttoSAVE = "Windows 64 Bit";
    }
    if (navigator.userAgent.indexOf("Windows NT 5.1") !== -1 || navigator.userAgent.indexOf("Windows XP") != -1) {
        _osnametoSAVE = "Windows XP";
    }
    if (navigator.userAgent.indexOf("Windows NT 5.2") != -1) {
        _osnametoSAVE = "Windows Server 2003";
    }

    if (navigator.userAgent.indexOf("Linux") != -1 || navigator.userAgent.indexOf("X11") != -1) {
        _osnametoSAVE = "Linux";
    }

    if (navigator.userAgent.indexOf("Mac_PowerPC") != -1 || navigator.userAgent.indexOf("Macintosh") != -1) {
        _osnametoSAVE = "MacOs";
    }

    if (navigator.userAgent.indexOf("QNX") != -1) {
        _osnametoSAVE = "QNX";
    }


    if (window.navigator.userAgent.lastIndexOf("Windows NT 6.1") != -1) {
        _osnametoSAVE = "WIndows 7";
    }

    if (window.navigator.userAgent.lastIndexOf("Windows NT 6.3") != -1) {
        _osnametoSAVE = "WIndows 8";
    }

    if (window.navigator.userAgent.lastIndexOf("MSIE") != -1) {

    }

    var off = (-new Date().getTimezoneOffset() / 60).toString();
    var tzo = off === '0' ? 'GMT' : off.indexOf('-') > -1 ? 'GMT' + off : 'GMT+' + off;

    _screenHeighttoSAVE = screen.height;
    _screenwidthtoSAVE = screen.width;
    _colordepthtoSAVE = screen.colorDepth;
    _timezonetoSAVE = tzo;//        timezonetoSAVE.name();
    _userlangtoSAVE = window.navigator.language;

    DeviceProfile.osdetails = _osnametoSAVE;
    DeviceProfile.osarchitect = _osArchitecttoSAVE;
    DeviceProfile.screenheight = _screenHeighttoSAVE;
    DeviceProfile.screenwidth = _screenwidthtoSAVE;
    DeviceProfile.colordepth = _colordepthtoSAVE;
    DeviceProfile.timezone = _timezonetoSAVE;
    DeviceProfile.userlanguage = _userlangtoSAVE;
    var devidd = _osnametoSAVE + _osArchitecttoSAVE + _screenHeighttoSAVE + _screenwidthtoSAVE + _colordepthtoSAVE + _userlangtoSAVE + userid;
    var wordArray = CryptoJS.enc.Utf8.parse(devidd);
    var base641 = CryptoJS.enc.Base64.stringify(wordArray);
    var hashedDeviceId = CryptoJS.SHA1(base641);
    deviceIdToUsef = hashedDeviceId;
    DeviceProfile.dpid = hashedDeviceId;
    return {"dpid": hashedDeviceId, "osName": _osnametoSAVE, "osArchitect": _osArchitecttoSAVE, "screenHeight": _screenHeighttoSAVE, "screenWidth": _screenwidthtoSAVE, "colorDepth": _colordepthtoSAVE, "timezone": _timezonetoSAVE, "userlang": _userlangtoSAVE};


}
//end

function checkStatus(userid)
{    var DeviceProfJson1 = getDeviceDetailstoSave(userid);
     var deviceId = DeviceProfJson1.dpid;
     console.log("userid"+userid+"deviceId"+deviceId);
    var keyforLocalStorage = CryptoJS.SHA1(userid + deviceId);
    var strData1 = $.jStorage.get("" + keyforLocalStorage);
     var countOfUser= $.jStorage.get(""+"_userCount");
     if(typeof (countOfUser) === "undefined" || strcmpRegister(countOfUser, "") === 0)
    {
        return 0;
    }
     else
   {
       return -2;
       
   }
    if (typeof (strData1) === "undefined" || strcmpRegister(strData1, "") === 0)
    {
      return 0;
    } 
    else
    {
        return -1;
        
    }
   

  
}

function CheckUser(username,latitude, longitude, cookieString) {
    var s = '../isRegisteredDevice?_lattitude=' + latitude + '&_longitude=' + longitude + '&_cookieString=' + cookieString;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
          
                window.location.href = data._url;

            } else if (strcmp(data._result, "success") === 0) {
               
             //  if(checkStatus(username.valueOf()) === -2){
            
                window.location.href = data._url;
//            }else{
//                  window.location.href = "./2farss.jsp";
//            }
                
            }
        }
    });
}




//  function CheckUser(latitude,longitude,cookieString) {
//            var s = '../checkuser?_lattitude=' + latitude + '&_longitude=' + longitude+'&_cookieString='+cookieString;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmp(data._result, "error") == 0) {
//                        Alert4login("<span><font color=red>" + data._message + "</font></span>");
//                      
//                    }else if (strcmp(data._result, "success") == 0) {
//                        Alert4login("<span><font color=blue>" + data._message + "</font></span>");
//                    }
//                }
//            });
//  }

function browserInfo() {
    var nVer = navigator.appVersion;
    var nAgt = navigator.userAgent;
    var browserName = navigator.appName;
    var fullVersion = '' + parseFloat(navigator.appVersion);
    var majorVersion = parseInt(navigator.appVersion, 10);
    var nameOffset, verOffset, ix;

// In Opera 15+, the true version is after "OPR/" 
    if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
        browserName = "Opera";
        fullVersion = nAgt.substring(verOffset + 4);
    }
// In older Opera, the true version is after "Opera" or after "Version"
    else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        browserName = "Opera";
        fullVersion = nAgt.substring(verOffset + 6);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            fullVersion = nAgt.substring(verOffset + 8);
    }
// In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
        browserName = "Microsoft Internet Explorer";
        fullVersion = nAgt.substring(verOffset + 5);
    }
// In Chrome, the true version is after "Chrome" 
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        browserName = "Chrome";
        fullVersion = nAgt.substring(verOffset + 7);
    }
// In Safari, the true version is after "Safari" or after "Version" 
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        browserName = "Safari";
        fullVersion = nAgt.substring(verOffset + 7);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            fullVersion = nAgt.substring(verOffset + 8);
    }
// In Firefox, the true version is after "Firefox" 
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        browserName = "Firefox";
        fullVersion = nAgt.substring(verOffset + 8);
    }
// In most other browsers, "name/version" is at the end of userAgent 
    else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) <
            (verOffset = nAgt.lastIndexOf('/')))
    {
        browserName = nAgt.substring(nameOffset, verOffset);
        fullVersion = nAgt.substring(verOffset + 1);
        if (browserName.toLowerCase() == browserName.toUpperCase()) {
            browserName = navigator.appName;
        }
    }
// trim the fullVersion string at semicolon/space if present
    if ((ix = fullVersion.indexOf(";")) != -1)
        fullVersion = fullVersion.substring(0, ix);
    if ((ix = fullVersion.indexOf(" ")) != -1)
        fullVersion = fullVersion.substring(0, ix);

    majorVersion = parseInt('' + fullVersion, 10);
    if (isNaN(majorVersion)) {
        fullVersion = '' + parseFloat(navigator.appVersion);
        majorVersion = parseInt(navigator.appVersion, 10);
    }
    ibrowser = browserName;
    agent = navigator.userAgent;
}


function getLocationbyLatLong(latitude, longitude, username) {
//    alert("hi");
    var latlang = encodeURIComponent(latitude + "," + longitude);
    var sensor = encodeURIComponent(false);
    var s;
    if (strcmp(window.location.protocol, "https:") !== 0) {
         s = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latlang + "&sensor=" + sensor;
    } else {
        s = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latlang + "&sensor=" + sensor;
    }
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            var loc = JSON.stringify(data);
           // alert(loc);
//            loc = base64Encode(loc);
            $("#_locationChunk").val(base64Encode(loc));
//            document.getElementById("locations").value = loc.toString();
            GetSecurePhrase(username, latitude, longitude, ilocation, ibrowser);
//             alert(locations);
        }
    });

}

function base64Encode(text) {

    if (/([^\u0000-\u00ff])/.test(text)) {
        throw new Error("Can't base64 encode non-ASCII characters.");
    }

    var digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
            i = 0,
            cur, prev, byteNum,
            result = [];

    while (i < text.length) {

        cur = text.charCodeAt(i);
        byteNum = i % 3;

        switch (byteNum) {
            case 0: //first byte
                result.push(digits.charAt(cur >> 2));
                break;

            case 1: //second byte
                result.push(digits.charAt((prev & 3) << 4 | (cur >> 4)));
                break;

            case 2: //third byte
                result.push(digits.charAt((prev & 0x0f) << 2 | (cur >> 6)));
                result.push(digits.charAt(cur & 0x3f));
                break;
        }

        prev = cur;
        i++;
    }

    if (byteNum == 0) {
        result.push(digits.charAt((prev & 3) << 4));
        result.push("==");
    } else if (byteNum == 1) {
        result.push(digits.charAt((prev & 0x0f) << 2));
        result.push("=");
    }

    return result.join("");
}