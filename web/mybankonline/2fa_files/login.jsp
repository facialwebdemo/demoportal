













var page;

var minUsernameLength = 8;
var maxUsernameLength = 20;
var minPasswordLength = 8;
var maxPasswordLength = 20;

var labelUsername = "Username";
var labelPassword = "Password";

function fnClear(thisForm) {
	resetForm(thisForm);
	return false;
}

function validateSubmit(thisForm){
	page="1";
	return validateRequired(thisForm);
}

function required() {
	if(page =="1"){
  		this.aa = new Array("username", labelUsername);  
  	}else{
  		this.aa = new Array("password", labelPassword);  
  	}
}

function mask() {
	if (page == "1") {
		this.aa = new Array("username", labelUsername, "^([a-zA-Z]+[0-9]+[a-zA-Z0-9]*|[0-9]+[a-zA-Z]+[a-zA-Z0-9]*)$", "{0} must be in alphanumeric format. Do not enter spaces or special characters."); 
	} else {
		this.aa = new Array("password", labelPassword, "^([a-zA-Z]+[0-9]+[a-zA-Z0-9]*|[0-9]+[a-zA-Z]+[a-zA-Z0-9]*)$", "{0} must be in alphanumeric format. Do not enter spaces or special characters.");
	}
}

function validateSubmit2(thisForm){
	page="2";
	return validateRequired(thisForm);
}
