<%-- 
    Document   : securephrase
    Created on : Jan 26, 2016, 2:09:47 PM
    Author     : mohanish
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en" style="display: block;"><script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script><script type="text/javascript">(function () {
            return window.SIG_EXT = {};
        })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">	
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#001e54">
        <meta name="msapplication-TileImage" content="/personal/images/mstile-144x144.png">
        <meta name="msapplication-config" content="/personal/images/browserconfig.xml">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="application-name" content="MyBankonline">	

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


        <title>MyBankonline</title>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp"></script>
<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
        <link href="./login_files/combined.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./login_files/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="./login_files/ibcommon.js"></script>
        <script type="text/javascript" src="./login_files/validation.jsp"></script>
        <script type="text/javascript" src="./login_files/jquery.preventDoubleSubmit.js"></script>
        <script type="text/javascript" src="./login_files/jquery.simplemodal.1.4.4.min.js"></script>
        <script type="text/javascript" src="./login_files/login.js"></script>
        <!--Newly added-->
        <script src="../assets/js/jquery.js"></script>
        <script src="../assets/js/pdfsigning.js"></script>
        <script src="../assets/js/json_sans_eval.js"></script>
        <script src="../assets/js/bootbox.min.js"></script>
        <script src="../assets/js/ajaxfileupload.js"></script>
        <script src="../assets/js/bootstrap-fileupload.js"></script>   
        <script src="../assets/js/bootstrap-transition.js"></script>
        <script src="../assets/js/bootstrap-alert.js"></script>
        <script src="../assets/js/bootstrap-modal.js"></script>
        <script src="../assets/js/bootstrap-dropdown.js"></script>
        <script src="../assets/js/bootstrap-scrollspy.js"></script>
        <script src="../assets/js/bootstrap-tab.js"></script>
        <script src="../assets/js/bootstrap-tooltip.js"></script>
        <script src="../assets/js/bootstrap-popover.js"></script>
        <script src="../assets/js/bootstrap-button.js"></script>
        <script src="../assets/js/bootstrap-collapse.js"></script>
        <script src="../assets/js/bootstrap-carousel.js"></script>
        <script src="../assets/js/bootstrap-typeahead.js"></script>
        <script src="../assets/js/bootstrap-datepicker.js"></script>


        <script src="../assets/js/json_sans_eval.js"></script>
        <script src="../assets/js/bootbox.min.js"></script>
        <script src="../assets/js/channels.js"></script>
        <script src="../assets/js/operators.js"></script>
        <script src="../assets/js/securephrase.js"></script>
        <!--<script src="../assets/js/jquery.sidr.min.js"></script>-->
        
        <link href="../assets/css/bootstrap.css" rel="stylesheet">
        <link href="../assets/css/bootstrap-fileupload.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
            /*    div {
                    margin-top:1em;
                    margin-bottom:1em;
                }
                input {
                    padding: .2em;
                    margin: .2em;
                }
                select {
                    padding: .2em;
                    margin: .2em;
                }
            */  

            #signatureparent {
                color:darkblue;

            }
            #signature {
                alignment-adjust: central;
                width:25%;
                hight:25%;
                border: 2px dotted black;
                background-color:lightgrey;
            }
            /*
            */    
            html.touch #content {
                alignment-adjust: central;
                float:center;
                width:25%;
                hight:20%;
            }
            /*    html.touch #scrollgrabber {
                    float:right;
                    width:4%;
                    margin-right:2%;
                    background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAFCAAAAACh79lDAAAAAXNSR0IArs4c6QAAABJJREFUCB1jmMmQxjCT4T/DfwAPLgOXlrt3IwAAAABJRU5ErkJggg==)
                }
                html.borderradius #scrollgrabber {
                    border-radius: 1em;
                }*/
        </style>
        <!--Newly added-->
        <style type="text/css">
            html, .notice_content { display: none; }
            .banner { margin-bottom:18px; }
            .alertcontent .box { text-align:left; margin:15px 20px 0; padding:5px 20px; color:#444; }
            .alertcontent .box ul { margin-left:20px; }
            .mobi #simplemodal-container { height:auto !important; }
        </style> 
        <script type="text/javascript">
        browserInfo();
        var securityPopup = false;


        securityPopup = true;


        if (self == top) {
            document.documentElement.style.display = 'block';
        }
        else {
            top.location = self.location;
        }

        function countdown(secs) {
            var btn = document.getElementById('alertclose');
            btn.value = 'Please wait... (' + secs + ')';
            if (secs < 1) {
                clearTimeout(timer);
                btn.disabled = false;
                btn.value = 'OK';
            }
            secs--;
            var timer = setTimeout('countdown(' + secs + ')', 1000);
        }

        $(function () {
            $('a.notice_title').click(function () {
                $(this).siblings('div.notice_content').slideToggle('fast');
                return false;
            });

            var currentDomain = window.location.hostname;
            var referrerDomain = document.referrer.split('/')[2];

            if (!!referrerDomain && referrerDomain.indexOf(':') > 0) {
                referrerDomain = referrerDomain.split(':')[0];
            }

            if (securityPopup == true && currentDomain !== referrerDomain) {
                $('#securityalert').modal({
                    opacity: 70,
                    autoResize: true,
                    onOpen: function (dialog) {
                        dialog.overlay.fadeIn('fast', function () {
                            dialog.data.hide();
                            dialog.container.fadeIn('slow', function () {
                                dialog.data.fadeIn('fast', function () {
                                    $('#simplemodal-container').css('height', 'auto');
                                    $.modal.setPosition();
                                });
                            });
                        });
                    },
                    onShow: function (dialog) {
                        countdown(4);
                    },
                    onClose: function (dialog) {
                        dialog.container.fadeOut('slow', function () {
                            dialog.overlay.fadeOut('fast', function () {
                                $.modal.close();
                                $('#username').focus();
                            });
                        });
                    }
                });
            }
        });

        function deleteCookie(c_name) {
            document.cookie = encodeURIComponent(c_name) + "=deleted; expires=" + new Date(0).toUTCString();
        }

        $(function () {
            deleteCookie("k");
            var k = null;
            if (k != null) {
                document.cookie = "k=" + k;
            }
        });
        </script>
        <body>
        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>
        <div id="container">
            <div id="content">
                <div class="banner">
                    <a href="#" target="_blank">
                         <img src="icons/SCB_echannels_internetbanking_web.jpg" width=100%" alt="Banner"/>
                    </a>
                </div>
                <div class="box_mask">
                    <div class="box box_gradient">
                        <div class="welcometitle">Welcome to <strong>Secure Phrase Demo </strong></div>
                        <div class="row-fluid">
                            <form class="form-horizontal" id="smsprimaryform" name="smsprimaryform">
                        <div id="legend">
                            <legend class="">Secure Phrase</legend>
                        </div>
                        
                       
                        <div class="row">
                        </div>
                       
                        <div class="control-group">
                            <label class="control-label"  for="username">New Phrase</label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <input type="text" id="_securePhrase"  name="_securePhrase"   class="input-block-level" placeholder="Enter IP/Host">
                                    <!--<input type="text" id="Enter Phrase" id="_securePhrase"   placeholder="Nilesh" class="input-xlarge">-->
                                </div>
                            </div>
                        </div>
                        <%//                            String strLabelBlue = "label-primary";
//                            String strLabelGreen = "label-success";
//                            String strLabelRed = "label-";

                        %>
                        <div class="control-group">
                            <label class="control-label"  for="username">Select Colour</label>
                            <div class="controls">
                                <select name="_select_color" id="_select_color" class="span2">
                                    <option value="1">Blue</option>                                             
                                    <option value="2">Green</option>
                                    <option value="3">Red</option>
                                    <option value="4">GRAY</option>                                             
                                    <option value="5">ORANGE</option>
                                    <option value="6">YELLOW</option>
                                    <option value="7">MAGENTA</option>
                                </select>
                                <!--<input id="sd" hidden="true" type="text" class="color" />-->
                                <button class="btn btn-primary" onclick="showNewImage(10)" type="button">Show Image >> </button>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label"  for="username">New Phrase</label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <div id="showNewImage" ></div>
                                    <!--<div id="confirmImage" ></div>-->
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Co-ordinate</label>
                            <div class="controls"> 
                                X:<span id="Nx"></span>
                                Y:<span id="Ny"></span>
                                <div class="input-prepend">
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">

                            var myNewImg = document.getElementById("showNewImage");
                            myNewImg.onmousedown = GetCoordinates;
                            function FindPosition(oElement)
                            {
                                if (typeof (oElement.offsetParent) != "undefined")
                                {
                                    for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent)
                                    {
                                        posX += oElement.offsetLeft;
                                        posY += oElement.offsetTop;
                                    }
                                    return [posX, posY];
                                }
                                else
                                {
                                    return [oElement.x, oElement.y];
                                }
                            }
                            function GetCoordinates(e)
                            {
                                var PosX = 0;
                                var PosY = 0;
                                var ImgPos;
                                ImgPos = FindPosition(myNewImg);
                                if (!e)
                                    var e = window.event;
                                if (e.pageX || e.pageY)
                                {
                                    PosX = e.pageX;
                                    PosY = e.pageY;
                                }
                                else if (e.clientX || e.clientY)
                                {
                                    PosX = e.clientX + document.body.scrollLeft
                                            + document.documentElement.scrollLeft;
                                    PosY = e.clientY + document.body.scrollTop
                                            + document.documentElement.scrollTop;
                                }
                                PosX = PosX - ImgPos[0];
                                PosY = PosY - ImgPos[1];
                                document.getElementById("Nx").innerHTML = PosX;
                                document.getElementById("Ny").innerHTML = PosY;
                            }
                            //-->

                        </script>

                        <div class="control-group">
                            <div class="controls">
                                <div id="save-sms-gateway-primary-result"></div>
                                <button class="btn btn-primary" onclick="geosetSecurePhrase(10)" type="button">Save Setting >> </button>
                                <script>
                                </script>
                                <!--                                <button class="btn btn-primary" onclick="showNewImage(10)" type="button">Show Image >> </button>-->
                            </div>
                        </div>
                        <!--<button class="btn btn-primary" onclick="showNewImage()" type="button">Save Setting >> </button>-->

                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid43" id="_hid43" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid44" id="_hid44" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <script>
                            SetImage();
                            document.getElementById("_hid43").style.display = 'none';
                            document.getElementById("_hid44").style.display = 'none';
                        </script>
                    </form>
                        </div>