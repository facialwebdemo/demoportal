<!DOCTYPE html>
<!-- saved from url=(0057)https://#/personal/login/login.do -->
<html lang="en" style="display: block;"><script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script><script type="text/javascript">(function () {
            return window.SIG_EXT = {};
        })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">	
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#001e54">
        <meta name="msapplication-TileImage" content="/personal/images/mstile-144x144.png">
        <meta name="msapplication-config" content="/personal/images/browserconfig.xml">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="application-name" content="MyBankonline">	

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


        <title>MyBankonline</title>


        <link href="./login_files/combined.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./login_files/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="./login_files/ibcommon.js"></script>
        <script type="text/javascript" src="./login_files/validation.jsp"></script>
        <script type="text/javascript" src="./login_files/jquery.preventDoubleSubmit.js"></script>
        <script type="text/javascript" src="./login_files/jquery.simplemodal.1.4.4.min.js"></script>
        <script type="text/javascript" src="./login_files/login.js"></script>
        <script src="js/mcColorPicker.js" type="text/javascript"></script>
        <script src="../assets/js/securephrase.js"></script>


        <style type="text/css">
            html, .notice_content { display: none; }
            .banner { margin-bottom:18px; }
            .alertcontent .box { text-align:left; margin:15px 20px 0; padding:5px 20px; color:#444; }
            .alertcontent .box ul { margin-left:20px; }
            .mobi #simplemodal-container { height:auto !important; }
        </style> 
        <script type="text/javascript">
        browserInfo();
        var securityPopup = false;


        securityPopup = true;


        if (self == top) {
            document.documentElement.style.display = 'block';
        }
        else {
            top.location = self.location;
        }

        function countdown(secs) {
//            var btn = document.getElementById('alertclose');
//            btn.value = 'Please wait... (' + secs + ')';
//            if (secs < 1) {
//                clearTimeout(timer);
//                btn.disabled = false;
//                btn.value = 'OK';
//            }
//            secs--;
//            var timer = setTimeout('countdown(' + secs + ')', 1000);
        }

        $(function () {
        $('a.notice_title').click(function () {
        $(this).siblings('div.notice_content').slideToggle('fast');
                return false;
        });
                var currentDomain = window.location.hostname;
                var referrerDomain = document.referrer.split('/')[2];
                if (!!referrerDomain && referrerDomain.indexOf(':') > 0) {
        referrerDomain = referrerDomain.split(':')[0];
        }

        if (securityPopup == true && currentDomain !== referrerDomain) {
//                $('#securityalert').modal({
//                    opacity: 70,
//                    autoResize: true,
//                    onOpen: function (dialog) {
//                        dialog.overlay.fadeIn('fast', function () {
//                            dialog.data.hide();
//                            dialog.container.fadeIn('slow', function () {
//                                dialog.data.fadeIn('fast', function () {
//                                    $('#simplemodal-container').css('height', 'auto');
//                                    $.modal.setPosition();
//                                });
//                            });
//                        });
//                    },
//                    onShow: function (dialog) {
////                        countdown(4);
//                    },
//                    onClose: function (dialog) {
//                        dialog.container.fadeOut('slow', function () {
//                            dialog.overlay.fadeOut('fast', function () {
//                                $.modal.close();
//                                $('#username').focus();
//                            });
//                        });
//                    }
//                });
//            }
        });
                function deleteCookie(c_name) {
                document.cookie = encodeURIComponent(c_name) + "=deleted; expires=" + new Date(0).toUTCString();
                }

        $(function () {
        deleteCookie("k");
                var k = null;
                if (k != null) {
        document.cookie = "k=" + k;
        }
        });        </script>
    </head>
    <body>

        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>

        <div id="container">
            <div id="content">
                <div id="securityalert">
                    <center><img src="./login_files/scam.png" alt="Security Alert" style="margin-bottom:20px;"></center>
                    <div class="alertclose">
                        <input type="button" id="alertclose" class="simplemodal-close" value="OK">
                    </div>
                </div>


                <div class="banner">


                    <a href="#" target="_blank">
                        <img src="./login_files/render_dynamic_img.jpg" alt="Banner">
                    </a>


                </div>

                <form method="post" action="2fa.jsp" autocomplete="off" class="txnform once" name="userForm" id="userForm">
                    <input type="hidden" name="step2" value="">
                    <input id="_apXCordinate" name="_apXCordinate" type="hidden" value=""/>
                    <input id="_apYCordinate" name="_apYCordinate" type="hidden" value=""/>
                    <form  autocomplete="off" action="" class="txnform once" name="securePhraseForm" id="securePhraseForm">
                        <div id="logincontent">

                            <div >
                                <div class="box_mask">
                                    <div class="box box_gradient">
                                        <div class="error" style="margin:0;">


                                        </div>
                                        <div class="welcometitle" style="text-align: center">Configure your Secure Phrase</strong> Demo</div>
                                        <!--                                        <div class="row"> 
                                                                                    <label for="imgReg">Configure your Secure Phrase</label>
                                                                                </div>-->
                                        <div class="row">
                                            <label for="imgReg">Set New Securephrase</label>
                                            <div>
                                                <input size="29" maxlength="40" type="text" name="_securePhrase" id="_securePhrase" tabindex="1">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="imgColor">Choose Colour:</label>


                                            <table style="text-align:center">
                                                <tr>
                                                    <td>
                                                        <div id="color1s" style="background-color: #A52A2A; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div id="color1s" style="background-color: #DC143C; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="color1s" style="background-color: #00008B; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="color1s" style="background-color: #E9967A; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="color1s" style="background-color: #FF1493; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div id="color1s" style="background-color: #FF69B4; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="color1s" style="background-color: #4B0082; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="color1s" style="background-color: #FF00FF; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div id="color1s" style="background-color: #FFA500; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="color1s" style="background-color: #6A5ACD; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div id="color1s" style="background-color: #9ACD32; height: 40px ;width:40px">
                                                            &nbsp;&nbsp;
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#A52A2A" onclick="SetColor('#A52A2A')"/>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#DC143C" onclick="SetColor('#DC143C')" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#00008B" onclick="SetColor('#00008B')" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#E9967A" onclick="SetColor('#E9967A')"/>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#FF1493" onclick="SetColor('#FF1493')" />
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#FF69B4" onclick="SetColor('#FF69B4')" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#4B0082" onclick="SetColor('#4B0082')" />
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#FF00FF" onclick="SetColor('#FF00FF')" />
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#FFA500" onclick="SetColor('#FFA500')" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#6A5ACD" onclick="SetColor('#6A5ACD')"  />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div  style="height: 40px ;width:40px;">
                                                            <input type="radio" id="_select_color" name="_select_color" value="#9ACD32" onclick="SetColor('#9ACD32')"/>
                                                        </div>
                                                    </td> 
                                                </tr>
                                                <!--                                            <tr>
                                                                                                <td width="24%" align="left" valign="top"><input type="button" name="btnShowImg" value="Get SecurePhrase" onclick="showNewImage()"></td>
                                                                                                <td width="76%" align="left" valign="top">
                                                                                                    <div id="_apNewImage"> </div><div id="_showMesgNewImage"></div>
                                                                                                </td>									  										
                                                                                            </tr> 
                                                                                            <tr>
                                                                                                <td width="24%" align="left" valign="top"><strong>Choose Sweetspot</strong></td>
                                                                                                <td width="76%" align="left" valign="top">
                                                                                                    <div id="_apNewImage"> </div><div id="_showMesgNewImage"></div>
                                                                                                </td>									  										
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="24%" align="left" valign="top"><strong>Confirm Sweetspot</strong></td>
                                                                                                <td width="76%" align="left" valign="top">
                                                                                                    <div id="_apNewImageConf"></div><div id="_showMesgNewImageConf"></div>
                                                                                                </td>									  										
                                                                                            </tr>-->
                                                <!--                                            <tr>
                                                                                                <td width="24%" align="left" valign="top"><strong></strong></td>
                                                                                                <td width="76%" align="left" valign="top">
                                                                                                    <div id="_apNewImageConf"></div>
                                                                                                    <div><button id="btn_imageRegister" onclick="geosetSecurePhrase(10)" type="button">Save Secure Phrase >> </button></div>
                                                                                                </td>									  										
                                                                                            </tr>-->

                                            </table>
                                        </div>
                                        <div class="row">
                                            <div>
                                                <input type="button" name="btnShowImg" value="Get SecurePhrase" onclick="showNewImage()">
                                            </div>

                                            <!--                                            <table>
                                                                                    <tr>
                                                                                        <td width="24%" align="left" valign="top"></td>
                                                                                            <td width="76%" align="left" valign="top">
                                                                                                <div id="_apNewImage"> </div><div id="_showMesgNewImage"></div>
                                                                                            </td>									  										
                                                                                        </tr> 
                                                                                        <tr>
                                                                                            <td width="24%" align="left" valign="top"><strong>Choose Sweetspot</strong></td>
                                                                                            <td width="76%" align="left" valign="top">
                                                                                                <div id="_apNewImage"> </div><div id="_showMesgNewImage"></div>
                                                                                            </td>									  										
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="24%" align="left" valign="top"><strong>Confirm Sweetspot</strong></td>
                                                                                            <td width="76%" align="left" valign="top">
                                                                                                <div id="_apNewImageConf"></div><div id="_showMesgNewImageConf"></div>
                                                                                            </td>									  										
                                                                                        </tr>
                                                                                        </table>-->
                                        </div>

                                        <div class="row" id="selectsweetspot" style="display: none">
                                            <!--                                             <tr>
                                                                                        <td width="24%" align="left" valign="top"></td>-->
                                            <!--<td width="76%" align="left" valign="top">-->
                                            <strong>Choose Sweetspot</strong>
                                            <div style="width: 76%">
                                                <div id="_apNewImage"> </div><div id="_showMesgNewImage"></div>
                                            </div>
                                            <!--</td>-->									  										
                                            <!--</tr>--> 
                                            <!--<tr>-->
                                            <!--<td width="24%" align="left" valign="top"><strong>Choose Sweetspot</strong></td>-->
                                            <!--<td width="76%" align="left" valign="top">-->
                                            </div>
                                        <div class="row" id="confirmsweetspot" style="display: none">
                                                <strong>Confirm Sweetspot</strong>
                                                <!--<strong>Choose Sweetspot</strong>-->
                                            <div style=" width:  24%;"></div>
                                            <div style="width: 76%;">
                                                <div id="_apNewImage"> </div><div id="_showMesgNewImage"></div>
                                            </div>    
                                        </div>
                                        <!--</td>-->									  										
                                        <!--</tr>-->
                                        <!--<tr>-->
                                        <!--<td width="24%" align="left" valign="top"><strong>Confirm Sweetspot</strong></td>-->
                                        <!--<td width="76%" align="left" valign="top">-->
                                        <div class="row">
                                            
                                            <div style=" width:  24%;"></div>
                                            <div style="width: 76%;">
                                                <div id="_apNewImageConf"></div><div id="_showMesgNewImageConf"></div>
                                            </div>   
                                        </div>
                                        <!--</td>-->									  										
                                        <!--</tr>-->





                                        <input type="hidden" name="_locationChunk" id="_locationChunk">
                                        <div class="row buttons" id="loginbuttons">
                                            <div>
                                                <input value="Clear" class="button_blue" onclick="return fnClear(this.form);" type="button" id="clear" name="clear" tabindex="3">
                                                <!--<input value="Login" class="button_red" type="submit" id="step2" name="step2" tabindex="2"><img id="busy" src="./login_files/busy.gif">-->
                                                <!--<input value="Login" class="button_red" onclick="return geoGetSecurePhrase(this.form);" type="button" id="step3" name="step3" tabindex="2"><img id="busy" src="./2fa_files/busy.gif">-->
                                                <input value="Save Secure Phrase" class="button_red" onclick="geosetSecurePhrase(10)" type="button" id="btn_imageRegister" name="step3" tabindex="2"><img id="busy" src="./2fa_files/busy.gif">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box">
                            <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/error.png&#39;);">Security Alert</div>
                            <ul class="bullet_square">

                                <li>Always verify your MyBankonline Secure Phrase before you login!</li>

                            </ul>
                        </div>
                        </div>

                        <!--                            <div class="right">
                                                        <div class="box">
                                                            <div id="assistance"><a href="mailto:info@MyBankfg.com"><img src="./login_files/banner_need_assistant.png" alt="Email us"></a></div>
                                                            <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/key.png&#39;);">Registration &amp; Login</div>
                                                            <ul class="bullet_square">
                                                                <li><a class="current" href="https://#/personal/register/register.do">First Time Registration</a></li>
                                                                <li><a href="http://#/Personal-Banking/eChannels/MyBankonline#firsttimeregistration-tab" target="_blank">How to Register?</a></li>
                                                                <li><a class="current" href="https://#/personal/reset/forgot_id_or_password.do">Forgot Username / Password</a></li>
                                                            </ul>
                                                            <br>
                                                            <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/world_link.png&#39;);">Other Links</div>
                                                            <ul class="bullet_square">
                                                                <li><a target="_blank" href="https://#/personal/rate_charges/e_rate_unsecured_view.do">View e-Rates</a></li>
                                                            </ul>
                                                        </div>
                        
                                                        <div class="box">
                                                            <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/note.png&#39;);">Notices</div>
                                                            <ul class="bullet_square">
                        
                        
                                                                <li>
                                                                    <a class="notice_title" href="https://#/personal/login/login.do#">System Downtime Notice</a>
                                                                    <div class="notice_content">
                                                                        Dear Valued Customer<br><br>
                        
                                                                        There will be system downtime due to scheduled maintenance <br>
                        
                                                                        21 Oct 2014 5.00am to 6.00am <br><br>
                        
                                                                        We sincerely apologise for any inconvenience caused. <br><br>
                        
                                                                        Thank you. <br>
                        
                                                                    </div>
                                                                </li>
                        
                        
                        
                                                                <li>
                                                                    <a class="notice_title" href="https://#/personal/login/login.do#">Hottest e-Rates in town!</a>
                                                                    <div class="notice_content">
                                                                        #Besurprised with MyBank XChange Hot e-Rates promotion! Save more today in the currency of your choice while the rates are hot! Limited offer only from 9.30am  12.00noon daily. Available until 28 November 2014. 
                        
                                                                        For more information on MyBank XChange, visit http://#/MyBankXChange
                        
                                                                        Thank you.
                                                                    </div>
                                                                </li>
                        
                        
                        
                                                                <li>
                                                                    <a class="notice_title" href="https://#/personal/login/login.do#">DBKL Payment</a>
                                                                    <div class="notice_content">
                                                                        Dear Valued Customer
                        
                                                                        Please be informed that Dewan Bandaraya Kuala Lumpur (DBKL) is currently updating to a new account number format. MyBankonline is in the upgrade process to accept the new revised account number format. In the meantime, kindly key in the old account number for DBKL payment via MyBankonline. 
                                                                        We will notify you in due course once the upgrade is completed. 
                                                                        We apologise for any inconvenience caused.
                                                                        Thank you.
                        
                                                                    </div>
                                                                </li>
                        
                        
                        
                                                                <li>
                                                                    <a class="notice_title" href="https://#/personal/login/login.do#">DiGi Bill Payment</a>
                                                                    <div class="notice_content">
                                                                        Dear Valued Customers, <br>
                                                                        Please be informed that DiGi Telecommunications Sdn Bhd (DiGi) is changing to a new account number format. All DiGi postpaid subscribers are required to pay their bills using the new DiGi bill account numbers effective 1 October 2014 onwards. The old bill account number will no longer be valid from this date. Please recreate your scheduled and favourite DiGi bill payment using your new DiGi bill account number to avoid any payment interruption. <br>Thank you. 
                        
                                                                    </div>
                                                                </li>
                        
                        
                        
                                                                <li>
                                                                    <a class="notice_title" href="https://#/personal/login/login.do#">Schedule of IBG Funds Received by Beneficiary</a>
                                                                    <div class="notice_content">
                                                                        <p> New! Schedule of Funds received by beneficiary. </p>
                        
                        
                        
                                                                        <p>Click <a href="http://#/Schedule-of-Funds-received-by-beneficiary" target="_blank"> here to view  </a>
                        
                                                                        </p></div>
                                                                </li>
                        
                        
                                                            </ul>
                                                        </div>
                                                    </div>-->

                        </div>
                    </form>
                    <div style="display: none;"><input type="hidden" name="_sourcePage" value="WBezwwSnZr3-Q8DOXz10QWlhYwprz9dNv5_maqamTOXKkDR2Ez-bsA=="><input type="hidden" name="__fp" value="W5QXcMGW308="></div>
                </form>
            </div>
            <!-- start #footer -->
            <div id="footer">
                <div class="compatibleView">Best viewed on Internet Explorer 7 &amp; above, Firefox and Chrome</div>
                <div class="footerlink"><a href="http://#/About-Us/About-the-Website/Privacy-Policy.aspx" target="_blank">Privacy Policy</a> | <a href="http://#/About-Us/About-the-Website/Client-Charter.aspx" target="_blank">Client Charter</a> | <a href="http://#/ABMB/media/MyLibrary/Shared/Files/Customer-Service-Charter.pdf" target="_blank">Customer Service Charter</a> | <a href="http://#/ABMB/media/MyLibrary/ABMB/PDF/aop_tnc.pdf" target="_blank">Terms &amp; Conditions</a></div>
                <div class="copyright">Copyright © 2014 MyBank (Demo site) </div>
            </div>
            <!-- end #footer -->
        </div>
        <script>
                    var xCoordinate = null;
                    var yCoordinate = null;
                    var resValidation = - 1;
                    var _apNewImageFP = document.getElementById("_apNewImage");
                    $('#_apNewImage').click(function (e) {
            document.getElementById("btn_imageRegister").disabled = true;
                    var ImgPos = FindPosition(_apNewImageFP);
                    var relX = e.pageX - ImgPos[0];
                    var relY = e.pageY - ImgPos[1];
                    xCoordinate = relX;
                    yCoordinate = relY;
                    $("#_showMesgNewImage").html("Selected!!!");
            });
                    var _apNewImageConfFP = document.getElementById("_apNewImageConf");
                    $('#_apNewImageConf').click(function (e) {
            document.getElementById("btn_imageRegister").disabled = true;
                    //var relX = e.pageX - this.offsetLeft;
                    //var relY = e.pageY - this.offsetTop;
                    var ImgPos = FindPosition(_apNewImageConfFP);
                    var relX = e.pageX - ImgPos[0];
                    var relY = e.pageY - ImgPos[1];
                    var deviation = 5;
                    var x1Deviation = xCoordinate;
                    var y1Deviation = yCoordinate;
                    var x2Deviation = relX;
                    var y2Deviation = relY;
                    //alert(x2Deviation  + "," + y2Deviation);

                    if (Number(x1Deviation) == Number(x2Deviation) && Number(y1Deviation) == Number(y2Deviation)) {
            resValidation = 0;
            }
            if (Number(x1Deviation) >= Number(x2Deviation) && Number(y1Deviation) >= Number(y2Deviation)) {

            resValidation = 0;
                    if (Number(x1Deviation) >= Number(x2Deviation + deviation)) {
            resValidation = - 1;
            }
            if (Number(y1Deviation) >= Number(y2Deviation + deviation)) {
            resValidation = - 1;
            }

            }
            if (Number(x1Deviation) >= Number(x2Deviation) && Number(y1Deviation) <= Number(y2Deviation)) {
            resValidation = 0;
                    if (Number(x1Deviation) >= Number(x2Deviation + deviation)) {
            resValidation = - 1;
            }
            if (Number(y2Deviation) >= Number(y1Deviation + deviation)) {
            resValidation = - 1;
            }

            }
            if (Number(x1Deviation) <= Number(x2Deviation) && Number(y1Deviation) >= Number(y2Deviation)) {
            resValidation = 0;
                    if (Number(x2Deviation) >= Number(x1Deviation + deviation)) {
            resValidation = - 1;
            }
            if (Number(y1Deviation) >= Number(y2Deviation + deviation)) {
            resValidation = - 1;
            }

            }
            if (Number(x1Deviation) <= Number(x2Deviation) && Number(y1Deviation) <= Number(y2Deviation)) {
            resValidation = 0;
                    if (Number(x2Deviation) >= Number(x1Deviation + deviation)) {
            resValidation = - 1;
            }
            if (Number(y2Deviation) >= Number(y1Deviation + deviation)) {
            resValidation = - 1;
            }
            }

            if (Number(resValidation) == - 1) {
            alert("Sweet spot does not match!!!");
                    $("#_apXCordinate").val("");
                    $("#_apYCordinate").val("");
                    $("#_showMesgNewImageConf").html("Mismatch, try again!!!");
            } else {
            alert("Sweet spot Selected!!!");
                    $("#_apXCordinate").val(relX);
                    $("#_apYCordinate").val(relY);
                    $("#_showMesgNewImageConf").html("Matched, please proceed!!!");
                    document.getElementById("btn_imageRegister").disabled = false;
            }

            });
        </script>


        <style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>