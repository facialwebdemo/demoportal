<%@page import="java.util.Iterator"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.common.AxiomProtectConnector"%>
<%@page import="com.mollatech.axiom.v2.core.rss.Remotesignature"%>
<%@page import="com.mollatech.axiom.bridge.crypto.LoadSettings"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/pdfsigning.js"></script>
<script src="../assets/js/json_sans_eval.js"></script>
<script src="../assets/js/bootbox.min.js"></script>
<script src="../assets/js/ajaxfileupload.js"></script>
<script src="../assets/js/bootstrap-fileupload.js"></script>   
<script src="../assets/js/bootstrap-transition.js"></script>
<script src="../assets/js/bootstrap-alert.js"></script>
<script src="../assets/js/bootstrap-modal.js"></script>
<script src="../assets/js/bootstrap-dropdown.js"></script>
<script src="../assets/js/bootstrap-scrollspy.js"></script>
<script src="../assets/js/bootstrap-tab.js"></script>
<script src="../assets/js/bootstrap-tooltip.js"></script>
<script src="../assets/js/bootstrap-popover.js"></script>
<script src="../assets/js/bootstrap-button.js"></script>
<script src="../assets/js/bootstrap-collapse.js"></script>
<script src="../assets/js/bootstrap-carousel.js"></script>
<script src="../assets/js/bootstrap-typeahead.js"></script>
<script src="../assets/js/bootstrap-datepicker.js"></script>
<script src="../assets/js/json_sans_eval.js"></script>
<script src="../assets/js/bootbox.min.js"></script>
<script src="../assets/js/channels.js"></script>
<script src="../assets/js/operators.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<!--<script src="../assets/js/jquery.sidr.min.js"></script>-->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<link href="../assets/css/bootstrap-fileupload.css" rel="stylesheet">
<style type="text/css">
    body {
        padding-top: 60px;
        padding-bottom: 40px;
    }
    /*    div {
            margin-top:1em;
            margin-bottom:1em;
        }
        input {
            padding: .2em;
            margin: .2em;
        }
        select {
            padding: .2em;
            margin: .2em;
        }
    */  

    #signatureparent {
        color:darkblue;

    }
    #signature {
        alignment-adjust: central;
        width:25%;
        hight:25%;
        border: 2px dotted black;
        background-color:lightgrey;
    }
    /*
    */    
    html.touch #content {
        alignment-adjust: central;
        float:center;
        width:25%;
        hight:20%;
    }
    /*    html.touch #scrollgrabber {
            float:right;
            width:4%;
            margin-right:2%;
            background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAFCAAAAACh79lDAAAAAXNSR0IArs4c6QAAABJJREFUCB1jmMmQxjCT4T/DfwAPLgOXlrt3IwAAAABJRU5ErkJggg==)
        }
        html.borderradius #scrollgrabber {
            border-radius: 1em;
        }*/
</style>
<%
    RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
    AxiomWrapper axWrapper = new AxiomWrapper();
    String sessionId = null;
    String qrDataUrl = "";
    if (request.getSession().getAttribute("sessionid") == null) {
        String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
        String password = LoadSettings.g_sSettings.getProperty("axiom.password");
        String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
        qrDataUrl = LoadSettings.g_sSettings.getProperty("axiom.qrcodeurl");
        sessionId = axWrapper.OpenSession(channelid, remotelogin, password);
    } else {
        sessionId = (String) request.getSession().getAttribute("sessionid");
    }
    Remotesignature[] signInfoByUser = axWrapper.getAllSignatureByUserid(sessionId, userObj.getRssUser().getUserId(), 1);
%>
<div class="container-fluid">
    <h3>Sign PDF With QR Code Authentication</h3>

    <div class="tabbable" id="REPORT">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#otpcharts" data-toggle="tab">Sign PDF</a></li>
            <li><a href="#otpreport" data-toggle="tab">See Your Documents</a></li>
            <li><a href="#otpauthrequest" data-toggle="tab">Document Requests</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="otpcharts">
                <div class="row-fluid">
                    <form class="form-horizontal" id="PDFSigning" name="PDFSigning">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label"  for="hwserialno">Enter Email</label>
                                <div class="controls">
                                    <input type="text" id="_emailId" name="_emailId" placeholder="Email ID" class="input-xlarge"/>
                                </div>
                            </div>
                          
                            <div class="control-group">
                                <label class="control-label"  for="otp">OTP</label>
                                <div class="controls">
                                    <input type="text" id="_otp" name="_otp" placeholder="Enter OTP" class="input-xlarge"/>
                                    <button class="btn btn-primary " style=" margin-left:20px; " onclick="sendotp()" type="button">Get OTP</button>
                                </div>

                            </div>
                            <input type="hidden" id="_filename" name="_filename">

                            <div class="controls-row" id="licenses">
                                <div class="row-fluid">
                                    <form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">
                                        <fieldset>

                                            <div class="control-group">
                                                <label class="control-label"  for="username">Select File:</label>                                    
                                                <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="input-append">
                                                        <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                            <span class="fileupload-preview"></span>
                                                        </div>
                                                        <span class="btn btn-file" >
                                                            <span class="fileupload-new">Select file</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" id="fileXMLToUploadEAD" name="fileXMLToUploadEAD"/>
                                                        </span>
                                                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                    </div>
                                                    <button class="btn btn-success" id="buttonUploadEAD"  onclick="PDFUpload()" >Upload Now>></button>
                                                </div>

                                            </div>
                                            <div class="control-group">
                                                <div class="row">
                                                <label class="control-label"  for="username">Allowed Phones(to download pdf)</label>
                                                <div class="controls">
                                                    <select class="span4" name="_allowedphones" id="_allowedphones">
                                                        <option value="Android">Android</option>
                                                        <option value="Iphone">Iphone</option>
                                                        <option value="Windows">Windows</option>
                                                    </select>
                                                </div>
                                                </div>
                                                 <div class="row">
                                                <label class="control-label"  for="username">Allow Location(to download pdf)</label>
                                                <div class="controls">
                                                    <select class="span4" name="_allowedCountry" id="_allowedCountry">
                                                        <option value="India">India</option>
                                                        <option value="Malesia">Malesia</option>
                                                        <option value="Cambodiya">Cambodiya</option>
                                                    </select>
                                                </div>
                                                 </div>
                                                  <div class="row">
                                                <label class="control-label"  for="username">Request for Download</label>
                                                <div class="controls">
                                                    <select class="span4" name="_checkEmail" id="_checkEmail">
                                                        <option value="docheck">Yes</option>
                                                        <option value="nocheck">NO</option>
                                                        
                                                    </select>
                                                </div>
                                                  </div>
                                            </div>
                                            <!--<button class="btn btn-success" id="buttonUploadEAD"  onclick="PDFUpload()" >Upload Now>></button>-->
                                        </fieldset>
                                    </form>

                                </div>
                                <!-- Submit -->
                            </div>
                            <input type="hidden" id="savemepost" name="savemepost">
                            <div class="control-group">
                                <label class="control-label"  for="otp">Signature Here</label>
                                <div class="controls">
                                    <div id="content">
                                        <div id="signatureparent">
                                            <div id="signature" ></div></div>
                                        <div id="tools" ></div><div id="displayarea" src="image.png" style="display: none;"></div>
                                        <!--<input type="hidden" id="savemepost" name="savemepost">-->
                                        <!--                             <form action="pdfsigning" method="POST">
                                                                       <input type="hidden" id="savemepost" name="savemepost">
                                                                     <input class="btn btn-primary btn-large" type="submit" value="Sign Submit" onclick="pngimage()">
                                                                    </form>-->

                                    </div>
                                </div>
                                <div id="scrollgrabber"></div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button class="btn btn-primary btn-large" onclick="signPDFImageAuth()" type="button">Sign and Publish Document >></button>
                                    <!--<input class="btn btn-primary btn-large" type="submit" value="Sign Submit" onclick="pngimage()">-->
                                    <div id="sign-pdf-result"></div>
                                    <!--<div class="span3" id="download-signed-pdf"></div>-->
                                    <!--</div>-->
                                    <!--                </div>
                                                    <div class="control-group">-->
                                    <!--                    <div class="controls">-->
<!--                                    <button class="btn btn-primary btn-large" id="download-signed-pdf-btn" style=" margin-left:20px; display: none" onclick="downloadpdf()" type="button">Download Signed PDF</button>
                                    <input class="btn btn-primary btn-large" type="submit" value="Sign Submit" onclick="pngimage()">
                                    <div id="sign-pdf-result"></div>
                                    <div class="span3" id="download-signed-pdf"></div>-->
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>




            </div>

            <div class="tab-pane" id="otpreport">  
                <div id="licenses_data_table">
                    <table class="table table-striped">
                        <tr>
                            <TD ><FONT size=3 ><B>Document Id</B></FONT></TD>
                            <TD ><FONT size=3 ><B>Document Type</B></FONT></TD>
                            <TD ><FONT size=3 ><B>Time Of Signing</B></FONT></TD>
                            <TD ><FONT size=3 ><B>Refrence Id</B></FONT></TD>
                           
                        </tr>
                        <%for (int i = 0; i < signInfoByUser.length; i++) {%>
                        <tr>
                             <TD ><FONT size=3 ><B><%=signInfoByUser[i].getDocid()%></B></FONT></TD>
                            <TD ><FONT size=3 ><B><%=signInfoByUser[i].getDocType()%></B></FONT></TD>
                            <TD ><FONT size=3 ><B><%=signInfoByUser[i].getTimeOfSigning()%></B></FONT></TD>
                            <TD ><FONT size=3 ><B><%=signInfoByUser[i].getReferenceid()%></B></FONT></TD>
                            
                                   </tr>




                        <%}%>

                    </table>
                </div>
            </div>
            <div class="tab-pane" id="otpauthrequest">  
                <div id="pdfAuthRequest">

                    <table class="table table-striped">
                        <tr>
                            <TD ><FONT size=3 ><B>Document Id</B></FONT></TD>
                            <TD ><FONT size=3 ><B>Document Type</B></FONT></TD>
                            <TD ><FONT size=3 ><B>Time Of Signing</B></FONT></TD>
                            <TD ><FONT size=3 ><B>Refrence Id</B></FONT></TD>
                               <TD ><FONT size=3 ><B>Device Id</B></FONT></TD>
                            <TD ><B>Authorize</B></FONT></TD>
                        </tr>
                        <%for (int i = 0; i < signInfoByUser.length; i++) {%>
                        <% if (signInfoByUser[i].getAllowedUsers() != null) {
                                HashMap deviceH = (HashMap) AxiomProtectConnector.deserializeFromObject(new ByteArrayInputStream(signInfoByUser[i].getAllowedUsers()));
                                if (deviceH != null) {
                                    
          Iterator it = deviceH.entrySet().iterator();
            while (it.hasNext()) {
        Map.Entry pair = (Map.Entry)it.next();
          String key =(String)pair.getKey();
         String value =(String) pair.getValue();
        System.out.println(pair.getKey() + " = " + pair.getValue());
                    if (value.contains("NO")) {%>
                         <tr>
                            <TD ><FONT size=3 ><B><%=signInfoByUser[i].getDocid()%></B></FONT></TD>
                            <TD ><FONT size=3 ><B><%=signInfoByUser[i].getDocType()%></B></FONT></TD>
                            <TD ><FONT size=3 ><B><%=signInfoByUser[i].getTimeOfSigning()%></B></FONT></TD>
                            <TD ><FONT size=3 ><B><%=signInfoByUser[i].getReferenceid()%></B></FONT></TD>
                            <TD ><FONT size=3 ><B><%=key%></B></FONT></TD>
                            <TD ><FONT size=3 ><B><input type="button" value="Authorize" class="btn-success" onclick="UpdateAllowedUsers('<%=key%>','<%=signInfoByUser[i].getReferenceid()%>')"></B></FONT></TD>  
                            </tr>

                        <%}

                                        }
                                    }
                                }
                            }%>

                    </table>
                </div>
            </div>



        </div>
    </div>

    <script>
        /*  @preserve
         jQuery pub/sub plugin by Peter Higgins (dante@dojotoolkit.org)
         Loosely based on Dojo publish/subscribe API, limited in scope. Rewritten blindly.
         Original is (c) Dojo Foundation 2004-2010. Released under either AFL or new BSD, see:
         http://dojofoundation.org/license for more information.
         */





        (function ($) {
            var topics = {};
            $.publish = function (topic, args) {
                if (topics[topic]) {
                    var currentTopic = topics[topic],
                            args = args || {};

                    for (var i = 0, j = currentTopic.length; i < j; i++) {
                        currentTopic[i].call($, args);
                    }
                }
            };
            $.subscribe = function (topic, callback) {
                if (!topics[topic]) {
                    topics[topic] = [];
                }
                topics[topic].push(callback);
                return {
                    "topic": topic,
                    "callback": callback
                };
            };
            $.unsubscribe = function (handle) {
                var topic = handle.topic;
                if (topics[topic]) {
                    var currentTopic = topics[topic];

                    for (var i = 0, j = currentTopic.length; i < j; i++) {
                        if (currentTopic[i] === handle.callback) {
                            currentTopic.splice(i, 1);
                        }
                    }
                }
            };
        })(jQuery);

    </script>
    <script src="../assets/js/jSignature.min.noconflict.js"></script>
    <script>



        function pngimage() {
            document.getElementById("savemepost").value =
                    document.getElementById("displayarea").innerHTML;

            alert(document.getElementById("savemepost").value);
            return true;
        }






        function strcmpselect(a, b)
        {
            return (a < b ? -1 : (a > b ? 1 : 0));
        }


        (function ($) {

            $(document).ready(function () {

                // This is the part where jSignature is initialized.
                var $sigdiv = $("#signature").jSignature({'UndoButton': true})

                        // All the code below is just code driving the demo. 
                        , $tools = $('#tools')
                        , $extraarea = $('#displayarea')
                        , pubsubprefix = 'jSignature.demo.'

                var export_plugins = $sigdiv.jSignature('listPlugins', 'export')
                        , chops = ['<select>', '<option value="">(select Image format)</option>']
                        , name
                for (var i in export_plugins) {
                    if (export_plugins.hasOwnProperty(0)) {

                        name = export_plugins[i]
                        if (name == "image") {
                            chops.push('<option value="' + name + '">' + name + '</option>')
                        }

                    }
                }
                chops.push('</select><span><b> or: </b></span>')

                $(chops.join('')).bind('change', function (e) {
                    if (e.target.value !== '') {
                        var data = $sigdiv.jSignature('getData', e.target.value)
                        $.publish(pubsubprefix + 'formatchanged')
                        if (typeof data === 'string') {
                            $('textarea', $tools).val(data)
                        } else if ($.isArray(data) && data.length === 2) {
                            $('textarea', $tools).val(data.join(','))
                            $.publish(pubsubprefix + data[0], data);
                        } else {
                            try {
                                $('textarea', $tools).val(JSON.stringify(data))
                            } catch (ex) {
                                $('textarea', $tools).val('Not sure how to stringify this, likely binary, format.')
                            }
                        }
                    }
                }).appendTo($tools)


                $('<input type="button" value="Reset">').bind('click', function (e) {
                    $sigdiv.jSignature('reset')
                }).appendTo($tools)

//                $('<div><textarea style="width:100%;height:7em;"></textarea></div>').appendTo($tools)

                $.subscribe(pubsubprefix + 'formatchanged', function () {
                    $extraarea.html('')
                })

                $.subscribe(pubsubprefix + 'image/svg+xml', function (data) {

                    try {
                        var i = new Image()
                        i.src = 'data:' + data[0] + ';base64,' + btoa(data[1])
                        $(i).appendTo($extraarea)
                    } catch (ex) {

                    }

                    var message = [
                        "If you don't see an image immediately above, it means your browser is unable to display in-line (data-url-formatted) SVG."
                                , "This is NOT an issue with jSignature, as we can export proper SVG document regardless of browser's ability to display it."
                                , "Try this page in a modern browser to see the SVG on the page, or export data as plain SVG, save to disk as text file and view in any SVG-capabale viewer."
                    ]
                    $("<div>" + message.join("<br/>") + "</div>").appendTo($extraarea)
                });

                $.subscribe(pubsubprefix + 'image/svg+xml;base64', function (data) {
                    var i = new Image()
                    i.src = 'data:' + data[0] + ',' + data[1]
                    $(i).appendTo($extraarea)

                    var message = [
                        "If you don't see an image immediately above, it means your browser is unable to display in-line (data-url-formatted) SVG."
                                , "This is NOT an issue with jSignature, as we can export proper SVG document regardless of browser's ability to display it."
                                , "Try this page in a modern browser to see the SVG on the page, or export data as plain SVG, save to disk as text file and view in any SVG-capabale viewer."
                    ]
                    $("<div>" + message.join("<br/>") + "</div>").appendTo($extraarea)
                });

                $.subscribe(pubsubprefix + 'image/png;base64', function (data) {
                    var i = new Image()
                    i.src = 'data:' + data[0] + ',' + data[1]
                    $('<span><b>As you can see, one of the problems of "image" extraction (besides not working on some old Androids, elsewhere) is that it extracts A LOT OF DATA and includes all the decoration that is not part of the signature.</b></span>').appendTo($extraarea)
                    $(i).appendTo($extraarea)
                });

                $.subscribe(pubsubprefix + 'image/jsignature;base30', function (data) {
                    $('<span><b>This is a vector format not natively render-able by browsers. Format is a compressed "movement coordinates arrays" structure tuned for use server-side. The bonus of this format is its tiny storage footprint and ease of deriving rendering instructions in programmatic, iterative manner.</b></span>').appendTo($extraarea)
                });

//                if (Modernizr.touch) {
//                    $('#scrollgrabber').height($('#content').height())
//                }

            })

        })(jQuery)
    </script>
