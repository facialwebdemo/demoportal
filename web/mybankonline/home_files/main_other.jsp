<%@page import="com.mollatech.web.token.SetAuthType"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<!DOCTYPE html>
<!-- saved from url=(0050)https://#/personal/main.do -->
<html lang="en"><script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script><script type="text/javascript">(function() {
            return window.SIG_EXT = {};
        })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="msapplication-tap-highlight" content="no">
        <% Object obj = request.getSession().getAttribute("OTPVerificationResult");
            String _message1 = null;
            String _message2 = null;
            String _message3 = null;
            String _message4 = null;
            String _message5 = null;
            String _message6 = null;
            boolean msg1flag = false;
            boolean msg2flag = false;
            boolean msg3flag = false;
            boolean msg4flag = false;
            boolean msg5flag = false;
            boolean msg6flag = false;

            int type = (Integer) request.getSession().getAttribute("type");

            if (type == 6) {

                if ((String) request.getSession().getAttribute("geoCheck") == "success") {
                    _message1 = "Geolocation verified";
                    msg1flag = true;
                } else {
                    _message1 = "Geolocation not verified";
                }

                if ((String) request.getSession().getAttribute("validImage") == "success") {
                    _message2 = "Valid Image verified";
                    msg2flag = true;
                } else {
                    _message2 = "Valid Image not verified";
                }

                if ((String) request.getSession().getAttribute("IpCheck") == "success") {
                    _message3 = "IP verified";
                    msg3flag = true;
                } else {
                    _message3 = "IP not verified";
                }

                if ((String) request.getSession().getAttribute("DeviceCheck") == "success") {
                    _message4 = "Device verified";
                    msg4flag = true;
                } else {
                    _message4 = "Device not verified";
                }

                if ((String) request.getSession().getAttribute("UserCheck") == "success") {
                    _message5 = "User verified";
                    msg5flag = true;
                } else {
                    _message5 = "User not verified";
                }

                if ((String) request.getSession().getAttribute("SweetSpotCheck") == "success") {
                    _message6 = "Sweet Spot verified";
                    msg6flag = true;
                } else {
                    _message6 = "Sweet Spot not verified";
                }

//            request.getSession().setAttribute("geoCheck", null);
//            request.getSession().setAttribute("validImage", null);
//            request.getSession().setAttribute("IpCheck", null);
//            request.getSession().setAttribute("DeviceCheck", null);
//            request.getSession().setAttribute("UserCheck", null);
//            request.getSession().setAttribute("SweetSpotCheck", null);
            }

            if (obj == null) {

        %>
        <!--        <script>
                    //document.getElementById("result-tab").style.display = "none";
                </script>-->

        <%} else {
                JSONObject jobj = (JSONObject) obj;
                _message1 = jobj.getString("_message1");
                _message2 = jobj.getString("_message2");
            }
            request.getSession().setAttribute("OTPVerificationResult", null);


        %>


        <title>MyBankonline</title>
        <link href="combined.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="jquery-1.11.1.min(1).js"></script>
        <script type="text/javascript" src="ibcommon.js"></script>
        <script type="text/javascript" src="validation.jsp"></script>
        <!--<script type="text/javascript" src="countdown.jsp"></script>-->
        <script type="text/javascript" src="jquery.preventDoubleSubmit.js"></script>
    </head>

    <body onunload="doOnUnload();">  
        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>

        <div id="container">
            <div id="opennav">
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
            </div>
            <div id="welcomebar">
                <span>vikram sareen</span>
                <ul>
                    <li><a class="button_red" href="https://#/personal/login/logout.do" id="logout"><img src="power.png" alt="Log Off"><span class="hidden-phone">Log Off</span></a></li>
                </ul>
            </div>
            <div id="sidebar">
                <div id="closenav">Menu</div>
                <!--Left Navi-->
                <ul class="menu">
                    <!--                    <li><a class="current" href="main.html">Home</a></li>-->
                    <li><a class="current" href="./userPortal.jsp">Home</a></li>
                    <!--<a class="expand" href="./userPortal.jsp">My Profile</a>-->
                    <li><a href="https://#/personal/main.do?view2=">View All My Accounts</a></li>
                    <li><a href="https://#/personal/account/deposit_accounts_enquiry.do">My Deposits</a></li>
                    <li><a href="https://#/personal/account/card_accounts_enquiry.do">My Cards</a></li>
                    <li><a href="https://#/personal/account/loan_accounts_enquiry.do">My Borrowings</a></li>
                    <li>
                        <a href="../transferibg_other.html">Payments</a>
                        <div class="div_submenu">
                            <ul class="submenu">
                                <li><a href="https://#/personal/payment/favourite_acc_maintenance.do">Favourites</a></li>
                                <li><a title="Pay Myself allows you to transfer funds between your own Current/Saving accounts." href="https://#/personal/payment/own_fund_transfer.do">Pay Myself</a></li>
                                <li>
                                    <a href="https://#/personal/payment/third_party_fund_transfer.do">Pay Others</a>
                                    <ul class="submenu-child">
                                        <li><a title="Pay Others in MyBank Bank allows you to transfer funds to other third party MyBank Bank Current/Saving account. Funds transfer will be available immediately on the beneficiary account." href="https://#/personal/payment/third_party_fund_transfer.do">In MyBank Bank</a></li>
                                        <li><a title="Pay Others in Other Bank allows you to transfer funds to another bank&#39;s account via InterBank GIRO (IBG) to a Current/Saving, Loan/Financing or Card account. Fund will be available in two to three working days at Beneficiary Bank." href="https://#/personal/payment/ibg_fund_transfer.do">In Other Bank (IBG)</a></li>
                                    </ul>
                                </li>
                                <li><a title="Pay Cards allows you to pay to your MyBank Bank Credit Card or top up your MyBank Bank Prepaid Card." href="https://#/personal/payment/card_payment.do">Pay Cards</a></li>
                                <li><a title="Pay Loan/Financing allows you to pay your MyBank Bank Loan/Financing account" href="trasnfer.html">Pay Loans / Financing</a></li>
                                <li><a href="https://#/personal/payment/bill_payment.do">Pay Bills</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a class="expand" href="https://#/personal/main.do#">Payments in Foreign Currency</a>
                        <div class="div_submenu">
                            <ul class="submenu">
                                <li><a href="https://#/personal/wcaftt/wca_ftt_add_favourite.do">Favourites</a></li>
                                <li><a title="Pay Myself allows you to transfer funds between your own Current/Saving accounts." href="https://#/personal/wcaftt/wca_pay_myself.do">Pay Myself</a></li>
                                <li>
                                    <a href="https://#/personal/wcaftt/wca_pay_others.do">Pay Others</a>
                                    <ul class="submenu-child">
                                       <li><a title="Pay Others in MyBank Bank (in Foreign Currency) allows you to transfer funds to other third party MyBank Bank MyBank XChange Account. Funds transfer will be available immediately on the beneficiary account." href="https://#/personal/wcaftt/wca_pay_others.do">In MyBank Bank</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a class="expand" href="https://#/personal/main.do#">Prepaid Reload</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/prepaid_reload/favour_prepaid_reload.do">Favourites</a></li>





























                                <li><a href="https://#/personal/prepaid_reload/prepaid_reload.do">Prepaid Reload</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/main.do#">e-Transaction Status</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/transaction/success_transaction.do">Successful</a></li>





























                                <li><a href="https://#/personal/transaction/failed_transaction.do">Failed</a></li>





























                                <li><a href="https://#/personal/transaction/upcoming_transaction.do">Upcoming</a></li>





























                                <li><a href="https://#/personal/transaction/cancelled_transaction.do">Cancelled</a></li>



                            </ul>
                        </div>
                    </li>
                </ul>

                <hr class="navi">


                <ul class="menu">
                    <li>


























                        <a class="expand" href="https://#/personal/main.do#">My e-Saving Account</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/acc_open/online_acc_opening.do">Account Opening</a></li>





























                                <li><a href="https://#/personal/acc_open/term_condition_acc_opening.do">Terms and Conditions</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/main.do#">e-Fixed Deposit</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_placement.do">Placement</a></li>





























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_upliftment.do">Withdrawal</a></li>





























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_update.do">Renewal Instruction</a></li>



                            </ul>
                        </div>
                    </li>
                    <li class="promo" id="promoefd">
                        <a href="https://#/personal/fixeddeposit/fixed_deposit_placement.do?data=promo"><span class="new">PROMO! </span>e-Fixed Deposit</a>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/main.do#">MyBank XChange Account</a>
                        <div class="div_submenu">



                            <ul class="submenu">					


























                                <li><a href="https://#/personal/fca/wca_account_opening.do">Account Opening</a></li>






























                                <li><a href="https://#/personal/fca/term_condition_wca_acc_opening.do">Terms and Conditions</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/main.do#">MyBank XChange Fixed Deposit</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/wcfd/w_c_fixed_deposit_placement.do">Placement</a></li>





























                                <li><a href="https://#/personal/wcfd/w_c_fixed_deposit_upliftment.do">Withdrawal</a></li>



                            </ul>
                        </div>
                    </li>	

                </ul>
                <hr class="navi">

                <ul class="menu">
                    <li class="hide">


























                        <a href="https://#/personal/trading/stock_trading.do">Stock Trading Sign On</a>



                    </li>
                    <li>
                        <a class="expand" href="https://#/personal/main.do#">Cheque Services</a>
                        <div class="div_submenu">
                            <ul class="submenu">
                                <li><a href="https://#/personal/cheque/cheque_status_inquiry.do">Cheque Details</a></li>
                                <li><a href="https://#/personal/cheque/stop_cheque.do">Stop Cheque</a></li>


                                <li><a href="https://#/personal/cheque/cheque_book_request.do">Cheque Book Request</a></li>



                            </ul>
                        </div>
                    </li>		
                    <li>


























                        <a href="https://#/personal/sr/statement_request.do">e-Statement</a>



                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/main.do#">Rates &amp; Charges</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/rate_charges/foreign_exchange_rate.do">Foreign Exchange Rates</a></li>



                                <li><a target="_blank" href="https://#/personal/rate_charges/interest_rate.do">Interest Rates</a></li>
                                <li><a target="_blank" href="https://#/personal/rate_charges/rate_of_return.do">Rate of Return</a></li>
                                <li><a target="_blank" href="https://#/personal/rate_charges/fees_and_charges.do">Fees and Charges</a></li>
                                <li><a href="https://#/personal/rate_charges/e_rate_secured_view.do">e-Rates (applicable to XChange accounts)</a></li>
                            </ul>
                        </div>
                    </li>	
                </ul>
                <hr class="navi">
                <ul class="menu">
                    <li>
                        <a class="expand" href="https://#/personal/main.do#">My Mailbox</a>
                        <div class="div_submenu">
                            <ul class="submenu">
                                <li><a href="https://#/personal/secured_msg/compose_message.do">Compose</a></li>
                                <li><a href="https://#/personal/secured_msg/inbox_messages.do">Inbox</a></li>
                                <li><a href="https://#/personal/secured_msg/sent_messages.do">Outbox</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>


























                        <!--<a class="expand" href="https://#/personal/main.do#">My Profile</a>-->
                    <li><a class="expand" href="./userPortal.jsp">My Profile</a></li>


                    <ul class="submenu">


























                        <li><a href="https://#/personal/profile/change_password.do">Change Password</a></li>





























                        <li><a href="https://#/personal/profile/change_secure_phrase.do">Change Secure Phrase</a></li>




















                        <li><a href="https://#/personal/profile/update_user_mail.do">Update Mailing Address</a></li>
                        <li><a href="https://#/personal/profile/update_user_e_mail.do">Update Email Address</a></li>
                        <li><a href="https://#/personal/profile/update_quick_link.do">Customize Quick Links</a></li>
                        <li><a href="https://#/personal/profile/update_trx_limit.do">Limit Maintenance</a></li>
                        <li><a href="https://#/personal/profile/update_linked_acc.do">Link/ Unlink Account</a></li>
                    </ul>
            </div>
        </li>
    </ul>
    <!--/Left Navi-->
</div>
<div id="main">
    <div id="viewLandingContent">
        <link href="looper.css" rel="styleSheet" type="text/css">
        <script type="text/javascript" src="looper.min.js"></script>
        <script type="text/javascript">

        function popupGame(form) {
            window.open('', 'formpopup', 'width=800,height=600,resizeable,scrollbars');
            form.target = 'formpopup';

            setTimeout(function() {
                location.replace("/personal/login/logout.do");
            }, 100);
        }

        </script>

        <div class="banner looper slide" data-looper="go" data-interval="3000" style="margin-bottom:15px;" tabindex="0">
            <div class="looper-inner">

                <div class="item active">			

                    <a href="https://#/personal/fixeddeposit/fixed_deposit_placement.do?data=promo">
                        <img src="render_dynamic_img.do" alt="Banner">
                    </a>



                </div>

            </div>
        </div>


        <div id="welcome">
            <div id="stats">
                <h2>


                    Welcome, vikram sareen!


                </h2>
                <div class="timestamp">05-Nov-2014 03:54 PM</div>
                <ul class="bullet_square">

                    <div id="result-tab">
                        <% if (_message1 != null) {
                                if (msg1flag == true) {
                        %>
                        <li><h4><font color="blue"><%=_message1%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" /></h4></li>
                                <%} else {%>
                        <li><h4><font color="blue"><%=_message1%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="cross.png" /></h4></li>
                                <%}
                            }%>

                        <% if (_message2 != null) {
                                if (msg2flag == true) {
                        %>
                        <li><h4><font color="blue"><%=_message2%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" /></h4></li>
                                <%} else {%>
                        <li><h4><font color="blue"><%=_message2%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="cross.png" /></h4></li>
                                <%}
                            }%>


                        <% if (_message6 != null) {
                                if (msg6flag == true) {
                        %>
                        <li><h4><font color="blue"><%=_message6%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" /></h4></li>
                                <%} else {%>
                        <li><h4><font color="blue"><%=_message6%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="cross.png" /></h4></li>
                                <%}
                            }%>

                        <% if (_message3 != null) {
                                if (msg3flag == true) {
                        %>
                        <li><h4><font color="blue"><%=_message3%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" /></h4></li>
                                <%} else {%>
                        <li><h4><font color="blue"><%=_message3%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="cross.png" /></h4></li>
                                <%}
                            }%>


                        <% if (_message4 != null) {
                                if (msg4flag == true) {
                        %>
                        <li><h4><font color="blue"><%=_message4%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" /></h4></li>
                                <%} else {%>
                        <li><h4><font color="blue"><%=_message4%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="cross.png" /></h4></li>
                                <%}
                            }%>



                        <% if (_message5 != null) {
                                if (msg5flag == true) {
                        %>
                        <li><h4><font color="blue"><%=_message5%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" /></h4></li>
                                <%} else {%>
                        <li><h4><font color="blue"><%=_message5%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="cross.png" /></h4></li>
                                <%}
                            }%>


                    </div>

                    <li>Your last successful login was on 
                        05-Nov-2014 03:28 PM

                    </li>
                    <li>Your last failed login was on






                    </li>
                    <li> 
                        You have
                        <a href="https://#/personal/transaction/failed_transaction.do">1
                            new failed transaction(s).</a>

                    </li>
                    <li>You have
                        <a href="https://#/personal/transaction/upcoming_transaction.do">0
                            upcoming transfers</a>	
                        and		    	
                        <a href="https://#/personal/transaction/upcoming_transaction.do">0
                            upcoming payments</a>	
                        in the next 2 weeks.	</li>
                </ul>
            </div>
            <div id="personal">
                <table class="nowrap">
                    <tbody><tr>
                            <td class="landing"><strong>My Mailbox</strong> </td>
                        </tr>
                        <tr>
                            <td><img src="icon_mail.gif" alt="" width="18" height="13"> 
                                <strong>You have</strong> 
                                <a href="https://#/personal/secured_msg/inbox_messages.do"><span class="redbold">2</span> 
                                    <strong>new message(s).</strong></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="landing"><strong style="float:left;">My Personal Details</strong><span style="float:right;">[ <a href="https://#/personal/profile/change_password.do">Edit</a> ]</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Name</strong>: VIKRAM SAREEN <br>
                                <strong>Email</strong>: vikrXXXXXXXX@gmail.com<br>
                                <strong>Contact</strong>: 6010366XXXX
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="clear"></div>
        </div>

















        <div id="besurprised">

            <div class="game_banner" id="ecoin_msg" style="float:left;"> Deposit into e-Fixed Deposit or MyBank XChange to receive tokens
            </div>

            <div style="position:absolute;">



            </div>
            <div id="remaining_coin" style="font-size:2em;">
                368
            </div>
        </div>
        <h3 style="float:left;">Account Overview</h3>
        <div id="viewall">
            <a class="button_red" href="https://#/personal/main.do?view2=">View All<span class="hidden-phone"> My Accounts</span></a>
        </div>
        <!-- casa accounts  -->
        <table class="table-org acctSummary results" id="casa">
            <thead>
                <tr>
                    <th class="groupedHead" colspan="5">Savings, Current and MyBank XChange Accounts</th></tr>
                <tr>
                    <th>Account</th>
                    <th>Account Number</th>
                    <th class="taborg-theven-right">Currency</th>
                    <th class="taborg-theven-right">Available Balance</th>
                    <th class="taborg-theven-right">Current Balance</th></tr></thead>
            <tfoot>
                <tr>
                    <td><span class="tertiary">Total MYR</span></td>
                    <td>&nbsp;</td>
                    <td>Total MYR</td>
                    <td>
                        162.45 CR
                    </td>
                    <td>
                        162.45 CR
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <tr class="odd">
                    <td>
                        SAVINGS ACCOUNT-SPI
                        <div class="secondary">
                            <a href="https://#/personal/account/deposit_accounts_enquiry.do?details=&viewingAccountNo=641990020014608&subGroupKey=DS">64199-0-02-001460-8</a>
                        </div>
                    </td>
                    <td>
                        <a href="https://#/personal/account/deposit_accounts_enquiry.do?details=&viewingAccountNo=641990020014608&subGroupKey=DS">64199-0-02-001460-8</a>


                    </td>
                    <td style="text-align:right;">MYR</td>
                    <td style="text-align:right;">

                        <span class="tertiary">MYR</span> 162.45 CR

                    </td>
                    <td style="text-align:right;">

                        <span class="tertiary">MYR</span> 162.45 CR

                    </td></tr></tbody></table>
        <br>













        <table class="table-org acctSummary results" id="creditcard">
            <thead>
                <tr>
                    <th class="groupedHead" colspan="5">Credit Card</th></tr>
                <tr>
                    <th>Card</th>
                    <th>Card Number</th>
                    <th class="taborg-theven-right">Currency</th>
                    <th class="taborg-theven-right">Statement Balance</th>
                    <th class="taborg-theven-right">Current Outstanding Balance</th></tr></thead>
            <tfoot>





                <tr>
                    <td><span class="tertiary">Total MYR</span></td>
                    <td>&nbsp;</td>
                    <td>Total MYR</td>
                    <td>
                        4,291.11 DR
                    </td>
                    <td>
                        4,710.96 DR
                    </td>
                </tr>

            </tfoot>
            <tbody>
                <tr class="odd">
                    <td>
                        MASTERCARD GOLD
                        <div class="secondary">

                            <a href="https://#/personal/account/card_accounts_enquiry.do?details=&viewingAccountNo=5465946304215016&subGroupKey=C">5465-9463-0421-5016</a>
                        </div>
                    </td>
                    <td>

                        <a href="https://#/personal/account/card_accounts_enquiry.do?details=&viewingAccountNo=5465946304215016&subGroupKey=C">5465-9463-0421-5016</a>
                    </td>
                    <td style="text-align:right;">
                        MYR
                    </td>
                    <td style="text-align:right;">
                        <span class="tertiary">MYR</span> 
                        <span title="Payment Due: 11-Nov-2014">
                            4,291.11 DR
                        </span>
                    </td>
                    <td style="text-align:right;">
                        <span class="tertiary">MYR</span> 4,710.96 DR
                    </td></tr></tbody></table>
        <br>
    </div>
    <script type="text/javascript">
        $('#viewLandingContent').load('/personal/main.do?viewContent=');
    </script>
</div>
<!-- start #footer -->
<div id="footer">
    <div class="compatibleView">Best viewed on Internet Explorer 7 &amp; above, Firefox and Chrome</div>
    <div class="footerlink"><a href="http://#/About-Us/About-the-Website/Privacy-Policy.aspx" target="_blank">Privacy Policy</a> | <a href="http://#/About-Us/About-the-Website/Client-Charter.aspx" target="_blank">Client Charter</a> | <a href="http://#/ABMB/media/MyLibrary/Shared/Files/Customer-Service-Charter.pdf" target="_blank">Customer Service Charter</a> | <a href="http://#/ABMB/media/MyLibrary/ABMB/PDF/aop_tnc.pdf" target="_blank">Terms &amp; Conditions</a></div>
    <div class="copyright">Copyright � 2014 MyBank (Demo site) </div>
</div>
<!-- end #footer -->
</div>
<script type="text/javascript">
    $('#container').on('click', 'a[href*=".do"]', function() {
        if (this.target == null || this.target == "") {
            var loadercontent = '<div id="spinner"><img src="/personal/images/ib/ajax-loader.gif" alt="Loading" width="128" height="15" /></div>';
            setTimeout(function() {
                $('#main').html(loadercontent);
            }, 500);
        }
    });

    $('#sidebar').on('click', 'a.expand', function(e) {
        e.preventDefault();
        window.location = $(this).siblings('.div_submenu').find('a').get(0).href;
    });

</script>

<%// if(type==SetAuthType.WEB_SEAL){

%>

<!--    <script>
var img = document.createElement("IMG");
img.src = "https://188.166.183.80:8443/face/GetWebSeal?urlUniqueId=430a698065f31167";
document.getElementById("imageDiv").appendChild(img);
</script>-->

<%//}%>

</body></html>