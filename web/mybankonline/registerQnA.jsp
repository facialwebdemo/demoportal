<%@page import="java.util.Random"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomQuestionAndAnswer"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomChallengeResponse"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<!DOCTYPE html>

<html lang="en" style="display: block;"><script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script><script type="text/javascript">(function () {
            return window.SIG_EXT = {};
        })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">	
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#001e54">
        <meta name="msapplication-TileImage" content="/personal/images/mstile-144x144.png">
        <meta name="msapplication-config" content="/personal/images/browserconfig.xml">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="application-name" content="MyBankonline">	

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


        <title>MyBankonline</title>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp"></script>
        <!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
        <link href="./login_files/combined.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./login_files/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="./login_files/ibcommon.js"></script>
        <script type="text/javascript" src="./login_files/validation.jsp"></script>
        <script type="text/javascript" src="./login_files/jquery.preventDoubleSubmit.js"></script>
        <script type="text/javascript" src="./login_files/jquery.simplemodal.1.4.4.min.js"></script>

        <script src="../assets/js/jquery.js"></script>
        <script src="../assets/js/pdfsigning.js"></script>
        <script src="../assets/js/json_sans_eval.js"></script>
        <script src="../assets/js/bootbox.min.js"></script>
        <script src="../assets/js/ajaxfileupload.js"></script>
        <script src="../assets/js/bootstrap-fileupload.js"></script>   
        <script src="../assets/js/bootstrap-transition.js"></script>
        <script src="../assets/js/bootstrap-alert.js"></script>
        <script src="../assets/js/bootstrap-modal.js"></script>
        <script src="../assets/js/bootstrap-dropdown.js"></script>
        <script src="../assets/js/bootstrap-scrollspy.js"></script>
        <script src="../assets/js/bootstrap-tab.js"></script>
        <script src="../assets/js/bootstrap-tooltip.js"></script>
        <script src="../assets/js/bootstrap-popover.js"></script>
        <script src="../assets/js/bootstrap-button.js"></script>
        <script src="../assets/js/bootstrap-collapse.js"></script>
        <script src="../assets/js/bootstrap-carousel.js"></script>
        <script src="../assets/js/bootstrap-typeahead.js"></script>
        <script src="../assets/js/bootstrap-datepicker.js"></script>


        <script src="../assets/js/json_sans_eval.js"></script>
        <script src="../assets/js/bootbox.min.js"></script>
        <script src="../assets/js/bootstrap-modal.js"></script>
        <link href="../assets/css/bootstrap.css" rel="stylesheet">
        <script type="text/javascript" src="../assets/js/QnA.js"></script>
    <body>
        <%
            String sessionId = session.getAttribute("_userSessinId").toString();
            AxiomWrapper wrapper = new AxiomWrapper();
            Random rand = new Random();
            int randomNum = rand.nextInt((7 - 5) + 1) + 5;
            AxiomChallengeResponse challengeResponse = wrapper.getQuestionsForRegistration(sessionId, randomNum, null);

        %>
        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>
        <div id="container">
            <div id="content">

                <div class="banner">
                    <a href="#" target="_blank">
                        <img src="icons/SCB_echannels_internetbanking_web.jpg" width=100%" alt="Banner"/>
                    </a>
                </div>

                <div class="box_mask">
                    <div class="box box_gradient">
                        <div class="welcometitle">Welcome to <strong>Questions And Answers Demo</strong></div>
                        <div class="row-fluid">
                            <form class="form-horizontal" id="QnAForm" name="QnAForm">
                                <fieldset>
                                    <%if (challengeResponse != null) {
                                            session.setAttribute("challengeResponse", challengeResponse);
                                            for (int i = 0; i < challengeResponse.getWebQAndA().size(); i++) {
                                                AxiomQuestionAndAnswer questionAndAnswer = challengeResponse.getWebQAndA().get(i);
                                    %>
                                    <div class="control-group">
                                        <label class="control-label"  for="hwserialno">Question <%=i + 1%> :</label>
                                        <div class="controls">
                                            <%=questionAndAnswer.getQuestion()%>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label"  for="hwserialno">Answer :</label>
                                        <div class="controls">
                                            <input type="text" id="ans<%=questionAndAnswer.getQuestionid()%>" name="ans<%=questionAndAnswer.getQuestionid()%>" placeholder="Enter Answer" class="input-xlarge"/>
                                        </div>
                                    </div>
                                    <%}
                                        }
                                    %>
                                    <input value="Save Details " class="button_blue" onclick="saveQnA()" type="button" >
                                </fieldset>
                            </form>
                        </div></div></div>
                <!-- start #footer -->
                <div id="footer">
                    <div class="compatibleView">Best viewed on Internet Explorer 7 &amp; above, Firefox and Chrome</div>
                    <div class="footerlink"><a href="http://#/About-Us/About-the-Website/Privacy-Policy.aspx" target="_blank">Privacy Policy</a> | <a href="http://#/About-Us/About-the-Website/Client-Charter.aspx" target="_blank">Client Charter</a> | <a href="http://#/ABMB/media/MyLibrary/Shared/Files/Customer-Service-Charter.pdf" target="_blank">Customer Service Charter</a> | <a href="http://#/ABMB/media/MyLibrary/ABMB/PDF/aop_tnc.pdf" target="_blank">Terms &amp; Conditions</a></div>
                    <div class="copyright">Copyright © 2014 MyBank (Demo site) </div>
                </div>
                <!-- end #footer -->
            </div>

    </body></html>