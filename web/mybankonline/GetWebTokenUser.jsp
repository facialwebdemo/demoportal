<%@page import="com.mollatech.axiom.common.AxiomProtectConnector"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page import="org.json.JSONObject"%>
<%
    try {
        String result = "success";
        String message = null;
        JSONObject json = new JSONObject();
        RssUserCerdentials rssUserObj = (RssUserCerdentials) session.getAttribute("_rssDemoUserCerdentials");

        if (rssUserObj == null) {
            result = "error";
            message = "Insufficient Parameters!!!";
            json.put("_result", result);
            json.put("_message", message);

            response.setContentType("application/json");
            out.print(json);
            return;
        }

        byte[] data = AxiomProtectConnector.Bas64SHA1Inner(rssUserObj.getRssUser().getUserId());
        String wtidq = new String(data);
        String hex = new String(data).replace("", "-");
        String wtid = hex;
        json.put("_identifier", wtid);
        json.put("_result", result);

        session.setAttribute("_apuserIDForWebToken", wtid);

        //cm.putSessionString("_apuserIDForWebToken", wtid);
        response.setContentType("application/json");

        out.print(json);

    } catch (Exception ex) {
        ex.printStackTrace();
    }


%>

