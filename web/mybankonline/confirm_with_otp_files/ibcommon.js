var capabilities = null;

function resetForm(theform) {
	// iterate over all of the inputs for the form
	// element that was passed in
	$(':input', theform).each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		// it's ok to reset the value attr of text inputs,
		// password inputs, and textareas
		if (type == 'text' || type == 'password' || tag == 'textarea')
			this.value = '';
		// checkboxes and radios need to have their checked state cleared
		// but should *not* have their 'value' changed
		else if (type == 'checkbox' || type == 'radio')
			this.checked = false;
		// select elements need to have their 'selectedIndex' property set to -1
		// (this works for both single and multiple select elements)
		else if (tag == 'select')
			this.selectedIndex = 0;
	});
	window.scroll(0,1);
	return false;
}

function getTime() {
    return (new Date()).getTime();
}

var initialTime = getTime();
var defaultTimer = 300 * 1000;
var timer = defaultTimer;
var timeoutUrlString;

function countDown() { 	 
	var now = getTime();
	if ((now - initialTime) >= timer ) {         
		window.top.location.href = timeoutUrlString;
		return;
	}
}

function timeoutInSecond(timerInSecond, timeoutUrl) {
	timer = timerInSecond * 1000;
	timeoutUrlString = timeoutUrl;
	setInterval(countDown, 1000);
}


/************* AOP common functions ************/

function disableSubmit() {
	return false;
}	

function getToday(offset) { // In format dd/mm/yyyy
	var today = new Date();
	if (offset) today.setDate(today.getDate() + offset);
	var dd = today.getDate();
	var mm = today.getMonth()+1;//January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd;}
	if(mm<10){mm='0'+mm;}
	return (yyyy+'-'+mm+'-'+dd);
}

function buildDropdown(id, list, val, name) {
	// do not allow duplicate options.
	var hash = (function() {
	    var keys = {};
	    return {
	        contains: function(key) {
	            return keys[key] === true;
	        },
	        add: function(key) {
	            if (keys[key] !== true)
	            {
	                keys[key] = true;
	            }
	        }
	    };
	})();
	
	
    var opt = "<option value=''>Please Select</option>";
    var tmpAry = new Array();
    var index = 0;

    // hash to make sure there are no duplicate val
    $.each(list, function () {    	 
    	// do not allow duplicate options.
    	if (!hash.contains( this[val]))
	    {	
    		tmpAry[index] = new Array();
       	 	tmpAry[index][0] = this[name];
       	 	tmpAry[index][1] = this[val];
       	 	index++;
	        hash.add( this[val] );
	    }
        
    });
    
    // sorting d array
    tmpAry.sort();
    
    // generate option
    for (var i=0;i<tmpAry.length;i++) {
    	opt += "<option value='" + tmpAry[i][1] + "'>" + tmpAry[i][0] + "</option>";
    }
    $('#' + id).html(opt);
}

function getReceipt(url) {
	if ($('body').hasClass('mobi')) {
		// For mobile devices, show an inline receipt
		$('#main').append('<div id="receipt" />');
		$('<iframe />', {
			name: 'r',
			id: 'r',
			src: url,
			width: '100%',
			height: '100%',
			frameborder: '0'
		}).appendTo('#receipt');
		$('<span id="close" onclick="closeReceipt();">Close</span>').appendTo('#receipt');
		if (/(iPad|iPhone|iPod)/.test(capabilities.userAgent)) {
			$('#receipt').css({
				'-webkit-overflow-scrolling': 'touch',
				'overflow': 'auto'
			});
		}
	}
	else {
		var receipt = window.open(url, 'receipt', 'toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=1,width=630,height=500');
		receipt.focus();
	}
	return false;
}

function closeReceipt() {
	$('#receipt').fadeOut( function() { $(this).remove(); });
}

$(function(){
	$('form').each(function () {
		var thisform = $(this);
		thisform.prepend(thisform.find('input.default[type="submit"]').clone().css({
			position: 'absolute',
			left: '-999px',
			top: '-999px',
			height: 0,
			width: 0
		}));
	});
});

