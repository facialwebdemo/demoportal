<%@page import="com.mollatech.axiom.bridge.crypto.LoadSettings"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.v2.core.rss.InitTransactionPackage"%>
<%@page import="java.util.Random"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.mollatech.axiom.v2.core.rss.TransactionStatus"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomStatus"%>
<%@page import="com.mollatech.web.token.SetAuthType"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<!DOCTYPE html>
<!-- saved from url=(0071)https://#/personal/payment/ibg_fund_transfer.do -->
<html lang="en">
    <script src="../../JS/bootbox.min.js" type="text/javascript"></script>
    <!--    MAYURI-->
    <script src="../../CryptoCore/AesUtil.js" type="text/javascript"></script>
    <script src="../../CryptoCore/aes.js" type="text/javascript"></script>
    <script src="../../CryptoCore/enc-base64.js" type="text/javascript"></script>
    <script src="../../CryptoCore/md5.js" type="text/javascript"></script>
    <script src="../../CryptoCore/ripemd160.js" type="text/javascript"></script>
    <script src="../../CryptoCore/sha.js" type="text/javascript"></script>
    <script src="../../CryptoCore/sha1.js" type="text/javascript"></script>
    <script src="../../CryptoCore/sha256.js" type="text/javascript"></script>
    <script src="../../CryptoCore/sha512.js" type="text/javascript"></script>
    <script src="http://yui.yahooapis.com/2.9.0/build/yahoo/yahoo-min.js"></script>
    <script src="../../closure/goog/base.js" type="text/javascript"></script>
    <!--    added-->
    <script src="../../closure/goog/deps.js" type="text/javascript"></script>

    <script src="../../JS/CryptoManager.js" type="text/javascript"></script>
    <script src="../../JS/OCRA.js" type="text/javascript"></script>
    <script src="../../JS/base64.js" type="text/javascript"></script>
    <script src="../../JS/crypto-1.1.js" type="text/javascript"></script>
    <script src="../../JS/crypto.js" type="text/javascript"></script>
    <script src="../../JS/hmac-sha1.js" type="text/javascript"></script>
    <script src="../../JS/hmac-sha256.js" type="text/javascript"></script>
    <script src="../../JS/hmac-sha512.js" type="text/javascript"></script>
    <script src="../../JS/jquery.js" type="text/javascript"></script>
    <script src="../../JS/json3.min.js" type="text/javascript"></script>
    <script src="../../JS/jstorage.js" type="text/javascript"></script>
    <script src="../../JS/prng.js" type="text/javascript"></script>
    <script src="../../JS/prng4.js" type="text/javascript"></script>
    <script src="../../JS/rng.js" type="text/javascript"></script>
    <script src="../../JS/sha1.js" type="text/javascript"></script>
    <script src="../../RSAJS/base64.js" type="text/javascript"></script>
    <script src="../../RSAJS/jsbn.js" type="text/javascript"></script>
    <script src="../../RSAJS/jsbn2.js" type="text/javascript"></script>
    <script src="../../RSAJS/json3.min.js" type="text/javascript"></script>
    <script src="../../RequiredLib/jsencrypt.js" type="text/javascript"></script>
    <script src="../../RequiredLib/jsrsasign-5.0.7-all-min.js" type="text/javascript"></script>

    <!--        <script src="RequiredLib/jsencrypt.js" type="text/javascript"></script>-->
    <% String _userID = (String) request.getSession().getAttribute("_userID");
        String authtype = (String) request.getSession().getAttribute("authtype");
    %>


    <script id="tinyhippos-injected">if (window.top.ripple) {
            window.top.ripple("bootstrap").inject(window, document);
        }</script><script type="text/javascript">(function () {
                return window.SIG_EXT = {};
            })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="msapplication-tap-highlight" content="no">



        <title>Pay Others in Other Bank (IBG) - Confirmation</title>










        <link href="combined.min.css" rel="stylesheet" type="text/css">



        <script type="text/javascript" src="jquery-1.11.1.min(1).js"></script>
        <script type="text/javascript" src="ibcommon.js"></script>
        <script type="text/javascript" src="validation.jsp"></script>
        <!--<script type="text/javascript" src="countdown.jsp"></script>-->
        <script type="text/javascript" src="jquery.preventDoubleSubmit.js"></script>
        <script type="text/javascript" src="oneTimeIbgFTPortlet.jsp"></script>
        <script src=".././js/DynamicLoader.js"></script>
        <script>


            loadjscssfile(".././js/json3.min.js", "js");
            loadjscssfile(".././js/jstorage.js", "js");
            loadjscssfile(".././js/modernizr.js", "js");
            loadjscssfile(".././assets/css/style.css", "css");
            loadjscssfile("../assets/css/div.css", "css");
            loadjscssfile(".././js/axiomprotect_wt_reg.js", "js");
            loadjscssfile(".././js/axiomprotect_wt.js", "js");
            loadjscssfile(".././js/checkwebtoken.js", "js");
        </script>
        <script src=".././js/sha.js"></script>
        <script src="../.js/jstorage.js"></script>
        <script src=".././js/crypto.js"></script>
        <script src=".././js/aes.js"></script>
        <script src=".././login_files/login.js"></script>
        <script src="../../rsasignverify/core.js"></script>
        <script src="../../rsasignverify/md5.js"></script>
        <script src="../../rsasignverify/sha1.js"></script>
        <script src="../../rsasignverify/sha256.js"></script>
        <script src="../../rsasignverify/ripemd160.js"></script>
        <script src="../../rsasignverify/x64-core.js"></script>
        <script src="../../rsasignverify/sha512.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/jsbn.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/jsbn2.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/rsa.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/rsa2.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/base64.js"></script>

        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/rsapem-1.1.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/rsasign-1.2.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/asn1hex-1.1.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/x509-1.1.js"></script>
        <script language="JavaScript" type="text/javascript" src="../../rsasignverify/crypto-1.1.js"></script>
        <script type="text/javascript">
            $(function () {
                var isRegisteredBenAcc = 'false';
                if (isRegisteredBenAcc == 'true') {
                    alert('Please confirm that the Beneficiary Account Number is correct before proceeding.');
                } else {
                    if (!$("#requestTac").is(':disabled')) {
                        alert('Please confirm that the Beneficiary Account Number is correct before proceeding.');
                    }
                }
            });
        </script>
    </head>

    <body onunload="doOnUnload();">  
        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>

        <div id="container">
            <div id="opennav">
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
            </div>







            <div id="welcomebar">
                <span>vikram sareen</span>
                <ul>
                    <li><a class="button_red" href="https://#/personal/login/logout.do" id="logout"><img src="power.png" alt="Log Off"><span class="hidden-phone">Log Off</span></a></li>
                </ul>
            </div>



            <div id="sidebar">
                <div id="closenav">Menu</div>











                <!--Left Navi-->
                <ul class="menu">


























                    <li><a href="https://#/personal/main.do">Home</a></li>



                    <li><a href="https://#/personal/main.do?view2=">View All My Accounts</a></li>


























                    <li><a href="https://#/personal/account/deposit_accounts_enquiry.do">My Deposits</a></li>





























                    <li><a href="https://#/personal/account/card_accounts_enquiry.do">My Cards</a></li>





























                    <li><a href="https://#/personal/account/loan_accounts_enquiry.do">My Borrowings</a></li>



                    <li>













                        <a class="expand current" href="https://#/personal/payment/ibg_fund_transfer.do#">Payments</a>
                        <div class="div_submenu on">




                            <ul class="submenu">



                                <li><a href="https://#/personal/payment/favourite_acc_maintenance.do">Favourites</a></li>




                                <li><a title="Pay Myself allows you to transfer funds between your own Current/Saving accounts." href="https://#/personal/payment/own_fund_transfer.do">Pay Myself</a></li>



                                <li>
                                    <a href="https://#/personal/payment/third_party_fund_transfer.do">Pay Others</a>
                                    <ul class="submenu-child">


























                                        <li><a title="Pay Others in MyBank Bank allows you to transfer funds to other third party MyBank Bank Current/Saving account. Funds transfer will be available immediately on the beneficiary account." href="https://#/personal/payment/third_party_fund_transfer.do">In MyBank Bank</a></li>
















                                        <li><a title="Pay Others in Other Bank allows you to transfer funds to another bank&#39;s account via InterBank GIRO (IBG) to a Current/Saving, Loan/Financing or Card account. Fund will be available in two to three working days at Beneficiary Bank." class="current" href="ibg_fund_transfer.html">In Other Bank (IBG)</a></li>

















                                    </ul>
                                </li>


























                                <li><a title="Pay Cards allows you to pay to your MyBank Bank Credit Card or top up your MyBank Bank Prepaid Card." href="https://#/personal/payment/card_payment.do">Pay Cards</a></li>





























                                <li><a title="Pay Loan/Financing allows you to pay your MyBank Bank Loan/Financing account" href="https://#/personal/payment/loan_payment.do">Pay Loans / Financing</a></li>





























                                <li><a href="https://#/personal/payment/bill_payment.do">Pay Bills</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">Payments in Foreign Currency</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/wcaftt/wca_ftt_add_favourite.do">Favourites</a></li>





























                                <li><a title="Pay Myself allows you to transfer funds between your own Current/Saving accounts." href="https://#/personal/wcaftt/wca_pay_myself.do">Pay Myself</a></li>



                                <li>
                                    <a href="https://#/personal/wcaftt/wca_pay_others.do">Pay Others</a>
                                    <ul class="submenu-child">


























                                        <li><a title="Pay Others in MyBank Bank (in Foreign Currency) allows you to transfer funds to other third party MyBank Bank MyBank XChange Account. Funds transfer will be available immediately on the beneficiary account." href="https://#/personal/wcaftt/wca_pay_others.do">In MyBank Bank</a></li>




                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">Prepaid Reload</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/prepaid_reload/favour_prepaid_reload.do">Favourites</a></li>





























                                <li><a href="https://#/personal/prepaid_reload/prepaid_reload.do">Prepaid Reload</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">e-Transaction Status</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/transaction/success_transaction.do">Successful</a></li>





























                                <li><a href="https://#/personal/transaction/failed_transaction.do">Failed</a></li>





























                                <li><a href="https://#/personal/transaction/upcoming_transaction.do">Upcoming</a></li>





























                                <li><a href="https://#/personal/transaction/cancelled_transaction.do">Cancelled</a></li>



                            </ul>
                        </div>
                    </li>
                </ul>

                <hr class="navi">


                <ul class="menu">
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">My e-Saving Account</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/acc_open/online_acc_opening.do">Account Opening</a></li>





























                                <li><a href="https://#/personal/acc_open/term_condition_acc_opening.do">Terms and Conditions</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">e-Fixed Deposit</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_placement.do">Placement</a></li>





























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_upliftment.do">Withdrawal</a></li>





























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_update.do">Renewal Instruction</a></li>



                            </ul>
                        </div>
                    </li>
                    <li class="promo" id="promoefd">
                        <a href="https://#/personal/fixeddeposit/fixed_deposit_placement.do?data=promo"><span class="new">PROMO! </span>e-Fixed Deposit</a>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">MyBank XChange Account</a>
                        <div class="div_submenu">



                            <ul class="submenu">					


























                                <li><a href="https://#/personal/fca/wca_account_opening.do">Account Opening</a></li>






























                                <li><a href="https://#/personal/fca/term_condition_wca_acc_opening.do">Terms and Conditions</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">MyBank XChange Fixed Deposit</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/wcfd/w_c_fixed_deposit_placement.do">Placement</a></li>





























                                <li><a href="https://#/personal/wcfd/w_c_fixed_deposit_upliftment.do">Withdrawal</a></li>



                            </ul>
                        </div>
                    </li>	

                </ul>
                <hr class="navi">

                <ul class="menu">
                    <li class="hide">


























                        <a href="https://#/personal/trading/stock_trading.do">Stock Trading Sign On</a>



                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">Cheque Services</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/cheque/cheque_status_inquiry.do">Cheque Details</a></li>





























                                <li><a href="https://#/personal/cheque/stop_cheque.do">Stop Cheque</a></li>





























                                <li><a href="https://#/personal/cheque/cheque_book_request.do">Cheque Book Request</a></li>



                            </ul>
                        </div>
                    </li>		
                    <li>


























                        <a href="https://#/personal/sr/statement_request.do">e-Statement</a>



                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">Rates &amp; Charges</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/rate_charges/foreign_exchange_rate.do">Foreign Exchange Rates</a></li>



                                <li><a target="_blank" href="https://#/personal/rate_charges/interest_rate.do">Interest Rates</a></li>
                                <li><a target="_blank" href="https://#/personal/rate_charges/rate_of_return.do">Rate of Return</a></li>
                                <li><a target="_blank" href="https://#/personal/rate_charges/fees_and_charges.do">Fees and Charges</a></li>


























                                <li><a href="https://#/personal/rate_charges/e_rate_secured_view.do">e-Rates (applicable to XChange accounts)</a></li>



                            </ul>
                        </div>
                    </li>	
                </ul>

                <hr class="navi">

                <ul class="menu">
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">My Mailbox</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/secured_msg/compose_message.do">Compose</a></li>





























                                <li><a href="https://#/personal/secured_msg/inbox_messages.do">Inbox</a></li>





























                                <li><a href="https://#/personal/secured_msg/sent_messages.do">Outbox</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <!--<a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">My Profile</a>-->
                    <li><a class="expand" href="./userPortal.jsp">My Profile</a></li>
                    <div class="div_submenu">



                        <ul class="submenu">


























                            <li><a href="https://#/personal/profile/change_password.do">Change Password</a></li>





























                            <li><a href="https://#/personal/profile/change_secure_phrase.do">Change Secure Phrase</a></li>





























                            <li><a href="https://#/personal/profile/update_user_mail.do">Update Mailing Address</a></li>





























                            <li><a href="https://#/personal/profile/update_user_e_mail.do">Update Email Address</a></li>





























                            <li><a href="https://#/personal/profile/update_quick_link.do">Customize Quick Links</a></li>





























                            <li><a href="https://#/personal/profile/update_trx_limit.do">Limit Maintenance</a></li>






























                            <li><a href="https://#/personal/profile/update_linked_acc.do">Link/ Unlink Account</a></li>



                        </ul>
                    </div>
                    </li>
                </ul>
                <!--/Left Navi-->


            </div>

            <div id="main">
                <h1>Pay Others in Other Bank (IBG) - Confirmation</h1>



                <div class="error">


                </div>


                <p class="instruction">
                    Please check the details below before you confirm the transfer.
                </p>

                <%
                    AxiomWrapper axWraper = new AxiomWrapper();
                    String sessionid = (String) session.getAttribute("_userSessinId");
                    RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssUserCerdentials");
                    String fromAccountNoConfirm = (String) session.getAttribute("fromAccountNo");
                    String beneficiaryBankConfirm = (String) session.getAttribute("beneficiaryBank");
                    String beneficiaryNameConfirm = (String) session.getAttribute("beneficiaryName");
                    String otherAccountConfirm = (String) session.getAttribute("otherAccount");
                    String emailConfirm = (String) session.getAttribute("email");
                    String amountConfirm = (String) session.getAttribute("amount");
                    String paymentTypeConfirm = (String) session.getAttribute("paymentType");
                    String remarksConfirm = (String) session.getAttribute("remarks");
                    String paymentDescriptionConfirm = (String) session.getAttribute("paymentDescription");
                    String firstProcessDateConfirm = (String) session.getAttribute("firstProcessDate");

                    String[] data1 = new String[3];
                    data1[0] = fromAccountNoConfirm;
                    data1[1] = otherAccountConfirm;
                    data1[2] = amountConfirm;

                    List<String> dataString = new ArrayList<String>(Arrays.asList(data1));
                    String hashCodeForSigning = axWraper.generateSignatureCodeRSS(sessionid, userObj.getRssUser().getUserId(), dataString, null);

                    session.setAttribute("hashCodeForSigning", hashCodeForSigning);

                    //added
                    byte[] bytePayload = Base64.encode("SamplePayload".getBytes());
                    String strPayLoad = new String(bytePayload);
                    // RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, username, AxiomWrapper.GET_USER_BY_USERID, strPayLoad);

                    Random randomGenerator = new Random();
                    int randomInt = randomGenerator.nextInt(10000000);
                    byte[] authId = Base64.encode(("" + randomInt).getBytes());
                    String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
                    String strauthId = new String(authId);
                    request.getSession().setAttribute("txid", strauthId);
                    request.getSession().setAttribute("strPayLoad", strPayLoad);
                    request.getSession().setAttribute("_userID", _userID);
                    request.getServletContext().setAttribute("txid", strauthId);
                    request.getServletContext().setAttribute("_userID", _userID);
                    InitTransactionPackage package1 = new InitTransactionPackage();
                    package1.setAuthid(strauthId);
                    package1.setChannelid(channelid);
                    package1.setExpirytimeInMins(3);
                    package1.setFriendlyMsg("Login");
                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                    SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm:ss a");
//            package1.setMessage("you are trying to login to system from IP: " + request.getRemoteAddr() + " with Location: " + location + " browser Name: " + browser + " ,OS: " + baseOs + " At Time: " + new Date());
                    JSONObject json1 = new JSONObject();
                    json1.put("_servermsg", "Login for Trasaction from the Server : " + request.getRemoteAddr());
                    json1.put("_ip", request.getRemoteAddr());
                    json1.put("_location", "NA");
                    json1.put("_browser", "NA");
                    json1.put("_os", "NA");
                    String currentDate = dateformat.format(new Date());
                    String currentTime = timeformat.format(new Date());
                    json1.put("_time", "" + currentTime);
                    json1.put("_date", "" + currentDate);

                    package1.setMessage(json1.toString());
                    package1.setSessionid(sessionid);
                    package1.setType(1);
                    // package1.setUserid(userObj.getRssUser().getUserId());
                    package1.setUserid(_userID);
                    AxiomStatus status = axWraper.initTransaction(package1, strPayLoad);
                    if ((status != null)) {
                        System.out.println("Intitiolization Transaction Status Regcode message" + status.getRegCodeMessage());
                    }   %>   //end
                <script>
                    var time = 0;
                    var cheackPushResponce = $.jStorage.get("pushStatus");
                    console.log("cheackPushResponce>>" cheackPushResponce);
                    if (cheackPushResponce !== null) {
                        while (cheackPushResponce !== 1 && time < 180) {
                            Thread.sleep(5000);
                            time += 5;
                        }
                        alert("Thanks For submitting Responce.");
                    }

                </script>

                <%

//                    if (cheackPushResponce != -1 || cheackPushResponce != null) {
//                    if (strauthId != null) {
//                        TransactionStatus Tstatus = axWraper.getStatus(sessionid, strauthId, strPayLoad);
//                        int time = 0;
//                        if (Tstatus != null) {
//                            while (Tstatus.getTransactionStatus().equalsIgnoreCase("pending") && time < 180) {
//                                Thread.sleep(5000);
//                                time += 5;
//                                Tstatus = axWraper.getStatus(sessionid, strauthId, strPayLoad);
//                            }
//
//                            if (Tstatus.equals("3") == true) {
//                %> 
<!--<script> window.alert("Thanks For Submitting Responce..!!!");</script>-->
<%
//                            }
//                        }
//                    }
                    // }
%>

                <form method="post" action="../../transactionauth" class="txnform once" name="ibgFtForm" id="ibgFtForm"><input value="Confirm" class="button_red default" type="submit" id="cfm" name="insert" tabindex="2" style="position: absolute; left: -999px; top: -999px; height: 0px; width: 0px;">
                    <input name="Txid" type="hidden" value="<%=strauthId%>">
                    <div style="display:none">
                        <input name="__token" type="hidden" value="cee0472d-68d2-4b1c-8b2f-cc2c70aaca27">
                    </div>
                    <div class="row">
                        <label for="fromAccountNo">From Account</label>
                        <div>
                            <%=fromAccountNoConfirm%>
                        </div>
                    </div>
                    <!-- *************************** Beneficiary Details ***************************** -->
                    <div class="row section">
                        <label for="beneficiaryDetails">Beneficiary Details</label>
                    </div>
                    <div class="row">
                        <label for="beneficiaryAccNo">Beneficiary Account Number</label>
                        <div>
                            <%=otherAccountConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="beneficiaryBank">Beneficiary Bank</label>
                        <div>
                            <%=beneficiaryBankConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="beneficiaryName">Beneficiary Name</label>
                        <div>
                            <%=beneficiaryNameConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="emailAddress">Email Address</label>
                        <div>
                            <%=emailConfirm%>
                        </div>
                    </div>
                    <!-- 		*************************** Beneficiary Identification Type***************************** -->
                    <!-- *************************** Payment Details ***************************** -->
                    <div class="row section">
                        <label>Transfer Details
                        </label>
                    </div>
                    <div class="row">
                        <label for="paymentType">Payment Type</label>
                        <div>
                            <%=paymentTypeConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="amount">Amount (RM)</label>
                        <div>
                            <%=amountConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="serviceCharge">Service Charge (RM)</label>
                        <div>
                            0.10
                        </div>
                    </div>
                    <div class="row">
                        <label for="recipientReference">Recipient's Reference</label>
                        <div>
                            <%=remarksConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label><label for="paymentDescription">Other Payment Details</label>
                        </label>
                        <div>
                            <%=paymentDescriptionConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="fiEffectiveDate">Effective Date</label>
                        <div>
                            <%=firstProcessDateConfirm%>
                        </div>
                    </div>
                    <div class="row tac">
                        <label for="transactionAutho"><label for="transactionAutho">Acceptance</label></label>
                        <div>
                            <span class="toggle-bg">
                                <input value="true" checked="checked" type="radio" id="repeatOptionY" name="repeatOption">                                
                                <span class="switch"></span>
                            </span>
                            I hereby confirm that I am transferring Amount of MYR <%=amountConfirm%> to Recipient identified as <%=beneficiaryNameConfirm%>, having account no <%=otherAccountConfirm%> with <%=beneficiaryBankConfirm%>. I am also authorizing this transaction by providing the Signature OTP to CA for non repudiation enforcement.
                        </div>
                    </div>
                    <input name="dataToSign" id="dataToSign" type="hidden" value="I hereby confirm that I am transferring Amount of MYR <%=amountConfirm%> to Recipient identified as <%=beneficiaryNameConfirm%>, having account no <%=otherAccountConfirm%> with <%=beneficiaryBankConfirm%>. I am also authorizing this transaction by providing the Signature OTP to CA for non repudiation enforcement.">
                    <%
                        // AxiomWrapper aw = new AxiomWrapper();
                        String[] data = new String[3];
                        data[0] = fromAccountNoConfirm;
                        data[1] = otherAccountConfirm;
                        data[2] = amountConfirm;
//                        String hashCodeForSigning = fromAccountNoConfirm + otherAccountConfirm + amountConfirm;
                        //  String authtype = (String) session.getAttribute("authtype");
                        AxiomCredentialDetails axiomCredentialDetails = null;
                        int type = (Integer) request.getSession().getAttribute("type");
                        userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
                        boolean flag = false;
                        if (type == SetAuthType.WEBTOKEN) {
                            for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                                axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                if (axiomCredentialDetails != null) {
                                    if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN
                                            && axiomCredentialDetails.getSubcategory() == AxiomWrapper.SW_WEB_TOKEN
                                            && axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                        }
                        //String hashCodeForSigning= aw.GenerateSignatureCode(sessionid,data, null);                        
%>    
                    <input name="hashCodeForSigning" id="hashCodeForSigning" type="hidden" value="<%=hashCodeForSigning%>">
                    <%String strUSerAgent = request.getHeader("User-Agent");
                        boolean bIE7 = strUSerAgent.contains("MSIE 7.0");
                        if (type == SetAuthType.SIGN_TX) {%>
                    <div class="row tac">
                        <label for="transactionAutho"><label for="transactionAutho">Authorization</label></label>
                        <div>
                            Please input <%=hashCodeForSigning%> into your security token to generate Signature (OCRA) One Time Password. 
                            OR click here to Request 
                            <!--                <select name="tokentype" id="tokentype">
                                                <option value="2">Software</option>
                                                <option value="3">Hardware </option>
                                                <option value="4">OOB</option>
                                            </select>                            -->
                            <input value="Get Signature OOB" class="button_plain" type="button" name="requestTac" id="requestTac" tabindex="4" onclick="geoSendSignatureOOB()">
                            <div id="sotpresult"></div>
                        </div>
                    </div>
                    <%
////                    HttpSession session = request.getSession(true);
//                            session.setAttribute("tokentype", tokentype );
                        }%>
                    <%
//                        int type = (Integer) request.getSession().getAttribute("type");
                        if (flag == true && type == SetAuthType.WEBTOKEN) {
                    %>
                    <div class="image" style="text-align:center">
                        <script> var urlGetWTUser2FA = ".././GetWebTokenUser.jsp";</script>
                        <img id="showimageWebToken" 
                             src="../AxisBankToken.png"/>
                        <style type="text/css">
                            .image {
                                position:relative;
                            }
                            <% if (bIE7 == true) { %>
                            .image .text {
                                position:absolute;
                                top:60px;
                                left:120px;
                            }
                            <% } else { %>
                            .image .text {
                                position:absolute;
                                top:40px;
                                left:350px;
                            }
                            <% } %>
                        </style>
                        <div class="text">
                            <h3>
                                <p id="Tz">ENTER PIN</p>
                            </h3>
                        </div>
                        <div style ="display:none">
                            X:<p id="Tx"></p>
                            Y:<p id="Ty"></p>
                            Digit:<p id="DigitSelected" ></p>
                        </div>
                    </div>

                    <!--rsa-->

                    <!--                    <div class="row tac">
                                            <label for="transactionAutho"><label for="transactionAutho">Authorization</label></label>
                                            <div>
                                                Please input <%//=hashCodeForSigning%> into your security token to generate Signature (OCRA) One Time Password. 
                                                OR click here to Request 
                                                <input value="OOB Signature OTP" class="button_plain" type="button" name="requestTac" id="requestTac" tabindex="4">
                                                <button type="button" name="requestTac" id="requestTac" tabindex="4" onclick="geoSendSignatureOOB(); return false;">OOB Signature OTP</button>
                                            </div>
                                        </div>-->
                    <!--                     Generated Signature<br/>
                                         <textarea name="siggenerated" id="siggenerated" rows="4" cols="65"></textarea>-->
                    <div class="row tac">
                        <label for="transactionAutho"><label for="transactionAutho">Generated Signature</label></label>
                        <div>		
                            <textarea name="datasign" id="datasign" rows="4" cols="65"></textarea>
                            <textarea name="siggenerated" id="siggenerated" rows="4" cols="65"></textarea>
                        </div>
                    </div>
                    <%}%>
                    <!--                    <div class="row tac">
                                            <label for="transactionAutho"><label for="transactionAutho">Transaction Authorization Code</label></label>
                                            <div>		
                                                <input size="8" maxlength="8" type="password" name="sotp" id="sotp">                                        
                                            </div>
                                        </div>-->
                    <div class="row tac" style="display:none;" id="HideForwebPush">
                        <label for="transactionAutho"><label for="transactionAutho">Transaction Authorization Code</label></label>
                        <div>		
                            <input size="8" maxlength="8" type="password" name="sotp" id="sotp">
                        </div>
                    </div>

                    <!--                    ADDED mAYURI-->
                    <!-- <td>Enter regCode: </td><td><input type="text" id="regcode"  name="regcode" value="">-->
                    <div id="forwebpushdiv" class="row tac" style="display:none;">
                        <!--<table>-->
                        <!--    <td>User Id: </td>-->
                        <td><input type="hidden" id="userid"  name="userid" value=<%=_userID%>></td><tr>
                        <input type="hidden"  name="license" value="eyJfc2lnbmF0dXJlIjoiWkU0ZUJnK2RJa3VHU3lrdHd3dEF5Ly9RVWpoWnJaQ24xaDYyWlBoWWN6T3JLK2daUkxkZEZzeG04bFBRY2c4M2ZTdTY4OEVKSmRkOHNiM0U4TExQQ3ZjWFZORkJpeTkwNURaZHRpNnRRdjZidUhPbzBlVWQ1d0NIb2FYdmJFM2dsWGZDcHkwSGNONHhMR3lmMDZrdzZKVWZ0eS9lallmWDU0VHEveTZPZmVFc2ZHMVZLS2gwRXlSZDhhK0dHZDR6cjNVT2svMFlHYjJxWUNYQXlocDJTVjgrUklrUGFwTnhSYmtnbTNZOTdNU05zeDBWOHJYamh4UXI1YUlmbkpiTUhTYUh2dUMyVEpldWdTaFR6L1B0Y2ZPcVRWV3JhRlFOcGNoeURoMGsvNmEwcEUyQWk3ZHk0MnRnKzM0dit6NkM2NmlLTXBSbmR3bmtmOE4yaktQT1V3PT0iLCJfZGF0YSI6ImV5SnBjRlZ5YkNJNkltaDBkSEE2THk4eE9ESXVOekF1TVRFMUxqRTNOam80TURnd0wwMXZZbWxzWlZSeWRYTjBRWGhwYjIwdmFYQm1hVzVrWlhJaUxDSnNWSGx3WlNJNk1Td2liRWx6YzNWbElqb3hMalF6T1RFNU1UWTROREUyTkVVeE1pd2ljMVpwYzJsaWJHVkxaWGtpT2lKTlNVZG1UVUV3UjBOVGNVZFRTV0l6UkZGRlFrRlJWVUZCTkVkT1FVUkRRbWxSUzBKblVVTXZSRGhOT1ZoVE5WSmhjekI0T0hNM2FURndVR0Z1V1N0Vk16a3hUVk0yYWxsVVRsUXJZek5wVnpka2JYWldaR2h2WmtodU5FTk5TMFZXYVVoNWFHazRUV2s1ZUZKM2VXdFJNVzFJTmxKelZtZEVURTV2YVU1QlRETk1hMFZ4VFRKMmRHSmpTRXhFYjNwMGJsZFFOM2hLU0hNclR6aHdWMFJETm10QlJGVlJiRzVzVFhwdVoycHhiREkxVUVaUVp6TnJiV2hPUm5CSFJISkpXRUpDZHpsTVVFdFBZMU5UYzJKVVNIZEpSRUZSUVVJaUxDSm5aVzlWY213aU9pSm9kSFJ3T2k4dk1UZ3lMamN3TGpFeE5TNHhOelk2T0RBNE1DOU5iMkpwYkdWVWNuVnpkRUY0YVc5dEwyZGxkR2RsYnlJc0lteEZlSEJwY25raU9qRXVOVGt6TkRBMk5qVTFNVEkyUlRFeUxDSndWbVZ5YzJsdmJpSTZJak11TUM0eElpd2lhMlY1VEdWdVozUm9Jam95TURRNGZRPT0ifQ==">

                        <label for="webtrustpass"><label for="webtrustpass">Password For Web Trust</label></label>
                        <table><td>
                            </td>
                            <!--                        <div>--><td>
                                <input type="password" id="pinO" name="pinO"></td>
                            <!--                        </div>-->
                            <!--</table>-->
                            <!--                    </div>-->
                            <!--                    <div class="row tac">-->
                            <!--                   <label for="webt">-->
                            <td>
                                <input type="button" style="display:none;" id="btnGenerateOtp" name="btnGenerateOtp" value="Generate OTP" onclick="generateOTPWT()"/>
                            </td>
                            <!--                        </label>-->
                            <!--                        <div>-->
                            <td>
                                <input type="text" name="txtotp" id="txtotp" style="display:none" ></td>
                        </table>
                    </div>
                </form><!--
                --> 
            </div>
            <script>








                var userregcode = "";
                // alert(userregcode);
                var userpin = "";
                var useruserid = "";
                var webTrustLicenseKey = "eyJfZGF0YSI6ImV5SnNSWGh3YVhKNUlqb3hMalE1T0RFeE16TTNOell4T0VVeE1pd2lhMlY1VEdWdVozUm9Jam95TURRNExDSnBjRlZ5YkNJNkltaDBkSEJ6T2k4dllYaHBiMjF3Y205MFpXTjBMbTF2Ykd4aGRHVmphQzVqYjIwNk9EUTBNeTlOYjJKcGJHVlVjblZ6ZEVGNGFXOXRMMmx3Wm1sdVpHVnlJaXdpWjJWdlZYSnNJam9pYUhSMGNITTZMeTloZUdsdmJYQnliM1JsWTNRdWJXOXNiR0YwWldOb0xtTnZiVG80TkRRekwwMXZZbWxzWlZSeWRYTjBRWGhwYjIwdloyVjBaMlZ2SWl3aWNGWmxjbk5wYjI0aU9pSXlMamd1TVNJc0lteEpjM04xWlNJNk1TNDBOemc0TkRjMk9ERXdORGxGTVRJc0luTldhWE5wWW14bFMyVjVJam9pVFVsSlFrbHFRVTVDWjJ0eGFHdHBSemwzTUVKQlVVVkdRVUZQUTBGUk9FRk5TVWxDUTJkTFEwRlJSVUZuVUZCRldscEdZaTlHU1RSRllUaHhaemd3UjJjMlkzcERUM3BHV21kMFEwbHZOa3RFYnprMGFtTlVjbmxrZVhKM1pXbDVkVVJLZUhrMmJteERjVWt6S3pkaFVqVkNNbEZLYTFOSVNDdFZabVY1VlRWMFNUWk1Va2hOZVdacVVsUmFRa3d4UW5neFdYUkVXVmxwV0cwd1NsQkxTblJtSzBweVQwMVJXa1JDVlhFMWR5dHhNV3N6VDJzMU9HdFBWbWhKWVdGNFkzRk5jRXBNYkdWR2NHbGxhRXRLWjBvNVl6UkVhR0ZNY2tadmJqSjRTMkpOTjNJMlpFUTJUak5qYWpBMmQycFdPV2xNYUZCcVdFbFNRekJ1ZEVaQlFrZFhObVZvTkZaSWRsbEpabTVoVEU1cGVYbHpjVFZJUzIxa01YQm5kakJ0YUU5RmNtTk5UMEp6ZWxBemJrdHJTa1I0WVVWdlJFcEJia053Y1VKMVpYVTNTMnN2Y1Zobk5WUXZWV3RzY2pCQksyMXVSMlpFZVdKRVdUWm9ZM0ZRYjFseFFuSlNaRU4zU1dKYVNFaDRRM0ZDZWxCUWQwRmxRME5CTjNsb0x6SXpaM2RKUkVGUlFVSWlMQ0p6Vm1semFXSnNaVXRsZVdadmNrVnVZM0o1Y0hScGIyNGlPaUpOU1VkbVRVRXdSME5UY1VkVFNXSXpSRkZGUWtGUlZVRkJORWRPUVVSRFFtbFJTMEpuVVVNdlJEaE5PVmhUTlZKaGN6QjRPSE0zYVRGd1VHRnVXU3RWTXpreFRWTTJhbGxVVGxRcll6TnBWemRrYlhaV1pHaHZaa2h1TkVOTlMwVldhVWg1YUdrNFRXazVlRkozZVd0Uk1XMUlObEp6Vm1kRVRFNXZhVTVCVEROTWEwVnhUVEoyZEdKalNFeEViM3AwYmxkUU4zaEtTSE1yVHpod1YwUkRObXRCUkZWUmJHNXNUWHB1WjJweGJESTFVRVpRWnpOcmJXaE9SbkJIUkhKSldFSkNkemxNVUV0UFkxTlRjMkpVU0hkSlJFRlJRVUlpTENKc1ZIbHdaU0k2TVgwPSIsIl9zaWduYXR1cmUiOiIwNjE5YmNiYzJiYjZkMmIxNWEwZmZhNDBkYjc2ODE0MmExYTkxNTljNDViMDAzMzRiZWMzZTQ0NTc2ZWExOGQyODliODE4MTJkZDkzZjlmYTAxZjQ2YjQ0ZmQ1MTc4M2M1ZGNkNWFlNzM4NWUzYjY3YTAzOGVkY2FhYzcyM2QzYTUwNDJjMzc1ZDMwNmZhYzkxYTcwODhkYmJjNmQ5NGY1MTE4OTAyODc3ZDM3ODYzY2FiZWY4MWU2ZTM1YzA1ZDViOGJmOWQ4NWRmYjIyNmMyMmY1OGRlYTM3ZDVlMDQxM2Y0YzVlYTIwY2FjMGRhODVjNjkzZThhNGMxNjJmODMxZDhlOWNhYWIwYmI2ZjM0NTExYjRlMDcxZDk1NTczZjMzMDNmY2UwNjI5MzQyNjY3MGRhMmFhOTVlOTBjMDJhYmIyNzRmNDk5MDFhMjI5NGM2MTg0YTE3ZjY3Nzc5NTliYTllOGI5YWJjYTVkZGI5ZjQwYTA4YjJhYWYxNmIwYjdhZTY2NWMyMTE3NjNhYTcwODQ0YjBjMGM3ZDkzZjEzNjU1MmQxZDhlMjY4ZmU4MWI4ZmYzMDE1OWRmMzVjY2Q3NGI3ODljMTljZDg4Y2Y3Zjc3NTFkMjJlNzI0YmE1NTQzMGVhMzZkNDc4MjU0MDBmZGU3OGI3YWMxMjNjYTYxZSJ9";
                var liscensekey = "eyJfc2lnbmF0dXJlIjoiZEMySkVaMXIxSElOZkkyb3pGKzFyeUcyUlQ1QUZGcWdjc0w2c0FYU0lqZ1V5Vnpudkl4NEZxUWRZMnFmeGozZWc1bDZqdlJ2UURSUkgxWkhIMDYrWVRIcHZJcHFjbEJSZEk4UmZMczM0Y0JyUDdWVkhaSmJCU1JlY3lwN3RHbkVLdm5zaUczRVc2Q281bkh6QWw0eElNTHN0QzlOZHY3RFZQME1yUWh4NzNHK1E1K0R2c2F1dVJNalZ6aFJDcHJWS0FweEl3L3Jma1lMQ3Z2bkJvL3ZEU29LQ0RzaHA2SWFZVkFkK2xiK0M3cE1DeUZ6Sms3QkZDTWF4R3pFdElHM25OOU1waG1ObjRkYmVaazVYeERKbVQrOXhKY3U5dHZmbnU5ZUhtc0MzSmEyUzQ1MWk4NjY4UmpKYThDMUd4czA3SFlYeGFUVTdWRXdQcDBOVE0vd05RPT0iLCJfZGF0YSI6ImV5SnBjRlZ5YkNJNkltaDBkSEJ6T2k4dllYaHBiMjF3Y205MFpXTjBMbTF2Ykd4aGRHVmphQzVqYjIwNk9EUTBNeTlOYjJKcGJHVlVjblZ6ZEVGNGFXOXRMMmx3Wm1sdVpHVnlJaXdpYkZSNWNHVWlPakVzSW14SmMzTjFaU0k2TVM0ME5qSTROelE0TlRjNU16RkZNVElzSW5OV2FYTnBZbXhsUzJWNUlqb2lUVWxIWmsxQk1FZERVM0ZIVTBsaU0wUlJSVUpCVVZWQlFUUkhUa0ZFUTBKcFVVdENaMUZETDBRNFRUbFlVelZTWVhNd2VEaHpOMmt4Y0ZCaGJsa3JWVE01TVUxVE5tcFpWRTVVSzJNemFWYzNaRzEyVm1Sb2IyWklialJEVFV0RlZtbEllV2hwT0UxcE9YaFNkM2xyVVRGdFNEWlNjMVpuUkV4T2IybE9RVXd6VEd0RmNVMHlkblJpWTBoTVJHOTZkRzVYVURkNFNraHpLMDg0Y0ZkRVF6WnJRVVJWVVd4dWJFMTZibWRxY1d3eU5WQkdVR2N6YTIxb1RrWndSMFJ5U1ZoQ1FuYzVURkJMVDJOVFUzTmlWRWgzU1VSQlVVRkNJaXdpWjJWdlZYSnNJam9pYUhSMGNITTZMeTloZUdsdmJYQnliM1JsWTNRdWJXOXNiR0YwWldOb0xtTnZiVG80TkRRekwwMXZZbWxzWlZSeWRYTjBRWGhwYjIwdloyVjBaMlZ2SWl3aWJFVjRjR2x5ZVNJNk1TNDBPVFF6TXpFM01UWTVPRGhGTVRJc0luQldaWEp6YVc5dUlqb2lNaTQ0TGpFaUxDSnJaWGxNWlc1bmRHZ2lPakl3TkRoOSJ9";
                function generateOTPWT()
                {
                    var webTrustLicenseKey1 = "eyJfZGF0YSI6ImV5SnNSWGh3YVhKNUlqb3hMalE1T0RFeE16TTNOell4T0VVeE1pd2lhMlY1VEdWdVozUm9Jam95TURRNExDSnBjRlZ5YkNJNkltaDBkSEJ6T2k4dllYaHBiMjF3Y205MFpXTjBMbTF2Ykd4aGRHVmphQzVqYjIwNk9EUTBNeTlOYjJKcGJHVlVjblZ6ZEVGNGFXOXRMMmx3Wm1sdVpHVnlJaXdpWjJWdlZYSnNJam9pYUhSMGNITTZMeTloZUdsdmJYQnliM1JsWTNRdWJXOXNiR0YwWldOb0xtTnZiVG80TkRRekwwMXZZbWxzWlZSeWRYTjBRWGhwYjIwdloyVjBaMlZ2SWl3aWNGWmxjbk5wYjI0aU9pSXlMamd1TVNJc0lteEpjM04xWlNJNk1TNDBOemc0TkRjMk9ERXdORGxGTVRJc0luTldhWE5wWW14bFMyVjVJam9pVFVsSlFrbHFRVTVDWjJ0eGFHdHBSemwzTUVKQlVVVkdRVUZQUTBGUk9FRk5TVWxDUTJkTFEwRlJSVUZuVUZCRldscEdZaTlHU1RSRllUaHhaemd3UjJjMlkzcERUM3BHV21kMFEwbHZOa3RFYnprMGFtTlVjbmxrZVhKM1pXbDVkVVJLZUhrMmJteERjVWt6S3pkaFVqVkNNbEZLYTFOSVNDdFZabVY1VlRWMFNUWk1Va2hOZVdacVVsUmFRa3d4UW5neFdYUkVXVmxwV0cwd1NsQkxTblJtSzBweVQwMVJXa1JDVlhFMWR5dHhNV3N6VDJzMU9HdFBWbWhKWVdGNFkzRk5jRXBNYkdWR2NHbGxhRXRLWjBvNVl6UkVhR0ZNY2tadmJqSjRTMkpOTjNJMlpFUTJUak5qYWpBMmQycFdPV2xNYUZCcVdFbFNRekJ1ZEVaQlFrZFhObVZvTkZaSWRsbEpabTVoVEU1cGVYbHpjVFZJUzIxa01YQm5kakJ0YUU5RmNtTk5UMEp6ZWxBemJrdHJTa1I0WVVWdlJFcEJia053Y1VKMVpYVTNTMnN2Y1Zobk5WUXZWV3RzY2pCQksyMXVSMlpFZVdKRVdUWm9ZM0ZRYjFseFFuSlNaRU4zU1dKYVNFaDRRM0ZDZWxCUWQwRmxRME5CTjNsb0x6SXpaM2RKUkVGUlFVSWlMQ0p6Vm1semFXSnNaVXRsZVdadmNrVnVZM0o1Y0hScGIyNGlPaUpOU1VkbVRVRXdSME5UY1VkVFNXSXpSRkZGUWtGUlZVRkJORWRPUVVSRFFtbFJTMEpuVVVNdlJEaE5PVmhUTlZKaGN6QjRPSE0zYVRGd1VHRnVXU3RWTXpreFRWTTJhbGxVVGxRcll6TnBWemRrYlhaV1pHaHZaa2h1TkVOTlMwVldhVWg1YUdrNFRXazVlRkozZVd0Uk1XMUlObEp6Vm1kRVRFNXZhVTVCVEROTWEwVnhUVEoyZEdKalNFeEViM3AwYmxkUU4zaEtTSE1yVHpod1YwUkRObXRCUkZWUmJHNXNUWHB1WjJweGJESTFVRVpRWnpOcmJXaE9SbkJIUkhKSldFSkNkemxNVUV0UFkxTlRjMkpVU0hkSlJFRlJRVUlpTENKc1ZIbHdaU0k2TVgwPSIsIl9zaWduYXR1cmUiOiIwNjE5YmNiYzJiYjZkMmIxNWEwZmZhNDBkYjc2ODE0MmExYTkxNTljNDViMDAzMzRiZWMzZTQ0NTc2ZWExOGQyODliODE4MTJkZDkzZjlmYTAxZjQ2YjQ0ZmQ1MTc4M2M1ZGNkNWFlNzM4NWUzYjY3YTAzOGVkY2FhYzcyM2QzYTUwNDJjMzc1ZDMwNmZhYzkxYTcwODhkYmJjNmQ5NGY1MTE4OTAyODc3ZDM3ODYzY2FiZWY4MWU2ZTM1YzA1ZDViOGJmOWQ4NWRmYjIyNmMyMmY1OGRlYTM3ZDVlMDQxM2Y0YzVlYTIwY2FjMGRhODVjNjkzZThhNGMxNjJmODMxZDhlOWNhYWIwYmI2ZjM0NTExYjRlMDcxZDk1NTczZjMzMDNmY2UwNjI5MzQyNjY3MGRhMmFhOTVlOTBjMDJhYmIyNzRmNDk5MDFhMjI5NGM2MTg0YTE3ZjY3Nzc5NTliYTllOGI5YWJjYTVkZGI5ZjQwYTA4YjJhYWYxNmIwYjdhZTY2NWMyMTE3NjNhYTcwODQ0YjBjMGM3ZDkzZjEzNjU1MmQxZDhlMjY4ZmU4MWI4ZmYzMDE1OWRmMzVjY2Q3NGI3ODljMTljZDg4Y2Y3Zjc3NTFkMjJlNzI0YmE1NTQzMGVhMzZkNDc4MjU0MDBmZGU3OGI3YWMxMjNjYTYxZSJ9";
                    var liscensekey = "eyJfc2lnbmF0dXJlIjoiZEMySkVaMXIxSElOZkkyb3pGKzFyeUcyUlQ1QUZGcWdjc0w2c0FYU0lqZ1V5Vnpudkl4NEZxUWRZMnFmeGozZWc1bDZqdlJ2UURSUkgxWkhIMDYrWVRIcHZJcHFjbEJSZEk4UmZMczM0Y0JyUDdWVkhaSmJCU1JlY3lwN3RHbkVLdm5zaUczRVc2Q281bkh6QWw0eElNTHN0QzlOZHY3RFZQME1yUWh4NzNHK1E1K0R2c2F1dVJNalZ6aFJDcHJWS0FweEl3L3Jma1lMQ3Z2bkJvL3ZEU29LQ0RzaHA2SWFZVkFkK2xiK0M3cE1DeUZ6Sms3QkZDTWF4R3pFdElHM25OOU1waG1ObjRkYmVaazVYeERKbVQrOXhKY3U5dHZmbnU5ZUhtc0MzSmEyUzQ1MWk4NjY4UmpKYThDMUd4czA3SFlYeGFUVTdWRXdQcDBOVE0vd05RPT0iLCJfZGF0YSI6ImV5SnBjRlZ5YkNJNkltaDBkSEJ6T2k4dllYaHBiMjF3Y205MFpXTjBMbTF2Ykd4aGRHVmphQzVqYjIwNk9EUTBNeTlOYjJKcGJHVlVjblZ6ZEVGNGFXOXRMMmx3Wm1sdVpHVnlJaXdpYkZSNWNHVWlPakVzSW14SmMzTjFaU0k2TVM0ME5qSTROelE0TlRjNU16RkZNVElzSW5OV2FYTnBZbXhsUzJWNUlqb2lUVWxIWmsxQk1FZERVM0ZIVTBsaU0wUlJSVUpCVVZWQlFUUkhUa0ZFUTBKcFVVdENaMUZETDBRNFRUbFlVelZTWVhNd2VEaHpOMmt4Y0ZCaGJsa3JWVE01TVUxVE5tcFpWRTVVSzJNemFWYzNaRzEyVm1Sb2IyWklialJEVFV0RlZtbEllV2hwT0UxcE9YaFNkM2xyVVRGdFNEWlNjMVpuUkV4T2IybE9RVXd6VEd0RmNVMHlkblJpWTBoTVJHOTZkRzVYVURkNFNraHpLMDg0Y0ZkRVF6WnJRVVJWVVd4dWJFMTZibWRxY1d3eU5WQkdVR2N6YTIxb1RrWndSMFJ5U1ZoQ1FuYzVURkJMVDJOVFUzTmlWRWgzU1VSQlVVRkNJaXdpWjJWdlZYSnNJam9pYUhSMGNITTZMeTloZUdsdmJYQnliM1JsWTNRdWJXOXNiR0YwWldOb0xtTnZiVG80TkRRekwwMXZZbWxzWlZSeWRYTjBRWGhwYjIwdloyVjBaMlZ2SWl3aWJFVjRjR2x5ZVNJNk1TNDBPVFF6TXpFM01UWTVPRGhGTVRJc0luQldaWEp6YVc5dUlqb2lNaTQ0TGpFaUxDSnJaWGxNWlc1bmRHZ2lPakl3TkRoOSJ9";
                    //            
                    //  userregcode1 = document.getElementById("regcode").value;
                    userpin1 = document.getElementById("pinO").value; // liscensekey= 
                    useruserid1 = document.getElementById("userid").value;
                    loadInner(webTrustLicenseKey1, useruserid1, userpin1);
                    var otp = generateOtp1();
                    document.getElementById("txtotp").value = otp;
                }
                //             function goInitialization()
                //             {
                //            var confirmData = "";
                //            userpin = document.getElementById("pinO").value;               // liscensekey= 
                //             useruserid = document.getElementById("userid").value;
                //            alert("calling confirm method");
                //       var  tonfirm='{"_visibleKey":"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/D8M9XS5Ras0x8s7i1pPanY+U391MS6jYTNT+c3iW7dmvVdhofHn4CMKEViHyhi8Mi9xRwykQ1mH6RsVgDLNoiNAL3LkEqM2vtbcHLDoztnWP7xJHs+O8pWDC6kADUQlnlMzngjql25PFPg3kmhNFpGDrIXBBw9LPKOcSSsbTHwIDAQAB","_channelid":"DxJUGvzOF1+zS7BaeclbdudlSIs=","_signature":"4091f4269b9dccb380e186147e4401a745b2ea13aa2b846a7eea9e9fdb1d1e9430daa0a60f9006afbc8921e21b5236c1cac87962d14c7e66dbd10c00786518ce42c5a4b6d922f9e1865ca0bdee44b845c5c8fbb1324956c251c21099123bab9af65cce6857f8943d912e1e8459240032e680d6577282af7a443ed06e0c3c81a4","_key":"fyXTgJBU0SRt1nQ1o/sJdc6KTwLwlhgQ8e/bA0MtjXek3YpyPQ9u23clkxrEf5al6cNdPktIolrg8UP4vsfLtoK/7HjdEqi4zDBvuItIRTr+d+7fT0EZcHgLDUTIYDTesYekugwjgivBRH1xr/hfBLoJdcIiffZj6oMIe+nfzLbTQO9fO9RFqU8tWMo2PSxJyS2b3TFg3S+KQQqtbbvKNJIi29I99rlAGxRi/BAWfttZOAgPg6h6t+Vnz9789eyels721TotEmeiezslQ4eFfpxYZBJrl01x4ZnCVIeCswkc7JKL6AAFrPnA39VW7raze7+nBendo0lcNdoWB2PSTA==","_data":"VvOWhzbLORakFrgnAv4kP+qkq8EkFRg+lNWAmT7g1e0di5mPbGy+CpCfpAKKZhDPUZ2ceRUa1Ww4YB0fJ5Y5zouSwNaSx3GvgS5z6csapx4IYUVBKbejF64cHY2FEd3uUK3xol7PqdvolI3TpyR7ZV2HrejiOdXoSfGMvVepAEvOEKdIewylI7Gdh327Z4V4t1WfBP/7Fc2kJmM/Az8cyoO65+l50xkQHwy1KtQcxbPI8VPNyIZLd5DnLDmoYS7EvknW0vwNQhXj3q1+u1NPCS4Qk5MSKyQ0OW2a5Ke6xtUH1v1khEW8I1MR3vwj8vSoCFJTQsmEsGxjyaFSEUHiNs7lT7sgMz3QdoDSMp/aGqPS02rQYNyJp89TGHh/8Re5+ZnJqdOp6nfbQPH+BI/gCjhcSifRDPrWilLaDpac1C+vfwZSGHREmTylYJN40TdY6lzKDdgUF1v0/PpcpkjH12XOerKSRHn1Za1eScVFWuQL0qI64XOIgbqlDkeSOV9mTlqmmwrWHmbjqcMSONcni/YvTvw6LeJat4KoTWuas2S5asmHWWAH+lnEesn0eb/5NAdBM1BdtyEA8bYZEjEcw+Ft0hybcJSBCVO/QZESt/xY0UKX56VxYxRKOIis+RR42mdA7cbC+ViGQZ7DYhvB/zW5N67+h8p6e7BSe44u1QTR0qJ5upRnGgI6rCC25KOdDXexO8q96I56/6r4d+87xZnPatFs1eGjdVNSmiWJWQF9F/HPMubhPAjj+E1VtSw6h3Y1FYjU1v37/5n4IQwijpvmFwzhOoUEIVn8AQ0pcytGVVEibPOsbYyV902XfkpW/aPftZadefrvOqQN/BKUIXSdMy53u2ZzQVVjRuIDcGVY3P8gDxhrWVhdivMSngdrqMZrfT41+KqiotJj5Ha1ld/Fgkkn824cTRYM7jphgedCR+XcPNObXHDw0HOL1G8hnHn0lryy6q94DYlm+gCDBWj8vIpaGCOMOsJlarrd4Hsj5xaif9ZHAg7FFUpCrdMOyyuX0diM9LE+eipI5Uh7ZfAAWw2Sr351Ec9BFDBIQXCGe5I1c3f8FkzH2+ZA92dWUVKijCuuD4Znqq6CTS5s+YA+xhydUbbc+dn+GjPPLCSzOmIC7S3SUNjP21aE+7CHCb6wTAciaSrges0RJkJGkUmhIwmOqZl0qkxN/zpON9TwAxPhda14YtkBAp+bD4O5WXFcFrTdyu3gER+dBxC8/PvF/sS1nHW2mPNXZUtsp7nDsNAn0lTv8Jx6EmyCfN5bayPuE5RRRIxdhQbC+7DiKmQPQU0bA7/H8sCNRZa7S6d5DtfPh89xnorCzKitaT6/hVKKnWcjVDEF511aEPOig6m3N0t431AT9bLsq91oTpqVNw8mW/Xol2CVaRnS8Q166XcVWz9lrK2xp3x7ZbhrFes/YkNYUIZu3BQylHItfVjenVFU3hTIByZK6eqBeQ8a5WSRvktIAiJf68/WqdbiXxja/1fnT1F+xIV70zmhNqhILgyVYeZ/EKwyb4YqEMy+DCIHtulBkJFOrNM/MLLejfxPYuHUL2agT2oPD9kFZjhrs66QWtwRLsHpTQ52Hw3hcjfMdgk62WWx6wvp11OoLf8f1V7hMpgWcKV9ZMggz7OfEVyOcKQ7CxKKQrSnWmHAcbC5dAUzW4wQj4iblUcE9jmXTZGLiV+Cn6lav6A1VITisSgYGY+u2gG+Zi4FKjK/H5eXFZIUffQXivu8812UgXFZNlBbQJGJlFHV3qgVaq/xdUUkx0NkNTztWyprq2Q9iNglxDHlE3JnQiYp7O0Ul7lMftu68/SPF0wucT5DFWn3B9V94uRnpVRjqNe7erTfEQoAQ2fhqQgkjI48Mg1K1TzEXd8YhdX7Kb994MzbHnGJddtDp6A16WNGPi/nXkyiuuEr08eD/v/rtRllw3nNSW3xvgVSC0iKdizTg1KqRnKbvyU580qftI+XfihIh0lmdqHWZ1KTNWwfwckZNK3B0SflrWioCOxxroRLCYp1YTmWW07k0B4tbrVbyBU8CGnSbghJikUQAoaLvJBK82VNAgz2jwFrsaLKfubEYaBeoreElm2gvhXJLgjRODwCPsvKk0SnYL8vuBbvsSe+b3BVY1s+xuu3uWpsBFlYvKkSMtna+gI8sFO1gOinBjROqQhIbyukcWBucDgZ/tF8ugXKGdTE+w7H8rIsKODleHNvcqHJL5qFv4HIX1tya48cW4+tQhGraFIH8RCEMT8nLItgytG+PQLCxYHPD4C5jA/mTyzIto42WsViDT2JEmBHjalU+GkPWjG7bdvAKIc9WdJzz8wIKg1WoWTS57vHl/utdHlIOc+u5BItoaGPei3tgOu0J0TxBIG0QYzmWFbOmV5kHh9p9Ma3wdEzyGYz76KqS6AaUsC/muKbZgjahIzdWX68plHZ2b5E05FKuqMouU1Jd3MUud2f7YQFWOeinrIVORT8v/dMzrVvKlK9vsTnY3nGlW4hmdzL2pCR8TY5sAHRGxDTdcw6imp4QDFgNvDL4zH+lSBhUCBzp8htgDl563Q2E5MxxkgY2jANS5KCfgiHKDUD6CS8ZmyU9LrODcVHKWibHlAzFZeBa5GRXZ8jZmXC/upEV8uCTQtrEreiYjkVGebBtPpyRF117Lb9dYPUezY3leIZWkWgT4+9FEs2Bo61Bjs0Ip9eQWxXDXybavGbf74pqH9ooxhv4LtntBGr72oN3TBgx7ESBEKq5avgvidnjdy9RdeLjt6fTq8Ij1T1mEaBfDkZuwxmpLcUPotXSUH0G6vr8gQmnIDiJwdEvQZJtpDqjAfzHkQMjj5FX/OLbAFBeTQrMcy0jtN7pQ/2XRGpm/R4sbHgn/pIilaUJVotO4mxJxg8sPgqyhPOAvTlDhqlq57PFhpapVL5tSzaiWgnPeMfxxCifkAGQOpTfL7jMdAx+atd/Uv2ctwr7MZzrGKOVVC3GQAKWoIecHL8FDmKbmWaoivsKS97qHZr0hY3NE1fOK8UGDF5BaRi1eGWt4scsrYqaryzIfdsoSo/UYf4/WP3ONFeognm20fT620E3Lbt6K8Itho9kRaLedDei8K839/tOHjd4DqgsC4Q5l/KqYIWVoElcXC7EqnigsHn8jGDjUlV202+P1ASc1nO6N9ZUElFJPeyObDtE02EAcbv1P4URQ8vMrDdS4xofnXscfezzcIuS2o7iitn3paItbFGROAvor+0ozpMgbLl0ZH63QXDYkmW7FdLemaKVF1n02noGbczvWE7af33CVjYRfuS6JEVdxjNFXS5Q8q6xcWqvSXhzdM70iXznNQ9yYPUhKJHTRT37PSL9PvnKVivrYr+qyXSTqG0eVj3cqARyUgLQj9hkEcGoNYMvJ3saRlEHBv+aG4eDB76QJsqsRzcnTG5pIbIxlyII8Fc0Cp7aimXRudyyxxhNm3TLnWtKFwHFh61eDYSIm4nfGVAfIehdmHNTBXaHwlIEifFQx51j9y4h4qjgZt3Fw4R8efteROMnIWHjuaS730/j6BAbibO3k8YTiNdgWaGroLF/duNrMyUTXyxD+VUySoULwAob5H7SJqyRrCJ4CNTTZw8SmA2N2gzHzPwkltSX/peDuEvIT/xDR6CPooC6y4tgZ/7tVs7K+pSV0rySoGtr2NXVLw8IbRgbGmB2N0eEe+rXh4/s5zKEk9FrSTLQUl3Iadtjgl2qiNGGx0VpYf+t/0T4Vp0+SW+HZlCHCGBLM7cvFF6fghBy6QxXe3gyaaT2kRMI0RNgaAvuCnQWw3gLV3auTOHKnEwceihuOuEZCSZHPfxjNa774L1uB1TIn7raLzqzH75zS3elPMhaggKs1vfx4Oq+ZRJaUcQIaEizyx/gPKpZpiL0I9+enzBallpuDkl+3DgpP5s73h8fakSi5k8Rkuvo2clLcOPm1KxRsE3oUCgaQNphB8rMfXQkV1RwXau194QWwcUpsv07eXmQPsnC7Qv4n4UaTbg5MPFkC+uFZQIAjeIgDUK0aapFlY82tFImnrmfYDw7BXf+wqOTQ4dKgUe/fYL1gE0memUPqJ6+YAzHq8gLb/dHTVJVKs1DL6756XwaYxbrqNMw0WVGa6sDFqVrxunMtjXZ+XLS23bJzVPFGLZ+MWAX8bWCCW++6NhhOIU/aKrbQvh3YIN/e49b5WqPwnblmGKTFkktTAywvlHpQL40SZHMcZcoeYCn+hZALE3+8S3VwOCsVjW6P6fc7jt50fL4rPkLAZAHvt+T13bpr3BLiAQihQQEeC6lIz71mlR60RTwfs/A5yw111Vf5SavtYdMpYSZqYVzVsfU49p5bbLMmJcMFL/NeePwzBPVZ/65GWdPxSDfqg/FIuKod47REAieKNEIH60gDSqGAOXoOPVQhm/wATpFVXFfQ0FxZsNlHpDjup0vULggkJUItMfWDwaPAoKg67dIvuSIyajKMMvhUzZjNQYxJJep4P1AzTkCYw94/n37vgsecheVfedqKsPZk2sru2ZoKtnG9Fi2PqRq1o9tULK9OQTN/kXd1NN9Z1nGeix7aZEm3zZlawoiCfow9t8jRl8V+PlQj1LMCSSDQFoJNv8dzeWdXwWONEtW8KpNLyqQ1vL5vHXu5nWxgHBdKELkk/NPS0iELbTPRXpkQ/SBPnt0y+pyxSkqcMxco9BL7Ps+MbuBTVLrOCaWzCD61noJUO0atJHjyme/Gl0vNBRAMcIA920PHz7C4H9UN6QM5V3mFbeq/httb6RY57cCUnl5YpmLEymmmXWQ91Bf1FoozO6ZVN4R/c8F0r5TVZzi45hG8B1+sJqBbJeOT33WqMAHvPWZD+FxExoBTi9SVMi0ZjjQkiiyJ5FGvhBCIDw5zLSod6GQ/vqNLcDvDUgThnse44YQqGZFtQOAIP5QOn8GyJmXLQS3Ocd1cqmSgPwetS1+uzgoYDwjLzxXU1nVvPix90WwkYVwT1/6/uLLTtq1HXRO1wwpbvzImuUIdPo1ZNFpHTnz0F0/0g0JKRMa9hQyB2LLiBM+G+65aYkEWNtqRatlWg9Y3ZAtB09F2uqtlwYvuz3pB6Sqyxqrk7p9ewf7ZTmqHO7XGuJ6W2BZLLjrJC2xU2xGPKUwP/qBzcrNHMLXWNaCEP5VL7xWBYCVc0ne2fvU1WuyV0FlJrdeq10sZj1h5JIEb0FCMIo0hTGwv6TLPATEOFF4ZGIZuLpgGVQHUFqecBxkFOYN+mZrKGHTO9r+d5yDJ+FktJrH+sk64cvLmVH2shwZDkOngfA0wWL9IaTFlvtxIqY8omtyVCLo3JkXw1hRcbn9KOyfae6y9rWqihu7E0+wSD2ENjXyDGSxdbynZ5px5Z7YkSnUjkYSoT9zTdOI56yGeGhQUdhZeAgm3IyoftHwQmeRZbgYnNFNNCsSU+abdChJP9k26s1brd1qnNtiPQcwc+V+rzl2xUMoWf1qI/jGrVeh0gxx4gl02wiSiYrk3IXSGI53x31RMMftTfDyxb8kzL9GZblXn/+vU7otyXLuDWbuSLN5IsQsxnRdlA5jYe4ay/CCjheeFjAXuECHzJtWYy38x9gm4CBQQOkt69JTaWpJO7IcXk2gHSD9dm7VLx1iDQirWx/DITA07o5hYKqfQyp5HO3NEzZKKC/pDZFWaM+fZaTZSGYTSFdp9+YDupDlzsg5oSqT0AwtywodBoFbjIy3ObcdKGyIsoiZbGyCtmq7bUiSfmEMll7EcZQ+jxki+scAU+8Bo2RjfaR9YtQllOAsEHY/DObVp8r8zEowCNZcEQCihAll45Ti801PypgqIGwV+xJMgOHRrlud8v8K7zuTINLP87jRH+G0GZkS7UXszDr0xNVwwtkrqj5SBZw8wqG4iziH0YUPUBCWFEpTjLKrdSo2nbflzmsmI5CIaBzH3SUsYPwCzhBCy3PencboVjL0vraHvccJOFHKO0XHafkz9AGb2u302pQ9MYDEJu5u+OT3PoEEyihtbuQ6xxYMqT3PrMwoA3NI4AFFjASq5o91WZwhl/j36WbeJP0YAfKz9EivCl4KMRKGqjpA833JDWmiv8KNFuOBhuurDmCqNcFwsafVMCl6ql2llaTy1oMqw0zw8dK36Qa1lwDTXD1ccwBsjPs8/mImIK2uuwYRxCI+aiFWh1um8W99srdvttJ4wusevRKr5AsWGygjU4Zw7bvWCfXnB07LbO4qwlFdQ1hAYQ6gK+DrzYI3KVBFFi7OP+DWfd/r89xlZ7UOTSvkgeCFoA3+FxkO0Y0UN5AM7zIYkTouPtqV1TmukCADuNg5TEe1B8t4XY50wAzyVstXtIHBc2tEXOpxEmGSxbnqHtgjSq+6N2qpM9S2klW2Z/uumE+MjYe2jYTfaoHJ7ojq0LMtt1WwDq95XiqV7znCWNfxbavbj/GVwPrMq0qTJD6VWCX4BFjTiuQvCzMaHgPi/K2iy9I02bDPgs2Y6Cx0r2HSn3TWKP15ajlIYa+ImThBLxyJvBYAYF+LFycciHwLuPjt1OUKTyXndg5yDpnFIlPX23JW0/t3/+xBhYvtxAgQCCgbj68C9lKj+fUMboo9ItuTPKKCxBv+erVoIVv/u1jfFE2FpAKHvqeXn1HVNW73P2dU+su6jYfsvWsVxpq6WEAQPdOkOLYmcsYwjp4I2flJTgq9tlbDlPrJE00SGDk1C9/sPXagofevks6g/n2OMUHexH2CuRM56nG60qT0sn1LSzUmMOLihdl3SEEiZ8BGgbqXhj7pbJVKwtUFBx/+LnG+1hm5WI3NDqm3bapmkPZZO1Jt4YvwEMdVHjhgOk9lKSZb4f4d9VAJoV/3ft/Jt6dniK/tFl0VZWX8EP883rLXLR7oXbSuoOToomvx8MYq1nGTaQb4vCv9lTs2bmfI/xrAUwu4yVcRrsg6uRn5RNIaBrR6KbXwgmd2BfREqW7vrWVTLB7fp5VYbaUJ7m7uEf5Q5jIbg0NBhn0p7NWQy2Jc7zWLr9yttvmEC1+PiyoxPuByPgQq7Tw4HcFmQ9pPGWTcdtQ0wgywcLBQQXDAePRguUa/vawSUz5jucBisTzx5B1XANp/N0ESsAG/dA3Dvc0OOdtVhXbELVooN0oap037RJwV9yJYyPDbCsK/4d/zfXgER5sQX3tj7VWI4d0+h2PwJrCEdOT320cKpkQlcinzTWaDbuvbWJ++9rIoMm27H3OOoY9tPAphIWEYkjrgb8=","_userid":"swati123"}';
                //        tonfirm=JSON.parse(tonfirm);;
                //        Confirm(liscensekey, userpin, tonfirm, useruserid);
                //           alert("Success or Failed");
                //          
                //         }
                function generateSotp1() {

                    var webTrustLicenseKey1 = "eyJfZGF0YSI6ImV5SnNSWGh3YVhKNUlqb3hMalE1T0RFeE16TTNOell4T0VVeE1pd2lhMlY1VEdWdVozUm9Jam95TURRNExDSnBjRlZ5YkNJNkltaDBkSEJ6T2k4dllYaHBiMjF3Y205MFpXTjBMbTF2Ykd4aGRHVmphQzVqYjIwNk9EUTBNeTlOYjJKcGJHVlVjblZ6ZEVGNGFXOXRMMmx3Wm1sdVpHVnlJaXdpWjJWdlZYSnNJam9pYUhSMGNITTZMeTloZUdsdmJYQnliM1JsWTNRdWJXOXNiR0YwWldOb0xtTnZiVG80TkRRekwwMXZZbWxzWlZSeWRYTjBRWGhwYjIwdloyVjBaMlZ2SWl3aWNGWmxjbk5wYjI0aU9pSXlMamd1TVNJc0lteEpjM04xWlNJNk1TNDBOemc0TkRjMk9ERXdORGxGTVRJc0luTldhWE5wWW14bFMyVjVJam9pVFVsSlFrbHFRVTVDWjJ0eGFHdHBSemwzTUVKQlVVVkdRVUZQUTBGUk9FRk5TVWxDUTJkTFEwRlJSVUZuVUZCRldscEdZaTlHU1RSRllUaHhaemd3UjJjMlkzcERUM3BHV21kMFEwbHZOa3RFYnprMGFtTlVjbmxrZVhKM1pXbDVkVVJLZUhrMmJteERjVWt6S3pkaFVqVkNNbEZLYTFOSVNDdFZabVY1VlRWMFNUWk1Va2hOZVdacVVsUmFRa3d4UW5neFdYUkVXVmxwV0cwd1NsQkxTblJtSzBweVQwMVJXa1JDVlhFMWR5dHhNV3N6VDJzMU9HdFBWbWhKWVdGNFkzRk5jRXBNYkdWR2NHbGxhRXRLWjBvNVl6UkVhR0ZNY2tadmJqSjRTMkpOTjNJMlpFUTJUak5qYWpBMmQycFdPV2xNYUZCcVdFbFNRekJ1ZEVaQlFrZFhObVZvTkZaSWRsbEpabTVoVEU1cGVYbHpjVFZJUzIxa01YQm5kakJ0YUU5RmNtTk5UMEp6ZWxBemJrdHJTa1I0WVVWdlJFcEJia053Y1VKMVpYVTNTMnN2Y1Zobk5WUXZWV3RzY2pCQksyMXVSMlpFZVdKRVdUWm9ZM0ZRYjFseFFuSlNaRU4zU1dKYVNFaDRRM0ZDZWxCUWQwRmxRME5CTjNsb0x6SXpaM2RKUkVGUlFVSWlMQ0p6Vm1semFXSnNaVXRsZVdadmNrVnVZM0o1Y0hScGIyNGlPaUpOU1VkbVRVRXdSME5UY1VkVFNXSXpSRkZGUWtGUlZVRkJORWRPUVVSRFFtbFJTMEpuVVVNdlJEaE5PVmhUTlZKaGN6QjRPSE0zYVRGd1VHRnVXU3RWTXpreFRWTTJhbGxVVGxRcll6TnBWemRrYlhaV1pHaHZaa2h1TkVOTlMwVldhVWg1YUdrNFRXazVlRkozZVd0Uk1XMUlObEp6Vm1kRVRFNXZhVTVCVEROTWEwVnhUVEoyZEdKalNFeEViM3AwYmxkUU4zaEtTSE1yVHpod1YwUkRObXRCUkZWUmJHNXNUWHB1WjJweGJESTFVRVpRWnpOcmJXaE9SbkJIUkhKSldFSkNkemxNVUV0UFkxTlRjMkpVU0hkSlJFRlJRVUlpTENKc1ZIbHdaU0k2TVgwPSIsIl9zaWduYXR1cmUiOiIwNjE5YmNiYzJiYjZkMmIxNWEwZmZhNDBkYjc2ODE0MmExYTkxNTljNDViMDAzMzRiZWMzZTQ0NTc2ZWExOGQyODliODE4MTJkZDkzZjlmYTAxZjQ2YjQ0ZmQ1MTc4M2M1ZGNkNWFlNzM4NWUzYjY3YTAzOGVkY2FhYzcyM2QzYTUwNDJjMzc1ZDMwNmZhYzkxYTcwODhkYmJjNmQ5NGY1MTE4OTAyODc3ZDM3ODYzY2FiZWY4MWU2ZTM1YzA1ZDViOGJmOWQ4NWRmYjIyNmMyMmY1OGRlYTM3ZDVlMDQxM2Y0YzVlYTIwY2FjMGRhODVjNjkzZThhNGMxNjJmODMxZDhlOWNhYWIwYmI2ZjM0NTExYjRlMDcxZDk1NTczZjMzMDNmY2UwNjI5MzQyNjY3MGRhMmFhOTVlOTBjMDJhYmIyNzRmNDk5MDFhMjI5NGM2MTg0YTE3ZjY3Nzc5NTliYTllOGI5YWJjYTVkZGI5ZjQwYTA4YjJhYWYxNmIwYjdhZTY2NWMyMTE3NjNhYTcwODQ0YjBjMGM3ZDkzZjEzNjU1MmQxZDhlMjY4ZmU4MWI4ZmYzMDE1OWRmMzVjY2Q3NGI3ODljMTljZDg4Y2Y3Zjc3NTFkMjJlNzI0YmE1NTQzMGVhMzZkNDc4MjU0MDBmZGU3OGI3YWMxMjNjYTYxZSJ9";
                    var liscensekey = "eyJfc2lnbmF0dXJlIjoiZEMySkVaMXIxSElOZkkyb3pGKzFyeUcyUlQ1QUZGcWdjc0w2c0FYU0lqZ1V5Vnpudkl4NEZxUWRZMnFmeGozZWc1bDZqdlJ2UURSUkgxWkhIMDYrWVRIcHZJcHFjbEJSZEk4UmZMczM0Y0JyUDdWVkhaSmJCU1JlY3lwN3RHbkVLdm5zaUczRVc2Q281bkh6QWw0eElNTHN0QzlOZHY3RFZQME1yUWh4NzNHK1E1K0R2c2F1dVJNalZ6aFJDcHJWS0FweEl3L3Jma1lMQ3Z2bkJvL3ZEU29LQ0RzaHA2SWFZVkFkK2xiK0M3cE1DeUZ6Sms3QkZDTWF4R3pFdElHM25OOU1waG1ObjRkYmVaazVYeERKbVQrOXhKY3U5dHZmbnU5ZUhtc0MzSmEyUzQ1MWk4NjY4UmpKYThDMUd4czA3SFlYeGFUVTdWRXdQcDBOVE0vd05RPT0iLCJfZGF0YSI6ImV5SnBjRlZ5YkNJNkltaDBkSEJ6T2k4dllYaHBiMjF3Y205MFpXTjBMbTF2Ykd4aGRHVmphQzVqYjIwNk9EUTBNeTlOYjJKcGJHVlVjblZ6ZEVGNGFXOXRMMmx3Wm1sdVpHVnlJaXdpYkZSNWNHVWlPakVzSW14SmMzTjFaU0k2TVM0ME5qSTROelE0TlRjNU16RkZNVElzSW5OV2FYTnBZbXhsUzJWNUlqb2lUVWxIWmsxQk1FZERVM0ZIVTBsaU0wUlJSVUpCVVZWQlFUUkhUa0ZFUTBKcFVVdENaMUZETDBRNFRUbFlVelZTWVhNd2VEaHpOMmt4Y0ZCaGJsa3JWVE01TVUxVE5tcFpWRTVVSzJNemFWYzNaRzEyVm1Sb2IyWklialJEVFV0RlZtbEllV2hwT0UxcE9YaFNkM2xyVVRGdFNEWlNjMVpuUkV4T2IybE9RVXd6VEd0RmNVMHlkblJpWTBoTVJHOTZkRzVYVURkNFNraHpLMDg0Y0ZkRVF6WnJRVVJWVVd4dWJFMTZibWRxY1d3eU5WQkdVR2N6YTIxb1RrWndSMFJ5U1ZoQ1FuYzVURkJMVDJOVFUzTmlWRWgzU1VSQlVVRkNJaXdpWjJWdlZYSnNJam9pYUhSMGNITTZMeTloZUdsdmJYQnliM1JsWTNRdWJXOXNiR0YwWldOb0xtTnZiVG80TkRRekwwMXZZbWxzWlZSeWRYTjBRWGhwYjIwdloyVjBaMlZ2SWl3aWJFVjRjR2x5ZVNJNk1TNDBPVFF6TXpFM01UWTVPRGhGTVRJc0luQldaWEp6YVc5dUlqb2lNaTQ0TGpFaUxDSnJaWGxNWlc1bmRHZ2lPakl3TkRoOSJ9";
                    //            
                    userregcode1 = document.getElementById("regcode").value;
                    userpin1 = document.getElementById("pinO").value; // liscensekey= 
                    useruserid1 = document.getElementById("userid").value;
                    loadInner(webTrustLicenseKey1, useruserid1, userpin1);
                    var dataforSotp = document.getElementById("txtsotp").value;
                    var array = dataforSotp.split(',');
                    var otp = generateSotp(array);
                    console.log("array" + array);
                    document.getElementById("txtsotp1").value = otp;
                }

                function goInitialization()
                {
                    alert("IN goInitialization on ibg fund");
                    userregcode = document.getElementById("regcode").value;
                    userpin = document.getElementById("pinO").value; // liscensekey= 
                    useruserid = document.getElementById("userid").value;
                    x = "pramodssss";
                    getLocation();
                    var retTOActivate = Initialize(webTrustLicenseKey, userregcode, userpin, useruserid);
                    var testData = {"_visibleKey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC\/D8M9XS5Ras0x8s7i1pPanY+U391MS6jYTNT+c3iW7dmvVdhofHn4CMKEViHyhi8Mi9xRwykQ1mH6RsVgDLNoiNAL3LkEqM2vtbcHLDoztnWP7xJHs+O8pWDC6kADUQlnlMzngjql25PFPg3kmhNFpGDrIXBBw9LPKOcSSsbTHwIDAQAB", "_signature": "3fc243a275fcb1c881f91bbfc0e0ba659fdc58dca006bb4c48f386ba09ea2c4284f5725957451b5b1e52f6addf0ddb314ae445a0743155fcb600007da753dbd561a01c521c3c87d9d940c1c06c8290f6b0054af13e37286dbab0fd651aed56f8a4d85fc977f67f1ff24d1ab68cac3e4e0f96ab02923769b562f22acf6a797143", "_key": "qLqXyM1KbxDBUAhIiDWsqPR7zzev9R0tGxgW5+6Td7Z\/fl8HhikuT2tRpTrz614G9BhBpN5H7E0CrkbUiB3xMlcYy1q+H0BmZ\/OvdADt8bnKVgMMM2LHOMK7o64U2av9RjY9JojeYVTch5x7JgDDoALzjdps8c2OD8gMEb92F3FxdVTtjv\/lR0hZBqLRF\/hRwp0v9ildzSywCW0X5ibBeY+Kn0aLqrjzg56iFHHVOGUSTI2DXLWFw5S6p68PEdk5cbietPabN1YSVP+MXXLN8RSTLQrgxt\/kYAFvd1l4cPfSjbRzmG+cXiHEOBr3HPOEms9HU7XtHEVWlge8Gcfv9g==", "_data": "VvOWhzbLORakFrgnAv4kPxnOSKKR0xi55pV+TH9rm\/PGRx9FQ3foClZxbdJgiiMsEWbNaXvzjW9lj2EjdnrybWTa5VLedbrN0L9e79xt5U1wOoC1njU80ot95bMbpn64CTeZUZYzdPYGxhKOD\/9AXB1LKQ3TJ3K8T\/c4R3nTqP2OI1v9ouYlBkvsZy5QIW2Xeww3vOcd5IXXT4wfpUS6K0uCt\/FHjT\/DgF5eXt4ka0drYE0eT4vKlDt6SrUIEzjhPXPn+d+qibPF\/ba3dxdmnYi\/+90nCs9T\/A0m7K97WvPYIyuthZHsa5hOWicKBB9uJFDdnuoXmo547jYt+m0W3esTOu6SE6lKsXpsZU4EtI0KSPDN7nM8JGIocauvIqjwTSUDfd96Gx75muNgwDYdDzGcfgQwkabD\/qRG8bdJ70Hsfty7w74XKEDd1EV5XCakqNbfo0PlNzsHdSAhEN4XkwF73sAx2YaRecHa0AtNmPohmItO7DhxUyya2WTe6+gvlVOtAilrP8QyVrjnZOD2bH64LVFNtcWgjIGkcGEliJtcj4y8mNQdzON6IRGHQs+DwBnpKbCdIwNd68EaEgiuVGC6ickirYAEYqx3cK6TB9NVWQp+oIweEM6RPPEeDaL2MMWHbOtzaQErikXRQyVZHBhX9QhMEawUNW0uPq89M6vnv6bb0hAPvGjMhUCoFnFJyOH+H48hu5WqVOMahcIvW5\/tX9Isq9LJ6Yi3sTLg+CqOgaV4+9axD2iL1KW+j4xnpAN3InnqHEn6NwWlecyhqav4meZvuNkOb2H9sPOvqfvD6+XO\/4Pahh5XrEThQO3\/CO3EVMjYsDK2U5PUCt3xKCcWb490CUpU8rfiUvOYZ5qp62QnriNEAgf0BXJFR1WKkHGIwIX5yQ9VdM3GKZxLYGBKOe320m1H4D58I7iIZ1VqfTiMw\/hV1f\/EaU5t0kPloWEWE7SKvuLp09oD4vf1IF8zq5v9eYE1igH1axuTPPzZTSnuUK9tM201d2E0iURObtl7nM9byRcmYFOOe\/VKlRdVt0e0QbaSr7hhzuJOZXhQZxi3MCLXzSZMnmbZRFzarbbg7Z\/P4FU8nISET+Z13LyFv27ZHF4GRqjs5wjK8VQ6gJov6hyRX9sWqlTAz\/mQPu6EeM6Z2uchsDa8G991SrRUPbA3gKPvFCzDQiipo24WPXjkTmHjFaviulhOonWdsyHm3T3Xnvpr\/TGLT0TMwLVOto6zhuiFJnE\/0VxNzvSFybV4\/R9ILkno+jn49QGt2I0CtTJz1o52p+Ntu1AJa20Bcn8N6jcNFbb\/QEem6zZqVMyqCY8xT+dZ8bWOqlugUxDRLGT6yP2TdcxKi9yQ9yKTfO2\/\/oRoDMNVOv6B3TrqJ4Uic6Y++q2K4tno6lt6Qw4x7JE9uFwy1IR+DbWgznv1t0LHGVbe\/cIderUAo0\/PU0Cd4CaxhQUZ6cR2+DqLnXYNE8IRNo0GS0v3jC8KIW7LDk+tFn55QIgHV\/KaNwwgH4t2MRHwWZKi5tKiL\/kik1yQrTNNnkRMrO9VeQdFQ+7AdWxh9+W4OwgpmpWHq9lQb\/B78lPT5BMsETGD4HV23m04wubiSygxabEXrtmxzfIgKlazVRqqbzOqTQjf1iBcN2nHuBVLguO9\/xFO6HaGek8ztSZFnEKa3GjAWr6NIuR3EAsMIxB3k9ucm87pLXv8COkULTgYyoODB+JuvzfrLeFLpsXa6l8x6hatNwSKqIEpyrOgjaCSdzRQu7Ne87pxrqFa5UN0TgDvHxF5R5aboo3W13ssj5lTUiDICoCoFDeAiHc+Vi31UGTDwaKlQijBcnOFisxj9yTe8CAucEahljvVmvU0n4w8dIbbau7VKHsUtUNttpiBnF4ihBXjSm6xVXOzAHEVJfZIA4QknTGrvfI4e\/NkTmtGpe+MCwTYZlvKcIDC7NY8CsVjyDSDwSZKPasNYvQPCeVtcOplYjcMgjnY8W2KaHrBoIvyBiqJ3rdivzIquycdIIdzeZFIJhn3eXKUr3rZEo5HhV7\/9NAsPKnlZwgg88VqsZn+8XETd0P9v7qhRl+Ty73aeA9q6rd64t5JD\/\/vHamhkIzzORrkVWGh7tn4XdQXMEHA7srfu2PTeKLA38CMwBcaZbx6rYUMQncbWa\/l0WD5rdfe1MaAmzrx1uT0rcYQ3ZD+Kk1wq6yhEdwwFSo4xSXM+rnLdy\/L\/1UJr1NMngR7NHXFGj8Ou2hHynz2o8cXIgsUVrhEXTS8t\/aNAWZ23mPAkdN3gFFwdEaP6MqL9T1k6zf\/+uZvusTlzBanKrh45g9SKwxf1XsEEvFB6T0DNUYDj2g0lCeXun89toyczJVWqK32PPjRGCS\/7OXJNm2aZt81LPxLkP97OQxvqAEfnFWPl1FPaXHD80BoQ9tOAt8FV1wWRR0xkI+lDaIPOnxT3zoqegRmAk2gIngvgH0s2oHNM2h6Qp2brAJC7pXPst1pfuup1IL2KAlCYwkFvGHPI9H4e63FRLtxtbbAFq58Ka3EGVR04rMil2VtO77fbuIBBuitfpu5Icf95ZbrK\/IY5Kn8vXxOnVAaocxefhykszsStvB6y8agQBhS8+MNrU\/qp0ZTyjVZI\/d1FnSWTK69WUcZAGsHgmxXkp2AFFayQ9cwuB0DsJHzDHddk6d2vtmdRNBCIMpnqjSK4EHRelLwwOJoPJ89U4Q2BJsyZ+F2yqE2+tqmH9JUwy2SlgAzTN6duCL4W6+u0lIybE49Dnp\/ON0ukL3cqdFIdhfxgy9E4\/b5yd786WEBkFk+WJ6i\/CHsy9yjaRNTMVOOsiWORLcgM1grUzHnGPHH96rSRTZAaPsIpbjz8YrKMeLGIvq5nsaxqAzwGD+LEdpUFHgTwB4+r2nueVgEBwG+1jIuS\/Dua7G0pVkA0s3Bf67MhShU\/2mdoPYee6C\/IbhqggfzDfiUQiWIJUFsPFahMQFfYFdg7KZaqPOhD3ipgR4EIKGjummQ3jsc75DaKmMqIx4e1Kd0cRfm+JdhnqI2WA5G9GoVNB3EQepxHmdDs\/dn3q9Fb05AaTc72wEpGCrMuke3T3oirhZyS7nwNSXVbBVeSSXkrv\/cFxu0WNsp\/erCyiTDNHSKZmaUJZs9kDhboFSxmsZrMG2NA8V+PIMYZ+uAY+dHnJ2vmPyq+OkjSfG2NFVtffZjEWqERY7vdHCED6Bxmg1BchYscmbOsFgOSk1hUs2ln\/WfJmOtxSd47fAJJWeChUg5KGF04gNh8LmMQA9UagLq712ajBhrpYw7qvLdSRRhQhtgyfZmNoA3vGL+nVBtPFawszjMLaXf4w60URmFlFytB51MrX3LeUIDf8sTvkL7IHoQST4yu8yUjdGP7f9aw8zOsBBGmkA5xNxK6gucozBia0nRdiZ0iS\/kKGm9boPdocAe+EeuIegWg\/wFd9J1it2CBDh7MLmVB3xYFle+Zb+UWu0qNckDFuwmkB1npMPTx6lduJ17ANl07Z7X4VrY\/Yo\/gL7rn2EBkpdosH6w\/zctJ+v4xgPgQ9ont8GvZN7nTJtS3u+zi3vzTPyHc4a2ru3M+Qwb1TiDonzWRhpafe84wxAXiW3py8Fidne6vVAlKWLpMKeZWo7EltC8ZTjIR2G8Q5n\/rVvPvmIYsbeAiCDI0D0YDgK7y9\/jRai1B\/3SKnb\/0sk0QO1kTigz8D7\/+73M1RcPhhKjO\/uAOHSgaN0dM8j3Qx+sxjb2qjcOr0AnHqmfDbBh2IUo1YBfEIcHmq0zHrn0JLkNk8AC1zOrWEzO8HuEQPTfv+TftLDXH8Jr5jnj3CG0kSEXqOLoR2DLNE6n0nlPF\/CDlwLAI9ozZWJWBTYIVX4wzFCaHwL5XoSQy1QByMQKwXW0foBNrWtZ0KmIfRT\/NUp4GCUzNO10ZzflTWL4JIhstGOwTH\/6YyDnGWx9LI9UBtfBSutMjasp\/Jk8k3j4qdgRtB+SAYi0zyS5u8TKS32KyBYmnCEumCvX6vZjvYLEz7BZNv9UHAOC30vZ17GVdFsTGfZ\/bVS8cuGAyt6mDF09FaKmzQkyjmPv7DYsxI07EdUYHynq1XutJsumQ3b5oHmNgKEDJ5pEpLI96S8GFYziViSma686SvCGzaIR3klWWhGNkKdkYZZZN46Ovb\/WpGhSb4NRxTJ0mnPj5tNGkyqGTo3N9NzfJGGP9gyktf3ukY3VyIhKLlRenAOyse1\/QtLBwKzdgA3IY0r\/l\/jCSVKDPgpllCShHEkQtlRhgJTP3gh+emUUT81UYDLS4mlH3egIE+O++WYGlOUAzmaVjOm2Y4O0GuONDeHDeZJCimyP\/SfenIGPNOj8msr9uoVqapPVAeDQwvRjHmZWiwkkixB7gLrovCbqZn0OBErAwIZ9T\/Stm4JT3dYiQKTTDXNuWwDtX36Svdq8ehIo26quInz43lzhGkuHrFaabeTGisATwnkiGvEWs\/nr+wSrDtHeU1ntfFM0m65Bz\/sLAuI1Ny1moSLabvBc+4gRMifYyngZ6QN6HnRRIJF+YDg9ujvrdBLpWx0najd5Tk72e2GdHABFy7qw76sRav2vxIT4Sh+fLmtzFfAOmUKbqw+Gcq5kO0HxsxWJb9A7WJPg603qh9t3aDJwMiFCBy8xtuji+2nPSuQqsDJzSfKFo\/wHzFv7S9lB2WRuc7mGsQjOEIyUnJBvreIUt09oiaDmjAfxJQWBp6JhX5WxPS7YB\/xor3u5+rSjsUAgfnwWI4xyvzzx3QrYKSUTBQroTDvzhVHrlQggPKUBXIiBmlF0yJnhrnfsVd5Y9nPqTMpBexsGwD2ahfL2CMbkVUddcsglSWmoU99ABmHPV51UYJTuEaTxHPHC3yvCdy1UbFqJIRyrvSrbRfcn7I1pA6+f5SlppwkPv47s4e0utk6Ptq36AobtsxUsI5gnnorgBwqSTCtVTFg5dvctnN28SdLmuiQNTlcgt579UkgnztilSy3DdsQTe9YbeUUi5Q\/US8TtoUSWTsMxiQgkW4wZMfvZ1EO\/ENTVvslgZPoI\/2r2foaca6WL6KZE1bbxJPhNG7EwhET3onLxauGdsq7ldc\/mJccYyjhnFjlbAAIz8JVWMhrThadOm4ex+QESngJ7XoV86t8qwpL9mMNAJuRahlPdWPieezphlDS4Sq9951Zo\/bCsKblnxyTDf\/4k\/0t71Tez1kXxxA+P7jXL1z8EtC7\/VNuTTrzX0epNBX3f7n9InUje4F7ciZH3W8xzMTcL14Kq1BaBeN2QaEDngIoB89hWbQtgLHukOefnAoVQpdKB+kAdCCPn+sdTr6ti9SX\/dPaV67MQCADyx7HRcKg\/ortp7L2odJwrFfBrSp\/D2704\/xkdfTVhdwoJjRWB6KVEUifu31ZTSBAey+FfWuAW24H3jx7XQQWXwXZJSYT3JX3L6U16KsEGbeBYMWpEOP0zqJl\/+bxdyDMqsDL5hYYgC94kKXbASV3uoEpxK2OvJRzifpG6uciu1NgEXWy+iZMpMWYwNACxC7nAT1uJVkw0yk44Lg036xWmo0E7fpwDxXEof9Fj9MQrN0jkOFSyFmW8f9VMkVbq+bI2twwruOdXpC8PJMSwCW4\/w4qRdZnCCaK3Uxdi3uJO9UkTwcUGs7sEFBr89Jxj6b8kHyur3jEP30X9YnoR2Y\/CltF\/OtsifnS4nG9gEOk90RxbIwYTkLNtffNicMb5z9jGBLdFUi+RjSib0sTkpkv4pxNPPNcV1Ik3bxPjjb93MUbzRFbPKORkU2ccaApwFMdSPbmtj6wnYIiwjOWTL6qeCjohuhW\/VvBOfJL78YC2kWHxN+kAeBIPn0A2n+guh6NxWQAc9Cetjq1Vxva+Yra0mLqEe1Wuz5njvuA2qoxcWczVcqywaoc14vo3TB\/41p\/Kgp\/f1u6zOmA3rzQ+tG4dmE2Mm4A6RROeRDwP7xw9+JsWhTI\/4nGRDw\/b52FC9fH0+\/32e9n+TSdkFCqvRrfbapf\/mrcsZXMrzX\/RQcYWaOmICSzzTicyh0UTc31XiZkFJFyw\/GR2jwdZGqgo6v1xQOL1eGBWYXYZicFbi7IkHedPN3unrNdX8PJsQTxPml+FFxzz3rpx8y9D2L+YzbKiHq9Z8lp0epPfpfce4uW1a0AQjZ9D++BHNjqL+IIO4p1bjdz+8DZ9Q90z0n49kDfa+TZnYT9\/yxF2eHPT0jkcJkq4pRpAprN6V0KOHIDi7PeLeiUJDx\/7814ZNRv2FsR8ejNDR\/gOuVLRXZZ9ucIjqXknT2DCPAb4u\/\/xr9hvubbz3AMaHeCwOwNzXsXieSpZ1wPypaJR1+1f5DRAsZ4TDfBUwleWWNyDRgcMSlK7gmIR2cM4qjLwt4ynMJLHEI5cQEnrR8PWSIKGslnDupMqHH8cH3cqhQOgFBkSLIvYhq7J0NE+MAsE47EPxRnv3njSleBe1vbGj3Y+GSvzud9KjY6rHoIlJ+PzOq99snYyR1hcnPEF6a6Jm7zivRFUUj7olow4\/r\/bumiKM6IQpujmnvslPGFbkvL1D6jAjjMLPi4rmfTUKgq2ZP5x1Ccc2F6BHybtgdDMhJJ\/cZiOXDoFrj\/qJTgM2RZYtRCPasRivEH2cUK+bQtIriRhe\/dMi0wEj76L0cvDvjUZTlfdcz81ssO40rg7DLJ4bRUOaoVbeTkUREp4290TT3L0+0ryacq+9ml3FBdwdRenbfpW6aEVSO5tGUaamwcpv07fMLPPKdcqD6kRB0PmoHyoCtvH3gGWUfyWHNDkhRMi0aYD6he\/v9S4Yg7VYDn6V\/c7XaQGa0q5AYM25Hl8QzWMc2IQcX3JoU4lJJ71tq+oZEQTYnBt0zBGdsES9c+adX+7BEtov3k6Nw6hfLycOdhF8jQa2IDH+iYu8Spn\/5m1X9KYzHl\/XN6uWMr2Q8wUegZIzUzdYCgrdKz6VFvPAFhCw5a1FXeN\/F+52NGx6Oy6z7QsedRclLczsoJKfeJBPzTP0TUP72o0RHAYA8S6D\/V2c\/oaSH3W4clWpb+nv2qnSqqP\/uDLYe+U11JCpYNhaawkR\/Umvzc5lYpe3XOhlCvUY8z4vPqKsEEhIYRNXwPSSTQMBH4wi2TQ7BQ9L\/KBjckI\/Za2K1Xxtr0nUUD3QsHhvKBgNq2c+E3fkX1DL8fUdCS3ap6rlntr6fRdBekZRWSjWYYrbmqhG4QHzAJvUByy7WQ0upgr9NlJcnWfsSmNhzEg9+spT9B+OuhG6vLgUXJR\/HACtSQOpudN2JzXaY8RB\/h6FIKDer+iRt1Rm\/0o7XHoSm+0ODtSY8ywi3P5fu6M7hXOD9qOKbLyyEZ+UfCy5UXgSgodFFvth9LaX4+KutWhkVQc1zaz4goqoffca53SI54ZzLUm8rQecSmZfkTC668lW\/tWm6mL9E9u3WcdYpxtWP6Pnav6zIe5z9I5cjoJYG4f24J2Nc1S4jjeoN4vaBKpUVfEokF8l2wIdAqOTN2PyQrlp6EHpP919wS8lXCHppANDq\/Wo8jEY+no\/7bKdtqI8875KRPRce5S7lg+DLwAnSr0xaehYYQqs9gd7nrYAsMcUvfjpGzFKD+zokSfjTky\/y7jADaL4j4kdNTbAbdq9vN9WJK7LPDH"};
                    alert("Initialization successfull..");
                    var jsondata = {"_data": retTOActivate, "_type": "initialize", "_userid": useruserid, "_emailid": "9407696", "TRUSTTYPE": "WT"};
                    $.ajax({
                        type: 'POST',
                        url: 'https://www.mollatech.com:8443/MobileTrustAxiom/Mobiletrust',
                        dataType: 'json',
                        data: jsondata,
                        crossDomain: true,
                        success: function (data) {
                            var inputData = data.message;
                            var testData = {"_visibleKey": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC\/D8M9XS5Ras0x8s7i1pPanY+U391MS6jYTNT+c3iW7dmvVdhofHn4CMKEViHyhi8Mi9xRwykQ1mH6RsVgDLNoiNAL3LkEqM2vtbcHLDoztnWP7xJHs+O8pWDC6kADUQlnlMzngjql25PFPg3kmhNFpGDrIXBBw9LPKOcSSsbTHwIDAQAB", "_signature": "3fc243a275fcb1c881f91bbfc0e0ba659fdc58dca006bb4c48f386ba09ea2c4284f5725957451b5b1e52f6addf0ddb314ae445a0743155fcb600007da753dbd561a01c521c3c87d9d940c1c06c8290f6b0054af13e37286dbab0fd651aed56f8a4d85fc977f67f1ff24d1ab68cac3e4e0f96ab02923769b562f22acf6a797143", "_key": "qLqXyM1KbxDBUAhIiDWsqPR7zzev9R0tGxgW5+6Td7Z\/fl8HhikuT2tRpTrz614G9BhBpN5H7E0CrkbUiB3xMlcYy1q+H0BmZ\/OvdADt8bnKVgMMM2LHOMK7o64U2av9RjY9JojeYVTch5x7JgDDoALzjdps8c2OD8gMEb92F3FxdVTtjv\/lR0hZBqLRF\/hRwp0v9ildzSywCW0X5ibBeY+Kn0aLqrjzg56iFHHVOGUSTI2DXLWFw5S6p68PEdk5cbietPabN1YSVP+MXXLN8RSTLQrgxt\/kYAFvd1l4cPfSjbRzmG+cXiHEOBr3HPOEms9HU7XtHEVWlge8Gcfv9g==", "_data": "VvOWhzbLORakFrgnAv4kPxnOSKKR0xi55pV+TH9rm\/PGRx9FQ3foClZxbdJgiiMsEWbNaXvzjW9lj2EjdnrybWTa5VLedbrN0L9e79xt5U1wOoC1njU80ot95bMbpn64CTeZUZYzdPYGxhKOD\/9AXB1LKQ3TJ3K8T\/c4R3nTqP2OI1v9ouYlBkvsZy5QIW2Xeww3vOcd5IXXT4wfpUS6K0uCt\/FHjT\/DgF5eXt4ka0drYE0eT4vKlDt6SrUIEzjhPXPn+d+qibPF\/ba3dxdmnYi\/+90nCs9T\/A0m7K97WvPYIyuthZHsa5hOWicKBB9uJFDdnuoXmo547jYt+m0W3esTOu6SE6lKsXpsZU4EtI0KSPDN7nM8JGIocauvIqjwTSUDfd96Gx75muNgwDYdDzGcfgQwkabD\/qRG8bdJ70Hsfty7w74XKEDd1EV5XCakqNbfo0PlNzsHdSAhEN4XkwF73sAx2YaRecHa0AtNmPohmItO7DhxUyya2WTe6+gvlVOtAilrP8QyVrjnZOD2bH64LVFNtcWgjIGkcGEliJtcj4y8mNQdzON6IRGHQs+DwBnpKbCdIwNd68EaEgiuVGC6ickirYAEYqx3cK6TB9NVWQp+oIweEM6RPPEeDaL2MMWHbOtzaQErikXRQyVZHBhX9QhMEawUNW0uPq89M6vnv6bb0hAPvGjMhUCoFnFJyOH+H48hu5WqVOMahcIvW5\/tX9Isq9LJ6Yi3sTLg+CqOgaV4+9axD2iL1KW+j4xnpAN3InnqHEn6NwWlecyhqav4meZvuNkOb2H9sPOvqfvD6+XO\/4Pahh5XrEThQO3\/CO3EVMjYsDK2U5PUCt3xKCcWb490CUpU8rfiUvOYZ5qp62QnriNEAgf0BXJFR1WKkHGIwIX5yQ9VdM3GKZxLYGBKOe320m1H4D58I7iIZ1VqfTiMw\/hV1f\/EaU5t0kPloWEWE7SKvuLp09oD4vf1IF8zq5v9eYE1igH1axuTPPzZTSnuUK9tM201d2E0iURObtl7nM9byRcmYFOOe\/VKlRdVt0e0QbaSr7hhzuJOZXhQZxi3MCLXzSZMnmbZRFzarbbg7Z\/P4FU8nISET+Z13LyFv27ZHF4GRqjs5wjK8VQ6gJov6hyRX9sWqlTAz\/mQPu6EeM6Z2uchsDa8G991SrRUPbA3gKPvFCzDQiipo24WPXjkTmHjFaviulhOonWdsyHm3T3Xnvpr\/TGLT0TMwLVOto6zhuiFJnE\/0VxNzvSFybV4\/R9ILkno+jn49QGt2I0CtTJz1o52p+Ntu1AJa20Bcn8N6jcNFbb\/QEem6zZqVMyqCY8xT+dZ8bWOqlugUxDRLGT6yP2TdcxKi9yQ9yKTfO2\/\/oRoDMNVOv6B3TrqJ4Uic6Y++q2K4tno6lt6Qw4x7JE9uFwy1IR+DbWgznv1t0LHGVbe\/cIderUAo0\/PU0Cd4CaxhQUZ6cR2+DqLnXYNE8IRNo0GS0v3jC8KIW7LDk+tFn55QIgHV\/KaNwwgH4t2MRHwWZKi5tKiL\/kik1yQrTNNnkRMrO9VeQdFQ+7AdWxh9+W4OwgpmpWHq9lQb\/B78lPT5BMsETGD4HV23m04wubiSygxabEXrtmxzfIgKlazVRqqbzOqTQjf1iBcN2nHuBVLguO9\/xFO6HaGek8ztSZFnEKa3GjAWr6NIuR3EAsMIxB3k9ucm87pLXv8COkULTgYyoODB+JuvzfrLeFLpsXa6l8x6hatNwSKqIEpyrOgjaCSdzRQu7Ne87pxrqFa5UN0TgDvHxF5R5aboo3W13ssj5lTUiDICoCoFDeAiHc+Vi31UGTDwaKlQijBcnOFisxj9yTe8CAucEahljvVmvU0n4w8dIbbau7VKHsUtUNttpiBnF4ihBXjSm6xVXOzAHEVJfZIA4QknTGrvfI4e\/NkTmtGpe+MCwTYZlvKcIDC7NY8CsVjyDSDwSZKPasNYvQPCeVtcOplYjcMgjnY8W2KaHrBoIvyBiqJ3rdivzIquycdIIdzeZFIJhn3eXKUr3rZEo5HhV7\/9NAsPKnlZwgg88VqsZn+8XETd0P9v7qhRl+Ty73aeA9q6rd64t5JD\/\/vHamhkIzzORrkVWGh7tn4XdQXMEHA7srfu2PTeKLA38CMwBcaZbx6rYUMQncbWa\/l0WD5rdfe1MaAmzrx1uT0rcYQ3ZD+Kk1wq6yhEdwwFSo4xSXM+rnLdy\/L\/1UJr1NMngR7NHXFGj8Ou2hHynz2o8cXIgsUVrhEXTS8t\/aNAWZ23mPAkdN3gFFwdEaP6MqL9T1k6zf\/+uZvusTlzBanKrh45g9SKwxf1XsEEvFB6T0DNUYDj2g0lCeXun89toyczJVWqK32PPjRGCS\/7OXJNm2aZt81LPxLkP97OQxvqAEfnFWPl1FPaXHD80BoQ9tOAt8FV1wWRR0xkI+lDaIPOnxT3zoqegRmAk2gIngvgH0s2oHNM2h6Qp2brAJC7pXPst1pfuup1IL2KAlCYwkFvGHPI9H4e63FRLtxtbbAFq58Ka3EGVR04rMil2VtO77fbuIBBuitfpu5Icf95ZbrK\/IY5Kn8vXxOnVAaocxefhykszsStvB6y8agQBhS8+MNrU\/qp0ZTyjVZI\/d1FnSWTK69WUcZAGsHgmxXkp2AFFayQ9cwuB0DsJHzDHddk6d2vtmdRNBCIMpnqjSK4EHRelLwwOJoPJ89U4Q2BJsyZ+F2yqE2+tqmH9JUwy2SlgAzTN6duCL4W6+u0lIybE49Dnp\/ON0ukL3cqdFIdhfxgy9E4\/b5yd786WEBkFk+WJ6i\/CHsy9yjaRNTMVOOsiWORLcgM1grUzHnGPHH96rSRTZAaPsIpbjz8YrKMeLGIvq5nsaxqAzwGD+LEdpUFHgTwB4+r2nueVgEBwG+1jIuS\/Dua7G0pVkA0s3Bf67MhShU\/2mdoPYee6C\/IbhqggfzDfiUQiWIJUFsPFahMQFfYFdg7KZaqPOhD3ipgR4EIKGjummQ3jsc75DaKmMqIx4e1Kd0cRfm+JdhnqI2WA5G9GoVNB3EQepxHmdDs\/dn3q9Fb05AaTc72wEpGCrMuke3T3oirhZyS7nwNSXVbBVeSSXkrv\/cFxu0WNsp\/erCyiTDNHSKZmaUJZs9kDhboFSxmsZrMG2NA8V+PIMYZ+uAY+dHnJ2vmPyq+OkjSfG2NFVtffZjEWqERY7vdHCED6Bxmg1BchYscmbOsFgOSk1hUs2ln\/WfJmOtxSd47fAJJWeChUg5KGF04gNh8LmMQA9UagLq712ajBhrpYw7qvLdSRRhQhtgyfZmNoA3vGL+nVBtPFawszjMLaXf4w60URmFlFytB51MrX3LeUIDf8sTvkL7IHoQST4yu8yUjdGP7f9aw8zOsBBGmkA5xNxK6gucozBia0nRdiZ0iS\/kKGm9boPdocAe+EeuIegWg\/wFd9J1it2CBDh7MLmVB3xYFle+Zb+UWu0qNckDFuwmkB1npMPTx6lduJ17ANl07Z7X4VrY\/Yo\/gL7rn2EBkpdosH6w\/zctJ+v4xgPgQ9ont8GvZN7nTJtS3u+zi3vzTPyHc4a2ru3M+Qwb1TiDonzWRhpafe84wxAXiW3py8Fidne6vVAlKWLpMKeZWo7EltC8ZTjIR2G8Q5n\/rVvPvmIYsbeAiCDI0D0YDgK7y9\/jRai1B\/3SKnb\/0sk0QO1kTigz8D7\/+73M1RcPhhKjO\/uAOHSgaN0dM8j3Qx+sxjb2qjcOr0AnHqmfDbBh2IUo1YBfEIcHmq0zHrn0JLkNk8AC1zOrWEzO8HuEQPTfv+TftLDXH8Jr5jnj3CG0kSEXqOLoR2DLNE6n0nlPF\/CDlwLAI9ozZWJWBTYIVX4wzFCaHwL5XoSQy1QByMQKwXW0foBNrWtZ0KmIfRT\/NUp4GCUzNO10ZzflTWL4JIhstGOwTH\/6YyDnGWx9LI9UBtfBSutMjasp\/Jk8k3j4qdgRtB+SAYi0zyS5u8TKS32KyBYmnCEumCvX6vZjvYLEz7BZNv9UHAOC30vZ17GVdFsTGfZ\/bVS8cuGAyt6mDF09FaKmzQkyjmPv7DYsxI07EdUYHynq1XutJsumQ3b5oHmNgKEDJ5pEpLI96S8GFYziViSma686SvCGzaIR3klWWhGNkKdkYZZZN46Ovb\/WpGhSb4NRxTJ0mnPj5tNGkyqGTo3N9NzfJGGP9gyktf3ukY3VyIhKLlRenAOyse1\/QtLBwKzdgA3IY0r\/l\/jCSVKDPgpllCShHEkQtlRhgJTP3gh+emUUT81UYDLS4mlH3egIE+O++WYGlOUAzmaVjOm2Y4O0GuONDeHDeZJCimyP\/SfenIGPNOj8msr9uoVqapPVAeDQwvRjHmZWiwkkixB7gLrovCbqZn0OBErAwIZ9T\/Stm4JT3dYiQKTTDXNuWwDtX36Svdq8ehIo26quInz43lzhGkuHrFaabeTGisATwnkiGvEWs\/nr+wSrDtHeU1ntfFM0m65Bz\/sLAuI1Ny1moSLabvBc+4gRMifYyngZ6QN6HnRRIJF+YDg9ujvrdBLpWx0najd5Tk72e2GdHABFy7qw76sRav2vxIT4Sh+fLmtzFfAOmUKbqw+Gcq5kO0HxsxWJb9A7WJPg603qh9t3aDJwMiFCBy8xtuji+2nPSuQqsDJzSfKFo\/wHzFv7S9lB2WRuc7mGsQjOEIyUnJBvreIUt09oiaDmjAfxJQWBp6JhX5WxPS7YB\/xor3u5+rSjsUAgfnwWI4xyvzzx3QrYKSUTBQroTDvzhVHrlQggPKUBXIiBmlF0yJnhrnfsVd5Y9nPqTMpBexsGwD2ahfL2CMbkVUddcsglSWmoU99ABmHPV51UYJTuEaTxHPHC3yvCdy1UbFqJIRyrvSrbRfcn7I1pA6+f5SlppwkPv47s4e0utk6Ptq36AobtsxUsI5gnnorgBwqSTCtVTFg5dvctnN28SdLmuiQNTlcgt579UkgnztilSy3DdsQTe9YbeUUi5Q\/US8TtoUSWTsMxiQgkW4wZMfvZ1EO\/ENTVvslgZPoI\/2r2foaca6WL6KZE1bbxJPhNG7EwhET3onLxauGdsq7ldc\/mJccYyjhnFjlbAAIz8JVWMhrThadOm4ex+QESngJ7XoV86t8qwpL9mMNAJuRahlPdWPieezphlDS4Sq9951Zo\/bCsKblnxyTDf\/4k\/0t71Tez1kXxxA+P7jXL1z8EtC7\/VNuTTrzX0epNBX3f7n9InUje4F7ciZH3W8xzMTcL14Kq1BaBeN2QaEDngIoB89hWbQtgLHukOefnAoVQpdKB+kAdCCPn+sdTr6ti9SX\/dPaV67MQCADyx7HRcKg\/ortp7L2odJwrFfBrSp\/D2704\/xkdfTVhdwoJjRWB6KVEUifu31ZTSBAey+FfWuAW24H3jx7XQQWXwXZJSYT3JX3L6U16KsEGbeBYMWpEOP0zqJl\/+bxdyDMqsDL5hYYgC94kKXbASV3uoEpxK2OvJRzifpG6uciu1NgEXWy+iZMpMWYwNACxC7nAT1uJVkw0yk44Lg036xWmo0E7fpwDxXEof9Fj9MQrN0jkOFSyFmW8f9VMkVbq+bI2twwruOdXpC8PJMSwCW4\/w4qRdZnCCaK3Uxdi3uJO9UkTwcUGs7sEFBr89Jxj6b8kHyur3jEP30X9YnoR2Y\/CltF\/OtsifnS4nG9gEOk90RxbIwYTkLNtffNicMb5z9jGBLdFUi+RjSib0sTkpkv4pxNPPNcV1Ik3bxPjjb93MUbzRFbPKORkU2ccaApwFMdSPbmtj6wnYIiwjOWTL6qeCjohuhW\/VvBOfJL78YC2kWHxN+kAeBIPn0A2n+guh6NxWQAc9Cetjq1Vxva+Yra0mLqEe1Wuz5njvuA2qoxcWczVcqywaoc14vo3TB\/41p\/Kgp\/f1u6zOmA3rzQ+tG4dmE2Mm4A6RROeRDwP7xw9+JsWhTI\/4nGRDw\/b52FC9fH0+\/32e9n+TSdkFCqvRrfbapf\/mrcsZXMrzX\/RQcYWaOmICSzzTicyh0UTc31XiZkFJFyw\/GR2jwdZGqgo6v1xQOL1eGBWYXYZicFbi7IkHedPN3unrNdX8PJsQTxPml+FFxzz3rpx8y9D2L+YzbKiHq9Z8lp0epPfpfce4uW1a0AQjZ9D++BHNjqL+IIO4p1bjdz+8DZ9Q90z0n49kDfa+TZnYT9\/yxF2eHPT0jkcJkq4pRpAprN6V0KOHIDi7PeLeiUJDx\/7814ZNRv2FsR8ejNDR\/gOuVLRXZZ9ucIjqXknT2DCPAb4u\/\/xr9hvubbz3AMaHeCwOwNzXsXieSpZ1wPypaJR1+1f5DRAsZ4TDfBUwleWWNyDRgcMSlK7gmIR2cM4qjLwt4ynMJLHEI5cQEnrR8PWSIKGslnDupMqHH8cH3cqhQOgFBkSLIvYhq7J0NE+MAsE47EPxRnv3njSleBe1vbGj3Y+GSvzud9KjY6rHoIlJ+PzOq99snYyR1hcnPEF6a6Jm7zivRFUUj7olow4\/r\/bumiKM6IQpujmnvslPGFbkvL1D6jAjjMLPi4rmfTUKgq2ZP5x1Ccc2F6BHybtgdDMhJJ\/cZiOXDoFrj\/qJTgM2RZYtRCPasRivEH2cUK+bQtIriRhe\/dMi0wEj76L0cvDvjUZTlfdcz81ssO40rg7DLJ4bRUOaoVbeTkUREp4290TT3L0+0ryacq+9ml3FBdwdRenbfpW6aEVSO5tGUaamwcpv07fMLPPKdcqD6kRB0PmoHyoCtvH3gGWUfyWHNDkhRMi0aYD6he\/v9S4Yg7VYDn6V\/c7XaQGa0q5AYM25Hl8QzWMc2IQcX3JoU4lJJ71tq+oZEQTYnBt0zBGdsES9c+adX+7BEtov3k6Nw6hfLycOdhF8jQa2IDH+iYu8Spn\/5m1X9KYzHl\/XN6uWMr2Q8wUegZIzUzdYCgrdKz6VFvPAFhCw5a1FXeN\/F+52NGx6Oy6z7QsedRclLczsoJKfeJBPzTP0TUP72o0RHAYA8S6D\/V2c\/oaSH3W4clWpb+nv2qnSqqP\/uDLYe+U11JCpYNhaawkR\/Umvzc5lYpe3XOhlCvUY8z4vPqKsEEhIYRNXwPSSTQMBH4wi2TQ7BQ9L\/KBjckI\/Za2K1Xxtr0nUUD3QsHhvKBgNq2c+E3fkX1DL8fUdCS3ap6rlntr6fRdBekZRWSjWYYrbmqhG4QHzAJvUByy7WQ0upgr9NlJcnWfsSmNhzEg9+spT9B+OuhG6vLgUXJR\/HACtSQOpudN2JzXaY8RB\/h6FIKDer+iRt1Rm\/0o7XHoSm+0ODtSY8ywi3P5fu6M7hXOD9qOKbLyyEZ+UfCy5UXgSgodFFvth9LaX4+KutWhkVQc1zaz4goqoffca53SI54ZzLUm8rQecSmZfkTC668lW\/tWm6mL9E9u3WcdYpxtWP6Pnav6zIe5z9I5cjoJYG4f24J2Nc1S4jjeoN4vaBKpUVfEokF8l2wIdAqOTN2PyQrlp6EHpP919wS8lXCHppANDq\/Wo8jEY+no\/7bKdtqI8875KRPRce5S7lg+DLwAnSr0xaehYYQqs9gd7nrYAsMcUvfjpGzFKD+zokSfjTky\/y7jADaL4j4kdNTbAbdq9vN9WJK7LPDH"};
                            console.log("OutPutFrom Initialize" + data);
                            alert("Sam Data" + data.message);
                            Confirm(webTrustLicenseKey, userpin, JSON.parse(data.message), useruserid);
                            alert("confirm successfull.");
                            /// loadInner(webTrustLicenseKey, useruserid, userpin);
                            //                   var otp=generateOtp1();
                            //                 console.log("otp"+otp);
                            //                var fruits = ["Banana", "Orange", "Apple", "Mango"];
                            //                var sotp=generateSotp(fruits);
                            //                  var xq= getOtpplus(useruserid);
                            //                console.log("sotp"+xq);


                        }
                    });
                }
            </script>
            <!--END-->


            <!--end-->
            <!--                    <div class="row buttons">-->
            <!--                        <div>-->

            <script src="../../JS/webpush.js" type="text/javascript"></script>
            <table style="align-self: center"><td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input value="Back" class="button_blue" type="submit" id="clear" name="back" tabindex="3">
                </td><td>
                    <input type="button" style="display:none" class="button_red default" id="btnverify" name="btnverify" value="Confirm" onclick="VerifyOtpPush()"/>
                </td></table> <br>
            <div id="votpResult" name="votpResult" style="color:green" style="font: bold" ></div>
            <div id="ConfirmResult" style="display:none">

                <input value="Confirm"  class="button_red default" type="submit" id="cfm" name="insert" tabindex="2"><img id="busy" src="busy.gif">
            </div>
            <!--                        </div>-->
            <!--                    </div>-->
            <div style="display: none;"><input type="hidden" name="_sourcePage" value="tBiqLs0iUtr-Q8DOXz10QeZa6fJHNqj42ZQC12ENHPR4YGLh1XPzgGRDYQhSZ8iIDcypq5A2vyk="><input value="F" type="hidden" name="paymentType"><input value="100002050" type="hidden" name="beneficiaryBank"><input value="641990020014608" type="hidden" name="fromAccountNo"><input value="false" type="hidden" name="registeredBeneAcc"><input value="" type="hidden" name="myBeneAccountNo"><input value="" type="hidden" name="numberOfTransfer"><input value="2014-11-05" type="hidden" name="firstProcessDate"><input value="false" type="hidden" name="beneIdValidation"><input value="" type="hidden" name="clearValues"><input value="" type="hidden" name="beneIdType"><input value="vikram sareen" type="hidden" name="beneficiaryName"><input value="false" type="hidden" name="repeatOption"><input value="Fund Transfer" type="hidden" name="remarks"><input value="" type="hidden" name="beneIdNo"><input value="10RMTransferForDemo" type="hidden" name="paymentDescription"><input value="100002050" type="hidden" name="beneBank"><input value="10" type="hidden" name="amount"><input value="" type="hidden" name="__token"><input value="vikramsareen@gmail.com" type="hidden" name="email"><input value="" type="hidden" name="frequency"><input value="7006608635" type="hidden" name="otherAccount"><input type="hidden" name="__fp" value="-bq4Gu4CAhDS0qSsZNXzqw2oTHXFGH53eb7w8cdF-oSqeHg8LTTfO1upZnCEqeUwbi5Z-r3u3PUStUe-x4Wf2j5Qhz6fj7lwTGX4q_3-AveZr3pFLqzlSXoKWPhJyQ80DSV5DoJIboe0UIMW9vCZEgj4UHOLPfUUHFpPdI2-tomHR_Ia06xPTdZSCijDfVRZ3CApZjmejRBtyeCvcMG3tvCbE9gfTYeHq1VA01DuqKOLnbjJfe0uQFJs0ovow5P0yFi2Y-_vhjcEQqSMZxI2UaHZwaKklI_1YB0sM_z9YpQx0HtB331pO5Idx_ViZbi3QJHdiAVgDGmKdcdEAhVeLPVpau5mmGRrrTzUr0digVshJusMcM0aFZfbsYt95GXO9sEnOpwKKTslO8KGzFbFojgTrjC5BVBPV5--qb6H96BDqtPsz5skCA=="></div></form>
    </div>

    <script>
        window.onload = function () {

            var myVal = "<%=authtype%>";
            if (myVal.valueOf() == "PUSH") {
//                 $("#forwebpushdiv").show(); HideForwebPush
                //btnverify btnGenerateOtp txtotp btnGenerateOtp
                document.getElementById('forwebpushdiv').style.display = 'block';
                document.getElementById('HideForwebPush').style.display = 'none';
                document.getElementById('btnverify').style.display = 'block';
                document.getElementById('ConfirmResult').style.display = 'none';
                document.getElementById('txtotp').style.display = 'block';
                document.getElementById('btnGenerateOtp').style.display = 'block';
            } else {
                document.getElementById('forwebpushdiv').style.display = 'none';
                document.getElementById('btnverify').style.display = 'none';
                document.getElementById('txtotp').style.display = 'none';
                document.getElementById('HideForwebPush').style.display = 'block';
                document.getElementById('ConfirmResult').style.display = 'block';
                document.getElementById('btnGenerateOtp').style.display = 'none';
            }
        }
    </script>
    <!-- start #footer -->
    <div id="footer">
        <div class="compatibleView">Best viewed on Internet Explorer 7 &amp; above, Firefox and Chrome</div>
        <div class="footerlink"><a href="http://#/About-Us/About-the-Website/Privacy-Policy.aspx" target="_blank">Privacy Policy</a> | <a href="http://#/About-Us/About-the-Website/Client-Charter.aspx" target="_blank">Client Charter</a> | <a href="http://#/ABMB/media/MyLibrary/Shared/Files/Customer-Service-Charter.pdf" target="_blank">Customer Service Charter</a> | <a href="http://#/ABMB/media/MyLibrary/ABMB/PDF/aop_tnc.pdf" target="_blank">Terms &amp; Conditions</a></div>
        <div class="copyright">Copyright © 2014 MyBank (Demo site) </div>
    </div>
    <!-- end #footer -->
</div>
<script type="text/javascript">
    $('#container').on('click', 'a[href*=".do"]', function () {
        if (this.target == null || this.target == "") {
            var loadercontent = '<div id="spinner"><img src="/personal/images/ib/ajax-loader.gif" alt="Loading" width="128" height="15" /></div>';
            setTimeout(function () {
                $('#main').html(loadercontent);
            }, 500);
        }
    });
    $('#sidebar').on('click', 'a.expand', function (e) {
        e.preventDefault();
        window.location = $(this).siblings('.div_submenu').find('a').get(0).href;
    });
    var regImage = document.getElementById("showimageWebToken");
    $('#showimageWebToken').click(function (e) {
        var ImgPos = FindPosition(regImage);
        var relX = e.pageX - ImgPos[0];
        var relY = e.pageY - ImgPos[1];
        GetUserPINForWebToken(relX, relY, <%=type%>);
    });
    function fireWhenReady() {
        if (typeof GetWTUser2FA1 != 'undefined') {
            GetWTUser2FA1();
        } else {
            setTimeout(fireWhenReady, 50);
        }
    }
    $(document).ready(fireWhenReady);
</script>

</body></html>
