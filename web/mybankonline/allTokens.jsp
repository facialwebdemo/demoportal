
<%@page import="com.mollatech.web.token.SetAuthType"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails"%>
<%
    int type = (Integer) request.getSession().getAttribute("type");
    AxiomCredentialDetails axiomCredentialDetails = null;
    String strUserId = (String) request.getSession().getAttribute("_username");
    RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
    String strUSerAgent = request.getHeader("User-Agent");
    boolean bIE7 = strUSerAgent.contains("MSIE 7.0");
    boolean flag = false;
    boolean flagoob = false;
    boolean flaghardware = false;
    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
        axiomCredentialDetails = userObj.getTokenDetails().get(i);
        if (axiomCredentialDetails != null) {
            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN
                    && axiomCredentialDetails.getSubcategory() == AxiomWrapper.SOFTWARE_TOKEN
                    && axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {

                flag = true;
                //break;
            }
            if (axiomCredentialDetails.getCategory() == AxiomWrapper.HARDWARE_TOKEN && axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                flaghardware = true;

                // break;
            }
            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN && axiomCredentialDetails.getSubcategory() == AxiomWrapper.OTP_TOKEN_SOFTWARE_MOBILE && axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                flag = true;
            }

            if (axiomCredentialDetails.getCategory() == AxiomWrapper.OUTOFBOUND_TOKEN && axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                flagoob = true;
            }

        }

    }

//    String strUserId = request.getParameter("username");
//    session.setAttribute("_username", strUserId);
%>

<!DOCTYPE html>
<!-- saved from url=(0057)https://#/personal/login/login.do -->
<html lang="en" style="display: block;"><script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script><script type="text/javascript">(function () {
            return window.SIG_EXT = {};
        })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">	
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#001e54">
        <meta name="msapplication-TileImage" content="/personal/images/mstile-144x144.png">
        <meta name="msapplication-config" content="/personal/images/browserconfig.xml">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="application-name" content="MyBankonline">	

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>AMEC</title>

        <link href="./2fa_files/combined.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./2fa_files/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="./2fa_files/ibcommon.js"></script>





        <script type="text/javascript" src="./2fa_files/validation.jsp"></script>
        <script type="text/javascript" src="./2fa_files/jquery.preventDoubleSubmit.js"></script>
        <script type="text/javascript" src="./2fa_files/login.jsp"></script>
        <script type="text/javascript" src="./login_files/login.js"></script>
        <script src="js/DynamicLoader.js"></script>
        <script>


        loadjscssfile("./js/json3.min.js", "js");
        loadjscssfile("./js/jstorage.js", "js");
        loadjscssfile("./js/modernizr.js", "js");
        loadjscssfile("../assets/css/style.css", "css");
        loadjscssfile("../assets/css/div.css", "css");
        loadjscssfile("./js/axiomprotect_wt_reg.js", "js");
        loadjscssfile("./js/axiomprotect_wt.js", "js");
        loadjscssfile("./js/checkwebtoken.js", "js");

        </script>
        <script src="js/sha.js"></script>
        <script src="js/jstorage.js"></script>
        <script src="js/crypto.js"></script>
        <script src="js/aes.js"></script>
        <script type="text/javascript">

        var tokenType = "";
        function getOOBOTP()
        {
            var datatoSend = {"tokenType": tokenType};
            $.ajax({
                type: 'POST',
                url: './seneOOBOTP',
                dataType: 'json',
                data: datatoSend,
                success: function (data) {
                    alert(data._message);
                }
            });
        }
        function ActivateOOB()
        {
            var s = './activateOOBtoken';
            tokenType = 9;

            var datatoSend = {"tokenType": tokenType};
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: datatoSend,
                success: function (data) {
                    alert(data._message);
                    window.location.href = "./index.jsp";
                }
            });
        }
        function goForOOBTOken()
        {
            document.getElementById("regOOBTOKEN").style.display = "block";
            document.getElementById("otpVerification").style.display = "none";
            document.getElementById("regMobileToken").style.display = "none";
            document.getElementById("registerdiv").style.display = "none";
        }
        function goForHardWare()
        {

            document.getElementById("regHardwareToken").style.display = "block";
            document.getElementById("otpVerification").style.display = "none";
            document.getElementById("regMobileToken").style.display = "none";
            document.getElementById("regOOBTOKEN").style.display = "none";
            document.getElementById("registerdiv").style.display = "none";
        }
        function goForSoftware()
        {
            tokenType = 8;
            document.getElementById("regMobileToken").style.display = "block";
            document.getElementById("otpVerification").style.display = "none";
            document.getElementById("regOOBTOKEN").style.display = "none";
            document.getElementById("registerdiv").style.display = "none";
            
        }

        function ActivateHardwareToken()
        {
            var s = './assignHardwareToken';
            var srno = document.getElementById("txtSerial").value;
            var otp = document.getElementById("otpact").value;

            txtSerial
            var datatoSend = {"tokenType": tokenType, "srno": srno, "otp":otp};
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: datatoSend,
                success: function (data) {
                    alert(data._message);
                    window.location.href = "./index.jsp";
                }
            });

        }
        function getRegCode()
        {
            var s = './AssignAndSendRegCode';
            var datatoSend = {"tokenType": tokenType};
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: datatoSend,
                success: function (data) {

                    alert(data._message);

                }
            });

        }
        if (self == top) {
            document.documentElement.style.display = 'block';
        }
        else {
            top.location = self.location;
        }
        $(function () {
            $('a.notice_title').click(function () {
                $(this).siblings('div.notice_content').slideToggle('fast');
                return false;
            });
        });
        </script> 
        <style type="text/css">
            html, .notice_content { display: none; }
            .banner { margin-bottom:18px; }
            .loginbuttons { margin:5px 0; }
        </style>
    </head>
    <body>

<!--        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>-->

        <div id="container">
            <div id="content">
                <div class="banner">

                    
                    <a href="#">
               <img src="icons/SCB_echannels_internetbanking_web.jpg" width=100%" alt="Banner"/>
<!--                        <img src="icons/SCB_echannels_internetbanking_web" alt="Banner"/>-->
<!--                        <img src="icons/mollatech.jpg" alt="Banner">-->
                    </a>


                </div>


                <form  autocomplete="off" class="txnform once" name="loginForm" id="loginForm">
                    <input type="hidden" name="step3" value="">

                    <div id="logincontent">

                        <div class="center">
                            <div class="box_mask">
                                <div class="box box_gradient">
                                    <div class="error" style="margin:0;">
                                        <input type="hidden" id="authType" name="authType" value="<%=type%>">
                                        <!--nilesh-->
                                        <%//               rssUserObj.getTokenDetails();
                                            //        AxiomCredentialDetails axiomCredentalOOBDetails = null;
                                            //        AxiomCredentialDetails axiomCredentalSWDetails = null;
                                            //        AxiomCredentialDetails axiomCredentalHWDetails = null;
                                            //        AxiomCredentialDetails axiomCredentalCERTBDetails = null;
                                            //        AxiomCredentialDetails axiomCredentalCRDetails = null;
                                            //        AxiomCredentialDetails axiomCredentalHWPKIDetails = null;
                                            //        AxiomCredentialDetails axiomCredentalSWPKIDetails = null;
                                            //        AxiomCredentialDetails axiomCredentalSecurePhrase = null;
                                            //        String sessionId = (String) request.getSession().getAttribute("_userSessinId");
                                            ////         String strUserId = (String) request.getSession().getAttribute("_username");
                                            //        AxiomWrapper axWraper = new AxiomWrapper();
                                            //        RssUserCerdentials rssUserObj = (RssUserCerdentials) session.getAttribute("_rssDemoUserCerdentials");
                                            //        if(rssUserObj != null){
                                            //        RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, rssUserObj.getRssUser().getUserName(), AxiomWrapper.GET_USER_BY_FULLNAME, null);
                                            //      if(userObj != null){
                                            //        session.setAttribute("_rssUserCerdentials", userObj);
                                            //        if (userObj.getTokenDetails() != null) {
                                            //            for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                                            //                AxiomCredentialDetails axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                            //                if (axiomCredentialDetails != null) {
                                            //                    if (axiomCredentialDetails.getCategory() == AxiomWrapper.SECURE_PHRASE) {
                                            //                        axiomCredentalSecurePhrase = axiomCredentialDetails;
                                            //                    }
                                            //                }
                                            //            }
                                            //        }
                                            //      }
                                            //        }

                                        %>

                                        <!--nilesh-->

                                    </div>
                                    <div class="welcometitle">Welcome to <strong> MyBank</strong>online</div>

                                    <div class="row norwd">
                                        <label>Username</label>
                                        <div><%=strUserId%></div>
                                    </div>



                                    <div class="row">

                                        <!--<div><img src="./2fa_files/render_dynamic_img(1).do" alt="Secure Phrase"></div>-->

                                        <!--<img src="userportal1.png" width="400" height="300" alt="" id="myImgId" />-->
                                        <!--        <p>X:<span id="x"></span></p>
                                                <p>Y:<span id="y"></span></p>-->
                                    </div>


                                    <!--                                    <div class="row">
                                                                            <div style="width:100%;margin-bottom:5px;"><img id="knight" src="./2fa_files/A.Knight 300x110.gif" alt="A.Knight"></div>
                                                                        </div>-->










                                    <div class="row" id="regMobileToken" style="display:none">
                                        <label>Download Application from this Link:</label>
                                        <table>
                                            <tr>
                                                <td>
                                                    
                                                    <a href="https://itunes.apple.com/us/app/axiom-otp-token/id554871445?mt=8"><img src="apple.ico"></a>
                                                    
                                                    <a href="https://play.google.com/store/apps/details?id=com.ideasventure.otptoken&hl=en"><img src="android.ico"></a>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="button" type="submit" value="Get registration Code" onclick="getRegCode()"/>                        
                                                </td>

                                            </tr>

                                        </table>


                                    </div>
                                    <div class="row" id="regHardwareToken" style="display:none">
                                        
                                        
                                        <label>Enter Serial No:</label>
                                        <div>
                                            <input type="text" id="txtSerial" name="txtSerial"/>
                                        </div>  
                                   
                                        <label>Enter OTP:</label>
                                        <div>
                                            <input type="password" id="otpact" name="otpact"/>
                                        </div>  
                                        <div>
                                    <input type="button" type="submit" value="Activate" onclick="ActivateHardwareToken()"/>                        
                                        </div>
<!--                                        <table>
                                            <tr>
                                                <td>
                                                    
                                                    Enter Serial No:

                                                </td>
                                                <td>
                                                    <input type="text" id="txtSerial" name="txtSerial"/>

                                                </td>
                                            </tr><tr>
                                                <td>
                                                    Enter OTP:

                                                </td>
                                                <td>
                                                    <input type="text" id="otpact" name="otpact"/>

                                                </td>

                                            </tr>
                                            <tr><td></td>
                                                <td>
                                                    <input type="button" type="submit" value="Activate" onclick="ActivateHardwareToken()"/>                        
                                                </td>

                                            </tr>

                                        </table>
-->

                                    </div>
                                    <div class="row" id="regOOBTOKEN" style="display:none">
                                        <input type="button" id="btnActivateOOB" name="btnActivateOOB" value="Activate OOB Email" onclick="ActivateOOB()"/>
                                    </div>


                                    <div id="otpVerification">
                                        <div class="row">
                                            Select Your Token  :
                                            <div class="inline">
                                            <select id="tokenTypeuse" name="tokenTypeuse">
                                                <%if(flaghardware==true){%>
                                                <option value="Hardware" id="hardwareoption" >Hardware</option>
                                                <%}if(flag==true){%>
                                                <option value="Mobile" id=""mobileoption >Mobile</option>
                                                <%}if(flagoob==true){%>
                                                <option value="OOB" id="ooboption" >OOB</option>

                                                <%
                                                    }
                                                %>
                                            </select>  
                                        </div>
                                        </div>
                                        <div class="row">

                                            <label for="password"><font color="blue">One Time Password</font></label>

                                            <div class="inline">
                                                <input size="15" maxlength="32" type="password" name="password" id="password" tabindex="1" />
                                                <input type="button" id="btngetOOB" name="btngetOOB" value="GET OTP(Out of Band)" onclick="getOOBOTP()"/>
                                            </div>
                                        </div>


                                    </div>

                                    <script>


                                    </script>
                                    <div class="row" id="passblock" style="display:none">
                                        <label for="password">Password<font color="blue">/One Time Password</font></label>
                                        <div>
                                            <input size="29" maxlength="32" type="password" name="password" id="password" tabindex="1">
                                        </div> 

                                    </div>
                                    <div class="row" id="registerdiv">
                                        <label for="password"><font color="blue">Register For: </font></label>
                                            <%if (flag == true && type == SetAuthType.WEBTOKEN) {%>

                                        <%}%>
                                        <%if (flag == false) {%>
                                        <div>
                                            <div class="row" id="regMain">
                                                <table>
                                                    <tr>

                                                        <td>
                                                            <a href="#/" onclick="goForSoftware()">Register for SoftwareToken(Mobile) </a>                     
                                                        </td>

                                                    </tr>
                                                </table>
                                                <%}
                                                    if (flaghardware == false) {%>
                                                <table>     
                                                    <tr>

                                                        <td>
                                                            <a href="#/" onclick="goForHardWare()">HardwareToken</a>                   
                                                        </td>

                                                    </tr>
                                                </table>
                                                <%}
                                                    if (flagoob == false) {%>
                                                <table>
                                                    <tr>

                                                        <td>
                                                            <a href="#/" onclick="goForOOBTOken()">OOBTOKEN</a>                   
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%}%>
                                            </div>
                                        </div>
                                            </div>
                                            <div id="login-result"></div>

                                            <div class="row buttons" id="loginbuttons">
                                                <div>
                                                    <input value="Clear" class="button_blue" onclick="return fnClear(this.form);" type="button" id="clear" name="clear" tabindex="3">
                                                    <!--<input value="Login" class="button_red" onclick="return validateSubmit2(this.form);" type="submit" id="step3" name="step3" tabindex="2"><img id="busy" src="./2fa_files/busy.gif">-->
                                                    <input value="Login" class="button_red" onclick="return geologins(this.form);" type="button" id="step3" name="step3" tabindex="2"><img id="busy" src="./2fa_files/busy.gif">
                                                </div>


                                            </div>

<!--                                            <div class="row buttons" id="loginbuttons">
                                                <label for="password">Attention:</label>
                                                <div>
                                                    Additional Geo-Fencing Tracking is activated and introduce "Where you are?" protection.
                                                </div>  
                                            </div>-->
                                        </div>
                                    </div>

                               
                             
<!--                            <div class="box">
                                <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/error.png&#39;);">Security Alert</div>
                                <ul class="bullet_square">

                                    <li>Always verify your MyBankonline Secure Phrase before you login!</li>

                                </ul>
                            </div>-->
                                            </div> 
                        <!--</div>-->
           

<!--                        <div class="right">
                            <div class="box">
                                <div id="assistance"><a href="mailto:info@MyBankfg.com"><img src="./2fa_files/banner_need_assistant.png" alt="Email us"></a></div>
                                <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/key.png&#39;);">Registration &amp; Login</div>
                                <ul class="bullet_square">
                                    <li><a class="current" href="https://#/personal/register/register.do">First Time Registration</a></li>
                                    <li><a href="http://#/Personal-Banking/eChannels/MyBankonline#firsttimeregistration-tab" target="_blank">How to Register?</a></li>
                                    <li><a class="current" href="https://#/personal/reset/forgot_id_or_password.do">Forgot Username / Password</a></li>
                                </ul>
                                <br>
                                <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/world_link.png&#39;);">Other Links</div>
                                <ul class="bullet_square">
                                    <li><a target="_blank" href="https://#/personal/rate_charges/e_rate_unsecured_view.do">View e-Rates</a></li>
                                </ul>
                            </div>

                            <div class="box">
                                <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/note.png&#39;);">Notices</div>
                                <ul class="bullet_square">


                                    <li>
                                        <a class="notice_title" href="https://#/personal/login/login.do#">System Downtime Notice</a>
                                        <div class="notice_content">
                                            Dear Valued Customer<br><br>

                                            There will be system downtime due to scheduled maintenance <br>

                                            21 Oct 2014 5.00am to 6.00am <br><br>

                                            We sincerely apologise for any inconvenience caused. <br><br>

                                            Thank you. <br>

                                        </div>
                                    </li>



                                    <li>
                                        <a class="notice_title" href="https://#/personal/login/login.do#">Hottest e-Rates in town!</a>
                                        <div class="notice_content">
                                            #Besurprised with MyBank XChange Hot e-Rates promotion! Save more today in the currency of your choice while the rates are hot! Limited offer only from 9.30am  12.00noon daily. Available until 28 November 2014. 

                                            For more information on MyBank XChange, visit http://#/MyBankXChange

                                            Thank you.
                                        </div>
                                    </li>



                                    <li>
                                        <a class="notice_title" href="https://#/personal/login/login.do#">DBKL Payment</a>
                                        <div class="notice_content">
                                            Dear Valued Customer

                                            Please be informed that Dewan Bandaraya Kuala Lumpur (DBKL) is currently updating to a new account number format. MyBankonline is in the upgrade process to accept the new revised account number format. In the meantime, kindly key in the old account number for DBKL payment via MyBankonline. 
                                            We will notify you in due course once the upgrade is completed. 
                                            We apologise for any inconvenience caused.
                                            Thank you.

                                        </div>
                                    </li>



                                    <li>
                                        <a class="notice_title" href="https://#/personal/login/login.do#">DiGi Bill Payment</a>
                                        <div class="notice_content">
                                            Dear Valued Customers, <br>
                                            Please be informed that DiGi Telecommunications Sdn Bhd (DiGi) is changing to a new account number format. All DiGi postpaid subscribers are required to pay their bills using the new DiGi bill account numbers effective 1 October 2014 onwards. The old bill account number will no longer be valid from this date. Please recreate your scheduled and favourite DiGi bill payment using your new DiGi bill account number to avoid any payment interruption. <br>Thank you. 

                                        </div>
                                    </li>



                                    <li>
                                        <a class="notice_title" href="https://#/personal/login/login.do#">Schedule of IBG Funds Received by Beneficiary</a>
                                        <div class="notice_content">
                                            <p> New! Schedule of Funds received by beneficiary. </p>



                                            <p>Click <a href="http://#/Schedule-of-Funds-received-by-beneficiary" target="_blank"> here to view  </a>

                                            </p></div>
                                    </li>


                                </ul>
                            </div>
                        </div>-->

                    </div>
                    <div style="display:none">
                        X:<span id="x"></span>
                        Y:<span id="y"></span>
                    </div>
                    <div style="display: none;"><input type="hidden" name="_sourcePage" value="N0txKETQlHr-Q8DOXz10QWlhYwprz9dNyx3-iYXi4HrKkDR2Ez-bsA=="><input type="hidden" name="__fp" value="TLk3ypLivr0="></div></form>

            </div>
            <!-- start #footer -->
            <div id="footer">
                <div class="compatibleView">Best viewed on Internet Explorer 7 &amp; above, Firefox and Chrome</div>
                <div class="footerlink"><a href="#" target="_blank">Privacy Policy</a> | <a href="#" target="_blank">Client Charter</a> | <a href="http://#/ABMB/media/MyLibrary/Shared/Files/Customer-Service-Charter.pdf" target="_blank">Customer Service Charter</a> | <a href="http://#/ABMB/media/MyLibrary/ABMB/PDF/aop_tnc.pdf" target="_blank">Terms &amp; Conditions</a></div>
                <div class="copyright">Copyright © 2014 MyBank (Demo site) </div>
            </div>
            <!-- end #footer -->
        </div>

        <style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>