<%@page import="com.mollatech.web.token.SetAuthType"%>
<html>
    <%
        int type = (Integer) request.getSession().getAttribute("type");
    %>
    <head>
        <script src="js/DynamicLoader.js"></script>
        <script src="js/jquery.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script>
            loadjscssfile("./js/json3.min.js", "js");
            loadjscssfile("./js/jstorage.js", "js");
            loadjscssfile("./js/modernizr.js", "js");
            loadjscssfile("../CSS/style.css", "css");
            loadjscssfile("../CSS/div.css", "css");
            loadjscssfile("./js/axiomprotect_wt_reg.js", "js");
            loadjscssfile("./js/axiomprotect_wt.js", "js");
            loadjscssfile("./js/checkwebtoken.js", "js");

        </script>
    </head>
    <body>
        <div id="2FAPageContent">

        </div>
        <script>
            var urlGetWTUser2FA = "./GetWebTokenUser";
            var url2FAInlined = "./2fa.jsp";//  
            <%if (type == SetAuthType.WEBTOKEN) {%>
            var urlRegisterInlined = "./register_inlined.jsp";
            <%}%>
            
           


            function fireWhenReady() {
                if (typeof GetWTUser2FA != 'undefined') {
                    GetWTUser2FA(<%=type %>);
                }
                else {
                    setTimeout(fireWhenReady, 50);
                }
            }
            $(document).ready(fireWhenReady);
        </script>
    </body>
</html>