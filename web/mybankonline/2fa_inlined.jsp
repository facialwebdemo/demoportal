﻿


<%
    String strUSerAgent = request.getHeader("User-Agent");
    boolean bIE7 = strUSerAgent.contains("MSIE 7.0");

%>

<!DOCTYPE html>


<html>
    <head>
        <%--  <script src="js/DynamicLoader.js"></script>--%>
        <script src="js/sha.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/jstorage.js"></script>
        <script src="js/crypto.js"></script>
        <script src="js/axiomprotect_wt.js"></script>
        <script src="js/aes.js"></script>
        <%-- <script src="<%=BankAwayRetail.Global.Web_Path%>/web/L001/retail/jsp/axiomprotect2/Webtoken/js/jquery.js"></script>
         <script src="<%=BankAwayRetail.Global.Web_Path%>/web/L001/retail/jsp/axiomprotect2/Webtoken/js/crypto.js"></script>
          <script src="<%=BankAwayRetail.Global.Web_Path%>/web/L001/retail/jsp/axiomprotect2/Webtoken/js/jstorage.js"></script>
         <script src="<%=BankAwayRetail.Global.Web_Path%>/web/L001/retail/jsp/axiomprotect2/Webtoken/js/aes.js"></script>
         <script src="<%=BankAwayRetail.Global.Web_Path%>/web/L001/retail/jsp/axiomprotect2/Webtoken/js/axiomprotect_wt.js"></script>
         <script src="<%=BankAwayRetail.Global.Web_Path%>/web/L001/retail/jsp/axiomprotect2/Webtoken/js/sha.js"></script>
          <link href="<%=BankAwayRetail.Global.Web_Path%>/web/L001/retail/jsp/axiomprotect2/Webtoken/div.css" rel="stylesheet" />--%>
        <%-- <script src="js/jquery.js"></script>
         <script src="js/crypto.js"></script>
         <script src="js/jstorage.js"></script>
         <script src="js/aes.js"></script>
         <script src="js/sha.js"></script>
         <script src="js/axiomprotect_wt.js"></script>--%>
        <link href="../assets/css/div.css" rel="stylesheet" />
    </head>
    <body>

        <TABLE cellSpacing=1 cellPadding=1 width=965 border=0>
            <TBODY>
                <TR>
                    <TD   style="PADDING-BOTTOM: 10px; PADDING-TOP: 10px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px" align=left>
                        <IMG border=0 alt="Axis Bank"
                             <IMG border=0 alt="Axis Bank" 
                             src="../assets/img/logo.gif">   </TD>
                    <TD 
                        style="PADDING-BOTTOM: 10px; PADDING-TOP: 10px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px" 
                        align=right>
                    </TD></TR>
                <TR>
                    <TD class=footLine vAlign=top width="100%" colSpan=2 align=left><IMG 
                            alt="" src="../assets/img/spacer.gif" width=1 
                            height=2></TD></TR></TBODY></TABLE>

        <br/>

        <%                            //formAction = "tranHttpHandler.aspx";
            //urlParam = "Action.En2FA.AxiomProtect2.WEB.login.VerifyCredential=Y";
            //formUrl = URLEncoderDecoder.encodeURL(formAction, urlParam, cm, "N");
            //string urlToVerifyOTP = formUrl; //"./VerifyCredential.aspx";
        %>

        <form id="VerifyCredentialFORM" name="VerifyCredentialFORM"  action="VerifyCredential.aspx" method="post">

            <div id="mainContainerWP" align="center">

                <div id="divActivateWebPin" style="text-align:center" >
                    <table style="width: 100%">
                        <tr>
                            <td style="font-size: 14px; font-family: Calibri,helvetica,sans-serif">&nbsp;&nbsp;  <b>Web PIN</b>
                                <hr/>
                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td style="text-align: left; font-size: 14px; font-family: Calibri,helvetica,sans-serif">* Use your mouse on the PINPAD to set your PIN and click on the green button.
                                * Your PIN should be minimum 4 digits long and not more than 8 digits. Click on 'Green' button after setting the PIN.

                            </td>

                        </tr>
                        <tr>
                            <td></td>

                        </tr>
                        <tr>
                            <td></td>

                        </tr>
                        <tr>
                            <td>
                                <div class="image" style="text-align:center">



                                    <img id="showimageWebToken" 
                                         src="AxisBankToken.png"/>

                                    <style type="text/css">
                                        .image {
                                            position:relative;
                                        }
                                        <% if (bIE7 == true) { %>

                                        .image .text {
                                            position:absolute;
                                            top:40px;
                                            left:120px;

                                        }

                                        <% } else { %>
                                        .image .text {
                                            position:absolute;
                                            top:35px;
                                            left:110px;
                                        }
                                        <% }%> %>
                                    </style>

                                    <div class="text">
                                        <h3>
                                            <p id="Tz">ENTER PIN</p>
                                        </h3>
                                    </div>
                                    <div style ="display:none">
                                        X:<p id="Tx"></p>
                                        Y:<p id="Ty"></p>
                                        Digit:<p id="DigitSelected" ></p>
                                    </div>
                                </div>

                                <script>

                                    var regImage = document.getElementById("showimageWebToken");
                                    $('#showimageWebToken').click(function (e) {
                                        var ImgPos = FindPosition(regImage);
                                        var relX = e.pageX - ImgPos[0];
                                        var relY = e.pageY - ImgPos[1];
                                        GetUserPINForWebToken(relX, relY);
                                    });
                                </script>
                            </td>

                        </tr>

                    </table>

                    <table style="width: 100%">
                        <tr>

                            <td>
                                <a href="#"   id="tokenDereg" style="color:#C60F52"   onclick="deregisterToken()">Deregister This Computer</a>

                            </td>

                        </tr>        
                        <tr>

                            <td >
                                <hr/>

                            </td>
                        </tr>

                    </table>


                    <table style="width: 100%">
                        <tr>

                            <td style="font-size: 14px; font-family: Calibri,helvetica,sans-serif">

                                &nbsp;&nbsp;&nbsp; NETSECURE Code:
                            </td>
                            <td>
                                <input id="_otp" name="_otp"  type="text" />
                            </td>
                        </tr>

                    </table>

                    <div style=" bottom:0; width:auto;height:auto">
                        <table style="width:100%">
                            <tr>
                                <td><br/><br/><br/> </td>
                            </tr>
                            <tr>
                                <td style="width:50%;text-align:right">			
                                </td>

                                <td style="width:45%;text-align:right">

                                    <input id="_gotoLogin" name="_gotoLogin" class="but" value="Secure Login" type="submit" />
                                </td>
                                <td style="width:5%;text-align:right">&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>

                        </form>
                    </div>



                </div>


            </div>

    </body>

</html>