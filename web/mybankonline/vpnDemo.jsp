<!DOCTYPE html>
<!-- saved from url=(0057)https://#/personal/login/login.do -->
<html lang="en" style="display: block;"><script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script><script type="text/javascript">(function() {
            return window.SIG_EXT = {};
        })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">	
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#001e54">
        <meta name="msapplication-TileImage" content="/personal/images/mstile-144x144.png">
        <meta name="msapplication-config" content="/personal/images/browserconfig.xml">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="application-name" content="MyBankonline">	

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


        <title>MyBankonline</title>


        <link href="./login_files/combined.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./login_files/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="./login_files/ibcommon.js"></script>
        <script type="text/javascript" src="./login_files/validation.jsp"></script>
        <script type="text/javascript" src="./login_files/jquery.preventDoubleSubmit.js"></script>
        <script type="text/javascript" src="./login_files/jquery.simplemodal.1.4.4.min.js"></script>
        <script type="text/javascript" src="./login_files/login.js"></script>
        <script src="js/mcColorPicker.js" type="text/javascript"></script>
        <script src="../assets/js/securephrase.js"></script>


        <style type="text/css">
            html, .notice_content { display: none; }
            .banner { margin-bottom:18px; }
            .alertcontent .box { text-align:left; margin:15px 20px 0; padding:5px 20px; color:#444; }
            .alertcontent .box ul { margin-left:20px; }
            .mobi #simplemodal-container { height:auto !important; }
        </style> 
        <script type="text/javascript">
        browserInfo();
        var securityPopup = false;


        securityPopup = true;


        if (self == top) {
            document.documentElement.style.display = 'block';
        }
        else {
            top.location = self.location;
        }

        function countdown(secs) {
//            var btn = document.getElementById('alertclose');
//            btn.value = 'Please wait... (' + secs + ')';
//            if (secs < 1) {
//                clearTimeout(timer);
//                btn.disabled = false;
//                btn.value = 'OK';
//            }
//            secs--;
//            var timer = setTimeout('countdown(' + secs + ')', 1000);
        }

        $(function () {
        $('a.notice_title').click(function () {
        $(this).siblings('div.notice_content').slideToggle('fast');
                return false;
        });
                var currentDomain = window.location.hostname;
                var referrerDomain = document.referrer.split('/')[2];
                if (!!referrerDomain && referrerDomain.indexOf(':') > 0) {
        referrerDomain = referrerDomain.split(':')[0];
        }

        if (securityPopup == true && currentDomain !== referrerDomain) {
//                $('#securityalert').modal({
//                    opacity: 70,
//                    autoResize: true,
//                    onOpen: function (dialog) {
//                        dialog.overlay.fadeIn('fast', function () {
//                            dialog.data.hide();
//                            dialog.container.fadeIn('slow', function () {
//                                dialog.data.fadeIn('fast', function () {
//                                    $('#simplemodal-container').css('height', 'auto');
//                                    $.modal.setPosition();
//                                });
//                            });
//                        });
//                    },
//                    onShow: function (dialog) {
////                        countdown(4);
//                    },
//                    onClose: function (dialog) {
//                        dialog.container.fadeOut('slow', function () {
//                            dialog.overlay.fadeOut('fast', function () {
//                                $.modal.close();
//                                $('#username').focus();
//                            });
//                        });
//                    }
//                });
//            }
        });
                function deleteCookie(c_name) {
                document.cookie = encodeURIComponent(c_name) + "=deleted; expires=" + new Date(0).toUTCString();
                }

        $(function () {
        deleteCookie("k");
                var k = null;
                if (k != null) {
        document.cookie = "k=" + k;
        }
        });</script>
    </head>
    <body>

        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>

        <div id="container">
            <div id="content">
                <div id="securityalert">
                    <center><img src="./login_files/scam.png" alt="Security Alert" style="margin-bottom:20px;"></center>
                    <div class="alertclose">
                        <input type="button" id="alertclose" class="simplemodal-close" value="OK">
                    </div>
                </div>


                <div class="banner">


                    <a href="#" target="_blank">
                        <img src="icons/SCB_echannels_internetbanking_web.jpg" width=100%" alt="Banner"/>
                    </a>


                </div>

                <form method="post" action="2fa.jsp" autocomplete="off" class="txnform once" name="userForm" id="userForm">
                    <input type="hidden" name="step2" value="">
                    <input id="_apXCordinate" name="_apXCordinate" type="hidden" value=""/>
                    <input id="_apYCordinate" name="_apYCordinate" type="hidden" value=""/>
                    <form  autocomplete="off" action="" class="txnform once" name="securePhraseForm" id="securePhraseForm">
                        <div id="logincontent">

                            <div >
                                <div class="box_mask">
                                    <div class="box box_gradient">
                                        <div class="error" style="margin:0;">


                                        </div>
                                        <!--<div class="welcometitle" style="text-align: center"> SSH Plugin Authentication Module Demo</div>-->
                                        <!--<div class="row">-->                                         
                                                                                <div class="row" style="text-align: center">                                          <label for="imgReg">Configure your Secure Phrase</label>
                                                                                    <b/>
                                                                                    <iframe  width="640" height="315" src="https://www.youtube.com/embed/XI7V4kg8OSI" frameborder="0" allowfullscreen align="middle"></iframe>   
                                                                                </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="box">
                            <div class="box_title" style="background-image:url(&#39;/personal/images/ib/icons/error.png&#39;);">Security Alert</div>
                            <ul class="bullet_square">

                                <li>Always verify your MyBankonline Secure Phrase before you login!</li>

                            </ul>
                        </div>
                        </div>

                        <!--                         
                        </div>
                    </form>
                    <div style="display: none;"><input type="hidden" name="_sourcePage" value="WBezwwSnZr3-Q8DOXz10QWlhYwprz9dNv5_maqamTOXKkDR2Ez-bsA=="><input type="hidden" name="__fp" value="W5QXcMGW308="></div>
                </form>
            </div>
                        <!-- start #footer -->
                        <div id="footer">
                            <div class="compatibleView">Best viewed on Internet Explorer 7 &amp; above, Firefox and Chrome</div>
                            <div class="footerlink"><a href="http://#/About-Us/About-the-Website/Privacy-Policy.aspx" target="_blank">Privacy Policy</a> | <a href="http://#/About-Us/About-the-Website/Client-Charter.aspx" target="_blank">Client Charter</a> | <a href="http://#/ABMB/media/MyLibrary/Shared/Files/Customer-Service-Charter.pdf" target="_blank">Customer Service Charter</a> | <a href="http://#/ABMB/media/MyLibrary/ABMB/PDF/aop_tnc.pdf" target="_blank">Terms &amp; Conditions</a></div>
                            <div class="copyright">Copyright © 2014 MyBank (Demo site) </div>
                        </div>
                        <!-- end #footer -->
                        </div>
                        <style>
                            .innerr {
                                display: inline-block;
                                vertical-align: middle;
                                background: yellow;
                                padding: 3px 5px;
                            }
                            .block4 {
                                /*        //color:#fff;background-color:#337ab7;*/
                                box-shadow: 5px 5px 5px #888888;
                                line-height: 90px;
                                width:250px;
                                margin-left:10px;
                                margin-top: 20px;
                            }
                            .inner4 {
                                line-height: normal; /* Reset line-height for the child. */
                                background: none;
                            }
                            #square {
                                width: 900px;
                                height: 900px;
                                background: red;
                            }
                            div.relative1 {
                                margin: 1%;
                                position: relative;
                                height: 100%;
                                border: 2px solid lightblue;
                                box-shadow: 5px 5px 5px #888888;
                                line-height: 100%;     
                            } 

                            div.absolute {
                                position: absolute;
                                top: 5%;
                                line-height: 100%;
                                bottom: 10%;
                                left: 27%;
                                width: 73%;
                                height: 70%;
                                font-size:100%;
                            }
                            .footerLink{
                                margin-top: 14%;
                                margin-left: 0%;
                                text-align: center;
                                line-height: 200%;              
                                background-color:darkcyan;                   
                            }
                            .footerMargin{
                                margin-bottom: 0%; 
                                color: white;
                            }
                            .slick-next {
                                border: 1px solid lightblue;
                                width: 20%;
                                height: 40%;
                                text-align: center;
                                margin: 5%;
                                margin-top: 7%;
                                background-color:lightblue;

                            }
                            .slick-next:after {
                                display: inline-block;
                                vertical-align: middle;
                                content: "";
                                height: 135%;
                            }
                        </style>

                        <style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>