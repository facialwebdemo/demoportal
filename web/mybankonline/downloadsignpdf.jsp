<%-- 
    Document   : signpdfreport
    Created on : Jan 25, 2016, 8:37:26 PM
    Author     : mohanish
--%>

<%@page import="java.io.File"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.awt.image.BufferedImage"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="com.mollatech.axiom.bridge.crypto.LoadSettings"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en" style="display: block;"><script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script><script type="text/javascript">(function() {
            return window.SIG_EXT = {};
        })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">	
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="https://#/personal/images/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#001e54">
        <meta name="msapplication-TileImage" content="/personal/images/mstile-144x144.png">
        <meta name="msapplication-config" content="/personal/images/browserconfig.xml">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="application-name" content="MyBankonline">	

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


        <title>MyBankonline</title>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp"></script>
        <!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
        <link href="./login_files/combined.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./login_files/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="./login_files/ibcommon.js"></script>
        <script type="text/javascript" src="./login_files/validation.jsp"></script>
        <script type="text/javascript" src="./login_files/jquery.preventDoubleSubmit.js"></script>
        <script type="text/javascript" src="./login_files/jquery.simplemodal.1.4.4.min.js"></script>
        <script type="text/javascript" src="./login_files/login.js"></script>
        <!--Newly added-->
        <script src="../assets/js/jquery.js"></script>
        <script src="../assets/js/pdfsigning.js"></script>
        <script src="../assets/js/json_sans_eval.js"></script>
        <script src="../assets/js/bootbox.min.js"></script>
        <script src="../assets/js/ajaxfileupload.js"></script>
        <script src="../assets/js/bootstrap-fileupload.js"></script>   
        <script src="../assets/js/bootstrap-transition.js"></script>
        <script src="../assets/js/bootstrap-alert.js"></script>
        <script src="../assets/js/bootstrap-modal.js"></script>
        <script src="../assets/js/bootstrap-dropdown.js"></script>
        <script src="../assets/js/bootstrap-scrollspy.js"></script>
        <script src="../assets/js/bootstrap-tab.js"></script>
        <script src="../assets/js/bootstrap-tooltip.js"></script>
        <script src="../assets/js/bootstrap-popover.js"></script>
        <script src="../assets/js/bootstrap-button.js"></script>
        <script src="../assets/js/bootstrap-collapse.js"></script>
        <script src="../assets/js/bootstrap-carousel.js"></script>
        <script src="../assets/js/bootstrap-typeahead.js"></script>
        <script src="../assets/js/bootstrap-datepicker.js"></script>


        <script src="../assets/js/json_sans_eval.js"></script>
        <script src="../assets/js/bootbox.min.js"></script>
        <script src="../assets/js/channels.js"></script>
        <script src="../assets/js/operators.js"></script>
        <!--<script src="../assets/js/jquery.sidr.min.js"></script>-->

        <link href="../assets/css/bootstrap.css" rel="stylesheet">
        <link href="../assets/css/bootstrap-fileupload.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
            /*    div {
                    margin-top:1em;
                    margin-bottom:1em;
                }
                input {
                    padding: .2em;
                    margin: .2em;
                }
                select {
                    padding: .2em;
                    margin: .2em;
                }
            */  

            #signatureparent {
                color:darkblue;

            }
            #signature {
                alignment-adjust: central;
                width:25%;
                hight:25%;
                border: 2px dotted black;
                background-color:lightgrey;
            }
            /*
            */    
            html.touch #content {
                alignment-adjust: central;
                float:center;
                width:25%;
                hight:20%;
            }
            /*    html.touch #scrollgrabber {
                    float:right;
                    width:4%;
                    margin-right:2%;
                    background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAFCAAAAACh79lDAAAAAXNSR0IArs4c6QAAABJJREFUCB1jmMmQxjCT4T/DfwAPLgOXlrt3IwAAAABJRU5ErkJggg==)
                }
                html.borderradius #scrollgrabber {
                    border-radius: 1em;
                }*/
        </style>
        <!--Newly added-->
        <style type="text/css">
            html, .notice_content { display: none; }
            .banner { margin-bottom:18px; }
            .alertcontent .box { text-align:left; margin:15px 20px 0; padding:5px 20px; color:#444; }
            .alertcontent .box ul { margin-left:20px; }
            .mobi #simplemodal-container { height:auto !important; }
        </style> 
        <script type="text/javascript">
        browserInfo();
        var securityPopup = false;


        securityPopup = true;


        if (self == top) {
            document.documentElement.style.display = 'block';
        }
        else {
            top.location = self.location;
        }

        function countdown(secs) {
            var btn = document.getElementById('alertclose');
            btn.value = 'Please wait... (' + secs + ')';
            if (secs < 1) {
                clearTimeout(timer);
                btn.disabled = false;
                btn.value = 'OK';
            }
            secs--;
            var timer = setTimeout('countdown(' + secs + ')', 1000);
        }

        $(function() {
            $('a.notice_title').click(function() {
                $(this).siblings('div.notice_content').slideToggle('fast');
                return false;
            });

            var currentDomain = window.location.hostname;
            var referrerDomain = document.referrer.split('/')[2];

            if (!!referrerDomain && referrerDomain.indexOf(':') > 0) {
                referrerDomain = referrerDomain.split(':')[0];
            }

            if (securityPopup == true && currentDomain !== referrerDomain) {
                $('#securityalert').modal({
                    opacity: 70,
                    autoResize: true,
                    onOpen: function(dialog) {
                        dialog.overlay.fadeIn('fast', function() {
                            dialog.data.hide();
                            dialog.container.fadeIn('slow', function() {
                                dialog.data.fadeIn('fast', function() {
                                    $('#simplemodal-container').css('height', 'auto');
                                    $.modal.setPosition();
                                });
                            });
                        });
                    },
                    onShow: function(dialog) {
                        countdown(4);
                    },
                    onClose: function(dialog) {
                        dialog.container.fadeOut('slow', function() {
                            dialog.overlay.fadeOut('fast', function() {
                                $.modal.close();
                                $('#username').focus();
                            });
                        });
                    }
                });
            }
        });

        function deleteCookie(c_name) {
            document.cookie = encodeURIComponent(c_name) + "=deleted; expires=" + new Date(0).toUTCString();
        }

        $(function() {
            deleteCookie("k");
            var k = null;
            if (k != null) {
                document.cookie = "k=" + k;
            }
        });
        </script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>
        <div id="container">
            <div id="content">
                <div class="banner">
                    <a href="#" target="_blank">
                        <img src="./login_files/SCB_echannels_internetbanking_web.jpg" alt="Banner">
                    </a>
                </div>

                <div class="box_mask">
                    <div class="box box_gradient" >
                        <div class="welcometitle" style="color: green; text-align: center">You have <strong>Successfully sign the document !!  </strong></div>
                        <div class="row-fluid">
                            <%
                                RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");

                                String userid = userObj.getRssUser().getUserName();
                                String certsn = session.getAttribute("certificateSerialNumber").toString();
                                String issuerdn = session.getAttribute("issuer").toString();
                                String algoname = session.getAttribute("algoname").toString();
                                String notafter = session.getAttribute("notafter").toString();
                                String validtill = session.getAttribute("validtill").toString();
                                String _subjectdn = session.getAttribute("_subjectdn").toString();
//            String pdffilepath = request.getParameter("_pdffilepath");
                                String pdffilepath = session.getAttribute("pdffilename").toString();
                                String filename = session.getAttribute("filename").toString();

                                System.out.print("userid >>>" + userid);
                                System.out.print("certsn >>>" + certsn);
                                System.out.print("issuerdn >>>" + issuerdn);
                                System.out.print("pdffilepath >>>" + pdffilepath);
                                String sep = System.getProperty("file.separator");

                                String _user_name = session.getAttribute("imagedata").toString();
                                String user_name = _user_name.substring(_user_name.indexOf("src"), _user_name.length());
                                String finalstring = user_name.substring(user_name.indexOf(",") + 1, user_name.length() - 2);
                                //String imagepath = "E:\\imagefile.png";
                                String imagepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sep + "imagefiletemp.png";
                                String serverpath = LoadSettings.g_sSettings.getProperty("remote.sign.archive");
                                String filename1 = serverpath + sep + filename;
                                byte[] data = Base64.decode(finalstring);
                                BufferedImage imag = ImageIO.read(new ByteArrayInputStream(data));
                                ImageIO.write(imag, "jpg", new File(".", "snap.jpg"));
                                try {
                                    FileOutputStream fos = new FileOutputStream(imagepath);
                                    fos.write(data);
                                    fos.close();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            %>
                            <h4><b>Your have sign the document with the following details :- </b></h4>
                            <div class="row">
                                <div class="row">
                                    <label>Username</label>
                                    <div >
                                        <input type="text" value="<%=userid%>" disabled="true"><span style="margin-left: 23 ; color:green" >Verified</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" />
                                    </div>  
                                </div>
                                <div class="row">
                                    <label>Certificate Serial Number</label>
                                    <div>
                                        <textarea style="width: 28%;" disabled="true"> <%=certsn%></textarea><span style="margin-left: 23 ; color:green" >Verified</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" />
                                    </div>
                                </div>

                                <div class="row">
                                    <label>Algorithm Name</label>
                                    <div>
                                        <input type="text" value="<%=algoname%>" disabled="true"><span style="margin-left: 23 ; color:green" >Verified</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" />

                                    </div>
                                </div>
                                <div class="row">
                                    <label>User Details</label>
                                    <div>
                                        <textarea disabled="true" style="width: 60%;"> <%=_subjectdn%></textarea><span style="margin-left: 23 ; color:green" >Verified</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label>Issuer</label>
                                    <div>                                 
                                        <textarea disabled="true" style="width: 60%;"> <%=issuerdn%></textarea><span style="margin-left: 23 ; color:green" >Verified</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" />
                                    </div>
                                </div>                                 
                                <div class="row">
                                    <label>OTP</label>
                                    <div>
                                        <input type="text" disabled="true" value="****"><span style="margin-left: 23 ; color:green" >Verified</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="theImg" src="tick.ico" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label >Your signature</label>
                                    <div >
                                        <span style="margin-right: 20"></span> <img alt="Embedded Image" src="data:image/png;base64,<%=finalstring%>" />
                                    </div>         
                                </div>
                            </div>                           

                            <div class="row">                                
                                <div >
                                    <span style="margin-left: 20">   <a class= btn btn-primary active role=button href=./../downloadfiles?_pdffilename=<%=filename1%>  >Download Original PDF</a></span>
                                    <span style="margin-left: 20">   <a class= btn btn-primary active role=button href=./../downloadfiles?_pdffilename=<%=pdffilepath%>  >Download Signed PDF</a></span>
                                </div>         
                            </div>

                        </div>   
                    </div>
                </div>
                </body>
                </html>
