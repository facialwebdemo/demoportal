

<%@page import="org.json.JSONObject"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<%
    try {
        AxiomWrapper axWrapper = new AxiomWrapper();
        String host = request.getContextPath().toLowerCase();

        String result = "success";
        String message = "going for web token authentication!!!";

        String _apActCode = (String) request.getParameter("_apregid");
        session.setAttribute("_apregid", _apActCode);
        //cm.getString("_apregid");

        String _apActCodeMessage = (String) session.getAttribute("_apActivationMessage");// cm.getString("_apActivationMessage"); 
        JSONObject json = new JSONObject();
        if (_apActCodeMessage == null || _apActCode == null) {
            json.put("_result", "error");
            json.put("_message", "Data is invalid/empty!!!");
            response.setContentType("application/json");
            out.print(json);
            return;
        } else {
            if (_apActCodeMessage.equals(_apActCode)) {
                json.put("_result", "success");
                json.put("_message", "Activation Code is valid and proceed with setting your PIN to safegaurd Web PIN token!!!");
                response.setContentType("application/json");
                out.print(json);

                session.setAttribute("_apActivationMessage", null);

                return;

            } else {
                json.put("_result", "error");
                json.put("_message", "Activation Code is invalid!!!");
                response.setContentType("application/json");
                out.print(json);
                return;
            }

        }

    } catch (Exception ex) {
        ex.printStackTrace();
    }


%>