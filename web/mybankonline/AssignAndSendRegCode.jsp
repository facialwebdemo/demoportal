


<%@page import="com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page import="org.json.JSONObject"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomStatus"%>
<%

    AxiomStatus aStatus = null;

    AxiomWrapper axWrapper = new AxiomWrapper();
    //  AxiomWrapper axWrapper = new AxiomWrapper();
    String host = request.getContextPath().toLowerCase();

    response.setContentType("application/json");
    String result = "success";
    String message = "Web Token updated successfully!!!";
    JSONObject json = new JSONObject();
    int retValue = 0;

    String sessionId = null;
    if (session.getAttribute("_apsession") == null) {

        json.put("_result", "error");
        json.put("_message", "Invalid Session!!!");

        response.setContentType("application/json");
        out.print(json);
        return;

    }
    sessionId = session.getAttribute("_apsession").toString();

    RssUserCerdentials _userObj = (RssUserCerdentials) session.getAttribute("_rssDemoUserCerdentials");// (RssUserCerdentials)session.getAttribute("_apuserObj");

    if (_userObj == null) {

        json.put("_result", "error");
        json.put("_message", "User not found!!!");

        response.setContentType("application/json");
        out.print(json);
        return;

    }

    String iStatusWebToken = null;
    if (session.getAttribute("_apWebTokenStatus") != null) {
        iStatusWebToken = session.getAttribute("_apWebTokenStatus").toString();

    }

    if (iStatusWebToken == null
            || iStatusWebToken.equals("null")
            || iStatusWebToken.equals("-10")) {  // unassigned

//        Array.Clear(_userObj.tokenDetails, 0, _userObj.tokenDetails.Length);
        int i_category = 2; // software token
        int i_subcategory = 1; // web token

        AxiomCredentialDetails axiomCredentialDetails = new AxiomCredentialDetails();
        axiomCredentialDetails.setCategory(i_category); // 2 - sw token
        axiomCredentialDetails.setSubcategory(i_subcategory);//1 - web token

//        _userObj.tokenDetails = null;
//        _userObj.tokenDetails = new ApConnect.AxiomRssCore.axiomCredentialDetails[1];
        _userObj.getTokenDetails().add(axiomCredentialDetails);

        aStatus = axWrapper.AssignCredential(sessionId, _userObj, null, null);

        if (aStatus != null && aStatus.getErrorcode() != 0) { // success for assign

            json.put("_result", "error");
            json.put("_message", "Web Token Assignment Failed!!!");

            response.setContentType("application/json");
            out.print(json);
            return;
        }

    } else {
        aStatus = axWrapper.sendNotification(sessionId, _userObj.getRssUser().getUserId(), 2, null, null);

    }

    //  else 
    //  if (iStatusWebToken.CompareTo("1") == 0)
    //  {
    //  Array.Clear(_userObj.tokenDetails,0,_userObj.tokenDetails.Length);
    //  ApConnect.AxiomRssCore.axiomCredentialDetails axiomCredentialDetails= new ApConnect.AxiomRssCore.axiomCredentialDetails();
    //  axiomCredentialDetails.category=2;// 2 - sw token
    //  axiomCredentialDetails.subcategory=1;//1 - web token
    //  axiomCredentialDetails.status=-2;
    //  _userObj.tokenDetails[0]=axiomCredentialDetails;
    //ApConnect.AxiomRssCore.axiomStatus aStatus1=axWrapper.ChangeUserCredentials(sessionId,_userObj,null);
    //   if (aStatus1 != null && aStatus1.errorcode != 0)
    //   { 
    //      json.Add("_result", "error");
    //      json.Add("_message", "Web Token Change Status Failed!!!");
    //      Context.Response.Clear();
    //      Context.Response.ContentType = "application/json";
    //      Context.Response.Write(json);
    //       return;
    //  }
    //  }
    String msgToSend = aStatus.getRegCodeMessage();
    String userIDToSend = session.getAttribute("username").toString();

    boolean bActCodeSendStatus = false;
              //Code to write for IVR
    //if (Session["_apActivationMessage"].ToString() == "Sent")
    //{

    //}
    //if (Session["_apUserIsNRI"].ToString() == "true")
    //{
    //}
    //   bActCodeSendStatus = axWrapper.SendActivationCode(userIDToSend, msgToSend);
    // bActCodeSendStatus = true;
    //ApConnect.AxiomRssCore.axiomStatus Status = axWrapper.sendNotification(sessionId, _userObj.rssUser.userId, 2, null, null);
    bActCodeSendStatus = true;

    if (bActCodeSendStatus == true) { // success for send reg code
        session.setAttribute("_apActivationMessage", msgToSend);
        session.setAttribute("ApmessageStatus", "Sent");
        json.put("_result", "success");
        json.put("_message", "Registration Code is send to your registered mobile phone number!!!");
//        var keybytes = Encoding.UTF8.GetBytes(Session["SessionIdentity"].ToString().Substring(0, 16));
//        var iv = Encoding.UTF8.GetBytes(Session["SessionIdentity"].ToString().Substring(7, 16));
//
//        byte[] enc = AxiomWrapper.EncryptStringToBytes(json.ToString(), keybytes, iv);
//        string op = Convert.ToBase64String(enc);
        JSONObject jnew = new JSONObject();
//        jnew.Add("ResultData", op);
        response.setContentType("application/json");
        out.print(jnew);

        return;
    } else {
        result = "error";
        message = "Failed to send registration code!!!";

        json.put("_result", result);
        json.put("_message", message);

        response.setContentType("application/json");
        out.print(json);
        return;
    }


%>
