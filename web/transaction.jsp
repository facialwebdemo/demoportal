<%@include file="header.jsp" %>
<script src="./assets/js/login.js"></script>


<div class="container-fluid">
    <h1 class="text-success">Transaction Details to Filled.. </h1>

    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id ="transactiondetails" name="transactiondetails">
            <fieldset> 

                <div class="control-group"> 
                    <label class="control-label"  for="username">Source Account No:</label>
                    <div class="controls"> 
                        <input type="text" id="_source" name="_source" placeholder="1234556788"  class="span4">
                    </div> 
                </div>
                <div class="control-group"> 
                    <label class="control-label"  for="username">Destination Account No:</label>
                    <div class="controls"> 
                        <input type="text" id="_destination" name="_destination" placeholder="2345782344" class="span4">
                    </div>
                </div> 

                <div class="control-group"> 
                    <label class="control-label"  for="username">Amount Transfered</label>
                    <div class="controls"> 
                        <input type="text" id="_amount" name="_amount" placeholder="10001.00" class="span4">
                    </div>
                </div> 


                <div class="control-group">
                    <div class="controls">
                        <div id="savecrtsettings-result"></div>
                        <button class="btn btn-primary" onclick="geoTransaction()" type="button" id="savecrtsettings">Save Details Now >></button>                        
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

</div>

<%@include file="footer.jsp" %>