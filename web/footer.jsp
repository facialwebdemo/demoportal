<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    Date dFooter = new Date();
    SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
    String strYYYY = sdfFooter.format(dFooter);

    long LongTime = dFooter.getTime() / 1000;
    SimpleDateFormat tz = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    String completeTimewithLocalTZ = tz.format(dFooter);


%>
<hr>
<script src="./assets/js/footer.js"></script>

<script>
//    setInterval(function(){checkValidSession()},180000);
</script>

<footer>
    <div align="center">
        <p><small> &copy; Molla Technologies 2009-<%=strYYYY%></small></p>
        <p>Local Date and Time::<%=completeTimewithLocalTZ%> (<%=LongTime%>)</p>                
    </div>
</footer>
</body>
</html>


