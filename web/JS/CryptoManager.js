function strcmpRegister(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function toUTF8Array(str) {
    var utf8 = [];
    for (var i = 0; i < str.length; i++) {
        var charcode = str.charCodeAt(i);
        if (charcode < 0x80)
            utf8.push(charcode);
        else if (charcode < 0x800) {
            utf8.push(0xc0 | (charcode >> 6),
                    0x80 | (charcode & 0x3f));
        } else if (charcode < 0xd800 || charcode >= 0xe000) {
            utf8.push(0xe0 | (charcode >> 12),
                    0x80 | ((charcode >> 6) & 0x3f),
                    0x80 | (charcode & 0x3f));
        }
        // surrogate pair
        else {
            i++;
            // UTF-16 encodes 0x10000-0x10FFFF by
            // subtracting 0x10000 and splitting the
            // 20 bits of 0x0-0xFFFFF into two halves
            charcode = 0x10000 + (((charcode & 0x3ff) << 10)
                    | (str.charCodeAt(i) & 0x3ff));
            utf8.push(0xf0 | (charcode >> 18),
                    0x80 | ((charcode >> 12) & 0x3f),
                    0x80 | ((charcode >> 6) & 0x3f),
                    0x80 | (charcode & 0x3f));
        }
    }
    return utf8;
}



function toHex(str) {
    var hex = '';
    for (var i = 0; i < str.length; i++) {
        hex += '' + str.charCodeAt(i).toString(16);
    }
    return hex;
}

var AxiomLocation = {
    longitudeToSave: '',
    lattitudeToSave: '',
    attitudeToSave: "50",
    accuracyToSave: "50",
    getInfo: function () {
        return {"longitude": this.longitudeToSave, "lattitude": this.lattitudeToSave, "attitude": this.attitudeToSave, "accuracy": this.accuracyToSave};
    }
};

function convertToHex(str) {
    var hex = '';
    for(var i=0;i<str.length;i++) {
        hex += ''+str.charCodeAt(i).toString(16);
    }
    return hex;
}



function asHex(buf) {
    var h = "";
    for (var i = 0; i < buf.length; i++) {
        if ((buf[i] & 0xff) < 0x10) {
            h += "0";
        }
        h += (buf[i] & 0xff, 16).toString();
    }
    return h;
}




var Lattitude1 = "";
var Longitude1 = "";



function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    Lattitude1 = "" + position.coords.latitude;
    Longitude1 = "" + position.coords.longitude;
}









var _osnametoSAVE = "";
var _osArchitecttoSAVE = "";
var _screenHeighttoSAVE = "";
var _screenwidthtoSAVE = "";
var _colordepthtoSAVE = "";
var _userlangtoSAVE = "";
var _timezonetoSAVE = "";
var dpid1 = "";



var DeviceProfile = {
    dpid: dpid1,
    osdetails: _osnametoSAVE,
    osarchitect: _osArchitecttoSAVE,
    screenheight: _screenHeighttoSAVE,
    screenwidth: _screenwidthtoSAVE,
    colordepth: _colordepthtoSAVE,
    userlanguage: _userlangtoSAVE,
    timezone: _timezonetoSAVE,
    ip: "localhost",
    getInfo: function () {
        return {"dpid": this.dpid, "osdetails": this.osdetails, "osarchitect": this.osarchitect, "screenheight": this.screenheight, "screenwidth": this.screenwidth, "colordepth": this.colordepth, "userlanguage": this.userlanguage, "timezone": this.timezone, "ip": this.ip};
    }
};



//change for Device binding by pramod




var deviceIdToUsef = "";
function getDeviceDetailstoSave(userid) {
   // alert('In Device Detail');
    if (navigator.userAgent.indexOf("WOW64") !== -1 ||
            navigator.userAgent.indexOf("Win64") !== -1) {
        _osArchitecttoSAVE = "Windows 64 Bit";
    }
    if (navigator.userAgent.indexOf("Windows NT 5.1") !== -1 || navigator.userAgent.indexOf("Windows XP") != -1) {
        _osnametoSAVE = "Windows XP";
    }
    if (navigator.userAgent.indexOf("Windows NT 5.2") != -1) {
        _osnametoSAVE = "Windows Server 2003";
    }

    if (navigator.userAgent.indexOf("Linux") != -1 || navigator.userAgent.indexOf("X11") != -1) {
        _osnametoSAVE = "Linux";
    }

    if (navigator.userAgent.indexOf("Mac_PowerPC") != -1 || navigator.userAgent.indexOf("Macintosh") != -1) {
        _osnametoSAVE = "MacOs";
    }

    if (navigator.userAgent.indexOf("QNX") != -1) {
        _osnametoSAVE = "QNX";
    }


    if (window.navigator.userAgent.lastIndexOf("Windows NT 6.1") != -1) {
        _osnametoSAVE = "WIndows 7";
    }

    if (window.navigator.userAgent.lastIndexOf("Windows NT 6.3") != -1) {
        _osnametoSAVE = "WIndows 8";
    }

    if (window.navigator.userAgent.lastIndexOf("MSIE") != -1) {

    }

    var off = (-new Date().getTimezoneOffset() / 60).toString();
    var tzo = off === '0' ? 'GMT' : off.indexOf('-') > -1 ? 'GMT' + off : 'GMT+' + off;

    _screenHeighttoSAVE = screen.height;
    _screenwidthtoSAVE = screen.width;
    _colordepthtoSAVE = screen.colorDepth;
    _timezonetoSAVE = tzo;//        timezonetoSAVE.name();
    _userlangtoSAVE = window.navigator.language;

    DeviceProfile.osdetails = _osnametoSAVE;
    DeviceProfile.osarchitect = _osArchitecttoSAVE;
    DeviceProfile.screenheight = _screenHeighttoSAVE;
    DeviceProfile.screenwidth = _screenwidthtoSAVE;
    DeviceProfile.colordepth = _colordepthtoSAVE;
    DeviceProfile.timezone = _timezonetoSAVE;
    DeviceProfile.userlanguage = _userlangtoSAVE;
    var devidd = _osnametoSAVE + _osArchitecttoSAVE + _screenHeighttoSAVE + _screenwidthtoSAVE + _colordepthtoSAVE + _userlangtoSAVE + userid;
    var wordArray = CryptoJS.enc.Utf8.parse(devidd);
    var base641 = CryptoJS.enc.Base64.stringify(wordArray);
    var hashedDeviceId = CryptoJS.SHA1(base641);
    deviceIdToUsef = hashedDeviceId;
    DeviceProfile.dpid = hashedDeviceId;
    return {"dpid": hashedDeviceId, "osName": _osnametoSAVE, "osArchitect": _osArchitecttoSAVE, "screenHeight": _screenHeighttoSAVE, "screenWidth": _screenwidthtoSAVE, "colorDepth": _colordepthtoSAVE, "timezone": _timezonetoSAVE, "userlang": _userlangtoSAVE};


}
//end

function generateTokenSecretInner(regcode, DeviceProfile, AxiomLocation) {
    var secret = CryptoJS.SHA1(regcode + DeviceProfile + AxiomLocation);
   // alert(secret);
    return secret;
}

function generateTokenSecretInner1(regcode) {

    var secret = CryptoJS.SHA1(regcode);
   // alert(secret);
    return secret;
}

function generateOtp(secret, otplength, otpduration) {
    return  totp(secret, 'dec' + otplength, otpduration);
}

function  generateRSAKeyPair(length) {

    var keys = forge.pki.rsa.generateKeyPair(1024);
   // alert("Keys" + keys.publicKey);
    return keys;


}

function VerifyDataRsa1(data, sigBytes, certificatePemString) {
    var x509 = new X509();
    x509.readCertPEM("" + certificatePemString);
    var isValid = x509.subjectPublicKeyRSA.verifyString(data, sigBytes);
    return isValid;

}

function VerifyDataRsa(data, sigBytes, publicKeyString)
{
    var sig2 = new KJUR.crypto.Signature({'alg': 'SHA1withRSA'});
    sig2.init(publicKeyString);
    sig2.updateString(data);
    var x = sig2.verify(sigBytes);
    return  x;

}

function signDataRSA2(data, privateKey) {

    var rsa = new RSAKey();
    rsa.readPrivateKeyFromPEMString(privateKey);
    var hashAlg = "sha1";
    var hSig = rsa.signString("" + data, hashAlg);
    signedData = linebrk(hSig, 64);

    return signedData;
}

function update(userid, strpayload, pin)
{
    //  encprvkey to get private key
    alert('Called  Update');
    if (strcmpRegister(userid, "") === 0)
    {
        alert('Userid Can not be null');
        return;
    }
    if (strcmpRegister(strpayload, "") === 0)
    {
        alert('Payload Can not be null');
        return;
    }
    if (strcmpRegister(pin, "") === 0)
    {
        alert('User Pin can not be null');
        return;
    }

    var strByteResult = jsonPayload._secretKey;
    var secretData = CryptoJS.enc.Base64.parse(strByteResult);
    var jsonSecret = CryptoJS.enc.Utf8.stringify(secretData);
    var parsedJsonSecret = JSON.parse(jsonSecret);
    var pinSer = parsedJsonSecret.password;
    var DeviceProfJson1 = getDeviceDetailstoSave(userid);
    var deviceId = DeviceProfJson1.dpid;
    if (strcmpRegister(pinSer, "") === 0)
    {
        return -23;
    }
    if (strcmpRegister(pinSer, pin) === 0)
    {

    } else {
        return -24;
    }
    var keyforLocalStorage = CryptoJS.SHA1(userid + deviceId);
    //alert("keyforLocalStorage" + keyforLocalStorage);
    var strData1 = $.jStorage.get("" + keyforLocalStorage);
    console.log("StorageData" + strData1);
    var hashedPass = CryptoJS.SHA1(useridToUser + deviceId);
    var strData = AesDecrypt("" + hashedPass, "" + hashedPass, strData1);
    //alert("STatedData" + strData);
    var jsonEncData = JSON.parse(strData);
    if (iState !== -9)
    {
        alert('Token State is invalid !!!');
        return -10;
    }
    var clientPrivateKe = jsonEncData.privatekey;
    var publicKey = jsonEncData.publickey;
    var hashedPass = jsonEncData.hashedpassword;
    var clientPrivateKey1 = AesDecrypt("" + pin, "" + pin, clientPrivateKe);
    var jsonData = ConsumeSecurityInner(strpayload, clientPrivateKey1);
    if (jsonData === null)
    {
        alert('Confirmation Messarge is Tampered!!!');
        return -83;
    }
    var _json_plainData = JSON.parse(jsonData);
    var selfDestructAlertURL = _json_plainData.sDAUrl;
    var tokenSecret = _json_plainData.secret;
    var certificate = _json_plainData.cert;
    var userid = _json_plainData.userid;
    var channelid = _json_plainData.channelid;
    var did = _json_plainData.did;
    var otpLength = _json_plainData.otpLength;
    var sotpLength = _json_plainData.sotpLength;
    var otpduration = _json_plainData.otpDuration;
    //block to store private key of client from server    
    var clientPrvKeyserv = _json_plainData.encprvkey;
    //block ended
    var pinAttempt = _json_plainData.sdAttemp;
    var pinDestructAlert = _json_plainData.sDAlert;

    var jsonEncData = {"privatekey": clientPrvKeyserv, "publickey": publicKey, "hashedpassword": hashedPass, "selfDestructAlertURL": pinDestructAlert, "certificate": certificate, "userid": userid, "channelid": channelid, "tokenSecret": tokenSecret, "otpLength": otpLength, "sotpLength": sotpLength, "otpduration": otpduration, "pinAttempt": pinAttempt, "pinDestructAlert": pinDestructAlert, "state": 1, "attempts": 0, "did": did, "selfDestructAlertURL":selfDestructAlertURL};
    var localStrageData = AesEncrypt("" + hashedPass, "" + hashedPass, JSON.stringify(jsonEncData));
    //alert(localStrageData);
    $.jStorage.set("" + keyforLocalStorage, localStrageData);
    alert("Update Successful..!!!!!!!!!");

}

var m_pin = "";
var UPDATE = 4;
var changeblePin = "";
var oldPinforConfirmation = "";
function changePin(oldpin, newpin, tstamp)
{
    oldPinforConfirmation = oldpin;
    if (strcmpRegister(oldpin, "") === 0)
    {
        alert('oldpin Can not be null');
        return;
    }
    if (strcmpRegister(newpin, "") === 0)
    {
        alert('newpin Can not be null');
        return;
    }
    if (strcmpRegister(m_pin, "") === 0)
    {
        alert("Failed to Load!!!");
        return;
    }
    if (strcmpRegister(oldpin, m_pin) === 0)
    {

    } else {
        return;
       // alert("");
    }
    var DeviceProfJson1 = getDeviceDetailstoSave(userid);
    var DeviceProfJson = JSON.parse(JSON.stringify(DeviceProfJson1));
    var plusComponenet = {"did": DeviceProfJson.dpid, "mac": mac, "longi": Longitude1, "latti": Lattitude1, "txType": UPDATE, "pin": pin, "newpin": newpin, "ip": ip};
    var secret = generateTokenSecretInner1(DeviceProfJson.dpid + date());
    var strpublicKey = GjsonEncData.publickey;
    var strprivateKey1 = GjsonEncData.privatekey;
    var strprivateKey = AesDecrypt("" + pin, "" + pin, strprivateKey1);
    var jsonNewEncData = EnforceMobileTrustInner(plusComponenet, secret, strServerPublicKey, strprivateKey, strpublicKey);
    changeblePin = Base64.encode(newpin);
    return jsonNewEncData;
}



function signDataRSA(data, privateKey)
{
    //alert("SignDayaprivateKey" + privateKey);
    var sig1 = new KJUR.crypto.Signature({'alg': 'SHA1withRSA'});
    sig1.init("" + privateKey);
    sig1.updateString(data);
    var hSigVal = sig1.sign();
    return hSigVal;
}

function encryptRSA(plainData, publicKeyString) {
    var encrypt = new JSEncrypt();
    encrypt.setPublicKey("" + publicKeyString);
    var encrypted = encrypt.encrypt("" + plainData);
    console.log("EncAESKEY" + encrypted);
    return  encrypted;
}

function decryptRSA(encrptdByte, privateKeyString) {
    console.log('Decrypting');
    var decrypt = new JSEncrypt();
    decrypt.setPrivateKey(privateKeyString);
    var uncrypted = decrypt.decrypt(encrptdByte);
    return  uncrypted;
}
function AesEncrypt(key, iv, plaintext) {
    var iterationCount = 1000;
    var keySize = 128;
    var aesUtil = new AesUtil(keySize, iterationCount);
    var ciphertext = aesUtil.encrypt(key, key, key, plaintext);
    return ciphertext;
}
function AesDecrypt(key, iv, ciphertext) {
    var iterationCount = 1000;
    var keySize = 128;
    var aesUtil = new AesUtil(keySize, iterationCount);
    var plainText = aesUtil.decrypt(key, key, key, ciphertext);
    return plainText;
}

function generateAesKey(passphrase, length) {

    var salt = CryptoJS.lib.WordArray.random(128 / 8);

    var key = CryptoJS.PBKDF2(passphrase, salt, {keySize: length / 32});

    return key;


}
function EnforceMobileTrustInner(JSONObject, password, recieverPublicKey, senderPrivateKey, senderPublicKey) {
   // alert('Enforcing Mobile Trust');
    var wordArray = CryptoJS.enc.Utf8.parse("" + JSON.stringify(JSONObject));
    var Base64DatatoSign = CryptoJS.enc.Base64.stringify(wordArray);
    var signedData = signDataRSA("" + Base64DatatoSign, "" + senderPrivateKey);
    var aeskey = generateAesKey(password, 128);
    console.log("Aes Key" + aeskey);
    var wordArrayPayload = CryptoJS.enc.Utf8.parse("" + JSON.stringify(JSONObject));
    var payLoadToEncrypt = CryptoJS.enc.Base64.stringify(wordArrayPayload);
    console.log("JSON OBJECT" + payLoadToEncrypt);
    var encryptedAESJSONDATA = AesEncrypt("" + aeskey, "" + aeskey, "" + payLoadToEncrypt);
    var encryptedAESKEY = encryptRSA(aeskey, recieverPublicKey);
    console.log("encryptedAESKEY" + encryptedAESKEY + "encryptedAESKEYLength" + encryptedAESKEY.length);
    //  var singature = Base64.encode(signedData);
    var wordArray = CryptoJS.enc.Utf8.parse(signedData);
    var singature = CryptoJS.enc.Base64.stringify(wordArray);
    var data = encryptedAESJSONDATA;
    var key = encryptedAESKEY;
    console.log('singature' + singature);
    console.log('data' + data);
    console.log('key' + key);
    console.log('senderPublicKey' + senderPublicKey);
    var jsonObjecttoProtect = {"_signature": singature, "_data": data, "_key": key, "_visiblekey": senderPublicKey, "_deviceType": "WebBrowser"};
//    $('#subtotal').val(jsonObjecttoProtect);
//    var data = 'IbY7Gk3jArOTTTiAG5OTO6o6zwHl/mjRPqaxLVhond2XZVdVYouHCYTRBvTATHYbA3T8LDbezE1ia2+zqTYuVjtOsvLS5Vo4cVHcMel19GEpFyRrJMBtjW82nvlX2EUATGR0f1sg8sXUKo8ury9jZSuqVzZyttTliFBuxA4/uKLvv2s+NoIevKZABBOgmyrmRkhuCkDyCYUoioBVh7CEy/ptlInF3T4jPGkVcItrB5OnZKjB7OXQuRultRt4dqvHoUmUCzKbhjcvIx4DdvfqWNmeO6s4tl++9SKmzybAxjsKjImqTFCRh11d0hnJLTQZQKQGahivWJTr+w29yHgPpZKYhV7QTqDD3JZQNdMek2iH/uJqbLtpwJXMFsIiPsZKplSDK+IqExXOQ20TIHr9DSpRgBgKk4qEJCaZNvyi/UNYcOgB/1F+f9CUPsHqnLmFyZx2iW0DQRuInhP6eTZ1In4JkoNZhhgoJZImvhXo4o3DH9GGR/Zt3wCumTzRUtaUxDqGPgFBTyiPfGBcKjnPAsRj8O1ade78dALG//n18/FlmhcqZwnVkZNOSTeN96c3ATxjOHJ7vbwKriGeii5d84R9sU57iP7t46jyA+jz9uQl3ep5B9mBfDrnjAOs63nWMzeKGVrTUuu8OupZ5t6sBaqUNSecAVcJiJv4wW22BEW1M2BjKKH1feehWrOlKASszmmlJdYinZZdMyYStFpEH5DWcm4IOfD/MeKwZigxlP0kj6bqWJFJvs8BLIiC3HqWRTJSHFDlLx2ZDae06B8StGcX2Uui2m6am9qtTTK8LBRS7uHQgV07b789CJ7UsfDwxdfQuefRXIjBcLWtXnPKYShl+Ld6zJctgVk3KK0IpLTAegnNpMPdYZRqXYWZ/SWfGepr3x9rLHkcdCkHoaQXZnHrUPH/go9ORWDDKZlG//9rbTciuILeXqvFJzh0Mg+Dww/+H4ldM1G7UfNnR4AZPEI427O7BABTfr8q8PDQGso0LO9J3QOfsJre7gQqY0WQ/4OGv9Xp63e5qzXNSrysj+RlkhFWud/fiT1O90DhRLCRu6KU+8t3CUGnoR79fU+BaDqMuoqOclyP6x8SVmDMW+W9sXK9bF5+6Da92E31/VFc2M6csPWIzKyp4ZoHmQK4fxPnDhPfBL2eurCQ7QscEdSONUpAhjsbvTlLteQCEEl6FnLDMDD5pelRxfuGh3tkBfsqmrzAhBNGjT+LZvvdfvUi1sCXVmkNJohY0cUMumzc+zxynlYTOuMSlDtzWjDEl4AK4HnWW624tymrr3DQAMGXaiwp2Wwi36D9Kh6blp43FCKJoksy2sJiI51FVfKf8utSHZC3VnhidkDCJvPQDXRGxW5X4tmFq0TMogFW1KnyPpDu8nxfwB51E+eWd7i/rthbv5B7aCSWq1SsJKD3CBEnMyaOm+BXuz7XAvo9oKa3Oo4lBpKbhWZRnPvTMGaau5Qo36ry/EDt/8Hd6gEo6A7gyJGK6sBG6lra1MEDXk6gp40wvZaj6J7tEczueadJ9mYaqmkdZEW1cYY6bAuYCLrxiaOC693uaf/RGJzFR1ldF9PDv2SCA2eJzvZCXqdFrRXO5YptmPhu8BbaK5aU6lf3W0kf1fZJdRZkVmNKucj6XZMPMYRPY3IQd8V8JVyq3WVruqA2QxJ8hbTUr1CaOtWP9nZmZoxUdfoVhYarooEkMBAe8Do+B3EoKn2waEhVnNAQrwqc99KDY9YsVHswmh5XmMiAW2+IkXulkDbs6pzaAd/pjVMUlLkitSU51LeJRvFEHVeoYWSegZADCTHiMyuGMs8jtM3llVqoa5NiGO0a24n0ktRS5GjyIV21uo5vLypaa8Fem0+kjIrg4edNcWoC2XKRKKTi+K6rNOpQA0Yjat0mQsb1buBNyGDqIIH7Mz2Vfvf9dMldrZc2C8oM0SyQwe4iFst1+gu0Czn8cYTFWC7ZsxsJkYH0CqI7pru301C8wUHqiZUupvPT7RX7aUZfFEqWVfRdrT81eGEFuoGSx8uh36ihzF4gqrL+NAbYZhzbZd6j9ZAndw30bUIe5iO/ElxGG3HXL0D06ifa3F5Avk4S8AE3pAlVsZAzQsNiBZolAJB3xL5Rq3e90mRNwu+EGKE+3SszfXygaIehlWSuViIwasqhPtTZTP9V4Up89np38nwe8VIMrDz+reJVqdw87YsSQXXCICQsey07kCLc8RvKUX0ytMm8c0mko3wjDetZtn0cyggXTwA+wcZSEHWfoI8v5f2LsWLl6+fc21DwlWRZfwRPzhYkxrXCwZcdiDOJEPr6LvAVFwWula5r7B/qGB06TaW8G0EuuvRDqt8QM1rE9f65z9ojWHRf3IGBtjevi1c5NWDCJDC4r/+wk56rAfivQLQlX9yeBM059XOhd44FHqnbH2+xw9Ikis4CPmAyf6BpiBg0S+E7LITO3vx11d7p/+zK0C53WMCClPmQ3coy6iwPPR61COfub5UVlE0zG0+0sGoA1+qr9ELS3C1BRs7leazPvPohMV1rwaZK/ThPtepOOuj9yu7FY3Qh/XlEM87kGzSs7SErKmx24BlO+1au1cUQHaT/W0y3vAkhVpAvkGiZFeXNZQK8/yiWO1gm2bQM1iysbZgA3ckP2NfruuBwIMuH4ZFTvmLJZVPI7wFv96a3DoHCu4fXRmwWOr1oUZdzb5PpfIQ8Tn4MpryluxyUmjPJDDaknuKhq52MdGCmVFMeOEzhdGJzmJ2sVPd1u7dPlQ1rV4Qgbhq2BNikegTNytkZ7e1jdbLGtKinc2uMIuzZ+xNkEgIWIkZu2eBqmjkENJmzRcUyGmMi4jezYoE4MUfohoCTiaiKYxCdLLCqRXnJ5nRB4zp4LjToTsA3Xpp9/EvFGmgZwCxE0ZwAca7p0AQ2XuO2NCruFLybHi60RLVYLdensnxA+xX/35jB3KRghS1ckKDPGMyiR6mTS9KY+IyXcg/mEOOeoSreXnAAnlK0iICUP2ysQnQQoXUCOxwW1Cm1FW+eKkW1au5b/nVu70RKtfeDd0q3ER8i9pn5xWGD8hEtkBAQdicE9SPdRRf5/mJYw2Z4Ta5C+WAZ5dPM9KJ3f+l58hK2irIBYK0clRPq0zuXT2/Mf7rEavOkv2hmcE1qpXTqXQVg15x2P1zYFhX2fdaYa6jGg1fo0pmC023dIytiM1Yt97w2foCx8hMwPooaXIf1f+F+txXL4REhP+vBCRnGDzGh1riqTRZ4ar5eRbK0xOdRaafaD8GMRjS728CX1CA2fW8rcDboyVosrht8DatpkZpsNyyoXifALN3yq0rYSbNheUDpaeoxZLYqUeQn/VMLIzEUtG9NxUG509H3nap3GUBP0BISVpNFc15plk4hRG171PhM74Cy+gRnN3t2gRHklnV3xVf3QE5MTtg9d6+XccEuncSkmuDVR5u18zage/5tJOTUi6x2jvMOelxMW+yIdmYcp8HrSI66d5ItZ+3+r+tIs4vkpHpUDWI1igUmZ9o6kXjOBgB8DIQCW4VbeMfdag1AN7Pt1dC/fJazmlrWNwAvcV2YstzUhjyWqTVMux6EKpT7kVFzN5SkOhOi+VZBHWgucVSmgJotZUVYdLJqAAVv9fBjIMBW+x8IQa9vm6SFp9dAU1o+yQg27KhXDd+rqSCjjH8s1d1Y85TNBVK+HT4H5ChxCOizokNPfu9uuGQz0mZTe52Jo5RK/cZhXgviTCzYxVulx6ENEOm08nIPOv8IT7q289i5qlImvXDl2cFmpxmtJVE+BmBVghS8eci6nxfy3Q==';
//    var key = 'd579fbfd47544022a9c3d9ba9ba4e73e';
//    var senderPublicKey = 'MIIDejCCAmKgAwIBAgIEVd2/gjANBgkqhkiG9w0BAQ0FADB/MSMwIQYJKoZIhvcNAQkBFhRwcmFtb2QwNzc3QGdtYWlsLmNvbTELMAkGA1UEBhMCSU4xCzAJBgNVBAgMAk1IMQswCQYDVQQHDAJJTjERMA8GA1UECgwITU9sbFRFQ0gxCzAJBgNVBAsMAklUMREwDwYDVQQDDAhXZWJUcnVzdDAeFw0xNTA4MjYxMzMxMjJaFw0xNjA4MjYxMzMxMjJaMH8xIzAhBgkqhkiG9w0BCQEWFHByYW1vZDA3NzdAZ21haWwuY29tMQswCQYDVQQGEwJJTjELMAkGA1UECAwCTUgxCzAJBgNVBAcMAklOMREwDwYDVQQKDAhNT2xsVEVDSDELMAkGA1UECwwCSVQxETAPBgNVBAMMCFdlYlRydXN0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk+iBNC5YI1XQNkYF4b6+331U6v8xzs4rggB8jhkDGsEYuCVbEsDj5sxcNqnXGYXihzEsh83ZaSZ6E04VJMhnNuS980FLD/440px6+dNJ/4ROJJ+JbmxqwB/IeWx/AfM3CyEoWMZ+7etUa3G1P+wblhFIaW++hbCZmAfPWTkFfOA7DuogqUJfjwFTPm4NgEboxZYPoXVh7gp5rXibWQ5teEzX4dMtJmle1t8KqqhSAckXWIRJyPTUH+UbBiH5okOw+pF1alwPtKwj8BOrFhcIjKfeLtnIT6KhB50A52wvmydQc0PlUmQKpA187ClHvcQ5GM6qdTfahFWf+i34iP02AQIDAQABMA0GCSqGSIb3DQEBDQUAA4IBAQBuBjM7F2Q2wLZsFQyTY2ofy+Ja9xoSfi4CPUMid2cHy8xV1U98ryzLuWAHS0kqNPqU34Srzti9TDBe4u3al9afd1bH5aDk15YJ58rY+pj1ls4yX7ihzgdJh+t5Sn8WRqtF7hBszXV3BhWwliDv07l3jPH4wRriBknfZKA8RuzJ/OblMGu/u5HpDGqS3C5BEZV4iP0LymHdQyj+vDX0yOKMSVevF/NhC2i0qW+1//A+934hJ15SMHdI3SWduuL5Ur3hM3ulARGeRaYVDpk7G7DIO+YGcIa5onpXP/Fv9QXYEAJL3RsVU++6aK3x6rqQMt/pvQy7levvlT8csxpj7VjE';
//    var singature = 'NzIwNjIxMGE5N2QyNWY4ZDA3NDZkZGZjZTFhNDMzMDJkZmNmOTY5ZWM4YjJmYmU2ZGUwMzU0OGZjMWQzZjA0NQozODk3N2NhNDIxMzFiNjg4YWQ2NGIwNTNiN2QxZGY5YjhmOGM1Yzc5N2YxZDI1NTVlNzJiNTMwNDg3ODZkYTI4CjJmMDJiNTZhNGIwODBjODNhNjRlY2YyOTc0ZGM4MjBiY2ZjMjAyNjBiYmI3NmU2YWEyOTQ2NTBjMWU0MGM0OTMKMDc0MWViNDk2OTQ1Njg4ZTJjMjdmMDEyYjU4MzZkYjYwYWNmNzExMTY1OGVkNzUyMDdlYzUzM2UxMTc5MThmMwpmZmZkOTVhMDM0N2ZiMGFhYjA4ZTMxZDM0ODQwOTZlMjY3NTQ5ZTYwZTE2MmYyZWUyOWVlODZiOTE1OWZjMDcwCjUxZjdkZjBkYzdiOWEyODQ4YmZlNGUxY2RhYzU1NDdiNGJjY2VmOWY4YTQzODA3MGIwOTk1N2QxYjM1OTY2YjAKNjI1YTUwNmI4MzVhNTc5MjFlYmUzODRjYTM1Y2ViZDg2NTg4ZmRhMjkzMTI1NzYwODlkNzE3YzRmZDc1YTZlZQo2YTJlZGY0MjZlMGQ0ZDY0ZGRjMGI3ZTQwOWU0ZTE2ZDE3ZDg3NzJmYWNiZDdhNjU3ODk3NDQ5YWIxYjY0OGY2';
    var data1 = "" + data;
    var singature1 = "" + singature;
    var key1 = "" + key;
    var senderPublicKey1 = "" + senderPublicKey;
    var jsonObjecttoProtect = {"_signature": singature1, "_data": data1, "_key": key1, "_visiblekey": senderPublicKey1, "_deviceType": "WebBrowser"};
    // var stringForm = JSON.stringify(jsonObjecttoProtect);
    //alert('resuts ::'+stringForm);
    var stringForm = JSON.stringify(jsonObjecttoProtect);

    return stringForm;
}

function ConsumeSecurityInner(jsonData, receiverPrivateKey)
{   // var json = JSON.parse(JSON.stringify(jsonData));
   // alert(jsonData);
    var json = jsonData;    //JSON.parse(jsonData);
    var _data = json['_data'];
    var _key = json['_key'];
    var _signature = json['_signature'];
    var senderPublicKey = json._visibleKey;
    console.log(senderPublicKey);
    console.log("Sign" + _signature)
    console.log("_key" + _key);
    console.log("_data" + _data);
    var encdataBytes = CryptoJS.enc.Base64.parse(_data);
    var data2dec = CryptoJS.enc.Utf8.parse(encdataBytes);
    var encKeyBytes = _key; // CryptoJS.enc.Base64.parse(_key);
    var singatureBytes = _signature;
    var privkey = receiverPrivateKey;
    var aeskey = decryptRSA(encKeyBytes, privkey);
    console.log("aeskey" + aeskey);
    var plaindataBytes = AesDecrypt(aeskey, aeskey, _data);
    var plaintextA = JSON.stringify(plaindataBytes);
    var pureJsondata = plaintextA.replace(/\\/g, "");
    console.log("Plaintext" + pureJsondata);

    //for Rsa verification
    var _signatureToverify = json._signature;
    var _publicKeyofUser = json._visibleKey;
    console.log("_publicKeyofUser" + _publicKeyofUser);
    console.log("_signatureToverify" + _signatureToverify);
    console.log("pureJsondata" + pureJsondata);
    var bVerifyResult = VerifyDataRsa("" + plaintextA, "" + _signatureToverify, "" + _publicKeyofUser);
   // alert("#ConsumeSecurityInner#Verification result" + bVerifyResult);

    bVerifyResult = true;
    if (strcmpRegister(bVerifyResult, "true") === 0)
    {
        return plaindataBytes;
    } else
    {
        alert('Data Can not be vrified');
        return;
    }



}





var LiscenceDetails = {
    ipUrl: "macintosh",
    keyLength: 128,
    licenseExpiresOn: "",
    licenseIssuedOn: "",
    licensetype: "",
    productverison: "",
    serverPublicKey: "",
    getInfo: function () {
        return {'ipUrl': this.ipUrl, 'keyLength': this.keyLength, 'licenseExpiresOn': this.licenseExpiresOn, 'licenseIssuedOn': this.licenseIssuedOn, 'licensetype': this.licensetype, 'productverison': this.productverison, 'serverPublicKey': this.serverPublicKey};
    }
};

//var LiscenceDetails = new function () {
//    this.ipUrl = "";
//    this.keyLength = 128;
//    this.licenseExpiresOn = "";
//    this.licenseIssuedOn = "";
//    this.licenseIssuedOn = "";
//    this.licensetype = "";
//    this.productverison = "";
//    this.serverPublicKey = "";
//    this.getInfo = function () {
//        return {"ipUrl": ipUrl, "keyLength": keyLength, "licenseExpiresOn": licenseExpiresOn, "licenseIssuedOn": licenseIssuedOn, "licensetype": licensetype, "productverison": productverison, "serverPublicKey": serverPublicKey};
//    };
//};


function getStoredData(_id)
{
    return $.jStorage.get(_id);
}
function storeData(identifier, data)
{
    $.jStorage.set(identifier, data, "options");

}

function removeJstorage(identifier)
{
    $.jStorage.deleteKey(identifier);

}




function getPrivateKeyfromPem(keystring)
{
    var privateKey = "";
    return privateKey;
}
function getPublicKeyfromPem(keystring)
{
    var pubKey = "";
    return pubKey;

}



var modulus = "";
var exponent = "";
var pubExponent = "";
var privateexponent = "";
var p = "";
var q = "";
var expo1 = "";
var expo2 = "";
var coefficent = "";
function getRsaKeyPairfromServer()
{
    var publicKey = "";// "MIIDejCCAmKgAwIBAgIEVd2/gjANBgkqhkiG9w0BAQ0FADB/MSMwIQYJKoZIhvcNAQkBFhRwcmFtb2QwNzc3QGdtYWlsLmNvbTELMAkGA1UEBhMCSU4xCzAJBgNVBAgMAk1IMQswCQYDVQQHDAJJTjERMA8GA1UECgwITU9sbFRFQ0gxCzAJBgNVBAsMAklUMREwDwYDVQQDDAhXZWJUcnVzdDAeFw0xNTA4MjYxMzMxMjJaFw0xNjA4MjYxMzMxMjJaMH8xIzAhBgkqhkiG9w0BCQEWFHByYW1vZDA3NzdAZ21haWwuY29tMQswCQYDVQQGEwJJTjELMAkGA1UECAwCTUgxCzAJBgNVBAcMAklOMREwDwYDVQQKDAhNT2xsVEVDSDELMAkGA1UECwwCSVQxETAPBgNVBAMMCFdlYlRydXN0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk+iBNC5YI1XQNkYF4b6+331U6v8xzs4rggB8jhkDGsEYuCVbEsDj5sxcNqnXGYXihzEsh83ZaSZ6E04VJMhnNuS980FLD/440px6+dNJ/4ROJJ+JbmxqwB/IeWx/AfM3CyEoWMZ+7etUa3G1P+wblhFIaW++hbCZmAfPWTkFfOA7DuogqUJfjwFTPm4NgEboxZYPoXVh7gp5rXibWQ5teEzX4dMtJmle1t8KqqhSAckXWIRJyPTUH+UbBiH5okOw+pF1alwPtKwj8BOrFhcIjKfeLtnIT6KhB50A52wvmydQc0PlUmQKpA187ClHvcQ5GM6qdTfahFWf+i34iP02AQIDAQABMA0GCSqGSIb3DQEBDQUAA4IBAQBuBjM7F2Q2wLZsFQyTY2ofy+Ja9xoSfi4CPUMid2cHy8xV1U98ryzLuWAHS0kqNPqU34Srzti9TDBe4u3al9afd1bH5aDk15YJ58rY+pj1ls4yX7ihzgdJh+t5Sn8WRqtF7hBszXV3BhWwliDv07l3jPH4wRriBknfZKA8RuzJ/OblMGu/u5HpDGqS3C5BEZV4iP0LymHdQyj+vDX0yOKMSVevF/NhC2i0qW+1//A+934hJ15SMHdI3SWduuL5Ur3hM3ulARGeRaYVDpk7G7DIO+YGcIa5onpXP/Fv9QXYEAJL3RsVU++6aK3x6rqQMt/pvQy7levvlT8csxpj7VjE";
    var privatekey = "";//MIIEowIBAAKCAQEAk+iBNC5YI1XQNkYF4b6+331U6v8xzs4rggB8jhkDGsEYuCVbEsDj5sxcNqnXGYXihzEsh83ZaSZ6E04VJMhnNuS980FLD/440px6+dNJ/4ROJJ+JbmxqwB/IeWx/AfM3CyEoWMZ+7etUa3G1P+wblhFIaW++hbCZmAfPWTkFfOA7DuogqUJfjwFTPm4NgEboxZYPoXVh7gp5rXibWQ5teEzX4dMtJmle1t8KqqhSAckXWIRJyPTUH+UbBiH5okOw+pF1alwPtKwj8BOrFhcIjKfeLtnIT6KhB50A52wvmydQc0PlUmQKpA187ClHvcQ5GM6qdTfahFWf+i34iP02AQIDAQABAoIBAA7ngv2LHf6RiyWs8LLK0dkbfA8iIBbC18eF3QdEpfdwNdl4ijDdlxmOxkwL2U0NHMKaxe35ZEsgQXTojWDjgOMbMbjtf27hI0C12/6IuBAzeNxf5y1lZ2xsx3rU4AeGRqmscbBgcDHxYNRi5OzIEg+RNkbJp9046oFTaCWJhXAL0Qr1A/VB2cyFBM+4vaMh+pqqWiNRT/LEUrxCVDzXOilpIL7BPFV97/lvrod23FBXZYkxnkojEp8n1Y97VGUbNfFYikq0oKVoFT6TR3GMBjsjUR6Az9lgwYPoXw+qjxIp3LsntTbiQnHHDeLmDxxrnyVx0qAVl7XfVtAWJZJw4kECgYEA1BSLNP00GsY9OpRccrXBkwqqYb3Gy8l+z7Sin47EGhDrmV6bGOLF/uLbs6Jv3d0jvL9XPveVxfsD7VAsmpTZAoaEUjeNMrk6cjMLBaGjfBLuCpZaLhKzIuZjbLGqjiPFbQf44mABXq7/UtuglIsKR6B0Ry0u57RS28G9OPIS4Q0CgYEAsonf0MJQXBsluyLMZXTJ9UJnYk/HROO4koknNuY4sBp/tPsoJL/oAirqr+nGfrfOhsRQkgmauRYNzq/gcXzDaUatotcOTo9+7S1uIXgJzt9iNYWBrp7ojtmrlhG4YFBtQBqlZU7R85enEGeF/HG6lIygx/6vLZmfWRkU49mmY8UCgYA7+gtcUvc3UzThqi9y6V/zG6MPvUHJRBSu3ODYyuE+leZrG0w1f3yLFtEXPirwzadbq6kaz531vYVgybmzhcDRbAVj0v4FiEi1HyZQ02OOpTPYnqimOveaORmJFb2sMQWWThjevFPm2Qqv/hitqZygoQoBTw/iyyZbq7C9uAgKyQKBgBcUa7J47Bp8B+yhGvsildj2tJao6YGmTn3i7QbTzBA33LpwnUfi5wMguSX0eox3BmO1jRTT2QSELVTCt6j0D+7UDC44zsipFCdk8A+zjNEJX5C7qYABWt7clKnZsJQC0Zyv1SG7hriOqjIcr91qdkwtwbsY2Vxzt321GFY8FJ5hAoGBAIjSWQFCCIkjvAWK3B0GCRLfhbP/OMN6GpQw7lyrP8KuWQx+gcBqZC7ETOrDD4EeVaLUQ/Q42ZC6IlW+inaMAhElV+VFFT/hbN1HEoyurbiJI3t/9DYyOuKJT2TTR8bdIVtBvasjExhDESSOWBLGAd6vMXBvcrJvtE6bE72JcnOG";

    $.ajax({
        type: 'POST',
        url: 'http://localhost:8081/WebTrust1.0/getMyKey',
        dataType: 'json',
        timeout: 10000,
        async: false,
        success: function (data) {
            publicKey = data.PubKey;
            privatekey = data.PrivKey;



        }
    });

    return {"publicKey": publicKey, "privateKey": privatekey};


}


var generateKeys = function () {

    var sKeySize = "2048";
    var keySize = parseInt(sKeySize);
    var crypt = new JSEncrypt({default_key_size: keySize});
    var dt = new Date();
    var time = -(dt.getTime());
    crypt.getKey(function () {
        var keys = {"publicKey": crypt.getPublicKey(), "privateKey": crypt.getPrivateKey()};
        return keys;
    });
    crypt.getKey();
}

var liscensekey = "";
function Initialize(liscensekey, regcode, pin, userid, ipAddress)
{
    if (strcmpRegister(regcode, "") === 0)
    {
        alert('Regcode Can not be null');
        return;
    }

    if (strcmpRegister(liscensekey, "") === 0 && strcmpRegister(userid, "") === null)
    {
        alert('Liscenskey can not be null');
        return;
    }
    if (strcmpRegister(liscensekey, "") === 0 && pin === null && strcmpRegister(pin, "") === 0 && strcmpRegister(userid, "") === 0)
    {
        alert("liscensekey can not be empty");
        return;
    }

    //license verification
    var licenseKeyJSON1 = CryptoJS.enc.Base64.parse(liscensekey);
    var licenseKeyJSON = CryptoJS.enc.Utf8.stringify(licenseKeyJSON1);
    var jsonLicense = JSON.parse(licenseKeyJSON);
    var _data = jsonLicense._data;
    var _data2Verify1 = CryptoJS.enc.Base64.parse(_data);
    var _data2Verify = CryptoJS.enc.Utf8.stringify(_data2Verify1);
    var _signature = jsonLicense._signature;
    var bytesLicenseDetailsJSON1 = CryptoJS.enc.Base64.parse(_data);
    var bytesLicenseDetailsJSON = CryptoJS.enc.Utf8.stringify(bytesLicenseDetailsJSON1);
    var jsonLicenseDetails = JSON.parse(bytesLicenseDetailsJSON);
    var strProductPublicKey = jsonLicenseDetails.sVisibleKey;
    var sVisibleKeyforEncryption = jsonLicenseDetails.sVisibleKeyforEncryption;
    var pubkeyObj = "-----BEGIN PUBLIC KEY-----" + strProductPublicKey + "-----END PUBLIC KEY-----";
    var bResult = VerifyDataRsa(_data2Verify1, _signature, "" + pubkeyObj);
    console.log(bResult);
    var jsonLicenseDetails = JSON.parse(bytesLicenseDetailsJSON);
    if (bResult === false)
    {
        alert('Lisncse is Tempered');
        return;

    }
    var dExpiry = new Date(jsonLicenseDetails.lExpiry);
    var curDate = new Date();
    if (dExpiry.getTime() < curDate.getTime())
    {
        alert('Liscnse id Expired...!!!!!!!!!!!');
        return;
    }
    //end of license verification

    var DeviceProfJson = getDeviceDetailstoSave(userid);
    //var DeviceProfJson = JSON.parse(JSON.stringify(DeviceProfJson1));
    console.log("DeviceProfileId" + DeviceProfJson.dpid);
    if (typeof (DeviceProfJson.dpid) === 'undefined' && typeof (DeviceProfJson.osArchitect) === 'undefined' && typeof (DeviceProfJson.osdetails) === 'undefined' && typeof (DeviceProfJson.screenheight) === 'undefined' && typeof (DeviceProfJson.screenwidth) === 'undefined' && typeof (DeviceProfJson.timezone) === 'undefined' && typeof (DeviceProfJson.userlanguage) === 'undefined')
    {
        alert('Device Profile Could not be extracted');
        return;
    }
    console.log("Lattitude1" + Lattitude1);
    if (typeof (Lattitude1) === 'undefined' && typeof (Longitude1) === 'undefined')
    {
        alert('GPS is Disabled. Please Turn On GPS and Try Again!!!');
        return;
    }
    var kp = KEYUTIL.generateKeypair("RSA", "2048");
    var publicKeyTouse = KEYUTIL.getPEM(kp.pubKeyObj, "PKCS8PUB");
    var privateKeyTouse = KEYUTIL.getPEM(kp.prvKeyObj, "PKCS8PRV");
    console.log("public key" + publicKeyTouse);
    console.log("Private key" + privateKeyTouse);
    var strpublicKey = publicKeyTouse;   //publicKey; //Base64.encode(publicKey);
    var strprivateKey = privateKeyTouse;   //privateKey; //Base64.encode(privateKey);
    var deviceIdtoStore = DeviceProfJson.dpid;
    var plainJsonData = {"ip": ipAddress + DeviceProfJson.ip, "did": "" + DeviceProfJson.dpid, "os": "" + DeviceProfJson.osName, "vKey": strpublicKey, "pKey": strprivateKey, "regcode": "" + regcode, "longi": "73.8567437" + Longitude1, "latti": "73.8567437" + Lattitude1, "pin": pin, macaddress: ""};
    // var licscenseJson = LiscenceDetails.getInfo();
    var strServerPublicKey = sVisibleKeyforEncryption;
    //need to change licscenseJson.serverPublicKey;    // JSON.parse(licscenseJson);
    console.log("strServerPublicKey" + strServerPublicKey);
    console.log("aeskeyforTest" + generateTokenSecretInner(regcode, JSON.stringify(DeviceProfJson)));
    var strDatatoSend = EnforceMobileTrustInner(plainJsonData, generateTokenSecretInner(regcode, JSON.stringify(DeviceProfJson), JSON.stringify(AxiomLocation)), strServerPublicKey, strprivateKey, strpublicKey);
    console.log("deviceIdtoStore" + deviceIdtoStore);
    var aesKeyforlocalEncr = CryptoJS.SHA1(userid + deviceIdtoStore);
    var privateKeytoStore = AesEncrypt("" + aesKeyforlocalEncr, "" + aesKeyforlocalEncr, strprivateKey);
    var publicKeytoStore = strpublicKey;
    console.log('privateKeytoStore' + privateKeytoStore);
    console.log('publicKeytoStore' + publicKeytoStore);
    var strState = -9;
    var strHashPassword = CryptoJS.SHA1(userid + deviceIdtoStore);
    var strJsonDatatoStore = {"privatekey": privateKeytoStore, "publickey": publicKeytoStore, "state": strState, "hashedpassword": strHashPassword};
    var localStorageData = AesEncrypt("" + strHashPassword, "" + strHashPassword, JSON.stringify(strJsonDatatoStore));
    console.log("localStorageData" + localStorageData);
    console.log("aesKeyforlocalEncr" + aesKeyforlocalEncr);
    $.jStorage.set("" + aesKeyforlocalEncr, localStorageData);
    return strDatatoSend;
}

function Confirm(liscensekey, pin, tonfirm, useridT)
{
    console.log("going for Confirmation");
    var useridToUser = useridT;
    if (strcmpRegister(liscensekey, "") === 0 && strcmpRegister(useridT, "") === 0 && strcmpRegister(pin, "") === 0 && strcmpRegister(tonfirm, "") === 0)
    {
        alert('Liscenskey can not be null');
        return;
    }

    //license verification
    var licenseKeyJSON1 = CryptoJS.enc.Base64.parse(liscensekey);
    var licenseKeyJSON = CryptoJS.enc.Utf8.stringify(licenseKeyJSON1);
    var jsonLicense = JSON.parse(licenseKeyJSON);
    var _data = jsonLicense._data;
    var _data2Verify1 = CryptoJS.enc.Base64.parse(_data);
    var _data2Verify = CryptoJS.enc.Utf8.stringify(_data2Verify1);
    var _signature = jsonLicense._signature;
    var bytesLicenseDetailsJSON1 = CryptoJS.enc.Base64.parse(_data);
    var bytesLicenseDetailsJSON = CryptoJS.enc.Utf8.stringify(bytesLicenseDetailsJSON1);
    var jsonLicenseDetails = JSON.parse(bytesLicenseDetailsJSON);
    var strProductPublicKey = jsonLicenseDetails.sVisibleKey;
    var pubkeyObj = "-----BEGIN PUBLIC KEY-----" + strProductPublicKey + "-----END PUBLIC KEY-----";
    var bResult = VerifyDataRsa(_data2Verify1, _signature, "" + pubkeyObj);
    console.log("bResult" + bResult);
    var jsonLicenseDetails = JSON.parse(bytesLicenseDetailsJSON);
    if (bResult === false)
    {
        alert('Lisncse is Tempered');
        return;

    }
    var dExpiry = new Date(jsonLicenseDetails.lExpiry);
    var curDate = new Date();
    if (dExpiry.getTime() < curDate.getTime())
    {
        alert('Liscnse id Expired...!!!!!!!!!!!');
        return;
    }
    var DeviceProfJson = getDeviceDetailstoSave(useridT);
    var deviceId = DeviceProfJson.dpid;
    if (DeviceProfJson.dpid === null && DeviceProfJson.osArchitect === null && DeviceProfJson.osdetails === null && DeviceProfJson.screenheight === null && DeviceProfJson.screenwidth === null && DeviceProfJson.timezone === null && DeviceProfJson.userlanguage === null)
    {
        alert('Device Profile Could not be extracted');
        return -81;
    }

    if (Lattitude1 === null && Longitude1 === null)
    {
        alert('GPS is Disabled. Please Turn On GPS and Try Again!!!');
        return -82;
    }


    if (bResult === true)
    {
        console.log("deviceId" + deviceId);
        var keyforLocalStorage = CryptoJS.SHA1(useridToUser + deviceId);
        console.log("keyforLocalStorage" + keyforLocalStorage);
        var strData1 = $.jStorage.get("" + keyforLocalStorage);
        console.log("StorageData" + strData1);
        var hashedPass = CryptoJS.SHA1(useridToUser + deviceId);
        var strData = AesDecrypt("" + hashedPass, "" + hashedPass, strData1);
        console.log("STatedData" + strData);
        var jsonEncData = JSON.parse(strData);
        var iState = jsonEncData.state;
        var publickey = jsonEncData.publickey;
        var hashedpassword = jsonEncData.hashedpassword;
        console.log(iState);
        if (iState !== -9)
        {
            alert('Token State is invalid !!!');
            return -10;
        }
        var clientPrivateKe = jsonEncData.privatekey;
        console.log("***********************************************Private Key****************************");
        console.log("clientPrivateKey" + clientPrivateKe);
        console.log("***********************************************End ofPrivate Key****************************");
        var clientPrivateKey1 = AesDecrypt("" + keyforLocalStorage, "" + keyforLocalStorage, clientPrivateKe);
        console.log("***********************************************Decryoted Private Key****************************");
        console.log("DecryptedclientPrivateKey" + clientPrivateKey1);
        console.log("*********************************************** DecrypoiedEnd ofPrivate Key****************************");
        console.log("Object for Confirm" + tonfirm);
        var jsonData = ConsumeSecurityInner(tonfirm, clientPrivateKey1);
        if (jsonData === null)
        {
            alert('Confirmation Messarge is Tampered!!!');
            return -83;
        }
        var _json_plainData = JSON.parse(jsonData);
        var selfDestructAlertURL = _json_plainData.sDAUrl;
        var tokenSecret = _json_plainData.secret;
        var certificate = _json_plainData.cert;
        var userid = _json_plainData.userid;
        var channelid = _json_plainData.channelid;
        var did = _json_plainData.did;
        var otpLength = _json_plainData.otpLength;
        var sotpLength = _json_plainData.sotpLength;
        var otpduration = _json_plainData.otpDuration;
        var clientPrvKeyserv = _json_plainData.encprvkey;
        var pinAttempt = _json_plainData.sdAttemp;
        var pinDestructAlert = _json_plainData.sDAlert;
        var clientPubKey=_json_plainData.visibleKey;
        console.log("clientPrvKeyservFromServer" + clientPrvKeyserv);
        var jsonEncData = {"privatekey": clientPrvKeyserv, "publickey": clientPubKey, "hashedpassword": hashedpassword, "selfDestructAlertURL": selfDestructAlertURL, "certificate": certificate, "userid": userid, "channelid": channelid, "tokenSecret": tokenSecret, "otpLength": otpLength, "sotpLength": sotpLength, "otpduration": otpduration, "pinAttempt": pinAttempt, "pinDestructAlert": pinDestructAlert, "state": 1, "attempts": 0, "clientPrvKey": clientPrivateKe};
        var localStrageData = AesEncrypt("" + hashedPass, "" + hashedPass, JSON.stringify(jsonEncData));
        console.log("localStrageData" + localStrageData);
        console.log("keyforLocalStorage" + keyforLocalStorage);
        $.jStorage.set("" + keyforLocalStorage, localStrageData);
        console.log("Confirmation Successful..!!!!!!!!!");
        return 0;
    } else
    {
        console.log("Data from server is Tempered.!!!!!!!");
        return -83;
    }

}

//
//function checkStatus(userid)
//{
//    var DeviceProfJson1 = getDeviceDetailstoSave(userid);
//    var deviceId = DeviceProfJson1.dpid;
//    var keyforLocalStorage = CryptoJS.SHA1(useridT + deviceId);
//    var strData1 = $.jStorage.get("" + keyforLocalStorage);
//    if (typeof (strData1) === "undefined" || strcmpRegister(strData1, "") === 0)
//    {
//        return -1;
//    } else
//    {
//        return 0;
//    }
//
//}





function checkStatusMod(userid)

{ 
   
    var DeviceProfJson1 = getDeviceDetailstoSave(userid);
     var deviceId = DeviceProfJson1.dpid;
     console.log("userid"+userid+"deviceId"+deviceId);
    var keyforLocalStorage = CryptoJS.SHA1(userid + deviceId);
    var strData1 = $.jStorage.get("" + keyforLocalStorage);
   
     var countOfUser= $.jStorage.get(""+"_userCount");
//     if(typeof (countOfUser) === "undefined" || strcmpRegister(countOfUser, "") === 0)
//    {
//        return 0;
//    }
//     else
//   {
//       return -2;
//       
//   }
    if (typeof (strData1) === "undefined" || strcmpRegister(strData1, "") === 0)
    {
      return -1;
    } 
    else
    {
        return 0;
        
    }
   

  
}


//gloabal variables
var GjsonEncData = {};
var ServerPublicKey = "";
var licenseObject = {};
var privateKeyforTempUse = "";
function loadInner(liscensekey, userid1, pin)
{
    var useridT = userid1;
    console.log('Called  Initialize' + "PIN" + pin);
    if (strcmpRegister(liscensekey, "") === 0 && pin === null && strcmpRegister(pin, "") === 0 && strcmpRegister(userid1, "") === 0)
    {
        alert("liscensekey can not be empty");
        return;
    }
    //license verification
    var licenseKeyJSON1 = CryptoJS.enc.Base64.parse(liscensekey);
    var licenseKeyJSON = CryptoJS.enc.Utf8.stringify(licenseKeyJSON1);
    var jsonLicense = JSON.parse(licenseKeyJSON);
    var _data = jsonLicense._data;
    var _data2Verify1 = CryptoJS.enc.Base64.parse(_data);
    var _data2Verify = CryptoJS.enc.Utf8.stringify(_data2Verify1);
    var _signature = jsonLicense._signature;
    var bytesLicenseDetailsJSON1 = CryptoJS.enc.Base64.parse(_data);
    var bytesLicenseDetailsJSON = CryptoJS.enc.Utf8.stringify(bytesLicenseDetailsJSON1);
    var jsonLicenseDetails = JSON.parse(bytesLicenseDetailsJSON);
    licenseObject = jsonLicenseDetails;
    var strProductPublicKey = jsonLicenseDetails.sVisibleKey;
    console.log("strProductPublicKey" + strProductPublicKey);
    var pubkeyObj = "-----BEGIN PUBLIC KEY-----" + strProductPublicKey + "-----END PUBLIC KEY-----";
    var bResult = VerifyDataRsa(_data2Verify1, _signature, "" + pubkeyObj);
    var jsonLicenseDetails = JSON.parse(bytesLicenseDetailsJSON);
    if (bResult === false)
    {
        alert('Lisncse is Tempered');
        return;

    }
    var dExpiry = new Date(jsonLicenseDetails.lExpiry);
    var curDate = new Date();
    if (dExpiry.getTime() < curDate.getTime())
    {
        alert('Liscnse id Expired...!!!!!!!!!!!');
        return;
    }
    //end of license verification

    console.log("LIcense Verification Result" + bResult);

    if (bResult === false)
    {
        alert('Lisncse is Tempered');
        return;

    }
    var DeviceProfJson1 = getDeviceDetailstoSave(useridT);
    var deviceId = DeviceProfJson1.dpid;
    var keyforLocalStorage = CryptoJS.SHA1(useridT + deviceId);
    console.log("KeyforLocalstpoareg" + keyforLocalStorage);
    var strData1 = $.jStorage.get("" + keyforLocalStorage);
    console.log("strData1" + strData1);
//    if (strcmpRegister(strData1, "") === 0 || strcmpRegister(strData1), "undefined")
//    {
//        alert("Your profile has been tempered..!!!!!");
//        return -9;
//    }
    var hashedPass = CryptoJS.SHA1(useridT + deviceId);
    console.log("hashedPass" + hashedPass);
    var strData = AesDecrypt("" + hashedPass, "" + hashedPass, strData1);
    GjsonEncData = JSON.parse(strData);
    var iState = GjsonEncData.state;
    console.log("Starte" + iState);
    var publickey = GjsonEncData.publickey;
    var hashedpassword = GjsonEncData.hashedpassword;
    console.log(iState);
    if (iState == -9)
    {
        alert('Token State is invalid !!!');
        return -10;
    }

    return 0;
}


function generateOtp1()
{
    var secret = GjsonEncData.tokenSecret;
    var otpLength = GjsonEncData.otpLength;
    var otpDuration = GjsonEncData.otpduration;
    console.log(secret);
    console.log(otpLength);
    console.log(otpDuration);
    var Gotp = totp(secret, 'dec' + otpLength, otpDuration);
    console.log(Gotp);
    return Gotp;
}

function totp(key, format, durationOTP) {
    var increment = durationOTP * 1000;
    var T = parseInt(new Date().getTime() / increment);
    return updateOtp(key, durationOTP);
}


function generateSotpplus()
{
    if (licenseObject == null)
    {
        console.log("License can not be empty");
        return;
    }



}

function generateSotp(data)
{
    var user_secret = GjsonEncData.tokenSecret;
    var otpLength = GjsonEncData.otpLength;
    var otpDuration = GjsonEncData.otpduration;
    console.log("otpLength" + otpLength);
    console.log("otpDuration" + otpDuration);
    var dataForCerification = "";
    for (var i = 0; i < data.length; i++)
    {
        dataForCerification =dataForCerification+data[i];

    }
    console.log("dataForCerification"+dataForCerification);
    var byteDataHex =convertToHex(dataForCerification);
    var ocra = "";
    var seed = "";
    var ocraSuite = "";
    var counter = "";
    var password = "";
    var question = "";
    var timeStamp = "";

    var STOP = 5;

    var myDate = new Date();
    var b = new BigInteger("0");
    b = new BigInteger("0" + myDate.getTime());
    b = b.divide(new BigInteger("60000"));
     timeStamp=b.toString(16);
    if (strcmpWTRUST(otpLength, '6') === 0)
    {
        ocraSuite = "OCRA-1:TOTP-SHA1-6:QA64";
    }
    if (strcmpWTRUST(otpDuration, "60")) {
        ocraSuite += "-T1M";
    } else if (strcmpWTRUST(otpDuration, "120")) {
        ocraSuite += "-T2M";
    } else if (strcmpWTRUST(otpDuration, "180")) {
        ocraSuite += "-T3M";
    }
         ocraSuite="OCRA-1:TOTP-SHA1-6:QA64-T1M";
         console.log("ocraSuite" + ocraSuite);
        console.log("user_secret" + user_secret);
         console.log("byteDataHex" + byteDataHex);
         console.log("timeStamp" + timeStamp);
        var userSecret32=user_secret.substr(0,32);
       var secretForOcra= toHex(userSecret32);  //parseInt(userSecret32, 16);
       console.log("secretForOcra"+secretForOcra);
    ocra = generateOCRA(ocraSuite, secretForOcra, "", byteDataHex, "", "", timeStamp);
    console.log("Key: Standard 20Byte  Q: " + question + "  OCRA: " + ocra);
    return ocra;

}


function ConfirmResetOrChangePin(toConfirm)
{
    var pfxPassword = "";
    if (strcmpRegister(toConfirm, "") === 0)
    {
        alert("Confirmation Data can not be null");
        return;
    }
    var byteToconfirm = CryptoJS.enc.Utf8.parse(toConfirm);
    var base641 = CryptoJS.enc.Base64.stringify(byteToconfirm);
    var plainData = AesDecrypt(changeblePin, changeblePin, base641);
    if (!strcmpRegister(plainData, "") === 0)
    {
        var parsedJson = JSON.parse(plainData);
        var res = parsedJson.result;
        if (!strcmpRegister(res, "") === 0)
        {
            pfxPassword = parsedJson.pfxPassword;
        }
    } else {
        return false;
    }
    if (!strcmpRegister(res, "") === 0)
    {
        var keyforLocalStorage = CryptoJS.SHA1(userid + deviceId);
        alert("keyforLocalStorage" + keyforLocalStorage);
        var strData1 = $.jStorage.get("" + keyforLocalStorage);
        console.log("StorageData" + strData1);
        var hashedPass = CryptoJS.SHA1(useridToUser + deviceId);
        var strData = AesDecrypt("" + hashedPass, "" + hashedPass, strData1);
       // alert("STatedData" + strData);
        var _json_plainData = JSON.parse(strData);

        var selfDestructAlertURL = _json_plainData.sDAUrl;
        var tokenSecret = _json_plainData.secret;
        var certificate = _json_plainData.cert;
        var userid = _json_plainData.userid;
        var channelid = _json_plainData.channelid;
        var did = _json_plainData.did;
        var otpLength = _json_plainData.otpLength;
        var sotpLength = _json_plainData.sotpLength;
        var otpduration = _json_plainData.otpDuration;
        //block to store private key of client from server    
        var clientPrvKeyserv = _json_plainData.encprvkey;
        //block ended
        //to ecrypt private key with another pin

        var clientPrivateKe = _json_plainData.privatekey;
        var publicKey = _json_plainData.publickey;
        var hashedPass = _json_plainData.hashedpassword;
        var clientPrivateKey1 = AesDecrypt("" + pin, "" + pin, clientPrivateKe);
        var encPrivateKeytoUpdate = AesEncrypt("" + changeblePin, "" + changeblePin, "" + clientPrivateKey1);
        //
        var pinAttempt = _json_plainData.sdAttemp;
        var pinDestructAlert = _json_plainData.sDAlert;
        var jsonEncData = {"privatekey": encPrivateKeytoUpdate, "publickey": publicKey, "hashedpassword": hashedPass, "selfDestructAlertURL": pinDestructAlert, "certificate": certificate, "userid": userid, "channelid": channelid, "tokenSecret": tokenSecret, "otpLength": otpLength, "sotpLength": sotpLength, "otpduration": otpduration, "pinAttempt": pinAttempt, "pinDestructAlert": pinDestructAlert, "state": 1, "attempts": 0, "did": did, "selfDestructAlertURL":selfDestructAlertURL};
        var localStrageData = AesEncrypt("" + hashedPass, "" + hashedPass, JSON.stringify(jsonEncData));
        $.jStorage.set("" + keyforLocalStorage, localStrageData);
        alert("ConfirmResetorChangePin Successful..!!!!!!!!!");


    }
}

function enableORDisableDebug()
{


}

function SelfDestroy(userid)
{
    var DeviceProfJson1 = getDeviceDetailstoSave(userid);
    var deviceId = DeviceProfJson1.dpid;
    var keyforLocalStorage = CryptoJS.SHA1(userid + deviceId);
    $.jStorage.deleteKey(keyforLocalStorage);
    var strData1 = $.jStorage.get("" + keyforLocalStorage);
    if (strcmpRegister(strData1, "") === 0)
    {
        return true;
    } else
    {
        return false;
    }
}

function getLogs()
{


}

function GetOTPPlus()
{

}

function GetSOTPPlus()
{


}

function getOtpplus(userid)
{
    var otp = generateOtp1();
    var DeviceProfJson1 = getDeviceDetailstoSave(userid);
    var jsonDatatoSend = {"did": DeviceProfJson1.dpid, "mac": DeviceProfJson1.dpid, "otp": otp, "_deviceType": "WebTrust", "longi": "101", "latti": "102", "txType": "2", "ip": "192.0.0.1"};
    var clientPrivateKe = GjsonEncData.privatekey;
    console.log("SOOTPclientPrivateKe" + clientPrivateKe);
    console.log("DeviceProfJson1.dpid" + DeviceProfJson1.dpid);
    var aeskeyforDecryption =""+CryptoJS.SHA1(userid + DeviceProfJson1.dpid);
    console.log("encryptionkley" + aeskeyforDecryption);
    var xop =aeskeyforDecryption.substring(0,32);
    var clientPrivateKey1 = AesDecrypt(xop, xop, clientPrivateKe);
    console.log("clientPrivateKey1" + clientPrivateKey1);
    var strProductPublicKey = licenseObject.sVisibleKeyforEncryption;
    console.log("strProductPublicKey" + strProductPublicKey);
    var clientPivateK = "-----BEGIN PRIVATE KEY-----" + clientPrivateKey1 + "-----END PRIVATE KEY-----";
    console.log("clientPivateK" + clientPivateK);
    var strDatatoSend = EnforceMobileTrustInner(jsonDatatoSend, DeviceProfJson1.dpid, strProductPublicKey, "" + clientPivateK, GjsonEncData.publickey);
    return strDatatoSend;
}


function getSotpOtpplus(userid,data)
{
    var otp = generateSotp(data);
    var DeviceProfJson1 = getDeviceDetailstoSave(userid);
    var jsonDatatoSend = {"did":DeviceProfJson1.dpid, "mac": DeviceProfJson1.dpid, "otp":otp, "_deviceType": "WebTrust", "longi": "101", "latti": "102", "txType": "2", "ip": "192.0.0.1"};
    var clientPrivateKe = GjsonEncData.privatekey;
    console.log("SOOTPclientPrivateKe" + clientPrivateKe);
    console.log("DeviceProfJson1.dpid" + DeviceProfJson1.dpid);
   var aeskeyforDecryption =""+CryptoJS.SHA1(userid + DeviceProfJson1.dpid);   //CryptoJS.SHA1(userid + DeviceProfJson1.dpid);
    console.log("encryptionkley" + aeskeyforDecryption);
    var xop = aeskeyforDecryption.substring(0,32);
    var clientPrivateKey1 = AesDecrypt(xop, xop, clientPrivateKe);
    console.log("clientPrivateKey1" + clientPrivateKey1);
    var strProductPublicKey = licenseObject.sVisibleKeyforEncryption;
    console.log("strProductPublicKey" + strProductPublicKey);
    var clientPivateK = "-----BEGIN PRIVATE KEY-----" + clientPrivateKey1 + "-----END PRIVATE KEY-----";
    console.log("clientPivateK" + clientPivateK);
    var strDatatoSend = EnforceMobileTrustInner(jsonDatatoSend, DeviceProfJson1.dpid, strProductPublicKey, "" + clientPivateK, GjsonEncData.publickey);
    return strDatatoSend;
}