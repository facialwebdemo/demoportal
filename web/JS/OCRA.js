
//goog.provide("auth.OCRA");
//goog.require("goog.crypt.Hmac");
//goog.require("goog.crypt.Sha1");
//goog.require("goog.crypt.Sha256");

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tools function 

// Convert byte array to string


function base64ArrayBuffer(arrayBuffer) {
  var base64    = '';
  var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

  var bytes         = new Uint8Array(arrayBuffer);
  var byteLength    = bytes.byteLength;
  var byteRemainder = byteLength % 3;
  var mainLength    = byteLength - byteRemainder;

  var a, b, c, d;
  var chunk;

  // Main loop deals with bytes in chunks of 3
  for (var i = 0; i < mainLength; i = i + 3) {
    // Combine the three bytes into a single integer
    chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

    // Use bitmasks to extract 6-bit segments from the triplet
    a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
    b = (chunk & 258048)   >> 12 // 258048   = (2^6 - 1) << 12
    c = (chunk & 4032)     >>  6 // 4032     = (2^6 - 1) << 6
    d = chunk & 63               // 63       = 2^6 - 1

    // Convert the raw binary segments to the appropriate ASCII encoding
    base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
  }

  // Deal with the remaining bytes and padding
  if (byteRemainder == 1) {
    chunk = bytes[mainLength]

    a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

    // Set the 4 least significant bits to zero
    b = (chunk & 3)   << 4 // 3   = 2^2 - 1

    base64 += encodings[a] + encodings[b] + '=='
  } else if (byteRemainder == 2) {
    chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

    a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
    b = (chunk & 1008)  >>  4 // 1008  = (2^6 - 1) << 4

    // Set the 2 least significant bits to zero
    c = (chunk & 15)    <<  2 // 15    = 2^4 - 1

    base64 += encodings[a] + encodings[b] + encodings[c] + '='
  }
  
  return base64
}
function bin2String(array) {

    var result = "";
    for (var i = 0; i < array.length; ++i) {
        
        if(array[i]<0)
        {
             result +="\uFFFD";
            
        }else{
  
       result += String.fromCharCode(array[i]);

        }
    }
 
    return result;
}
function trimNull(a) {
  var c = a.indexOf('\0');
  if (c>-1) {
    return a.substr(0, c);
  }
  return a;
}


function packq(bytes) {
    var str = "";
// You could make it faster by reading bytes.length once.
    for(var i = 0; i < bytes.length; i += 2) {
// If you're using signed bytes, you probably need to mask here.
        var char = bytes[i] << 8;
// (undefined | 0) === 0 so you can save a test here by doing
//     var char = (bytes[i] << 8) | (bytes[i + 1] & 0xff);
        if (bytes[i + 1])
            char |= bytes[i + 1];
// Instead of using string += you could push char onto an array
// and take advantage of the fact that String.fromCharCode can
// take any number of arguments to do
//     String.fromCharCode.apply(null, chars);
        str += String.fromCharCode(char);
    }
    return str;
}

function unicodeToChar(text) {
   return text.replace(/\\u[\dA-F]{4}/gi, 
          function (match) {
               return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
          });
}

function pack(bytes) {
    var chars = [];
    for(var i = 0, n = bytes.length; i < n;) {
        chars.push(((bytes[i++] & 0xff) << 8) | (bytes[i++] & 0xff));
    }
    return String.fromCharCode.apply(null, chars);
}

function hex2a(hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}




function bytesToHex(b) {
  var hexchars = '0123456789abcdef';
  var hexrep = new Array(b.length * 2);

  for (var i = 0; i < b.length; ++i) {
    hexrep[i * 2] = hexchars.charAt((b[i] >> 4) & 15);
    hexrep[i * 2 + 1] = hexchars.charAt(b[i] & 15);
  }
  return hexrep.join('');
}

function stringToBytes(s) {
  var bytes = new Array(s.length);

  for (var i = 0; i < s.length; ++i) {
    bytes[i] = s.charCodeAt(i) & 255;
  }
  return bytes;
}

function hexToBytes(str) {
  var arr = [];

  for (var i = 0; i < str.length; i += 2) {
    arr.push(parseInt(str.substring(i, i + 2), 16));
  }

  return arr;
}

function hexStringToByte2(str) {
  if (!str) {
    return new Uint8Array();
  }
  
  var a = [];
  for (var i = 0, len = str.length; i < len; i+=2) {
    a.push(parseInt(str.substr(i,2),16));
//  console.log("hexStringToByte    ::  " + a.pop());
    }
  
  
  return a;
}


function hexStringToByte(str) {
  if (!str) {
    return new Uint8Array();
  }
  
  var a = [];
  for (var i = 0, len = str.length; i < len; i+=2) {
    a.push(parseInt(str.substr(i,2),16));
//  console.log("hexStringToByte    ::  " + a.pop());
    }
  //return a;
   return new Int8Array(a);
}




// Java's ArrayCopy() cast.
function ArrayCopy(src, srcPos, dest, destPos, length) {
	for (; srcPos < length; srcPos++) {
		dest[destPos++] = src[srcPos];
	}
	return dest;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// OCRA functions.

/**	 
 * This method uses the JCE to provide the crypto
 * algorithm.
 * HMAC computes a Hashed Message Authentication Code with the
 * crypto hash algorithm as a parameter.
 *
 * @param crypto	the crypto algorithm (HmacSHA1, HmacSHA256, HmacSHA512)
 * @param key   	the bytes to use for the HMAC key
 * @param text   	the message or text to be authenticated.
 */

function toUTF8Array(str) {
    var utf8 = [];
    for (var i=0; i < str.length; i++) {
        var charcode = str.charCodeAt(i);
        if (charcode < 0x80) utf8.push(charcode);
        else if (charcode < 0x800) {
            utf8.push(0xc0 | (charcode >> 6), 
                      0x80 | (charcode & 0x3f));
        }
        else if (charcode < 0xd800 || charcode >= 0xe000) {
            utf8.push(0xe0 | (charcode >> 12), 
                      0x80 | ((charcode>>6) & 0x3f), 
                      0x80 | (charcode & 0x3f));
        }
        // surrogate pair
        else {
            i++;
            // UTF-16 encodes 0x10000-0x10FFFF by
            // subtracting 0x10000 and splitting the
            // 20 bits of 0x0-0xFFFFF into two halves
            charcode = 0x10000 + (((charcode & 0x3ff)<<10)
                      | (str.charCodeAt(i) & 0x3ff));
            utf8.push(0xf0 | (charcode >>18), 
                      0x80 | ((charcode>>12) & 0x3f), 
                      0x80 | ((charcode>>6) & 0x3f), 
                      0x80 | (charcode & 0x3f));
        }
    }
    return utf8;
}

function hmac_sha1(crypto, key, text) {
	// Initialize HMAC algorithm.
	var hmac = null;
	if (crypto === "HmacSHA1") {
		hmac = new goog.crypt.Hmac(new goog.crypt.Sha1(),key);
	}
	else if (crypto === "HmacSHA256") {
		hmac = new goog.crypt.Hmac(new goog.crypt.Sha256(),key);
	}
	else {
		return "";
		console.log("DEBUG : use SHA512");
	}
	return bytesToHex(hmac.getHmac(text));
}

// 10 powers.
var DIGITS_POWER = [1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000];

/**
 * This method generates an OCRA HOTP value for the given
 * set of parameters.
 *
 * @param ocraSuite    the OCRA Suite
 * @param key          the shared secret, HEX encoded
 * @param counter      the counter that changes on a per use
 *                     basis, HEX encoded
 * @param question     the challenge question, HEX encoded
 * @param password     a password that can be used, HEX encoded
 * @param sessionInformation Static information that identifies
 *                     the current session, Hex encoded
 * @param timeStamp    a value that reflects a time
 *
 * @return A numeric var in base 10 that includes
 * {@link truncationDigits} digits
 */
function generateOCRA(ocraSuite, key, counter, question, password, sessionInformation, timeStamp) {
	var codeDigits = 0;
	var crypto = "";
	var result = null;
	var ocraSuiteLength = stringToBytes(ocraSuite).length;
	var counterLength = 0;
	var questionLength = 0;
	var passwordLength = 0;
	var sessionInformationLength = 0;
	var timeStampLength = 0;

	// The OCRASuites components
	var CryptoFunction = ocraSuite.split(":")[1];
	var DataInput = ocraSuite.split(":")[2];

	if (CryptoFunction.toLowerCase().indexOf("sha1") > 1) {
		crypto = "HmacSHA1";
	}
	if (CryptoFunction.toLowerCase().indexOf("sha256") > 1) {
		crypto = "HmacSHA256";
	}
	if (CryptoFunction.toLowerCase().indexOf("sha512") > 1) {
		crypto = "HmacSHA512";
	}
	
	// How many digits should we return
	codeDigits = parseInt(CryptoFunction.substring(CryptoFunction.lastIndexOf("-") + 1));

	// The size of the byte array message to be encrypted
	// Counter
	if (DataInput.toLowerCase().substr(0,1) === "c") {
		// Fix the length of the HEX var
		while (counter.length < 16) {
			counter = "0" + counter;
		}
		counterLength = 8;
	}
	
	// Question - always 128 bytes
	if (DataInput.toLowerCase().substr(0,1) === "q" || (DataInput.toLowerCase().indexOf("-q") >= 0)) {
		while (question.length < 256) {
			question = question + "0";
		}
		questionLength = 128;
	}

	// Password - sha1
	if (DataInput.toLowerCase().indexOf("psha1") > 1) {
		while (password.length < 40) {
			password = "0" + password;
		}
		passwordLength = 20;
	}

	// Password - sha256
	if (DataInput.toLowerCase().indexOf("psha256") > 1) {
		while (password.length < 64) {
			password = "0" + password;
		}
		passwordLength = 32;
	}

	// Password - sha512
	if (DataInput.toLowerCase().indexOf("psha512") > 1) {
		while (password.length < 128) {
			password = "0" + password;
		}
		passwordLength = 64;
	}

	// sessionInformation - s064
	if (DataInput.toLowerCase().indexOf("s064") > 1) {
		while (sessionInformation.length < 128) {
			sessionInformation = "0" + sessionInformation;
		}
		sessionInformationLength = 64;
	}

			// sessionInformation - s128
	if (DataInput.toLowerCase().indexOf("s128") > 1) {
		while (sessionInformation.length < 256) {
			sessionInformation = "0" + sessionInformation;
		}
		sessionInformationLength = 128;
	}

	// sessionInformation - s256
	if (DataInput.toLowerCase().indexOf("s256") > 1) {
		while (sessionInformation.length < 512) {
			sessionInformation = "0" + sessionInformation;
		}
		sessionInformationLength = 256;
	}

	// sessionInformation - s512
	if (DataInput.toLowerCase().indexOf("s512") > 1) {
		while (sessionInformation.length < 1024) {
			sessionInformation = "0" + sessionInformation;
		}
		sessionInformationLength = 512;
	}

	// TimeStamp
	if (DataInput.toLowerCase().substr(0,1) === "t" || (DataInput.toLowerCase().indexOf("-t") > 1)) {
		while (timeStamp.length < 16) {
			timeStamp = "0" + timeStamp;
		}
		timeStampLength = 8;
	}
        console.log("timeStampLength"+timeStampLength);
	// Remember to add "1" for the "00" byte delimiter
	var msg = [];
	msg.length = (ocraSuiteLength + counterLength + questionLength + passwordLength + sessionInformationLength + timeStampLength + 1);

      console.log("Length Of Array  :: "+msg.length);


	// Put the bytes of "ocraSuite" parameters varo the message
	var bArray = stringToBytes(ocraSuite);
	msg = ArrayCopy(bArray, 0, msg, 0, bArray.length);

	// Delimiter
	msg[bArray.length] = 0x00;

	// Put the bytes of "Counter" to the message
	// Input is HEX encoded
	if (counterLength > 0) {
		bArray = hexStringToByte(counter);
		msg = ArrayCopy(bArray, 0, msg, ocraSuiteLength + 1, bArray.length);
	}


	// Put the bytes of "question" to the message
	// Input is text encoded
	if (questionLength > 0) {
		bArray = hexStringToByte(question);
		msg = ArrayCopy(bArray, 0, msg, ocraSuiteLength + 1 + counterLength, bArray.length);
	}

	// Put the bytes of "password" to the message
	// Input is HEX encoded
	if (passwordLength > 0) {
		bArray = hexStringToByte(password);
		msg = ArrayCopy(bArray, 0, msg, ocraSuiteLength + 1 + counterLength + questionLength, bArray.length);
	}

	// Put the bytes of "sessionInformation" to the message
	// Input is text encoded
	if (sessionInformationLength > 0) {
		bArray = hexStringToByte(sessionInformation);
		msg = ArrayCopy(bArray, 0, msg, ocraSuiteLength + 1 + counterLength + questionLength + passwordLength, bArray.length);
	}
        
	// Put the bytes of "time" to the message
	// Input is text value of minutes
       // var  array1 = new Uint8Array(msg);
    
        var binaryString1 = String.fromCharCode.apply(null, msg);
        console.log("binaryStringBeforeTimeStamp"+binaryString1);
         
	if (timeStampLength > 0) {
		bArray = hexStringToByte(timeStamp);
         
             msg = ArrayCopy(bArray,0, msg, ocraSuiteLength + 1 + counterLength + questionLength + passwordLength + sessionInformationLength, bArray.length);
	}
   
        var binaryString2 = String.fromCharCode.apply(null, msg);
        console.log("binaryStringAfterTimeStamp"+new Int8Array(binaryString2));
        console.log("MsgBits"+msg);
            var binaryString12=bin2String(new Int8Array(msg));
             console.log("binaryString2"+binaryString12);
         // var desired=  binaryString12.replace(/[^\x00-\x7F]/g,"\uFFFD");
        var desired=  binaryString12.replace(/[^\x00-\x7F]/g,"99");
       // desired=desired.trim();
          bArray = hex2a(""+key);
       console.log("bArray"+bArray+"bArray"+bArray.length+"msg"+msg.length);
       var  array = new Uint8Array(msg);
       var binaryString = String.fromCharCode.apply(null, array);
        console.log("bArray"+bArray);
         var arrMsg=toUTF8Array(desired);
       console.log("desired"+desired);
        var b64encoded = base64ArrayBuffer(msg);
       var hash= CryptoJS.HmacSHA1(b64encoded,bArray);
        hash=""+hash;
        console.log("MyHash"+hash);
       var bytehex=hexStringToByte(""+hash);
       for (var i = 0; i < bytehex.length; i++) {
       console.log("Entry " + i + ": " + bytehex[i]);
     }
       hash = bytehex;
       var testOffset= bytehex[bytehex.length - 1] & 0x80;
       console.log("TestOffSet"+testOffset+bytehex);
       console.log("HashOffset"+bytehex[testOffset]);
//          hash=toUTF8Array(hash);
          
	console.log("KeyHash"+hash);
	console.log("hashLength"+hash.length);
	var offset = hash[hash.length - 1] & 0xf;
        
        console.log("Offset"+offset);
        console.log("Normaloffset"+hash[offset]);
        console.log("Test"+(hash[offset] & 0x7f));
         console.log("Test"+( hash[offset + 1] & 0xff));
       
       var binary = ((hash[offset] & 0x7f) << 24) | ((hash[offset + 1] & 0xff) << 16) | ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);
        
       console.log("binary"+""+binary);
	 var otp = binary % DIGITS_POWER[codeDigits];
       
	result = otp.toString();
	while (result.length < codeDigits) {
		result = "0" + result;
	}
	return result;
}
