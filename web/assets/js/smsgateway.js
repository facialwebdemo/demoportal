function strcmpSMS(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4SMSSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}


function ChangeActiveStatusSMS(perference,value) {
    if ( perference == 1) {
        if ( value == 1) {
            $('#_status').val("1");
            $('#_status-primary-sms').html("Active");
        } else {
            $('#_status').val("0");
            $('#_status-primary-sms').html("Suspended");
        }
    } else if ( perference == 2) {
        if ( value == 1) {
            $('#_statusS').val("1");
            $('#_status-secondary-sms').html("Active");
        } else {
            $('#_statusS').val("0");
            $('#_status-secondary-sms').html("Suspended");
        }
    }
}

function ChangeFailOverSMS(value) {
    //1 for enabled
    //0 for disabled
    if ( value == 1) {
        $('#_autofailover').val("1");
        $('#_autofailover-primary-sms').html("Enabled");
    } else {
        $('#_autofailover').val("0");
        $('#_autofailover-primary-sms').html("Disabled");
    }
}

function ChangeRetrySMS(preference,count) {
    //1 for enabled
    //0 for disabled
    if ( preference == 1) {
        if ( count == 2) {
            $('#_retries').val("2");
            $('#_retries-primary-sms').html("2 retries");
        } else if ( count == 3) {
            $('#_retries').val("3");
            $('#_retries-primary-sms').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retries').val("5");
            $('#_retries-primary-sms').html("5 retries");
        }
    } else if ( preference == 2) {
        if ( count == 2) {
            $('#_retriesS').val("2");
            $('#_retries-secondary-sms').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesS').val("3");
            $('#_retries-secondary-sms').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesS').val("5");
            $('#_retries-secondary-sms').html("5 retries");
        }
    }
}
function ChangeRetryDurationSMS(perference,duration) {
    //1 for enabled
    //0 for disabled
    if ( perference == 1) {
        if ( duration == 10) {
            $('#_retryduration').val("10");
            $('#_retryduration-primary-sms').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retryduration').val("30");
            $('#_retryduration-primary-sms').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retryduration').val("60");
            $('#_retryduration-primary-sms').html("60 seconds");
        }
    } else if ( perference == 2) {
        if ( duration == 10) {
            $('#_retrydurationS').val("10");
            $('#_retryduration-secondary-sms').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationS').val("30");
            $('#_retryduration-secondary-sms').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationS').val("60");
            $('#_retryduration-secondary-sms').html("60 seconds");
        }
    }
}



function smsprimary(){
    var s = './loadsmssettings?_type=1&_preference=1';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_status').val(data._status);
            $('#_ip').val(data._ip);
            $('#_port').val(data._port);
            $('#_userId').val(data._userId);
            $('#_password').val(data._password);
            $('#_phoneNumber').val(data._phoneNumber);
            
            if (data._logConfirmation == true) {
                //alert("data._logConfirmation true");
                $('#_logConfirmation').attr("checked", true);
            //$('#_logConfirmation').attr("value", "true");
                
            }else{
                //alert("data._logConfirmation false");
                $('#_logConfirmation').attr("checked", false);
            //$('#_logConfirmation').attr("value", "false");
            }
            $('#_className').val(data._className);
            $('#_reserve1').val(data._reserve1);
            $('#_reserve2').val(data._reserve2);
            $('#_reserve3').val(data._reserve3);
            $('#_autofailover').val(data._autofailover);
            $('#_retries').val(data._retries);
            $('#_retryduration').val(data._retryduration);
            $('#_messgeLength').val(data._messgeLength);
            
            if(data._messgeLength == 1)
                MessageLengthSMS(1,1);
            else
                MessageLengthSMS(1,0);


            //alert(data._status);

            if (data._status == 1)
                ChangeActiveStatusSMS(1,1);
            else
                ChangeActiveStatusSMS(1,0);

            if (data._autofailover == 1)
                ChangeFailOverSMS(1);
            else
                ChangeFailOverSMS(0);

            if (data._retryduration == 10 )
                ChangeRetryDurationSMS(1,10);
            else if (data._retryduration == 30)
                ChangeRetryDurationSMS(1,30);
            else if (data._retryduration == 60)
                ChangeRetryDurationSMS(1,60);
            else
                ChangeRetryDurationSMS(1,10);

            if (data._retries == 2)
                ChangeRetrySMS(1,2);
            else if (data._retries == 3)
                ChangeRetrySMS(1,3);
            else if (data._retries == 5)
                ChangeRetrySMS(1,5);
            else
                ChangeRetrySMS(1,2);            
        }
    });
}

function smssecondary(){
    var s = './loadsmssettings?_type=1&_preference=2';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_statusS').val(data._statusS);
            $('#_ipS').val(data._ipS);
            $('#_portS').val(data._portS);
            $('#_userIdS').val(data._userIdS);
            $('#_passwordS').val(data._passwordS);
            $('#_phoneNumberS').val(data._phoneNumberS);

            if (data._logConfirmationS == true) {
                $('#_logConfirmationS').attr("checked", true);                
            }else{
                $('#_logConfirmationS').attr("checked", false);
            }
            $('#_classNameS').val(data._classNameS);
            $('#_reserve1S').val(data._reserve1S);
            $('#_reserve2S').val(data._reserve2S);
            $('#_reserve3S').val(data._reserve3S);
            $('#_retriesS').val(data._retriesS);
            $('#_retrydurationS').val(data._retrydurationS);
            
            $('#_smessgeLength').val(data._messgeLengthS);
            if(data._messgeLengthS == 1)
                MessageLengthSMS(2,1);
            else
                MessageLengthSMS(2,0);


            if (data._statusS == 1)
                ChangeActiveStatusSMS(2,1);
            else
                ChangeActiveStatusSMS(2,0);

            if (data._retrydurationS == 10)
                ChangeRetryDurationSMS(2,10);
            else if (data._retrydurationS == 30)
                ChangeRetryDurationSMS(2,30);
            else if (data._retrydurationS == 60)
                ChangeRetryDurationSMS(2,60);
            else
                ChangeRetryDurationSMS(2,10);

            if (data._retriesS == 2)
                ChangeRetrySMS(2,2);
            else if (data._retriesS == 3)
                ChangeRetrySMS(2,3);
            else if (data._retriesS == 5)
                ChangeRetrySMS(2,5);
            else
                ChangeRetrySMS(2,2);            
        }
    }
    );
}

function LoadSMSSetting(type){
    if (type == 1) {
        smsprimary();
    } else if (type == 2 ){
        smssecondary();
    }
}


function editSMSprimary(){
    var s = './editsmssettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#smsprimaryform").serialize(),
        success: function(data) {
            if ( strcmpSMS(data._result,"error") == 0 ) {
                $('#save-sms-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpSMS(data._result,"success") == 0 ) {
//                $('#save-sms-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function editSMSsecondary(){
    var s = './editsmssettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#smsecondaryform").serialize(),
        success: function(data) {
            if ( strcmpSMS(data._result,"error") == 0 ) {
                $('#save-sms-gateway-secondary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpSMS(data._result,"success") == 0 ) {
//                $('#save-sms-gateway-secondary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function EditSMSSetting(type){    
    if (type == 1) {
        editSMSprimary();
    } else if (type == 2 ){
        editSMSsecondary();
    }
}

function LoadTestSMSConnectionUI(type){
    if ( type == 1 )
        $("#testSMSPrimary").modal();
    else
        $("#testSMSSecondary").modal();
}

function testconnectionSMSprimary(){

    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#testSMSPrimaryForm").serialize(),
        success: function(data) {
              pleaseWaitDiv.modal('hide');
            if ( strcmpSMS(data._result,"error") == 0 ) {
                  $('#test-sms-primary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                   $("#_testmsg").val("");
                $("#_testphone").val("");
               }
            else if ( strcmpSMS(data._result,"success") == 0 ) {
                $('#test-sms-primary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
                 $("#_testmsg").val("");
                $("#_testphone").val("");
            }
           
        }
    });
}

function testconnectionSMSsecondary(){
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection';
    pleaseWaitDiv.modal();

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testSMSSecondaryForm").serialize(),
        success: function(data) {
              pleaseWaitDiv.modal('hide');
            if ( strcmpSMS(data._result,"error") == 0 ) {
                $('#test-sms-secondary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $("#_testmsgS").val("");
                $("#_testphoneS").val("");
            }
            else if ( strcmpSMS(data._result,"success") == 0 ) {
                $('#test-sms-secondary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $("#_testmsgS").val("");
                $("#_testphoneS").val("");
            }
           
        }
    });
}


function TestSMSConnection(type){
    if (type == 1) {
        testconnectionSMSprimary();
    } else if (type == 2 ){
        testconnectionSMSsecondary();
    }
}

function MessageLengthSMS(preference,count) {
    if ( preference == 1) {
        if ( count == 1) {
//              alert(count);
            $('#_messgeLength').val("1");
            $('#_messgeLength-primary-sms').html("No");
        } else if ( count == 0) {
//            alert(count);
            $('#_messgeLength').val("0");
            $('#_messgeLength-primary-sms').html("Yes");
        }
       
    } 
    else if ( preference == 2) {
        if ( count == 1) {
//              alert(count);
            $('#_messgeLengthS').val("1");
            $('#_messgeLength-secondary-sms').html("No");
        } else if ( count == 0) {
//            alert(count);
            $('#_messgeLengthS').val("0");
            $('#_messgeLength-secondary-sms').html("Yes");
        }
       
    } 
}

