function strcmpEMAIL(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4EMAILSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}


function ChangeActiveStatusEMAIL(perference,value) {
    if ( perference == 1) {
        if ( value == 1) {
            $('#_statusEMAIL').val("1");
            $('#_status-primary-EMAIL').html("Active");
        } else {
            $('#_statusEMAIL').val("0");
            $('#_status-primary-EMAIL').html("Suspended");
        }
    } else if ( perference == 2) {
        if ( value == 1) {
            $('#_statusEMAILS').val("1");
            $('#_status-secondary-EMAIL').html("Active");
        } else {
            $('#_statusEMAILS').val("0");
            $('#_status-secondary-EMAIL').html("Suspended");
        }
    }
}

function ChangeFailOverEMAIL(value) {
    //1 for enabled
    //0 for disabled    
    if ( value == 1) {
        $('#_autofailoverEMAIL').val(1);
        $('#_autofailover-primary-EMAIL').html("Enabled");
    } else {
        $('#_autofailoverEMAIL').val(0);
        $('#_autofailover-primary-EMAIL').html("Disabled");
    }
}
function ChangeRetryEMAIL(preference,count) {
    //1 for enabled
    //0 for disabled
    if ( preference == 1) {
        if ( count == 2) {
            $('#_retriesEMAIL').val("2");
            $('#_retries-primary-EMAIL').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesEMAIL').val("3");
            $('#_retries-primary-EMAIL').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesEMAIL').val("5");
            $('#_retries-primary-EMAIL').html("5 retries");
        }
    } else if ( preference == 2) {
        if ( count == 2) {
            $('#_retriesEMAILS').val("2");
            $('#_retries-secondary-EMAIL').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesEMAILS').val("3");
            $('#_retries-secondary-EMAIL').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesEMAILS').val("5");
            $('#_retries-secondary-EMAIL').html("5 retries");
        }
    }
}
function ChangeRetryDurationEMAIL(perference,duration) {
    //1 for enabled
    //0 for disabled
    if ( perference == 1) {
        if ( duration == 10) {
            $('#_retrydurationEMAIL').val("10");
            $('#_retryduration-primary-EMAIL').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationEMAIL').val("30");
            $('#_retryduration-primary-EMAIL').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationEMAIL').val("60");
            $('#_retryduration-primary-EMAIL').html("60 seconds");
        }
    } else if ( perference == 2) {
        if ( duration == 10) {
            $('#_retrydurationEMAILS').val("10");
            $('#_retryduration-secondary-EMAIL').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationS').val("30");
            $('#_retryduration-secondary-EMAIL').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationEMAILS').val("60");
            $('#_retryduration-secondary-EMAIL').html("60 seconds");
        }
    }
}



function EMAILprimary(){
    var s = './loademailsettings?_preference=1';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            
            $('#_ipEMAIL').val(data._ip);
            $('#_portEMAIL').val(data._port);
            $('#_userIdEMAIL').val(data._userId);
            $('#_passwordEMAIL').val(data._password);
            $('#_classNameEMAIL').val(data._className);
            $('#_reserve1EMAIL').val(data._reserve1);
            $('#_reserve2EMAIL').val(data._reserve2);
            $('#_reserve3EMAIL').val(data._reserve3);
            $('#_autofailoverEMAIL').val(data._autofailover);
            $('#_retriesEMAIL').val(data._retries);
            $('#_retrydurationEMAIL').val(data._retryduration);
            
            $('#_fromNameEMAIL').val(data._fromname);
            $('#_fromEmailEMAIL').val(data._fromemail);

            if (data._isAuthRequired === true) {
                $('#_authRequiredEMAIL').attr("checked", true);
            }else{
                $('#_authRequiredEMAIL').attr("checked", false);
            }
            
            if (data._isSsl === true) {
                $('#_sslEMAIL').attr("checked", true);
            }else if(data._isSsl === false){
              $('#_sslEMAIL').attr("checked", false);
            }
            
            //alert(data._status);

            if (data._status == 1)
                ChangeActiveStatusEMAIL(1,1);
            else
                ChangeActiveStatusEMAIL(1,0);

            if (data._autofailover == 1)
                ChangeFailOverEMAIL(1);
            else
                ChangeFailOverEMAIL(0);

            if (data._retryduration == 10 )
                ChangeRetryDurationEMAIL(1,10);
            else if (data._retryduration == 30)
                ChangeRetryDurationEMAIL(1,30);
            else if (data._retryduration == 60)
                ChangeRetryDurationEMAIL(1,60);
            else
                ChangeRetryDurationEMAIL(1,10);

            if (data._retries == 2)
                ChangeRetryEMAIL(1,2);
            else if (data._retries == 3)
                ChangeRetryEMAIL(1,3);
            else if (data._retries == 5)
                ChangeRetryEMAIL(1,5);
            else
                ChangeRetryEMAIL(1,2);
        }
    });
}

function EMAILsecondary(){
    var s = './loademailsettings?_preference=2';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            
            $('#_ipEMAILS').val(data._ip);
            $('#_portEMAILS').val(data._port);
            $('#_userIdEMAILS').val(data._userId);
            $('#_passwordEMAILS').val(data._password);
            $('#_classNameEMAILS').val(data._className);
            $('#_reserve1EMAILS').val(data._reserve1);
            $('#_reserve2EMAILS').val(data._reserve2);
            $('#_reserve3EMAILS').val(data._reserve3);
            $('#_autofailoverEMAILS').val(data._autofailover);
            $('#_retriesEMAILS').val(data._retries);
            $('#_retrydurationEMAILS').val(data._retryduration);
            
            $('#_fromNameEMAILS').val(data._fromname);
            $('#_fromEmailEMAILS').val(data._fromemail);

            if (data._isAuthRequired == true) {
                $('#_authRequiredEMAILS').attr("checked", true);
            }else{
                $('#_authRequiredEMAILS').attr("checked", false);
            }
            
            if (data._isSsl == true) {
                $('#_sslEMAILS').attr("checked", true);
            }else{
                $('#_sslEMAILS').attr("checked", false);
            }
            
            //alert(data._status);


            if (data._autofailover == 1)
                ChangeFailOverEMAIL(2,1);
            else
                ChangeFailOverEMAIL(2,0);


            if (data._status == 1)
                ChangeActiveStatusEMAIL(2,1);
            else
                ChangeActiveStatusEMAIL(2,0);

            if (data._retryduration == 10 )
                ChangeRetryDurationEMAIL(2,10);
            else if (data._retryduration == 30)
                ChangeRetryDurationEMAIL(2,30);
            else if (data._retryduration == 60)
                ChangeRetryDurationEMAIL(2,60);
            else
                ChangeRetryDurationEMAIL(2,10);

            if (data._retries == 2)
                ChangeRetryEMAIL(2,2);
            else if (data._retries == 3)
                ChangeRetryEMAIL(2,3);
            else if (data._retries == 5)
                ChangeRetryEMAIL(2,5);
            else
                ChangeRetryEMAIL(2,2);
        }
    }
    );
}

function LoadEMAILSetting(type){
    if (type == 1) {
        EMAILprimary();
    } else if (type == 2 ){
        EMAILsecondary();
    }
}


function editEMAILprimary(){
    var s = './editemailsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#emailprimaryform").serialize(),
        success: function(data) {
            if ( strcmpEMAIL(data._result,"error") == 0 ) {
                $('#save-EMAIL-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4EMAILSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpEMAIL(data._result,"success") == 0 ) {
//                $('#save-EMAIL-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4EMAILSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function editEMAILsecondary(){
    var s = './editemailsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#emailsecondaryform").serialize(),
        success: function(data) {
            if ( strcmpEMAIL(data._result,"error") == 0 ) {
                $('#save-EMAIL-gateway-secondary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4EMAILSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpEMAIL(data._result,"success") == 0 ) {
//                $('#save-EMAIL-gateway-secondary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4EMAILSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function EditEMAILSetting(type){
    if (type == 1) {
        editEMAILprimary();
    } else if (type == 2 ){
        editEMAILsecondary();
    }
}

function LoadTestEMAILConnectionUI(type){
    if ( type == 1 )
        $("#testEMAILPrimary").modal();
    else
        $("#testEMAILSecondary").modal();
}

function testconnectionEMAILprimary(){
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testEMAILPrimaryForm").serialize(),
        success: function(data) {
              pleaseWaitDiv.modal('hide');
            if ( strcmpEMAIL(data._result,"error") == 0 ) {
                $('#test-EMAIL-primary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                 $("#_testmsg").val("");
                $("#_testphone").val("");
            }
            else if ( strcmpEMAIL(data._result,"success") == 0 ) {
                $('#test-EMAIL-primary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
                 $("#_testmsg").val("");
                $("#_testphone").val("");
            }
          
        }
    });
}

function testconnectionEMAILsecondary(){
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testEMAILSecondaryForm").serialize(),
        success: function(data) {
               pleaseWaitDiv.modal('hide');
            if ( strcmpEMAIL(data._result,"error") == 0 ) {
                $('#test-EMAIL-secondary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                 $("#_testmsgS").val("");
                $("#_testphoneS").val("");
            }
            else if ( strcmpEMAIL(data._result,"success") == 0 ) {
                $('#test-EMAIL-secondary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
           $("#_testmsgS").val("");
                $("#_testphoneS").val("");
            }
         
        }
    });
}

function TestEMAILConnection(type){
    if (type == 1) {
        testconnectionEMAILprimary();
    } else if (type == 2 ){
        testconnectionEMAILsecondary();
    }
}
