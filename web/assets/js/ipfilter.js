function strcmpIpFilter(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4IpFilterSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}


function ChangeStatusContentFilter(value) {
   
        if ( value == 1) {
            $('#_statusIP').val("1");
            $('#_status-content-filter').html("Active");
        } else {
            $('#_statusIP').val("0");
            $('#_status-content-filter').html("Suspended");
        }
    
}


function ContentFilter(){
    var s = './loadcontentfiltersetting';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
          ChangeStatusContentFilter($('#_statusIP').val(data._status));
           $('#_content').val(data._content);
        }
    });
}



function SaveIpFilerList(){
    $('#SaveIpFilteringButton').attr("disabled", true);
    
    var s = './editipfiltersetting';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ipfilterform").serialize(),
        success: function(data) {
            if ( strcmpIpFilter(data._result,"error") == 0 ) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4IpFilterSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveIpFilteringButton').attr("disabled", false);
            }
            else if (strcmpIpFilter(data._result,"success") == 0 ) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
               Alert4IpFilterSetting("<span><font color=blue>" + data._message + "</font></span>");
                
            }
        }
    });
}

