
function strcmpTemplates(a, b)
{   
    return (a<b?-1:(a>b?1:0));  
}

function Alert4Templates(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}


function RefreshMobileTemplatesList() {
    window.location.href = "./TemplateMobileList.jsp"    
}

function RefreshEmailTemplatesList() {
    window.location.href = "./TemplateEmailList.jsp"    
}


function editMessagetemplates(){
    var s = './edittemplates';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#messageedittemplateform").serialize(),
        success: function(data) {
            if ( strcmpTemplates(data._result,"error") == 0 ) {
                $('#edittemplateM-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                  Alert4Templates(data._message);
            }
            else if ( strcmpTemplates(data._result,"success") == 0 ) {
                $('#edittemplateM-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Templates(data._message);
                $('#buttonEditMessage').attr("disabled", true);
                
            }
        }
    }); 
}
function editEmailtemplates(){
    var s = './editemailtemplate';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#emailtemplateForm").serialize(),
        success: function(data) {
            if ( strcmpTemplates(data._result,"error") == 0 ) {
                $('#edittemplateE-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Templates(data._message);
            }
            else if ( strcmpTemplates(data._result,"success") == 0 ) {
                $('#edittemplateE-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Templates(data._message);
                $('#buttonEditMessage').attr("disabled", true);
               
            }
        }
    }); 
}

function loadMessageTemplateDetails(_tid){
    var s = './loadtemplate?_tid='+_tid;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpTemplates(data._result,"success") == 0 ) {    
                
                $('#idEditTemplateName').html(data._name);
                $('#idMessageTemplateId').val(data._tid);
                $("#_templateM_name").val(data._name);
                //$("#_templateM_subject").val(data._subject);
                $("#_templateM_body").val(data._body);
                $("#_templateM_variable").val(data._variables);
                $('#edittemplateM-result').html("");
                //$("#editMessageTemplate").modal();
            } else {
                Alert4Templates("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function loadEmailTemplateDetails(_tid){
    var s = './loadtemplate?_tid='+_tid;
    
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpTemplates(data._result,"success") == 0 ) {
                $('#idEditEmailTemplateName').html(data._name);                                
                $('#idEmailTemplateId').val(data._tid);
                $("#_templateE_name").val(data._name);
                $("#_templateE_subject").val(data._subject);
                $("#_templateE_body").val(data._body);
                $("#_templateE_variable").val(data._variables);
                $('#edittemplateE-result').html("");
                //$("#editEmailTemplate").modal();
            } else {
                Alert4Templates("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}



function removeTemplate(_tid){
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removetemplate?_tid='+_tid;
   
            $.ajax({
                type: 'GET',    
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpTemplates(data._result,"success") == 0 ) {
                        Alert4Templates("<span><font color=red>" + data._message + "</font></span>");                        
                        window.setTimeout(RefreshMobileTemplatesList, 2000);
              
                    } else {
                        Alert4Templates("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}


function removeEmailTemplate(_tid){
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removetemplate?_tid='+_tid;
   
            $.ajax({
                type: 'GET',    
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpTemplates(data._result,"success") == 0 ) {
                        Alert4Templates("<span><font color=red>" + data._message + "</font></span>");                        
                        window.setTimeout(RefreshEmailTemplatesList, 2000);
              
                    } else {
                        Alert4Templates("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}


function addtemplate(){
    var s = './savetemplates';
    $.ajax({
        type: 'POST',
        url:s,
        dataType: 'json',
        data: $("#templateform").serialize(),
        success: function(data) {
            if ( strcmpTemplates(data._result,"error") == 0 ) {
                $('#save-template-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                
            }
            else if ( strcmpTemplates(data._result,"success") == 0 ) {
                $('#save-template-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Templates(data._message);                                
                $('#SaveMobileMsgTemplate').attr("disabled", true);
            }
        }
    }); 
}

function addtemplate(){
    var s = './savetemplates';
    $.ajax({
        type: 'POST',
        url:s,
        dataType: 'json',
        data: $("#templateform").serialize(),
        success: function(data) {
            if ( strcmpTemplates(data._result,"error") == 0 ) {
                $('#save-template-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                
            }
            else if ( strcmpTemplates(data._result,"success") == 0 ) {
                $('#save-template-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Templates(data._message);                                
                $('#SaveMobileMsgTemplate').attr("disabled", true);
            }
        }
    }); 
}

function addemailtemplate(){
    var s = './savetemplates';
     $.ajax({
        type: 'POST',
        url:s,
        dataType: 'json',
        data: $("#emailtemplateForm").serialize(),
        success: function(data) {
            if ( strcmpTemplates(data._result,"error") == 0 ) {
                $('#save-secondrytemplate-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                
            }
            else if ( strcmpTemplates(data._result,"success") == 0 ) {
                $('#save-secondrytemplate-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Templates(data._message);
                $('#emailTemplate-button').attr("disabled", true);
            }
        }
    }); 
}



function ChangeType(value) {
    
    if ( value === 1) {
        $('#_types').val("1");
        $('#_type-primary-sms').html("Mobile");
    } 
    else if(value === 2){
        $('#_types').val("2");
        $('#_type-primary-sms').html("Email");
            
    }
    
        
}

function changetemplatestatus(_tid, _status,uidiv) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {

            var s = './changetemplatestatus?_status=' + _status + '&_templateid=' + encodeURIComponent(_tid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTemplates(data._result, "success") == 0) {
                        Alert4Templates("<span><font color=blue>" + data._message + "</font></span>");
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);

                    } else {
                        Alert4Templates("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}