function strcmpSocial(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4SocialSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}



function ChangeRetrySocial(type, count) {
    //1 for enabled
    //0 for disabled
    if (type == 1) {
        if (count == 2) {
            $('#_retries').val("2");
            $('#_retries-primary-social').html("2 retries");
        } else if (count == 3) {
            $('#_retries').val("3");
            $('#_retries-primary-social').html("3 retries");
        }
        else if (count == 5) {
            $('#_retries').val("5");
            $('#_retries-primary-social').html("5 retries");
        }
    }else if (type == 2) {
        if (count == 2) {
            $('#_linkedInretries').val("2");
            $('#_retries-linkedin-social').html("2 retries");
        } else if (count == 3) {
            $('#_linkedInretries').val("3");
            $('#_retries-linkedin-social').html("3 retries");
        }
        else if (count == 5) {
            $('#_linkedInretries').val("5");
            $('#_retries-linkedin-social').html("5 retries");
        }
    }else if (type == 3) {
        if (count == 2) {
            $('#_twitterretries').val("2");
            $('#_retries-twitter-social').html("2 retries");
        } else if (count == 3) {
            $('#_twitterretries').val("3");
            $('#_retries-twitter-social').html("3 retries");
        }
        else if (count == 5) {
            $('#_twitterretries').val("5");
            $('#_retries-twitter-social').html("5 retries");
        }
    }
}


function ChangeActiveStatusSocial(type, value) {
    if (type == 1) {
        if (value == 1) {
            $('#_status').val("1");
            $('#_status-primary-social').html("Active");
        } else {
            $('#_status').val("0");
            $('#_status-primary-social').html("Suspended");
        }
    } else if (type == 2) {
        if (value == 1) {
            $('#_linkedInstatus').val("1");
            $('#_status-linkedin-social').html("Active");
        } else {
            $('#_linkedInstatus').val("0");
            $('#_status-linkedin-social').html("Suspended");
        }
    } else if (type == 3) {
        if (value == 1) {
            $('#_twitterstatus').val("1");
            $('#_status-twitter-social').html("Active");
        } else {
            $('#_twitterstatus').val("0");
            $('#_status-twitter-social').html("Suspended");
        }
    }
}


function ChangeRetryDurationSocial(type, duration) {
    //1 for enabled
    //0 for disabled
    if (type == 1) {
        if (duration == 10) {
            $('#_retryduration').val("10");
            $('#_retryduration-primary-social').html("10 seconds");
        } else if (duration == 30) {
            $('#_retryduration').val("30");
            $('#_retryduration-primary-social').html("30 seconds");
        }
        else if (duration == 60) {
            $('#_retryduration').val("60");
            $('#_retryduration-primary-social').html("60 seconds");
        }
    }else if (type == 2) {
        if (duration == 10) {
            $('#_linkedInretryduration').val("10");
            $('#_retryduration-linkedin-social').html("10 seconds");
        } else if (duration == 30) {
            $('#_linkedInretryduration').val("30");
            $('#_retryduration-linkedin-social').html("30 seconds");
        }
        else if (duration == 60) {
            $('#_linkedInretryduration').val("60");
            $('#_retryduration-linkedin-social').html("60 seconds");
        }
    }else if (type == 3) {
        if (duration == 10) {
            $('#_twitterretryduration').val("10");
            $('#_retryduration-twitter-social').html("10 seconds");
        } else if (duration == 30) {
            $('#_twitterretryduration').val("30");
            $('#_retryduration-twitter-social').html("30 seconds");
        }
        else if (duration == 60) {
            $('#_twitterretryduration').val("60");
            $('#_retryduration-twitter-social').html("60 seconds");
        }
    }
}

function EditSocialSetting(type) {
    if (type == 1) {
        editFacebookSetting(type);
    } else if (type == 2) {
        editLinkedInSetting(type);
    } else if (type == 3) {
        editTwittersecondary(type);
    }
}

function editFacebookSetting(val) {
    var s = './editsocialsettings?_type='+val;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#facebookform").serialize(),
        success: function(data) {
            if (strcmpSocial(data._result, "error") == 0) {
                $('#save-social-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4SocialSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpSocial(data._result, "success") == 0) {
                $('#save-social-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4SocialSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}
function editLinkedInSetting(val) {
    var s = './editsocialsettings?_type='+val;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#linkedinform").serialize(),
        success: function(data) {
            if (strcmpSocial(data._result, "error") == 0) {
                $('#save-linkedin-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4SocialSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpSocial(data._result, "success") == 0) {
                $('#save-linkedin-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4SocialSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}
function editTwittersecondary(val) {
    var s = './editsocialsettings?_type='+val;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#twitterform").serialize(),
        success: function(data) {
            if (strcmpSocial(data._result, "error") == 0) {
                $('#save-social-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4SocialSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpSocial(data._result, "success") == 0) {
                $('#save-social-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4SocialSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function LoadSocialSetting(type) {
    if (type == 1) {
        loadfacebook();
    } else if (type == 2) {
        loadlinkedin();
    }else if (type == 3) {
        loadtwitter();
    }
}


function loadfacebook() {
    var s = './loadsocialsettings?_type=1';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            $('#_facebookappid').val(data._facebookappid);
            $('#_facebookappsecret').val(data._facebookappsecret);
            $('#_facebookURL').val(data._facebookURL);
            $('#_reserve1').val(data._reserve1);
            $('#_reserve2').val(data._reserve2);
            $('#_reserve3').val(data._reserve3);
            $('#_retries').val(data._retries);
            $('#_retryduration').val(data._retryduration);
            $('#_status').val(data._status);
           if (data._status == 1)
                ChangeActiveStatusSocial(1, 1);
            else
                ChangeActiveStatusSocial(1, 0);

            if (data._retryduration == 10)
                ChangeRetryDurationSocial(1, 10);
            else if (data._retryduration == 30)
                ChangeRetryDurationSocial(1, 30);
            else if (data._retryduration == 60)
                ChangeRetryDurationSocial(1, 60);
            else
                ChangeRetryDurationSocial(1, 10);

            if (data._retries == 2)
                ChangeRetrySocial(1, 2);
            else if (data._retries == 3)
                ChangeRetrySocial(1, 3);
            else if (data._retries == 5)
                ChangeRetrySocial(1, 5);
            else
                ChangeRetrySocial(1, 2);
           }
    });
}

function loadlinkedin() {
    var s = './loadsocialsettings?_type=2';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            $('#_linkedinkey').val(data._linkedinkey);
            $('#_linkedinsecret').val(data._linkedinsecret);
            $('#_linkedintoken').val(data._linkedintoken);
            $('#_linkedinaccesssecret').val(data._linkedinaccesssecret);
            $('#_linkedinURL').val(data._linkedinURL);
            $('#_linkedinAccessURL').val(data._linkedinAccessURL);
            $('#_linkedinreserve1').val(data._linkedinreserve1);
            $('#_linkedinreserve2').val(data._linkedinreserve2);
            $('#_linkedinreserve3').val(data._linkedinreserve3);
             $('#_linkedInstatus').val(data._linkedInstatus);
             $('#_linkedInretries').val(data._linkedInretries);
             $('#_linkedInretryduration').val(data._linkedInretrydurations);

            //alert(data._status);

            if (data._linkedInstatus == 1)
                ChangeActiveStatusSocial(2, 1);
            else
                ChangeActiveStatusSocial(2, 0);

            if (data._linkedInretryduration == 10)
                ChangeRetryDurationSocial(2, 10);
            else if (data._linkedInretryduration == 30)
                ChangeRetryDurationSocial(2, 30);
            else if (data._linkedInretryduration == 60)
                ChangeRetryDurationSocial(2, 60);
            else
                ChangeRetryDurationSocial(2, 10);

            if (data._linkedInretries == 2)
                ChangeRetrySocial(2, 2);
            else if (data._linkedInretries == 3)
                ChangeRetrySocial(2, 3);
            else if (data._linkedInretries == 5)
                ChangeRetrySocial(2, 5);
            else
                ChangeRetrySocial(2, 2);
        }
    });
}
function loadtwitter() {
    var s = './loadsocialsettings?_type=3';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            $('#_twitterkey').val(data._twitterkey);
            $('#_twittersecret').val(data._twittersecret);
            $('#_twitterURL').val(data._twitterURL);
            $('#_twitterCallbackURL').val(data._twitterCallbackURL);
            $('#_twitterreserve1').val(data._twitterreserve1);
            $('#_twitterreserve2').val(data._twitterreserve2);
            $('#_twitterreserve3').val(data._twitterreserve3);
//            $('#_retries').val(data._retries);
            $('#_twitterretryduration').val(data._twitterretryduration);
            $('#_twitterstatus').val(data._twitterstatus);
             $('#_twitterretries').val(data._twitterretries);
            //alert(data._status);
            if (data._twitterstatus == 1)
                ChangeActiveStatusSocial(3, 1);
            else
                ChangeActiveStatusSocial(3, 0);

            if (data._twitterretryduration == 10)
                ChangeRetryDurationSocial(3, 10);
            else if (data._twitterretryduration == 30)
                ChangeRetryDurationSocial(3, 30);
            else if (data._twitterretryduration == 60)
                ChangeRetryDurationSocial(3, 60);
            else
                ChangeRetryDurationSocial(3, 10);

            if (data._twitterretries == 2)
                ChangeRetrySocial(3, 2);
            else if (data._twitterretries == 3)
                ChangeRetrySocial(3, 3);
            else if (data._twitterretries == 5)
                ChangeRetrySocial(3, 5);
            else
                ChangeRetrySocial(3, 2);

//            alert(data._messgeLength);

        }
    });
}

