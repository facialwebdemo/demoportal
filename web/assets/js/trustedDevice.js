/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function strcmpTrustedDevice(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4TrustedDevice(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshTrustedDevice() {
    window.location.href = "./trustedevice.jsp";
}

function DeviceAudits(_userid, _itemtype, _duration) {

    var s = './getMobiletrustaudit?_userid=' + encodeURIComponent(_userid) + "&_duration=" + _duration + "&_format=pdf" + "&_itemType=" + encodeURIComponent(_itemtype);
    window.location.href = s;
    return false;

}

function changeDeviceStatus(status, userid, uidiv) {
    var s = './changedevicestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpTrustedDevice(data._result, "error") == 0) {
                Alert4TrustedDevice("<span><font color=red>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTrustedDevice(data._result, "success") == 0) {
                Alert4TrustedDevice("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}


function loadEditTrustedDeviceDetails(userid) {
    var s = './getTrustDevice?_callid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (Alert4TrustedDevice(data._result, "success") == 0) {

                $("#editTrustedDeviceDetails").modal();
            } else {
                Alert4TrustedDevice("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}


function removeTrustedDevice(_userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeTrustDevice?_userid=' + _userid;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTrustedDevice(data._result, "success") == 0) {
                        Alert4TrustedDevice("<span><font color=red>" + data._message + "</font></span>");
                        window.setTimeout(RefreshTrustedDevice, 2000);

                    } else {
                        Alert4TrustedDevice("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function changedevicestatus(status, userid, uidiv) {
    var s = './changedevicestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpTrustedDevice(data._result, "error") == 0) {
                Alert4TrustedDevice("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTrustedDevice(data._result, "success") == 0) {
                Alert4TrustedDevice("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}

function searchDevices() {

    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

    if (val.length <= 3) {
        Alert4TrustedDevice("Keyword should be meaningful and/or more than 3 characters");
        return;
    }
    var s = './trusteddevicetables.jsp?_searchtext=' + val;
     pleaseWaitDiv.modal();
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
            $('#device_table_main').html(data);
             pleaseWaitDiv.modal('hide');
        }
    });
}

function ClientPasswordTrail(_userid, _duration) {
    var s = './trusteddevicepasswordattempts.jsp?_userid=' + encodeURIComponent(_userid) + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;
}

function ClientPasswordReport(_userId, _duration) {

    var s = './getClientPasswordAuditTrailsReport?_userId=' + encodeURIComponent(_userId) + "&_duration=" + _duration + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function ClientPasswordReportPDF(_userId, _duration) {

    var s = './getClientPasswordAuditTrailsReport?_userId=' + encodeURIComponent(_userId) + "&_duration=" + _duration + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function PasswordAttemptsMap(_userid, duration) {

    var jsonData = $.ajax({
        url: './trusteddevicepasswordattemptsmap.jsp?_userid=' + encodeURIComponent(_userid) + '&_duration=' + duration,
//        url: "./parkingmaps.jsp",
        dataType: "json",
        async: false
    }).responseText;

    var map = new google.maps.Map(document.getElementById('map_transaction'), {
        zoom: 2,
        center: new google.maps.LatLng(3.15837, 103.76141),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infoWindow = new google.maps.InfoWindow;

    var onMarkerClick = function() {
        var marker = this;
        var latLng = marker.getPosition();
        infoWindow.setContent('Transaction Coordinates:' +
                latLng.lat() + ', ' + latLng.lng() + ',' + latLng.address);
        infoWindow.open(map, marker);
    };

    google.maps.event.addListener(map, 'click', function() {
        infoWindow.close();
    });

//    var myJsonObj = jsonParse(jsonData);
//    for (var k in myJsonObj) {
//        var marker1 = new google.maps.Marker({
//            map: map,
//            position: new google.maps.LatLng(myJsonObj[k]._lat, myJsonObj[k]._lng)
//            
//            });
//        google.maps.event.addListener(marker1, 'click', onMarkerClick);
//    }
    var myJsonObj = jsonParse(jsonData);
    for (var k in myJsonObj) {
        var marker1 = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(myJsonObj[k]._lat, myJsonObj[k]._lng),
            txType: myJsonObj[k]._txtype

        });
//        google.maps.event.addListener(marker1, 'click', onMarkerClick);
//     makeInfoWindowEvent(map, infoWindow,'<h3>'+myJsonObj[k]._txtype+'<h3><h4>'+myJsonObj[k]._city+'</h4>', marker1);
//        alert(myJsonObj[k]._zipcode);
        makeInfoWindowEventDestroy(map, infoWindow, '<p><h3>Name ::' + myJsonObj[k]._uname + '</h3><h4>Location ::'
                + myJsonObj[k].address + '</h3><h4>Reason ::' + myJsonObj[k]._reason + '</h4>'
                + '<h5>Time ::' + myJsonObj[k]._dtime + '</h5></p>', marker1);

        function makeInfoWindowEventDestroy(map, infowindow, contentString, marker1) {
            google.maps.event.addListener(marker1, 'click', function() {
                infowindow.setContent(contentString);
                infowindow.open(map, marker1);
            });
        }
    }

}
