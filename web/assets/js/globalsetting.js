function strcmpGlobal(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}


function Alert4GlobalSetting(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function SetPACE(category, value, _div, disp) {

    if (category === 1) {
        $('#_slowPaceMobile').val(value);
        $(_div).html(disp);
    }
    else if (category === 2) {
        $('#_normalPaceMobile').val(value);
        $(_div).html(disp);
    } else if (category === 3) {
        $('#_fastPaceMobile').val(value);
        $(_div).html(disp);
    } else if (category === 4) {
        $('#_hyperPaceMobile').val(value);
        $(_div).html(disp);
    }
}


function SetPACEEmail(category, value, _div, disp) {
    //    alert(category);
    //    alert(value);
    if (category === 1) {
        $('#_slowPaceEmail').val(value);
        $(_div).html(disp);
    }
    else if (category === 2) {
        $('#_normalPaceEmail').val(value);
        $(_div).html(disp);
    } else if (category === 3) {
        $('#_fastPaceEmail').val(value);
        $(_div).html(disp);
    } else if (category === 4) {
        $('#_hyperPaceEmail').val(value);
        $(_div).html(disp);
    }
}
function SetVoiceSetting(value, _div, disp) {
    if (value == 1) {
        $('#_voiceSetting').val(value);
        $(_div).html(disp);
    }
    else if (value == 2) {
        $('#_voiceSetting').val(value);
        $(_div).html(disp);
    }
}


function LoadGlobalSettings() {
    var s = './loadglobalsettings';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            $('#_slowPaceMobile').val(data._slowPaceMobile);
            $('#_normalPaceMobile').val(data._normalPaceMobile);
            $('#_fastPaceMobile').val(data._fastPaceMobile);
            $('#_hyperPaceMobile').val(data._hyperPaceMobile);
            $('#_slowPaceEmail').val(data._slowPaceEmail);
            $('#_normalPaceEmail').val(data._normalPaceEmail);
            $('#_fastPaceEmail').val(data._fastPaceEmail);
            $('#_hyperPaceEmail').val(data._hyperPaceEmail);
            $('#_FXListner').val(data._FXListner);
            $('#_voiceSetting').val(data._voiceSetting);
            $('#_vduration').val(data._vduration);
            //      alert(data._slowPaceMobile);

            if (data._slowPaceMobile == 1) {
                SetPACE(1, 1, '#_slowPaceMobile_div', '1 message/second');
            }
            else if (data._slowPaceMobile == 3) {

                SetPACE(1, 3, '#_slowPaceMobile_div', '3 messages/second');
            }

            else if (data._slowPaceMobile == 5) {

                SetPACE(1, 5, '#_slowPaceMobile_div', '5 messages/second');
            }

            if (data._normalPaceMobile == 6)
                SetPACE(2, 6, '#_normalPaceMobile_div', '6 messages/second');
            else if (data._normalPaceMobile == 8)
                SetPACE(2, 8, '#_normalPaceMobile_div', '8 messages/second');
            else if (data._normalPaceMobile == 10)
                SetPACE(2, 10, '#_normalPaceMobile_div', '10 messages/second');

            if (data._fastPaceMobile == 15)
                SetPACE(3, 15, '#_fastPaceMobile_div', '15 messages/second');
            else if (data._fastPaceMobile == 20)
                SetPACE(3, 20, '#_fastPaceMobile_div', '20 messages/second');
            else if (data._fastPaceMobile == 25)
                SetPACE(3, 25, '#_fastPaceMobile_div', '25 messages/second');
            else if (data._fastPaceMobile == 30)
                SetPACE(3, 30, '#_fastPaceMobile_div', '30 messages/second');

            if (data._hyperPaceMobile == 35)
                SetPACE(4, 35, '#_hyperPaceMobile_div', '35 messages/second');
            else if (data._hyperPaceMobile == 50)
                SetPACE(4, 50, '#_hyperPaceMobile_div', '50 messages/second');
            else if (data._hyperPaceMobile == 75)
                SetPACE(4, 75, '#_hyperPaceMobile_div', '75 messages/second');
            else if (data._hyperPaceMobile == 100)
                SetPACE(4, 100, '#_hyperPaceMobile_div', '100 messages/second');


            //email

            if (data._slowPaceEmail == 1)
                SetPACEEmail(1, 1, '#_slowPaceEmail_div', '1 message/second');
            else if (data._slowPaceEmail == 3)
                SetPACEEmail(1, 3, '#_slowPaceEmail_div', '3 messages/second');
            else if (data._slowPaceEmail == 5)
                SetPACEEmail(1, 5, '#_slowPaceEmail_div', '5 messages/second');

            if (data._normalPaceEmail == 6)
                SetPACEEmail(2, 6, '#_normalPaceEmail_div', '6 messages/second');
            else if (data._normalPaceEmail == 8)
                SetPACEEmail(2, 8, '#_normalPaceEmail_div', '8 messages/second');
            else if (data._normalPaceEmail == 10)
                SetPACEEmail(2, 10, '#_normalPaceEmail_div', '10 messages/second');

            if (data._fastPaceEmail == 15)
                SetPACEEmail(3, 15, '#_fastPaceEmail_div', '15 messages/second');
            else if (data._fastPaceEmail == 20)
                SetPACEEmail(3, 20, '#_fastPaceEmail_div', '20 messages/second');
            else if (data._fastPaceEmail == 25)
                SetPACEEmail(3, 25, '#_fastPaceEmail_div', '25 messages/second');
            else if (data._fastPaceEmail == 30)
                SetPACEEmail(3, 30, '#_fastPaceEmail_div', '30 messages/second');

            if (data._hyperPaceEmail == 35)
                SetPACEEmail(4, 35, '#_hyperPaceEmail_div', '35 messages/second');
            else if (data._hyperPaceEmail == 50)
                SetPACEEmail(4, 50, '#_hyperPaceEmail_div', '50 messages/second');
            else if (data._hyperPaceEmail == 75)
                SetPACEEmail(4, 75, '#_hyperPaceEmail_div', '75 messages/second');
            else if (data._hyperPaceEmail == 100)
                SetPACE(4, 100, '#_hyperPaceMobile_div', '100 messages/second');

            //voice
            //      alert(data._voiceSetting);
            if (data._voiceSetting == 1)
                SetVoiceSetting(1, '#_voiceSetting_div', 'Yes');
            else if (data._voiceSetting == 2)
                SetVoiceSetting(1, '#_voiceSetting_div', 'No');
            
            
            $('#_weigtage').val(data._weigtage);
                $('#_answersattempts').val(data._answersattempts);
                
                if(data._weigtage == 75)
                    SetPercentage(75, '75 Percent');
                else if(data._weigtage == 80)
                     SetPercentage(80, '80 Percent');
                else if(data._weigtage == 90)
                     SetPercentage(90, '90 Percent');
                else if(data._weigtage == 100)
                     SetPercentage(100, '100 Percent');
                 
                 if(data._answersattempts == 3)
                     setAttempts(3, '3 attempts');
                 else if(data._answersattempts == 5)
                     setAttempts(5, '5 attempts');
                 else if(data._answersattempts == 7)
                     setAttempts(7, '7 attempts');

        }
    });
}

function editGlobalSettings() {
    var s = './editglobalsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#globalsettingsform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") == 0) {
                $('#save-global-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpGlobal(data._result, "success") == 0) {
                $('#save-global-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function SetPercentage(count, disp) {

    if (count == 75) {
        $('#_weigtage').val(count);
        $('#_weigtage_div').html(disp);
    } else if (count == 80) {
        $('#_weigtage').val(count);
        $('#_weigtage_div').html(disp);
    }
    else if (count == 90) {
        $('#_weigtage').val(count);
        $('#_weigtage_div').html(disp);
    } else if (count == 100) {
        $('#_weigtage').val(count);
        $('#_weigtage_div').html(disp);
    }

}

function setAttempts(count, disp) {

    if (count == 3) {
        $('#_answersattempts').val(count);
        $('#_answersattempts_div').html(disp);
    } else if (count == 5) {
        $('#_answersattempts').val(count);
        $('#_answersattempts_div').html(disp);
    }
    else if (count == 7) {
        $('#_answersattempts').val(count);
        $('#_answersattempts_div').html(disp);
    }

}