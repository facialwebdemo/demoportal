function ChangeEcopinType(value) {
   
       if ( value === 1) {
        $('#_changeStatusType').val("1");
        $('#_change-Status-Type-Axiom').html("Operator Controlled");
    } else if(value === 2){
        $('#_changeStatusType').val("2");
        $('#_change-Status-Type-Axiom').html("Delivery");
    } else if(value === 3){
        $('#_changeStatusType').val("3");
        $('#_change-Status-Type-Axiom').html("Policy Check");
    }else if(value === 4){
        $('#_changeStatusType').val("4");
        $('#_change-Status-Type-Axiom').html("QA Validation");
    }else if(value === 0){
        $('#_changeStatusType').val("0");
        $('#_change-Status-Type-Axiom').html("All");
    }
    
}
function BarChartDemo1(val1, val2, val3,val) {
    var s = './ecopinbarchart?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val3+"&_searchtext=" + encodeURIComponent(val);
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function DonutChart1(val1, val2, val3,val) {
    var s = './ecopindonutchart?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val3+"&_searchtext=" + encodeURIComponent(val);
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function ecopinReportCSV(){
    
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val = document.getElementById('_searchtext').value;
    var val3 = document.getElementById('_changeStatusType').value;
   
    var s = './ecopinreport?_startdate=' + val1 + "&_searchtext=" + encodeURIComponent(val) + "&_enddate=" + val2 + "&_type=" + val3+"&_reporttype="+1;
    window.location.href = s;
    return false;
}
 
function ecopinReportPDF(){
  
     var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val = document.getElementById('_searchtext').value;
    var val3 = document.getElementById('_changeStatusType').value;
    var s = './ecopinreport?_startdate=' + val1 + "&_searchtext=" + encodeURIComponent(val) + "&_enddate=" + val2 + "&_type=" + val3+"&_reporttype="+0;
    window.location.href = s;
    return false;
}

function searchEcopinRecords(){
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val = document.getElementById('_searchtext').value;
    var val3 = document.getElementById('_changeStatusType').value;
//    alert(val);
        
    var s = './ecopinreporttable.jsp?_startdate=' + val1 + "&_searchtext=" + encodeURIComponent(val) + "&_enddate=" + val2 + "&_type=" + val3;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
             $( "#MsgReportgraph" ).empty();
            $( "#MsgReportgraph1" ).empty();
            
          var day_data = null;
            day_data = DonutChart1(val1, val2, val3,val);
            Morris.Donut({
                element: 'MsgReportgraph',
                data: day_data
            });
        
          var day_data1 = null;
            day_data1 = BarChartDemo1(val1, val2, val3,val);
            Morris.Bar({
                element: 'MsgReportgraph1',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });
        }
  
      });
}




//chart and donut
function EcopinReport(val, val1,val2) {

    //Policy check
    var s = './ecopinuserchartsandtable.jsp?_userID=' + encodeURIComponent(val) + '&_endDate=' + encodeURIComponent(val1) + '&_startDate='+encodeURIComponent(val2);
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            $("#PolicyCheckDonutChart").empty();
            $("#PolicyCheckBarChart").empty();
            var day_data1 = null;
            day_data1 = BarChartEcopinReport(val,val1, val2,1);

            Morris.Bar({
                element: 'PolicyCheckBarChart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data = null;
            day_data = DonutChartEcopinReport(val,val1,val2,1);
            Morris.Donut({
                element: 'PolicyCheckDonutChart',
                data: day_data
            });
            
//QA Validation

            $("#QAValidationDonutChart").empty();
            $("#QAValidationBarChart").empty();
            var day_data1 = null;
            day_data1 = BarChartEcopinReport(val,val1,val2,5);

            Morris.Bar({
                element: 'QAValidationBarChart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data = null;
            day_data = DonutChartEcopinReport(val,val1,val2,5);
            Morris.Donut({
                element: 'QAValidationDonutChart',
                data: day_data
            });

//   Delivery

            $("#DeliveryDonutChart").empty();
            $("#DeliveryBarChart").empty();
            var day_data2 = null;
            day_data2 = BarChartEcopinReport(val,val1, val2,3);
            Morris.Bar({
                element: 'DeliveryBarChart',
                data: day_data2,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data2 = null;
            day_data2 = DonutChartEcopinReport(val,val1,val2,3);
            Morris.Donut({
                element: 'DeliveryDonutChart',
                data: day_data2
            });
//
            //OperatorControlled
            
            $("#OperatorControlledDonutChart").empty();
            $("#OperatorControlledBarChart").empty();
            var day_data3 = null;
            day_data3 = BarChartEcopinReport(val,val1,val2,4);
            Morris.Bar({
                element: 'OperatorControlledBarChart',
                data: day_data3,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data3 = null;
            day_data3 = DonutChartEcopinReport(val,val1,val2,4);
            Morris.Donut({
                element: 'OperatorControlledDonutChart',
                data: day_data3
            });

            //Dual Operator Controlled
            
            $("#DualOperatorControlledDonutChart").empty();
            $("#DualOperatorControlledBarChart").empty();
            var day_data3 = null;
            day_data3 = BarChartEcopinReport(val,val1,val2,4);
            Morris.Bar({
                element: 'DualOperatorControlledBarChart',
                data: day_data3,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data3 = null;
            day_data3 = DonutChartEcopinReport(val,val1,val2,4);
            Morris.Donut({
                element: 'DualOperatorControlledDonutChart',
                data: day_data3
            });


            //ecopin table
            var s = './ecopinuserdatatable.jsp?_userID=' + encodeURIComponent(val) + '&_endDate=' + encodeURIComponent(val2) + '&_startDate=' + encodeURIComponent(val1);
            $.ajax({
                type: 'GET',
                url: s,
                success: function(data) {
                     $('#ecopinusertable').html(data);
                }});



        }});
    
}

function BarChartEcopinReport(val,val1,val2,val3) {
//    alert(val)
    var s = './ecopinbarchart?_userID=' + encodeURIComponent(val) + '&_endDate=' + encodeURIComponent(val1) + '&_startDate='+encodeURIComponent(val2)+'&_type='+encodeURIComponent(val3);
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function DonutChartEcopinReport(val,val1,val2,val3) {
    var s = './ecopindonutchart?_userID=' + encodeURIComponent(val) + '&_endDate=' + encodeURIComponent(val1) + '&_startDate='+encodeURIComponent(val2)+'&_type='+encodeURIComponent(val3);
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}


function EcopinReportTable(val, val1, val2,val3) {

    var s = './InteractionResponseReportTable.jsp?_interactionID=' + val + '&_QNo=' + val2 + '&_interactionExecutionID=' + val1;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {

            if (val2 == 0) {//greetings
              
                var ele = document.getElementById("greetings_data_table");
                ele.style.display = "block";
                $('#greetings_data_table').html(data);
                document.getElementById("SearchResponseButton").style.display = 'none';
                var ele = document.getElementById("HideSearchResponseButton");
                ele.style.display = "block";
            } else if (val2 == 1) {//question1
                var ele = document.getElementById("question1_data_table");
                ele.style.display = "block";
                $('#question1_data_table').html(data);
                document.getElementById("Searchquestion1ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion1ResponseButton");
                ele.style.display = "block";
            } else if (val2 == 2) {//question2
                var ele = document.getElementById("question2_data_table");
                ele.style.display = "block";
                $('#question2_data_table').html(data);
                document.getElementById("Searchquestion2ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion2ResponseButton");
                ele.style.display = "block";
            } else if (val2 == 3) {//question3
                var ele = document.getElementById("question3_data_table");
                ele.style.display = "block";
                $('#question3_data_table').html(data);
                document.getElementById("Searchquestion3ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion3ResponseButton");
                ele.style.display = "block";
            } else if (val2 == 4) {//question4
                var ele = document.getElementById("question4_data_table");
                ele.style.display = "block";
                $('#question4_data_table').html(data);
                document.getElementById("Searchquestion4ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion4ResponseButton");
                ele.style.display = "block";
            } else if (val2 == 5) {//question4
                var ele = document.getElementById("question5_data_table");
                ele.style.display = "block";
                $('#question5_data_table').html(data);
                document.getElementById("Searchquestion5ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion5ResponseButton");
                ele.style.display = "block";
            }

        }
    });
}


function ecopinReportDownloadCSV(val,val1,val2){
    var s = './ecopinreport?_startdate=' + val1 + "&_userId=" + encodeURIComponent(val) + "&_enddate=" + val2 +"&_reporttype="+1;
    window.location.href = s;
    return false;
}
 
function ecopinReportDownloadPDF(val,val1,val2){
   var s = './ecopinreport?_startdate=' + val1 + "&_userId=" + encodeURIComponent(val) + "&_enddate=" + val2 +"&_reporttype="+0;
    window.location.href = s;
    return false;
}


function ecopinSystemReportDownloadCSV(val,val1,val2){
    var s = './ecopinreport?_startdate=' + val1 + "&_userId=" + encodeURIComponent(val) + "&_enddate=" + val2 +"&_reporttype="+1+"&_reportFormat="+2;
    window.location.href = s;
    return false;
}
 
function ecopinSystemReportDownloadPDF(val,val1,val2){
   var s = './ecopinreport?_startdate=' + val1 + "&_userId=" + encodeURIComponent(val) + "&_enddate=" + val2 +"&_reporttype="+0+"&_reportFormat="+2;
    window.location.href = s;
    return false;
}

function EcopinSystemReport() {
     var val = encodeURIComponent(document.getElementById('startdate').value);
    var val1 = encodeURIComponent(document.getElementById('enddate').value);

    //Policy check
    var s = './EcopinSystemChartAndTable.jsp?_endDate=' + val1 + '&_startDate='+val;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#SystemReport').html(data);
            $("#SystemPolicyCheckDonutChart").empty();
            $("#SystemPolicyCheckBarChart").empty();
            var day_data1 = null;
            day_data1 = SystemBarChartEcopinReport(val,val1,4);

            Morris.Bar({
                element: 'SystemPolicyCheckBarChart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data = null;
            day_data = SystemDonutChartEcopinReport(val,val1,4);
            Morris.Donut({
                element: 'SystemPolicyCheckDonutChart',
                data: day_data
            });
    //QA Validation

            $("#SystemQAValidationDonutChart").empty();
            $("#SystemQAValidationBarChart").empty();
            var day_data1 = null;
            day_data1 = SystemBarChartEcopinReport(val,val1,5);

            Morris.Bar({
                element: 'SystemQAValidationBarChart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data = null;
            day_data = SystemDonutChartEcopinReport(val,val1,5);
            Morris.Donut({
                element: 'SystemQAValidationDonutChart',
                data: day_data
            });

//   Delivery

            $("#SystemDeliveryDonutChart").empty();
            $("#SystemDeliveryBarChart").empty();
            var day_data2 = null;
            day_data2 = SystemBarChartEcopinReport(val,val1,3);
            Morris.Bar({
                element: 'SystemDeliveryBarChart',
                data: day_data2,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data2 = null;
            day_data2 = SystemDonutChartEcopinReport(val,val1,3);
            Morris.Donut({
                element: 'SystemDeliveryDonutChart',
                data: day_data2
            });
//
            //OperatoeControlled
            
            $("#SystemOperator1ControlledDonutChart").empty();
            $("#SystemOperator1ControlledBarChart").empty();
            var day_data3 = null;
            day_data3 = SystemBarChartEcopinReport(val,val1,1);
            Morris.Bar({
                element: 'SystemOperator1ControlledBarChart',
                data: day_data3,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data3 = null;
            day_data3 = SystemDonutChartEcopinReport(val,val1,1);
            Morris.Donut({
                element: 'SystemOperator1ControlledDonutChart',
                data: day_data3
            });
            
             //Dual OperatoeControlled
            
            $("#SystemDualOperatorControlledDonutChart").empty();
            $("#SystemDualOperatorControlledBarChart").empty();
            var day_data3 = null;
            day_data3 = SystemBarChartEcopinReport(val,val1,2);
            Morris.Bar({
                element: 'SystemDualOperatorControlledBarChart',
                data: day_data3,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data3 = null;
            day_data3 = SystemDonutChartEcopinReport(val,val1,2);
            Morris.Donut({
                element: 'SystemDualOperatorControlledDonutChart',
                data: day_data3
            });

            //ecopin table
            var s = './ecopinuserdatatable.jsp?_endDate=' + val + '&_startDate=' + val1 +'&_reporttype=' + 2 ;
            $.ajax({
                type: 'GET',
                url: s,
                success: function(data) {
                     $('#SystemecopinReporttable').html(data);
                }});
        }});
 }

function SystemBarChartEcopinReport(val,val1,val2) {
//    alert(val)
    var s = './ecopinbarchart?_endDate=' + val1 + '&_startDate='+val+'&_type='+encodeURIComponent(val2)+'&_reporttype=' + 2 ;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function SystemDonutChartEcopinReport(val,val1,val2) {
    var s = './ecopindonutchart?_endDate=' + val1 + '&_startDate='+val+'&_type='+encodeURIComponent(val2)+'&_reporttype=' + 2 ;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}



function searchUsersEcopinRecords() {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
//    alert(val1);
    var val = document.getElementById('_searchtext').value;
    var s = './ecopinusersreporttable.jsp?_startdate=' + val1 + "&_searchtext=" + encodeURIComponent(val) + "&_enddate=" + val2;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
        }
    });

}