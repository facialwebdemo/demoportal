function strcmp(a, b)
{   
    return (a<b?-1:(a>b?1:0));  
}

function get_time_zone_offset() {
    var current_date = new Date();
    return -current_date.getTimezoneOffset() / 60;
}

function Alert4login(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result === false) {
        } else {
    //end here
    }
    });
}


function TellTimezone(){
    var s = './timezone?_gmt='+get_time_zone_offset();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //alert(data._result);
        }
    }); 
}

function RefreshforgotPassword() {
    window.location.href = "./index.jsp";
}

function forgotpassword() {
    var s = './forgotpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") === 0) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if (strcmp(data._result, "success") === 0) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshforgotPassword, 5000);
            }
        }
    });
}

 function changeoperatorpasswordAfter1stLogin(){
    var s = './changeoprpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ChangePasswordform").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") === 0 ) {
                $('#Password-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#_oldPassword').val("");
                $('#_newPassword').val("");
                $('#_confirmPassword').val("");
            //ClearAddOperatorForm();
            }
            else if ( strcmp(data._result,"success") === 0 ) {
                $('#Password-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                $('#passwordChange').attr("disabled",true);
                window.setTimeout(RefreshLogout, 5000);
            }
        }
    }); 
}



function Login(latitude,longitude){ 
    
    var s = './login?_lattitude=' + latitude + '&_longitude=' + longitude;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") === 0 ) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }else if( strcmp(data._result,"success") === 0 ) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;                
            }
        }
    }); 
}

function RefreshLogout() {
    window.location.href = "./signout.jsp";    
} 

//function geologins() {
//    if (navigator.geolocation) {
//        navigator.geolocation.getCurrentPosition(success, error, geo_options);
//    } else { 
//        Login("latitude","longitude");
//        
////        alert("Geolocation services are not supported by your web browser.");
//    }
//    function success(position) {
//        var latitude = position.coords.latitude;
//        var longitude = position.coords.longitude;
//        var altitude = position.coords.altitude;
//        var accuracy = position.coords.accuracy;
//     Login(latitude,longitude);
//    }
//    function error(error) { 
//         Login("latitude","longitude");
//        
////        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
//    }
//    ;
//    var geo_options = {
//        enableHighAccuracy: true,
//        maximumAge: 30000,
//        timeout: 27000
//    };
//}  


function validategeo() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        validate("latitude","longitude");
//        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
     validate(latitude,longitude);
    }
    function error(error) {
        validate("latitude","longitude");
//        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
} 

function validate(latitude,longitude){ 
    var s = './secondfactorauth?_lattitude=' + latitude + '&_longitude=' + longitude;
//    var s = './login';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#secondfactor").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") === 0 ) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }else if( strcmp(data._result,"success") === 0 ) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;                
            }
        }
    }); 
} 




function transdetails(latitude,longitude){ 
   
    var s = './transactiondetails?_lattitude=' + latitude + '&_longitude=' + longitude;
//    var s = './login';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#transactiondetails").serialize(),
        success: function(data) {
             if ( strcmp(data._result,"error") === 0 ) {
                $('#savecrtsettings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }else if( strcmp(data._result,"success") === 0 ) {
                $('#savecrtsettings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;                
            }
        }
    }); 
} 



function transauthentication(latitude,longitude){ 
    var s = './transactionauth?_lattitude=' + latitude + '&_longitude=' + longitude;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#transactionauth").serialize(),
        success: function(data) {
             if ( strcmp(data._result,"error") === 0 ) {
                $('#savecrtsettings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }else if( strcmp(data._result,"success") === 0 ) {
                $('#savecrtsettings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;                
            }
        }
    }); 
} 

function geoSendOOB(){ 
 if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        transdetails(latitude,longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}

function sendOOB(){ 
    var s = './sendOOB?_lattitude=' + latitude + '&_longitude=' + longitude;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#transactionauth").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") === 0 ) {
                $('#savecrtsettings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }else if( strcmp(data._result,"success") === 0 ) {
                $('#savecrtsettings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;                
            }
        }
    }); 
}


function geoTransaction() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        transdetails(latitude,longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
} 

function geoTransactionValidation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        transauthentication(latitude,longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
} 


function Transauthentication() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        transauthentication(latitude,longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
} 


//nilesh
//function geoGetSecurePhrase() {
//    if (navigator.geolocation) {
//        navigator.geolocation.getCurrentPosition(success, error, geo_options);
//    } else { 
//        GetSecurePhrase("latitude","longitude");
//        
////        alert("Geolocation services are not supported by your web browser.");
//    }
//    function success(position) {
//        var latitude = position.coords.latitude;
//        var longitude = position.coords.longitude;
//        var altitude = position.coords.altitude;
//        var accuracy = position.coords.accuracy;
//     GetSecurePhrase(latitude,longitude);
//    }
//    function error(error) { 
//         GetSecurePhrase("latitude","longitude");
//        
////        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
//    }
//    ;
//    var geo_options = {
//        enableHighAccuracy: true,
//        maximumAge: 30000,
//        timeout: 27000
//    };
//}  
function GetSecurePhrase(latitude,longitude){ 
//    alert(latitude);
    var s = './login?_lattitude=' + latitude + '&_longitude=' + longitude;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#securePhraseForm").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") === 0 ) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }else if( strcmp(data._result,"success") === 0 ) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;                
            }
        }
    }); 
}
// function SetImage() {
//                     var imagelink = "<image src='./ShowImage'/>";
////                     alert(imagelink);
//                    $('#showimageEAD').html(imagelink);
// }