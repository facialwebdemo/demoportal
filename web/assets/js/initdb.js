function strcmp(a, b)
{   
    return (a<b?-1:(a>b?1:0));  
}

function get_time_zone_offset( ) {
    var current_date = new Date();
    return -current_date.getTimezoneOffset() / 60;
}

//function Login(){
//    var s = './login';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#loginForm").serialize(),
//        success: function(data) {
//            if ( strcmp(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmp(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;                
//            }
//        }
//    }); 
//    
//}
//
//function ForgotPassword(){
//    var s = './forgotpw';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#forgotPasswdForm").serialize(),
//        success: function(data) {
//            if ( strcmp(data._result,"error") == 0 ) {
//                $('#forgot-password-result').html("<span><font color=red>" + data._message + "</font></span>");
//            }
//            else if ( strcmp(data._result,"success") == 0 ) {
//                $('#forgot-password-result').html("<span><font color=blue>" + data._message + "</font></span>");                
//            }
//        }
//    });
//}
//
//
//function TellTimezone(){
//    var s = './timezone?_gmt='+get_time_zone_offset();
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            //alert(data._result);
//        }
//    }); 
//}


function CloseThisWindow() {
 var Browser = navigator.appName;
 var indexB = Browser.indexOf('Explorer');

 if (indexB > 0) {
    var indexV = navigator.userAgent.indexOf('MSIE') + 5;
    var Version = navigator.userAgent.substring(indexV, indexV + 1);

    if (Version >= 7) {
        window.open('', '_self', '');
        window.close();
    }
    else if (Version == 6) {
        window.opener = null;
        window.close();
    }
    else {
        window.opener = '';
        window.close();
    }

 }
else {
    window.close();
 }
}

function DBInit(){    
    $('#INITDBbutton').attr("disabled", true);    
    var s = './initdb';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#InitDBForm").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") == 0 ) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#INITDBbutton').attr("disabled", false);    
            }
            else if ( strcmp(data._result,"success") == 0 ) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(CloseThisWindow, 10000);                
            }
        }
    }); 
}

function ChannelInit(){
    $('#INITFACEbutton').attr("disabled", true);    
    var s = './initface';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#InitFACEForm").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") == 0 ) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#INITFACEbutton').attr("disabled", false);    
            }
            else if ( strcmp(data._result,"success") == 0 ) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(CloseThisWindow, 10000); 
            }
        }
    }); 
}

function SSLInit(){
    $('#MAKEITSECUREbutton').attr("disabled", true);    
    var s = './makeitsecure';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmp(data._result,"error") == 0 ) {
                $('#SSLInit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#MAKEITSECUREbutton').attr("disabled", false);  
            }
            else if ( strcmp(data._result,"success") == 0 ) {
                $('#SSLInit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(CloseThisWindow, 10000);                
            }
        }
    }); 
}

