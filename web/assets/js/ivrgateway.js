function strcmpIVR(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4IVRSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}


function ChangeActiveStatusIVR(perference,value) {
    if ( perference == 1) {
        if ( value == 1) {
            $('#_statusIVR').val("1");
            $('#_status-primary-ivr').html("Active");
        } else {
            $('#_statusIVR').val("0");
            $('#_status-primary-ivr').html("Suspended");
        }
    } else if ( perference == 2) {
        if ( value == 1) {
            $('#_statusIVRS').val("1");
            $('#_status-secondary-ivr').html("Active");
        } else {
            $('#_statusIVRS').val("0");
            $('#_status-secondary-ivr').html("Suspended");
        }
    }
}

function ChangeFailOverIVR(value) {
    //1 for enabled
    //0 for disabled
    if ( value == 1) {
        $('#_autofailoverIVR').val("1");
        $('#_autofailover-primary-ivr').html("Enabled");
    } else {
        $('#_autofailoverIVR').val("0");
        $('#_autofailover-primary-ivr').html("Disabled");
    }
}
function ChangeRetryIVR(preference,count) {
    //1 for enabled
    //0 for disabled
    if ( preference == 1) {
        if ( count == 2) {
            $('#_retriesIVR').val("2");
            $('#_retries-primary-ivr').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesIVR').val("3");
            $('#_retries-primary-ivr').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesIVR').val("5");
            $('#_retries-primary-ivr').html("5 retries");
        }
    } else if ( preference == 2) {
        if ( count == 2) {
            $('#_retriesIVRS').val("2");
            $('#_retries-secondary-ivr').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesIVRS').val("3");
            $('#_retries-secondary-ivr').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesIVRS').val("5");
            $('#_retries-secondary-ivr').html("5 retries");
        }
    }
}
function ChangeRetryDurationIVR(perference,duration) {
    //1 for enabled
    //0 for disabled
    if ( perference == 1) {
        if ( duration == 10) {
            $('#_retrydurationIVR').val("10");
            $('#_retryduration-primary-ivr').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationIVR').val("30");
            $('#_retryduration-primary-ivr').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationIVR').val("60");
            $('#_retryduration-primary-ivr').html("60 seconds");
        }
    } else if ( perference == 2) {
        if ( duration == 10) {
            $('#_retrydurationIVRS').val("10");
            $('#_retryduration-secondary-ivr').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationS').val("30");
            $('#_retryduration-secondary-ivr').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationIVRS').val("60");
            $('#_retryduration-secondary-ivr').html("60 seconds");
        }
    }
}



function ivrprimary(){
    var s = './loadivrsettings?_preference=1';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_status').val(data._status);
            $('#_ipIVR').val(data._ip);
            $('#_portIVR').val(data._port);
            $('#_userIdIVR').val(data._userId);
            $('#_passwordIVR').val(data._password);
            $('#_phoneNumberIVR').val(data._phoneNumber);
            
            if (data._logConfirmation == true) {
                //alert("data._logConfirmation true");
                $('#_logConfirmationIVR').attr("checked", true);
            //$('#_logConfirmation').attr("value", "true");
                
            }else{
                //alert("data._logConfirmation false");
                $('#_logConfirmationIVR').attr("checked", false);
            //$('#_logConfirmation').attr("value", "false");
            }
            $('#_classNameIVR').val(data._className);
            $('#_reserve1IVR').val(data._reserve1);
            $('#_reserve2IVR').val(data._reserve2);
            $('#_reserve3IVR').val(data._reserve3);
            $('#_autofailoverIVR').val(data._autofailover);
            $('#_retriesIVR').val(data._retries);
            $('#_retrydurationIVR').val(data._retryduration);

            //alert(data._status);

            if (data._status == 1)
                ChangeActiveStatusIVR(1,1);
            else
                ChangeActiveStatusIVR(1,0);

            if (data._autofailover == 1)
                ChangeFailOverIVR(1);
            else
                ChangeFailOverIVR(0);

            if (data._retryduration == 10 )
                ChangeRetryDurationIVR(1,10);
            else if (data._retryduration == 30)
                ChangeRetryDurationIVR(1,30);
            else if (data._retryduration == 60)
                ChangeRetryDurationIVR(1,60);
            else
                ChangeRetryDurationIVR(1,10);

            if (data._retries == 2)
                ChangeRetryIVR(1,2);
            else if (data._retries == 3)
                ChangeRetryIVR(1,3);
            else if (data._retries == 5)
                ChangeRetryIVR(1,5);
            else
                ChangeRetryIVR(1,2);
        }
    });
}

function ivrsecondary(){
    var s = './loadivrsettings?_preference=2';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_statusS').val(data._statusS);
            $('#_ipIVRS').val(data._ipS);
            $('#_portIVRS').val(data._portS);
            $('#_userIdIVRS').val(data._userIdS);
            $('#_passwordIVRS').val(data._passwordS);
            $('#_phoneNumberIVRS').val(data._phoneNumberS);

            if (data._logConfirmationS == true) {
                $('#_logConfirmationIVRS').attr("checked", true);
            }else{
                $('#_logConfirmationIVRS').attr("checked", false);
            }
            $('#_classNameIVRS').val(data._classNameS);
            $('#_reserve1IVRS').val(data._reserve1S);
            $('#_reserve2IVRS').val(data._reserve2S);
            $('#_reserve3IVRS').val(data._reserve3S);
            $('#_retriesIVRS').val(data._retriesS);
            $('#_retrydurationIVRS').val(data._retrydurationS);

            if (data._statusS == 1)
                ChangeActiveStatusIVR(2,1);
            else
                ChangeActiveStatusIVR(2,0);

            if (data._retrydurationS == 10)
                ChangeRetryDurationIVR(2,10);
            else if (data._retrydurationS == 30)
                ChangeRetryDurationIVR(2,30);
            else if (data._retrydurationS == 60)
                ChangeRetryDurationIVR(2,60);
            else
                ChangeRetryDurationIVR(2,10);

            if (data._retriesS == 2)
                ChangeRetryIVR(2,2);
            else if (data._retriesS == 3)
                ChangeRetryIVR(2,3);
            else if (data._retriesS == 5)
                ChangeRetryIVR(2,5);
            else
                ChangeRetryIVR(2,2);
        }
    }
    );
}

function LoadIVRSetting(type){
    if (type == 1) {
        ivrprimary();
    } else if (type == 2 ){
        ivrsecondary();
    }
}


function editIVRprimary(){
    var s = './editivrsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#voiceprimaryform").serialize(),
        success: function(data) {
            if ( strcmpIVR(data._result,"error") == 0 ) {
                $('#save-ivr-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4IVRSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpIVR(data._result,"success") == 0 ) {
//                $('#save-ivr-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4IVRSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function editIVRsecondary(){
    var s = './editivrsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ivrsecondaryform").serialize(),
        success: function(data) {
            if ( strcmpIVR(data._result,"error") == 0 ) {
                $('#save-ivr-gateway-secondary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4IVRSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpIVR(data._result,"success") == 0 ) {
//                $('#save-ivr-gateway-secondary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4IVRSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function EditIVRSetting(type){
    if (type == 1) {
        editIVRprimary();
    } else if (type == 2 ){
        editIVRsecondary();
    }
}

function LoadTestIVRConnectionUI(type){
    if ( type == 1 )
        $("#testIVRPrimary").modal();
    else
        $("#testIVRSecondary").modal();
}

function testconnectionIVRprimary(){
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testIVRPrimaryForm").serialize(),
        success: function(data) {
            if ( strcmpIVR(data._result,"error") == 0 ) {
                $('#test-ivr-primary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpIVR(data._result,"success") == 0 ) {
                $('#test-ivr-primary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
            pleaseWaitDiv.modal('hide');
        }
    });
}

function testconnectionIVRsecondary(){
var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testIVRSecondaryForm").serialize(),
        success: function(data) {
            if ( strcmpIVR(data._result,"error") == 0 ) {
                $('#test-ivr-secondary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpIVR(data._result,"success") == 0 ) {
                $('#test-ivr-secondary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
            pleaseWaitDiv.modal('hide');
        }
    });
}


function TestIVRConnection(type){
    if (type == 1) {
        testconnectionIVRprimary();
    } else if (type == 2 ){
        testconnectionIVRsecondary();
    }
}

