/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strcmpScheduler(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4SchedulerSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}

function RefreshSchedulers() {
    window.location.href = "./scheduler.jsp";
}

function GatewayType(value) {
    if(value == 0){
        $('#_gatewayType').val("0");
        $('#_gatewayType-scheduler').html("ALL");
    }else if ( value == 1) {
        $('#_gatewayType').val("1");
        $('#_gatewayType-scheduler').html("SMS");
    } else if(value == 2){
        $('#_gatewayType').val("2");
        $('#_gatewayType-scheduler').html("USSD");
    }else if(value == 3){
        $('#_gatewayType').val("3");
        $('#_gatewayType-scheduler').html("VOICE");
    }else if(value == 4){
        $('#_gatewayType').val("4");
        $('#_gatewayType-scheduler').html("EMAIL");
    }else if(value == 5){
        $('#_gatewayType').val("5");
        $('#_gatewayType-scheduler').html("FAX");
    }else if(value == 6){
        $('#_gatewayType').val("6");
        $('#_gatewayType-scheduler').html("FACEBOOK");
    }else if(value == 7){
        $('#_gatewayType').val("7");
        $('#_gatewayType-scheduler').html("LINKEDIN");
    }else if(value == 8){
        $('#_gatewayType').val("8");
        $('#_gatewayType-scheduler').html("TWITTER");
    }else if(value == 18){
        $('#_gatewayType').val("18");
        $('#_gatewayType-scheduler').html("ANDROIDPUSH");
    }else if(value == 19){
        $('#_gatewayType').val("19");
        $('#_gatewayType-scheduler').html("IPHONEPUSH");
    }
}

function GatewayStatus(value) {
    if(value == 99){
        $('#_gatewayStatus').val("99");
        $('#_gatewayStatus-scheduler').html("ALL");
    }else if ( value == 0) {
        $('#_gatewayStatus').val("0");
        $('#_gatewayStatus-scheduler').html("SENT");
    } else if ( value == 2) {
        $('#_gatewayStatus').val("2");
        $('#_gatewayStatus-scheduler').html("PENDING");
    } else if ( value == -1) {
        $('#_gatewayStatus').val("-1");
        $('#_gatewayStatus-scheduler').html("FAILED");
    } else if ( value == -5) {
        $('#_gatewayStatus').val("-5");
        $('#_gatewayStatus-scheduler').html("BLOCKED");
    }
}

//function ReportType(value) {
//    alert(value);
//    if(value == 99){
//        $('#_reportType').val("99");
//        $('#_reportType-scheduler').html("PDF&CSV");
//    }else if (value == 0) {
//        $('#_reportType').val("0");
//        $('#_reportType-scheduler').html("PDF");
//    } else if (value == 1) {
//        $('#_reportType').val("1");
//        $('#_reportType-scheduler').html("CSV");
//    } 
//}

function ReportTypeScheduler(value) {
    if(value == 99){
        $('#_reportType').val("99");
        $('#_reportType-scheduler').html("PDF&CSV");
    }else if ( value == 0) {
        $('#_reportType').val("0");
        $('#_reportType-scheduler').html("PDF");
    } else if ( value == 1) {
        $('#_reportType').val("1");
        $('#_reportType-scheduler').html("CSV");
    } 
}

function SchedulerStatus(value) {
    if(value == 0){
        $('#_schedulerStatus').val("0");
        $('#_schedulerStatus-scheduler').html("In-Active");
    }else if ( value == 1) {
        $('#_schedulerStatus').val("1");
        $('#_schedulerStatus-scheduler').html("Active");
    } 
}

function GatewayPreference(value) {
    if(value == 0){
        $('#_gatewayPreference').val("0");
        $('#_gatewayPreference-scheduler').html("Both");
    }else if ( value == 1) {
        $('#_gatewayPreference').val("1");
        $('#_gatewayPreference-scheduler').html("Primary Gateway");
    } else if ( value == 2) {
        $('#_gatewayPreference').val("2");
        $('#_gatewayPreference-scheduler').html("Secondary Gateway");
    }
}

function ReportType(value) {
    if(value == 0){
        $('#_reportType').val("99");
        $('#_reportType-scheduler').html("PDF&CSV");
    }else if ( value == 1) {
        $('#_reportType').val("0");
        $('#_reportType-scheduler').html("PDF");
    } else if ( value == 2) {
        $('#_reportType').val("1");
        $('#_reportType-scheduler').html("CSV");
    }
}

function SchedulerDuration(value) {
    if(value == 0){
        $('#_schedulerDuration').val("0");
        $('#_schedulerDuration-scheduler').html("Hourly");
    } else if(value == 1){
        $('#_schedulerDuration').val("1");
        $('#_schedulerDuration-scheduler').html("Daily");
    }else if ( value == 7) {
        $('#_schedulerDuration').val("7");
        $('#_schedulerDuration-scheduler').html("Weekly");
    }else if ( value == 30) {
        $('#_schedulerDuration').val("30");
        $('#_schedulerDuration-scheduler').html("Monthly");
    }else if ( value == 91) {
        $('#_schedulerDuration').val("91");
        $('#_schedulerDuration-scheduler').html("Quarterly");
    }else if ( value == 365) {
        $('#_schedulerDuration').val("365");
        $('#_schedulerDuration-scheduler').html("Yearly");
    }
    
}

function OperatorRoles(val) {
    if(val == 1){
        $('#_operatorRoles').val(val);
        $('#_operatorRoles-scheduler').html("sysadmin");
    }else if(val == 2){
         $('#_operatorRoles').val(val);
        $('#_operatorRoles-scheduler').html("admin");
    }else if(val == 3){
         $('#_operatorRoles').val(val);
        $('#_operatorRoles-scheduler').html("helpdesk");
    }else if(val == 4){
         $('#_operatorRoles').val(val);
        $('#_operatorRoles-scheduler').html("reporter");
    }
}

function addScheduler(){
    $('#addnewSchedulerSubmitBut').attr("disabled", true);
    var s = './addscheduler';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewSchedulerForm").serialize(),
        success: function(data) {
            if ( strcmpScheduler(data._result,"error") == 0 ) {
                $('#add-new-scheduler-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewSchedulerSubmitBut').attr("disabled", false);
                //ClearAddOperatorForm();
                //window.setTimeout(RefreshOperators, 3000);
            }
            else if ( strcmpScheduler(data._result,"success") == 0 ) {
                $('#add-new-scheduler-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                window.setTimeout(RefreshSchedulers, 3000);
            }
        }
    }); 
}

function ChangeSchedulerStatus(schedulerName,status,uidiv){
    var s = './changeschedulerstatus?_schedulerName='+encodeURIComponent(schedulerName)+ '&_status='+status;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpScheduler(data._result,"error") == 0 ) {
                Alert4SchedulerSetting("<span><font color=red>" + data._message + "</font></span>");
            //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpScheduler(data._result,"success") == 0 ) {
                Alert4SchedulerSetting("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#'+uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}

//function loadEditSchedulerDetails(val){
//    var s = './getSchedulerDetails?_schedulerName='+encodeURIComponent(val);
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            if ( strcmpScheduler(data._result,"success") == 0 ) {
//                $('#idEditSchedulerName').html(data.idEditSchedulerName);
//                $("#_SchedulerNameE").val(data._SchedulerName);
//                $("#_schedulerStatusE").val(data._schedulerStatusE);
//                $("#_gatewayTypeE").val(data._gatewayType);
//                $("#_gatewayStatus").val(data._gatewayStatus);
//                $("#_operatorRoles").val(data._operatorRoles);
//                $("#_tagIDMC").val(data._tagsList);
//                $("#_gatewayPreferenceE").val(data._gatewayPreference);
//                 $("#_schedulerDurationE").val(data._schedulerDuration);
//                 $("#editScheduler").modal();
//               
//            } else {
//                Alert4SchedulerSetting("<span><font color=red>" + data._message + "</font></span>");
//            }
//        }
//    });
//}
function loadEditSchedulerDetails(val){
    var s = './editscheduler.jsp?_schedulerName='+encodeURIComponent(val);
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#editScheduler0').html(data);
//            $("#_EditSchedulerContact").select2(); 
            $('#editScheduler0').modal('show');
        }
    });
}

function editScheduler(){
    $('#addEditSchedulerSubmitBut').attr("disabled", true);
    var s = './editscheduler';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editSchedulerForm").serialize(),
        success: function(data){
            if ( strcmpScheduler(data._result,"error") == 0 ) {
                $('#editscheduler-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addEditContactSubmitBut').attr("disabled", true);
            }else if (strcmpScheduler(data._result,"success") == 0 ) {
                $('#editscheduler-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshSchedulers(), 3000);
            }
        }
    }); 
}


function removeScheduler(val) {
    bootbox.confirm("<h2><font color=red>The archived reports for this schedule will also be DELETED. Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removescheduler?_schedulerName='+encodeURIComponent(val);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpScheduler(data._result, "success") == 0) {
                        Alert4SchedulerSetting("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshSchedulers, 2000);
                    } else {
                        strcmpScheduler("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}


function GetSchedulerReport(_trackingId,_reportype,_type){
//    alert(_fileParh);
     var s = './getschedulerreport?_trackingId='+_trackingId+'&_reporttype='+_reportype+'&_type='+_type;
    window.location.href = s;
    return false;
}