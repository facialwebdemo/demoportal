function strcmpContacts(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4Contacts(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}
function RefreshContacts() {
    window.location.href = "./contacts.jsp";
}



function UploadContactFile()
{
    $.ajaxFileUpload
    (
    {
        url:'./fileuploadsubmit.jsp',
        //secureuri:false,
        fileElementId:'fileToUploadEAD',
        dataType: 'json',
        success: function (data, status)
        {
            if(typeof(data.error) != 'undefined')
            {
                if(data.error != '')
                {
                    Alert4Contacts(data.error);
                }
                else
                {                               
                    var ads_image_ele = document.getElementById("ads_imageEAD");
                    //ads_image_ele.value=data.msg;
                    ads_image_ele.value=data.filename;
                                
                    var ele = document.getElementById("uploadImageEAD");
                    var text = document.getElementById("displayTextEAD");
                    if(ele.style.display == "block") {
                        ele.style.display = "none";
                        //text.innerHTML = ads_image_ele.value + " file uploaded. Click for different file upload.";
                        text.innerHTML = data.msg + " file uploaded. Click for different file upload.";
                    }
                    else {
                        ele.style.display = "block";
                        text.innerHTML = "hide";
                    }
                    Alert4Contacts(data.msg + " uploaded successfully");
             
                }
            }
        },
        error: function (data, status, e)
        {
            Alert4Contacts(e);
        }
    }
    )
    return false;
}

function toogleEAD() {
    var ele = document.getElementById("uploadImageEAD");
    var text = document.getElementById("displayTextEAD");
    if(ele.style.display == "block") {
        ele.style.display = "none";
        text.innerHTML = "click here";
    }
    else {
        ele.style.display = "block";
        text.innerHTML = "hide";
    }
}

function RefreshTemplates() {
    window.location.href = "./templates.jsp";
}


function addContactFile(){
    $('#addnewContactSubmitBut').attr("disabled", true);
     
    //alert('hello');
    var s = './addcontacts';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#uploadContactsFileForm").serialize(),
        success: function(data) {
            if ( strcmpContacts(data._result,"error") == 0 ) {
                $('#add-new-contact-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewContactSubmitBut').attr("disabled", false);
            //ClearAddTemplateForm();
            }
            else if ( strcmpContacts(data._result,"success") == 0 ) {
             
                $('#add-new-contact-result').html("<span><font color=blue>" + data._message + "</font></span>");
                 $('#download-contact').html("<a href=./failedtoaddcontact?_Format="+data._iformat+">Download Failed Entries</a>");
               
                window.setTimeout(RefreshContacts, 3000);
            }
        }
    }); 
}


//function addContactFile(){
//    $('#addnewContactSubmitBut').attr("disabled", true);
//    //alert('hello');
//    var s = './addcontacts';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#uploadContactsFileForm").serialize(),
//        success: function(data) {
//            if ( strcmpContacts(data._result,"error") == 0 ) {
//                $('#add-new-contact-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                $('#addnewContactSubmitBut').attr("disabled", false);
//            //ClearAddTemplateForm();
//            }
//            else if ( strcmpContacts(data._result,"success") == 0 ) {
//                $('#add-new-contact-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.setTimeout(RefreshContacts, 3000);
//            }
//        }
//    }); 
//}

function addContact(){
    $('#addnewSingleContactSubmitBut').attr("disabled", true);
    var s = './addsinglecontact';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#addContactsForm").serialize(),
        success: function(data) {
            if ( strcmpContacts(data._result,"error") == 0 ) {
                $('#add-create-contact-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewSingleContactSubmitBut').attr("disabled", false);
            //ClearAddTemplateForm();
            }
            else if ( strcmpContacts(data._result,"success") == 0 ) {
                $('#add-create-contact-result').html("<span><font color=blue>" + data._message + "</font></span>");
            window.setTimeout(RefreshContacts, 3000);
            }
        }
    }); 
}



function searchContacts(type){
    var value1;
    if ( type == 1 ) {
        value1 = $('#_keyword').val();        
    } else {
        //search using tag id 
        var node = document.getElementById("_tags2Search");
        var index = node.selectedIndex;
        value1 = node.options[index].value;
        
    }
    
    //var s = './searchcontacts?_type=' + type + "&_keyword="+value1;
    var s = './contactstable.jsp?_type=' + type + "&_keyword="+value1;
    
    $('#SearchContactsUsngTagsDIV').attr("disabled", true);
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            if ( type == 1 ) {
                $('#contants_table_main').html(data);
            } else {
                $('#contants_table_main1').html(data);
            }
        }
    });
}


function deleteContact(_cid){
    bootbox.confirm("<h2>Are you sure?</h2>", function(result) {
        if (result == false) {                    
        } else {
            var s = './deletecontact?_contactId='+_cid;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpContacts(data._result,"error") == 0 ) {
                        Alert4Contacts(data._message);
                    }
                    else if ( strcmpContacts(data._result,"success") == 0 ) {
                        Alert4Contacts(data._message);      
                        var tr_to_hide = "contact_search_" + _cid;
                        
                        var ele = document.getElementById(tr_to_hide);
                        ele.style.display = "none";                        
                    }
                }
            }); 
        }
    });
}


function editContact(){
    $('#addEditContactSubmitBut').attr("disabled", true);
    var s = './editcontact';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editContactForm").serialize(),
        success: function(data){
            if ( strcmpContacts(data._result,"error") == 0 ) {
                $('#edit-contact-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addEditContactSubmitBut').attr("disabled", true);
            }
            else if ( strcmpContacts(data._result,"success") == 0 ) {
                $('#edit-contact-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshContacts, 3000);
            }
        }
    }); 
}


function getContact(_cid){
    var s = './contactedit.jsp?_contactId='+_cid;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#editContact0').html(data);
            $("#_tagIDEditContact").select2(); 
            $('#editContact0').modal('show');
        }
    });
}



function SaveTags(){
    $('#SaveTagsButton').attr("disabled", true);
    
    var s = './edittagsetting';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#tagsform").serialize(),
        success: function(data) {
            if ( strcmpContacts(data._result,"error") == 0 ) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Contacts("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveTagsButton').attr("disabled", false);
            }
            else if ( strcmpContacts(data._result,"success") == 0 ) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Contacts("<span><font color=blue>" + data._message + "</font></span>");
               window.setTimeout(RefreshContacts, 3000);
                
            }
        }
    });
}


function UpdateDeleteSelectContactList(_cid){
    
    var cid = "#_sel"+_cid; 
    
    var isChecked = $(cid).is(':checked');
    
    var currentList = $('#_deleteContactsList').val();
    
    if ( isChecked ==false) {
        var cidS = "" + _cid + ",";
        currentList = currentList.replace(cidS,"");
    } else {
        var cidS = "" + _cid + ",";
        var n = currentList.search(cidS);        
        if ( n == -1 ){
            currentList = currentList + cidS;
        } 
    }       
    
    $('#_deleteContactsList').val(currentList);    
    
    var ele = document.getElementById("DeleteMultipleContacts");
    ele.style.display = "block";   
    
        //var ele = document.getElementById("ChangeContactStatus");
    //ele.style.display = "block"; 
  
    //document.getElementById('body1').style.display = "block"; 
    
    
    
    //$("#DeleteMultipleContacts").show();
   
    
    
    //alert($('#_deleteContactsList').val());
}
function ChangeContactStatus(){
    
    bootbox.confirm("<h2><font color=red> Do you wish to reverse status for selected contacts?</font></h2>", function(result) {
        if (result == false) {                    
        } else {
            
            var s = './changecontactsstatus';
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $("#deleteContactListForm").serialize(),
                success: function(data){
                    if ( strcmpContacts(data._result,"error") == 0 ) {
                        //$('#delete-multiple-contact-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                        //$('#deleteContactSubmitBut').attr("disabled", false);
                        Alert4Contacts(data._message);                        
                    }
                    else if ( strcmpContacts(data._result,"success") == 0 ) {
//                        $('#delete-multiple-contact-result').html("<span><font color=blue>" + data._message + "</font></span>");
                       Alert4Contacts(data._message);                        
//                        window.setTimeout(RefreshContacts, 3000);
//                        var listofContactsToHide = $('#_deleteContactsList').val();
//                        var listContacts =  listofContactsToHide.split(",");
//                        for (i=0;i<listContacts.length;i++){
//                            var _cid = listContacts[i];
//                            var tr_to_hide = "#contact_search_" + _cid; 
//                            $(tr_to_hide).hide();
//                            //var ele = document.getElementById(tr_to_hide);
//                            //ele.style.display = "none";                        
//                        }
                       
                    }
                }
            }); 
            
            
            
//            var s = './deletecontact?_contactId='+_cid;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if ( strcmpContacts(data._result,"error") == 0 ) {
//                        Alert4Contacts(data._message);
//                    }
//                    else if ( strcmpContacts(data._result,"success") == 0 ) {
//                        Alert4Contacts(data._message);      
//                        var tr_to_hide = "contact_search_" + _cid;
//                        
//                        var ele = document.getElementById(tr_to_hide);
//                        ele.style.display = "none";                        
//                    }
//                }
//            }); 
        }
    });
}

function deleteContactList(){
    
    bootbox.confirm("<h2><font color=red>Do you wish to delete selected contacts?</font></h2>", function(result) {
        if (result == false) {                    
        } else {
            
            var s = './deletemultiplecontact';
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $("#deleteContactListForm").serialize(),
                success: function(data){
                    if ( strcmpContacts(data._result,"error") == 0 ) {
                        //$('#delete-multiple-contact-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                        //$('#deleteContactSubmitBut').attr("disabled", false);
                        Alert4Contacts(data._message);                        
                    }
                    else if ( strcmpContacts(data._result,"success") == 0 ) {
                        //$('#delete-multiple-contact-result').html("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshContacts, 3000);
                        var listofContactsToHide = $('#_deleteContactsList').val();
                        var listContacts =  listofContactsToHide.split(",");
                        for (i=0;i<listContacts.length;i++){
                            var _cid = listContacts[i];
                            var tr_to_hide = "#contact_search_" + _cid; 
                            $(tr_to_hide).hide();
                            //var ele = document.getElementById(tr_to_hide);
                            //ele.style.display = "none";                        
                        }
                        Alert4Contacts(data._message);                        
                    }
                }
            }); 
            
            
            
//            var s = './deletecontact?_contactId='+_cid;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if ( strcmpContacts(data._result,"error") == 0 ) {
//                        Alert4Contacts(data._message);
//                    }
//                    else if ( strcmpContacts(data._result,"success") == 0 ) {
//                        Alert4Contacts(data._message);      
//                        var tr_to_hide = "contact_search_" + _cid;
//                        
//                        var ele = document.getElementById(tr_to_hide);
//                        ele.style.display = "none";                        
//                    }
//                }
//            }); 
        }
    });
}



function tempemail(){
    Alert4Contacts("Bulk Email Messages is getting processed. You will get email when completed!!!");
}

function tempmobile(){
    Alert4Contacts("Bulk Mobile Messages is getting processed. You will get email when completed!!!");
}



