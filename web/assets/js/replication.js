function strcmpDBMaster(a, b)
{   
    return (a<b?-1:(a>b?1:0));  
}

function Alert4DBMaster(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}


function ConfigMaster(){
    
    $.ajax({
        type: 'GET',
        url: './savedbslave',
        dataType: 'json',
        data: $("#ConfigMasterForm").serialize(),
        success: function(data) {
            if ( strcmpDBMaster(data._result,"success") == 0 ) {
                $('#master-file-id').html(data._fileid);
                $('#master-file-index').html(data._index);
                $("#_dnid").val(data._dnid);
                $('#idConfigMasterButton').attr("disabled", true);
                $('#idDownloadMasterDBCopyButton').attr("disabled", false);
            } else {
                Alert4DBMaster("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
    
}

function DownloadMasterDBCopy(){
    $("#_dnid").val(data._dnid);    
    var s = './downloadmasterdb?_dnid='+_dnid;
    window.location.href = s;
    return false;
    
    
    
}

function StartStateDBMaster(){
    $('#master-file-id').html("");
    $('#master-file-index').html("");
    $("#_dnid").val("");
    $('#idConfigMasterButton').attr("disabled", false);
    $('#idDownloadMasterDBCopyButton').attr("disabled", true);    
}