function strcmpTokens(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Tokens(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshTokenList() {
    window.location.href = "./otptokens.jsp";
}



//function changeotpstatus(status, userid,_category,_subcategory) {
//    var s = './changeotpstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid) +"&_category="+_category+ "&_subcategory="+_subcategory;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            if (strcmpTokens(data._result, "error") == 0) {
//                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
//            }
//            else if (strcmpTokens(data._result, "success") == 0) {
//                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//            //window.setTimeout(RefreshTokenList, 2000);
//            }
//        }
//    });
//}

function changeotpstatus(status, userid, _category, _subcategory, uidiv) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        }
        else {
            var s = './changeotpstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });
}

function changesoftstatus(status, userid) {
    var s = './changesoftwarestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshTokenList, 2000);

            }
        }
    });
}


function loadresyncSW(userid, _srno, _category) {
    $("#_userIDS").val(userid);
    $("#_srno").val(_srno);
    $("#_categoryS").val(_category);
    $("#ResyncSoftware").modal();
}

function loadregisterHW(userid, _category, _subcategory) {
    $("#_userid").val(userid);
    $("#_category").val(_category);
    $("#_subcategory").val(_subcategory);
    $("#HardRegistration").modal();
}


function resyncsoftware() {
    var s = './resynctoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ResyncSoftwareform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#ResyncSoftware-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonResyncSoftware').attr("disabled", false);
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                $('#ResyncSoftware-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonResyncSoftware').attr("disabled", true);

            }
        }
    });
}

function loadresyncHW(userid, _srno, _category) {
    $("#_userIDHW").val(userid);
    $("#_srnoHW").val(_srno);
    $("#_categoryHW").val(_category);
    $("#ResyncHardware").modal();
}

function resynchardware() {
    var s = './resynctoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ResyncHardwareform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#ResyncHardware-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonResyncHardware').attr("disabled", false);
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                $('#ResyncHardware-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonResyncHardware').attr("disabled", true);
            }
        }
    });
}







//function assigntokenOOBandSW(userid,_category,_subcategory, uidiv,udiv) {
//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './assigntoken?_userid=' + encodeURIComponent(userid) + "&_category="+_category+ "&_subcategory="+_subcategory;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpTokens(data._result, "error") == 0) {
//                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpTokens(data._result, "success") == 0) {
//                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//                        //window.setTimeout(RefreshTokenList, 2000);
//                        var uiToChange = '#' + uidiv;
//                        $(uiToChange).html(data._value);
//                        var uToChange='#' + udiv;
//                        $(uToChange).html(data._status);
//                    }
//                }
//            });
//        }
//    });
//}


function assigntokenOOBandSW(userid, _category, _subcategory, uidiv, udiv) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assigntoken?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                        var uToChange = '#' + udiv;
                        $(uToChange).html(data._status);
                    }
                }
            });
        }
    });
}


//function changetokenOOBandSW(userid,_category,_subcategory, uidiv) {
//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './changetokentype?_userid=' + encodeURIComponent(userid) + "&_category="+_category+ "&_subcategory="+_subcategory;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpTokens(data._result,"error") == 0) {
//                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpTokens(data._result,"success") == 0) {
//                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//                        //window.setTimeout(RefreshTokenList, 2000);
//                        var uiToChange = '#' + uidiv;
//                        $(uiToChange).html(data._value);
//                    }
//                }
//            });
//        }
//    });
//}

function changetokenOOBandSW(userid, _category, _subcategory, uidiv) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changetokentype?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });
}



function sendregistration(userid, _category, _subcategory) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            //var s = './sendregistration?_userid=' + userid;
            var s = './sendregistration?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;

            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function assignsoftwaretoken(token, userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assignsoftwaretoken?_token=' + token + '&_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshTokenList, 2000);

                    }
                }
            });
        }
    });
}

function assignhardwaretoken() {
    var s = './assigntoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#HardwareRegistrationform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                $('#_serialnumber').val("");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshTokenList, 2000);
                $("#HardRegistration").modal('hide');
                searchOtpUsers();
            }
        }
    });

}

function loadDetails(_userid, _category, _subcategory) {
    var s = './getpushmapper?_mapperid=' + encodeURIComponent(_mid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpPushmappers(data._result, "success") == 0) {

                $("#_userid").val(data._mapperID);
                $("#_category").val(data._messagefomat);
                $("#_subcategory").val(data._templateName);
                $("#_templateTypeE").val(data._templateType);
                $("#_templateClassE").val(data._templateClass);
                $('#editPushMappers-result').html("");
                $("#editPushMappers").modal();
            } else {
                Alert4Pushmappers("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}



function changehardstatus(status, userid) {
    var s = './changehardwarestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshTokenList, 2000);
            }
        }
    });
}



function searchOtpUsers() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

    if (val.length < 1) {
        //Alert4Tokens("Keyword should be meaningful and/or more than 3 characters");
        Alert4Tokens("Search keyword cannot be blank!!!");
        return;
    }
    
    var s = './otptables.jsp?_searchtext=' + val;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            $('#users_table_main').html(data);
        }
    });
}


function OtpAudits(_userid, _itemtype, _duration) {

    var s = './otpaudit?_userid=' + encodeURIComponent(_userid) + "&_itemType=" + _itemtype + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;
}


function ChangeOtpCategory(value) {
    //1 for enabled
    //0 for disabled
    if (value === 1) {
        $('#_changeCategory').val("1");
        $('#_change_category_Axiom').html("SOFTWARE OTP TOKEN");
    } else if (value === 2) {
        $('#_changeCategory').val("2");
        $('#_change_category_Axiom').html("HARDWARE OTP TOKEN");
    } else if (value === 3) {
        $('#_changeCategory').val("3");
        $('#_change_category_Axiom').html("OUT OF BAND OTP TOKEN");
    }
}


//function generateOtpTable() {
//    var val = document.getElementById('_changeCategory').value;
//    var val1 = document.getElementById('_changeOTPStatus').value;
//     var s = './otptokensreporttable.jsp?_changeCategory=' + val +"&_changeOTPStatus="+val1;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#search_otp_main').html(data);
//        }
//    });
//   
//}
//
//function OTPReport(){
//     var val = document.getElementById('_changeCategory').value;
//    var val1 = document.getElementById('_changeOTPStatus').value;
//    var s = './otptokensreport?_changeCategory='+val +"&_changeOTPStatus="+val1+"&_reporttype="+1;
//    window.location.href = s;
//    return false;
//}
// 
//function OTPReportpdf(){
//     var val = document.getElementById('_changeCategory').value;
//    var val1 = document.getElementById('_changeOTPStatus').value;
//    var s = './otptokensreport?_changeCategory='+val +"&_changeOTPStatus="+val1+"&_reporttype="+0;
//    window.location.href = s;
//    return false;
//}

function ChangeOtpStateType(value) {

    if (value === 1) {
        $('#_changeOTPStatus').val("1");
        $('#_change_OtpStatus_Axiom').html("Active State");
    } else if (value === -1) {
        $('#_changeOTPStatus').val("-1");
        $('#_change_OtpStatus_Axiom').html("Locked State");
    } else if (value === 0) {
        $('#_changeOTPStatus').val("0");
        $('#_change_OtpStatus_Axiom').html("Assigned State");
    } else if (value === -5) {
        $('#_changeOTPStatus').val("-5");
        $('#_change_OtpStatus_Axiom').html("Lost State");
    } else if (value === -2) {
        $('#_changeOTPStatus').val("-2");
        $('#_change_OtpStatus_Axiom').html("Suspended State");
    } else if (value === 2) {
        $('#_changeOTPStatus').val("2");
        $('#_change_OtpStatus_Axiom').html("Show All States");
    } else if (value === -10) {
        $('#_changeOTPStatus').val("-10");
        $('#_change_OtpStatus_Axiom').html("Free And Available State");
    }

}


function DonutChart(val, val1) {
    var s = './otpdonutchart?_changeCategory=' + val + "&_changeOTPStatus=" + val1;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function BarChartDemo(val, val1) {
    var s = './otpbarchart?_changeCategory=' + val + "&_changeOTPStatus=" + val1;
    ;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

//function generateOtpTable() {
//    var val = document.getElementById('_changeCategory').value;
//    var val1 = document.getElementById('_changeOTPStatus').value;
//    
//    $('#graph').empty();
//    $('#graph1').empty();
//    
//    var day_data = DonutChart(val,val1);
//    Morris.Donut({
//        element: 'graph',
//        data: day_data
//
//    });
//        
//    var day_data = BarChartDemo(val,val1);
//    Morris.Bar({
//        element: 'graph1',
//        data: day_data,
//        xkey: 'label',
//        ykeys: ['value'],
//        labels: ['value'],
//        barColors: function(type) {
//            if (type === 'bar') {
//
//                return '#0066CC';
//            }
//            else {
//
//                return '#0066CC';
//            }
//        }
//    });
//    var s = './otptokensreporttable.jsp?_changeCategory=' + val + "&_changeOTPStatus=" + val1;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#licenses_data_table').html(data);
//        }
//    });
//
//}

function OTPReport() {
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;
    var s = './otptokensreport?_changeCategory=' + val + "&_changeOTPStatus=" + val1 + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function OTPReportpdf() {
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;
    var s = './otptokensreport?_changeCategory=' + val + "&_changeOTPStatus=" + val1 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function OTPLength(value) {
    if (value === 6) {
        $('#_OTPLength').val("6");
        $('#_OTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_OTPLength').val("7");
        $('#_OTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_OTPLength').val("8");
        $('#_OTPLength_div').html("8 Digits");
    }
}


function SOTPLength(value) {
    if (value === 6) {
        $('#_SOTPLength').val("6");
        $('#_SOTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_SOTPLength').val("7");
        $('#_SOTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_SOTPLength').val("8");
        $('#_SOTPLength_div').html("8 Digits");
    }
}

function OATHAlgo(value) {
    if (value === 1) {
        $('#_OATHAlgoType').val("1");
        $('#_OATHAlgoType_div').html("Event Based");
    } else if (value === 2) {
        $('#_OATHAlgoType').val("2");
        $('#_OATHAlgoType_div').html("Time Based");
    }
}

//sw otp 

function SWOTPLength(value) {
    if (value === 6) {
        $('#_SWOTPLength').val("6");
        $('#_SWOTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_SWOTPLength').val("7");
        $('#_SWOTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_SWOTPLength').val("8");
        $('#_SWOTPLength_div').html("8 Digits");
    }
}


function SWSOTPLength(value) {
    if (value === 6) {
        $('#_SWSOTPLength').val("6");
        $('#_SWSOTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_SWSOTPLength').val("7");
        $('#_SWSOTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_SWSOTPLength').val("8");
        $('#_SWSOTPLength_div').html("8 Digits");
    }
}

function SWOATHAlgo(value) {
    if (value === 1) {
        $('#_SWOATHAlgoType').val("1");
        $('#_SWOATHAlgoType_div').html("Event Based");
    } else if (value === 2) {
        $('#_SWOATHAlgoType').val("2");
        $('#_SWOATHAlgoType_div').html("Time Based");
    }
}

//OTP TOken Setting

function OTPAttempts(value) {
    if (value === 3) {
        $('#_OTPAttempts').val("3");
        $('#_OTPAttempts_div').html("3 times");
    } else if (value === 5) {
        $('#_OTPAttempts').val("5");
        $('#_OTPAttempts_div').html("5 times");
    } else if (value === 7) {
        $('#_OTPAttempts').val("7");
        $('#_OTPAttempts_div').html("7 times");
    }
}


function ValidationSteps(value) {
    if (value === 1) {
        $('#_ValidationSteps').val("1");
        $('#_ValidationSteps_div').html("1 step");
    } else if (value === 3) {
        $('#_ValidationSteps').val("3");
        $('#_ValidationSteps_div').html("3 steps");
    } else if (value === 5) {
        $('#_ValidationSteps').val("5");
        $('#_ValidationSteps_div').html("5 steps");
    } else if (value === 7) {
        $('#_ValidationSteps').val("7");
        $('#_ValidationSteps_div').html("7 steps");
    } else if (value === 10) {
        $('#_ValidationSteps').val("10");
        $('#_ValidationSteps_div').html("10 steps");
    }
}

function TimeDuration(value) {
    if (value === 60) {
        $('#_duration').val("60");
        $('#_duration_div').html("60 seconds");
    } else if (value === 120) {
        $('#_duration').val("120");
        $('#_duration_div').html("120 seconds");
    } else if (value === 180) {
        $('#_duration').val("180");
        $('#_duration_div').html("180 seconds");
    }
}

function MultipleTokens(value) {
    if (value == true) {
        $('#_MultipleToken').val("true");
        $('#_MultipleToken_div').html("Yes, reset attempts for all tokens.");
    } else if (value == false) {
        $('#_MultipleToken').val("false");
        $('#_MultipleToken_div').html("No, do not reset attempts for all tokens. ");
    }
}


function EnforceMasking(value) {
    if (value == true) {
        $('#_EnforceMasking').val(true);
        $('#_EnforceMasking_div').html("Yes, Apply Masking.");
    } else if (value == false) {
        $('#_EnforceMasking').val(false);
        $('#_EnforceMasking_div').html("No, Disable Masking.");
    }
}



function RegistrationExpiryTime(value) {
    if (value === 3) {
        $('#_RegistrationExpiryTime').val("3");
        $('#_RegistrationExpiryTime_div').html("3 minutes");
    } else if (value === 5) {
        $('#_RegistrationExpiryTime').val("5");
        $('#_RegistrationExpiryTime_div').html("5 minutes");
    } else if (value === 10) {
        $('#_RegistrationExpiryTime').val("10");
        $('#_RegistrationExpiryTime_div').html("10 minutes");
    } else if (value === 30) {
        $('#_RegistrationExpiryTime').val("30");
        $('#_RegistrationExpiryTime_div').html("30 minutes");
    }
}



function editOTPSettings() {
    var s = './editotpsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#otpsettingsform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#save-otp-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
//                $('#save-otp-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function LoadOTPSettings() {
    var s = './loadotpsettings';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {

            $('#_OTPLength').val(data._OTPLength);
            if (data._OTPLength === 6)
                OTPLength(6);
            else if (data._OTPLength === 7)
                OTPLength(7);
            else if (data._OTPLength === 8)
                OTPLength(8);


            $('#_SOTPLength').val(data._SOTPLength);
            if (data._SOTPLength === 6)
                SOTPLength(6);
            else if (data._SOTPLength === 7)
                SOTPLength(7);
            else if (data._SOTPLength === 8)
                SOTPLength(8);

            $('#_OATHAlgoType').val(data._OATHAlgoType);
            if (data._OATHAlgoType === 1)
                OATHAlgo(1);
            else if (data._OATHAlgoType === 2)
                OATHAlgo(2);

            //sw

            $('#_SWOTPLength').val(data._SWOTPLength);
            if (data._SWOTPLength === 6)
                SWOTPLength(6);
            else if (data._SWOTPLength === 7)
                SWOTPLength(7);
            else if (data._SWOTPLength === 8)
                SWOTPLength(8);


            $('#_SWSOTPLength').val(data._SWSOTPLength);
            if (data._SWSOTPLength === 6)
                SWSOTPLength(6);
            else if (data._SWSOTPLength === 7)
                SWSOTPLength(7);
            else if (data._SWSOTPLength === 8)
                SWSOTPLength(8);

            $('#_SWOATHAlgoType').val(data._SWOATHAlgoType);
            if (data._SWOATHAlgoType === 1)
                SWOATHAlgo(1);
            else if (data._SWOATHAlgoType === 2)
                SWOATHAlgo(2);
            //attempt  

            $('#_OTPAttempts').val(data._OTPAttempts);
            if (data._OTPAttempts === 3)
                OTPAttempts(3);
            else if (data._OTPAttempts === 5)
                OTPAttempts(5);
            else if (data._OTPAttempts === 7)
                OTPAttempts(7);


            $('#_ValidationSteps').val(data._ValidationSteps);
            if (data._ValidationSteps === 1)
                ValidationSteps(1);
            else if (data._ValidationSteps === 3)
                ValidationSteps(3);
            else if (data._ValidationSteps === 5)
                ValidationSteps(5);
            else if (data._ValidationSteps === 7)
                ValidationSteps(7);
            else if (data._ValidationSteps === 10)
                ValidationSteps(10);

            $('#_duration').val(data._duration);
            if (data._duration === 60)
                TimeDuration(60);
            else if (data._duration === 120)
                TimeDuration(120);
            else if (data._duration === 180)
                TimeDuration(180);

            $('#_MultipleToken').val(data._MultipleToken);
            if (data._MultipleToken === true)
                MultipleTokens(true);
            else if (data._MultipleToken === false)
                MultipleTokens(false);


            $('#_RegistrationExpiryTime').val(data._RegistrationExpiryTime);
            if (data._RegistrationExpiryTime === 3)
                RegistrationExpiryTime(3);
            else if (data._RegistrationExpiryTime === 5)
                RegistrationExpiryTime(5);
            else if (data._RegistrationExpiryTime === 10)
                RegistrationExpiryTime(10);
            else if (data._RegistrationExpiryTime === 30)
                RegistrationExpiryTime(30);


            $('#_EnforceMasking').val(data._EnforceMasking);
            if (data._EnforceMasking === true)
                EnforceMasking(true);
            else if (data._EnforceMasking === false)
                EnforceMasking(false);

        }
    });
}

function generateOtpTable() {
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;

    var s = './otptokensreporttable.jsp?_changeCategory=' + val + "&_changeOTPStatus=" + val1;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);

            var day_data = DonutChart(val, val1);
            Morris.Donut({
                element: 'graph',
                data: day_data

            });

            var day_data = BarChartDemo(val, val1);
            Morris.Bar({
                element: 'graph1',
                data: day_data,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });

        }
        //    $('#graph').empty();
        //    $('#graph1').empty();
        //    
    });

}

function UploadTokensFileXML()
{
    $.ajaxFileUpload
            (
                    {
                        type: 'POST',
                        url: './hwotptokeuploadsubmit',
                        fileElementId: 'fileXMLToUploadEAD',
                        dataType: 'json',
                        success: function(data, status)
                        {

                            if (strcmpTokens(data._result, "error") == 0) {
                                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");

                            }
                            else if (strcmpTokens(data._result, "success") == 0) {
                                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");

                            }
                            $('#download-Entries').html("<a href=./failedtoaddtokenentries?_Format=" + data._iformat + ">Download Fail Entry File</a>");
                        },
                        error: function(data, status, e)
                        {
                            Alert4Tokens(e);
                        }
                    }
            )
    return false;
}

function uploadlic() {
    var ele = document.getElementById("uploadXMLImageEAD");
    var text = document.getElementById("displayXMLTextEAD");
    if (ele.style.display == "block") {
        ele.style.display = "none";
        text.innerHTML = "click here";
    }
    else {
        ele.style.display = "block";
        text.innerHTML = "hide";
    }
}
function HardwareOTPTokenUpload() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './hwotptokeuploadsubmit';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#uploadXMLFormEAD").serialize(),
        success: function(data) {
              pleaseWaitDiv.modal('hide');
            if ( strcmpTokens(data._result,"error") == 0 ) {
//                $('#save-otp-tokens').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpTokens(data._result,"success") == 0 ) {
//                $('#save-otp-tokens').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
             
            }
            if( strcmpTokens(data._failed, "0") != 0 || strcmpTokens(data._success, "0") != 0){
                document.getElementById("result-div").style.visibility = "visible";
            if (strcmpTokens(data._failed, "0") != 0)
            {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                document.getElementById("download-FailEntries").style.visibility = "visible";
//                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-FailEntries").style.background = "#0077b3";
            }
            if (strcmpTokens(data._success, "0") != 0)
            {
                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
            }
            else if (strcmpTokens(data._failed, "0") != 0 && strcmpTokens(data._success, "0") != 0)
            {
                document.getElementById("download-FailEntries").style.visibility = "visible";
                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-FailEntries").style.background = "#0077b3";
                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
            }
//            pleaseWaitDiv.modal('hide');
        }
    },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}

//function HardwareOTPTokenUpload() {
//
//    $('#buttonUploadEAD').attr("disabled", true);
//
//    var s = './hwotptokeuploadsubmit?_password=' + document.getElementById('_xmlpassowrd').value;
//    $.ajaxFileUpload({
//        type: 'POST',
//        fileElementId: 'fileXMLToUploadEAD',
//        url: s,
//        dataType: 'json',
//        success: function(data, status) {
//
//            if (strcmpTokens(data._result, "error") == 0) {
//                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//
//
//            }
//            else if (strcmpTokens(data._result, "success") == 0) {
//                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//
//
//            }
//
//            if (strcmpTokens(data._failed, "0") != 0)
//            {
//                document.getElementById("download-FailEntries").style.visibility = "visible";
////                document.getElementById("download-SuccessEntries").style.visibility = "visible";
//                //document.getElementById("download-FailEntries").style.background = "#0077b3";
//            }
//            if (strcmpTokens(data._success, "0") != 0)
//            {
//                document.getElementById("download-SuccessEntries").style.visibility = "visible";
//                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
//            }
//            else if (strcmpTokens(data._failed, "0") != 0 && strcmpTokens(data._success, "0") != 0)
//            {
//                document.getElementById("download-FailEntries").style.visibility = "visible";
//                document.getElementById("download-SuccessEntries").style.visibility = "visible";
//                //document.getElementById("download-FailEntries").style.background = "#0077b3";
//                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
//            }
//
//            //$('#download-FailEntries').html("<a href=./failedtoaddtokenentries?_Format=" + data._iformat + ">Download Fail Entry File</a>");
//            //$('#download-SuccessEntries').html("<a href=./Successaddtokenentries?_Format=" + data._iformat + ">Download Success Entry File</a>");
//        },
//        error: function(data, status, e)
//        {
//            alert(e);
//        }
//    });
//}

function SuccessEntriesDownload() {
    //var val = document.getElementById('_changeCategory').value;
    //var val1 = document.getElementById('_changeOTPStatus').value;
//    alert("SuccessEntriesDownload");
    var s = './Successaddtokenentries';//?_Format=' + _format ;//+ "&_changeOTPStatus=" + val1 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function FailedEntriesDownload() {
    //var val = document.getElementById('_changeCategory').value;
    //var val1 = document.getElementById('_changeOTPStatus').value;
//    alert("FailedEntriesDownload");
    var s = './failedtoaddtokenentries';//?_Format=' + _format ;//+ "&_changeOTPStatus=" + val1 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function HardwarePKITokenUpload() {

    $('#buttonUploadPKITOKENEAD').attr("disabled", true);

    var s = './pkitokensfileuload';
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'PKITOKENfileUploadEAD',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");


            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");


            }

            if (strcmpTokens(data._failed, "0") != 0)
            {
                document.getElementById("download-FailEntries").style.visibility = "visible";
//                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-FailEntries").style.background = "#0077b3";
            }
            if (strcmpTokens(data._success, "0") != 0)
            {
                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
            }
            else if (strcmpTokens(data._failed, "0") != 0 && strcmpTokens(data._success, "0") != 0)
            {
                document.getElementById("download-FailEntries").style.visibility = "visible";
                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-FailEntries").style.background = "#0077b3";
                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
            }

            //$('#download-FailEntries').html("<a href=./failedtoaddtokenentries?_Format=" + data._iformat + ">Download Fail Entry File</a>");
            //$('#download-SuccessEntries').html("<a href=./Successaddtokenentries?_Format=" + data._iformat + ">Download Success Entry File</a>");
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}

function LoadTestOTPUI(_uid, type) {
    $('#_userIDOV').val(_uid);
    $('#_usertypeIDS').val(type);
    $("#verifyOTP").modal();
}

function verifyOTP() {
    alert("OTPTokJs");
    var s = './verifyOTP';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#verifyOTPForm").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                alert("OTP JS");
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                $('#_oobotp').val("");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                $('#_oobotp').val("");
            }
        }
    });
}

function UploadXMLFile() {
    $('#buttonUploadXMLEAD').attr("disabled", true);
    var s = './uploadOTPXmlFile?_type=' + 1;
    $.ajaxFileUpload({
        fileElementId: 'fileXMLToUploadEAD',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpTokens(data.result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpTokens(data.result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadPASSEAD').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}
function UploadPasswordFile() {
    $('#buttonUploadPASSEAD').attr("disabled", true);
    var s = './uploadOTPXmlFile?_type=' + 2;
    $.ajaxFileUpload({
        fileElementId: 'fileXMLToUploadEADPASSWORD',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpTokens(data.result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data.message + "</font></span>");

            }
            else if (strcmpTokens(data.result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data.message + "</font></span>");
                $('#save-otp-tokens').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}

function InitialState() {
    $('#buttonUploadPASSEAD').attr("disabled", true);
    $('#save-otp-tokens').attr("disabled", true);
}

function CheckSecretHWToken() {
    
    $('#checkSecretButton').attr("disabled", true);
    var s = './checkTokenSecrete';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#HWTOKENSECRETVALIDITY").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#check-secret-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#checkSecretButton').attr("disabled", false);
            }
            else if (strcmpTokens(data._result, "success") == 0) {
//                alert("hi");
                $('#check-secret-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#checkSecretButton').attr("disabled", true);
//                $('#_serialno').val("");
//                $("#CheckSecret").modal('hide');  
            }
        }
    });
}

function generateOtpTablev2() {
     $('#generatereportButton').attr("disabled", true);
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;
    var ele = document.getElementById("REPORT");
    ele.style.display = "block";   
     var ele = document.getElementById("refreshButton");
    ele.style.display = "block";   
        var day_data = DonutChart(val, val1);
            Morris.Donut({
                element: 'graph',
                data: day_data

            });

            var day_data = BarChartDemo(val, val1);
            Morris.Bar({
                element: 'graph1',
                data: day_data,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });
    
    $('#licenses_data_table').html("<h3>Loading....</h3>");
//  var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
  
  
//     $('#licenses_data_table').html(pleaseWaitDiv);
    var s = './otptokensreporttable.jsp?_changeCategory=' + val + "&_changeOTPStatus=" + val1;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
        }
          
        //    
    });

}
 function RefreshTokenReport() {
    window.location.href = "./otptokensreport.jsp";
}