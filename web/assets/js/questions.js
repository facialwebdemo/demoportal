
function strcmpQuestions(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Questions(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function RefreshQuestionsList() {
    window.location.href = "./QuestionsList.jsp";
}

function SetPercentage(category, value, _div, disp) {
    //    alert(category);
    //    alert(value);
    if (category === 1) {
        $('#_weigtage_div').val(value);
        $(_div).html(disp);
    }

}
function editQuestion() {
    var s = './editQuestion';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editQuestionForm").serialize(),
        success: function(data) {
            if (strcmpQuestions(data._result, "error") == 0) {
                $('#editQuestion-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if (strcmpQuestions(data._result, "success") == 0) {
                $('#editQuestion-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#editQuestionButton').attr("disabled", true);
                //window.setTimeout(RefreshUsersList, 2000);
                window.setTimeout(RefreshQuestionsList, 3000);

            }
        }
    });
}

function addQuestion() {
    $('#addQuestionButton').attr("disabled", true);
    var s = './addQuestion';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#questionForm").serialize(),
        success: function(data) {
            if (strcmpQuestions(data._result, "error") == 0) {
                $('#addQuestion-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                //Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
                $('#addQuestionButton').attr("disabled", false);
            }
            else if (strcmpQuestions(data._result, "success") == 0) {
                $('#addQuestion-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshQuestionsList, 3000);
            }
        }
    });
}


function loadQuestionDetails(_qid) {
    var s = './loadQuestionDetails?_qid=' + encodeURIComponent(_qid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpQuestions(data._result, "success") == 0) {
//                $('#idEditUser').html(data._Name);
               
                $('#_eqid').val(_qid);
                $("#_equestion").val(data._equestion);
                $("#_e_weightage").val(data._e_weightage);
                $("#_e_status").val(data._e_status);
                $('#editQuestion-result').html("");
                $("#editQuestion").modal();
            } else {
                Alert4Questions("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}


function removeQuestion(questionid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removequestion?_questionid=' + encodeURIComponent(questionid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpQuestions(data._result, "success") == 0) {
                        Alert4Questions("<span><font color=red>" + data._message + "</font></span>");
                        window.setTimeout(RefreshUsersList, 2000);
                    } else {
                        Alert4Questions("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}





function loadMessageTemplateDetails(_tid) {
    var s = './loadtemplate?_tid=' + _tid;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpTemplates(data._result, "success") == 0) {

                $('#idEditTemplateName').html(data._name);
                $('#idMessageTemplateId').val(data._tid);
                $("#_templateM_name").val(data._name);
                //$("#_templateM_subject").val(data._subject);
                $("#_templateM_body").val(data._body);
                $("#_templateM_variable").val(data._variables);
                $('#edittemplateM-result').html("");
                //$("#editMessageTemplate").modal();
            } else {
                Alert4Templates("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}






function ChangeType(value) {

    if (value === 1) {
        $('#_types').val("1");
        $('#_type-primary-sms').html("Mobile");
    }
    else if (value === 2) {
        $('#_types').val("2");
        $('#_type-primary-sms').html("Email");

    }

}