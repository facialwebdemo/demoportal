function strcmpfax(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4faxSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}


function ChangeActiveStatusfax(perference,value) {
    if ( perference == 1) {
        if ( value == 1) {
            $('#_status').val("1");
            $('#_status-primary-fax').html("Active");
        } else {
            $('#_status').val("0");
            $('#_status-primary-fax').html("Suspended");
        }
    } else if ( perference == 2) {
        if ( value == 1) {
            $('#_statusS').val("1");
            $('#_status-secondary-fax').html("Active");
        } else {
            $('#_statusS').val("0");
            $('#_status-secondary-fax').html("Suspended");
        }
    }
}

function ChangeFailOverfax(value) {
    //1 for enabled
    //0 for disabled
    if ( value == 1) {
        $('#_autofailover').val("1");
        $('#_autofailover-primary-fax').html("Enabled");
    } else {
        $('#_autofailover').val("0");
        $('#_autofailover-primary-fax').html("Disabled");
    }
}

function ChangeRetryfax(preference,count) {
    //1 for enabled
    //0 for disabled
    if ( preference == 1) {
        if ( count == 2) {
            $('#_retries').val("2");
            $('#_retries-primary-fax').html("2 retries");
        } else if ( count == 3) {
            $('#_retries').val("3");
            $('#_retries-primary-fax').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retries').val("5");
            $('#_retries-primary-fax').html("5 retries");
        }
    } else if ( preference == 2) {
        if ( count == 2) {
            $('#_retriesS').val("2");
            $('#_retries-secondary-fax').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesS').val("3");
            $('#_retries-secondary-fax').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesS').val("5");
            $('#_retries-secondary-fax').html("5 retries");
        }
    }
}
function ChangeRetryDurationfax(perference,duration) {
    //1 for enabled
    //0 for disabled
    if ( perference == 1) {
        if ( duration == 10) {
            $('#_retryduration').val("10");
            $('#_retryduration-primary-fax').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retryduration').val("30");
            $('#_retryduration-primary-fax').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retryduration').val("60");
            $('#_retryduration-primary-fax').html("60 seconds");
        }
    } else if ( perference == 2) {
        if ( duration == 10) {
            $('#_retrydurationS').val("10");
            $('#_retryduration-secondary-fax').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationS').val("30");
            $('#_retryduration-secondary-fax').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationS').val("60");
            $('#_retryduration-secondary-fax').html("60 seconds");
        }
    }
}



function faxprimary(){
    var s = './loadfaxsettings?_type=1&_preference=1';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_status').val(data._status);
            $('#_ip').val(data._ip);
            $('#_port').val(data._port);
            $('#_userId').val(data._userId);
            $('#_password').val(data._password);
            $('#_phoneNumber').val(data._phoneNumber);
            
            if (data._logConfirmation == true) {
                //alert("data._logConfirmation true");
                $('#_logConfirmation').attr("checked", true);
            //$('#_logConfirmation').attr("value", "true");
                
            }else{
                //alert("data._logConfirmation false");
                $('#_logConfirmation').attr("checked", false);
            //$('#_logConfirmation').attr("value", "false");
            }
            $('#_className').val(data._className);
            $('#_reserve1').val(data._reserve1);
            $('#_reserve2').val(data._reserve2);
            $('#_reserve3').val(data._reserve3);
            $('#_autofailover').val(data._autofailover);
            $('#_retries').val(data._retries);
            $('#_retryduration').val(data._retryduration);

            //alert(data._status);

            if (data._status == 1)
                ChangeActiveStatusfax(1,1);
            else
                ChangeActiveStatusfax(1,0);

            if (data._autofailover == 1)
                ChangeFailOverfax(1);
            else
                ChangeFailOverfax(0);

            if (data._retryduration == 10 )
                ChangeRetryDurationfax(1,10);
            else if (data._retryduration == 30)
                ChangeRetryDurationfax(1,30);
            else if (data._retryduration == 60)
                ChangeRetryDurationfax(1,60);
            else
                ChangeRetryDurationfax(1,10);

            if (data._retries == 2)
                ChangeRetryfax(1,2);
            else if (data._retries == 3)
                ChangeRetryfax(1,3);
            else if (data._retries == 5)
                ChangeRetryfax(1,5);
            else
                ChangeRetryfax(1,2);            
        }
    });
}

function faxsecondary(){
    var s = './loadfaxsettings?_type=1&_preference=2';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_statusS').val(data._statusS);
            $('#_ipS').val(data._ipS);
            $('#_portS').val(data._portS);
            $('#_userIdS').val(data._userIdS);
            $('#_passwordS').val(data._passwordS);
            $('#_phoneNumberS').val(data._phoneNumberS);

            if (data._logConfirmationS == true) {
                $('#_logConfirmationS').attr("checked", true);                
            }else{
                $('#_logConfirmationS').attr("checked", false);
            }
            $('#_classNameS').val(data._classNameS);
            $('#_reserve1S').val(data._reserve1S);
            $('#_reserve2S').val(data._reserve2S);
            $('#_reserve3S').val(data._reserve3S);
            $('#_retriesS').val(data._retriesS);
            $('#_retrydurationS').val(data._retrydurationS);

            if (data._statusS == 1)
                ChangeActiveStatusfax(2,1);
            else
                ChangeActiveStatusfax(2,0);

            if (data._retrydurationS == 10)
                ChangeRetryDurationfax(2,10);
            else if (data._retrydurationS == 30)
                ChangeRetryDurationfax(2,30);
            else if (data._retrydurationS == 60)
                ChangeRetryDurationfax(2,60);
            else
                ChangeRetryDurationfax(2,10);

            if (data._retriesS == 2)
                ChangeRetryfax(2,2);
            else if (data._retriesS == 3)
                ChangeRetryfax(2,3);
            else if (data._retriesS == 5)
                ChangeRetryfax(2,5);
            else
                ChangeRetryfax(2,2);            
        }
    }
    );
}

function LoadfaxSetting(type){
    if (type == 1) {
        faxprimary();
    } else if (type == 2 ){
        faxsecondary();
    }
}


function editfaxprimary(){
    var s = './editfaxsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#faxprimaryform").serialize(),
        success: function(data) {
            if ( strcmpfax(data._result,"error") == 0 ) {
                $('#save-fax-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpfax(data._result,"success") == 0 ) {
                $('#save-fax-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function editfaxsecondary(){
    var s = './editfaxsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#faxecondaryform").serialize(),
        success: function(data) {
            if ( strcmpfax(data._result,"error") == 0 ) {
                $('#save-fax-gateway-secondary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpfax(data._result,"success") == 0 ) {
                $('#save-fax-gateway-secondary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function EditfaxSetting(type){    
    if (type == 1) {
        editfaxprimary();
    } else if (type == 2 ){
        editfaxsecondary();
    }
}

function LoadTestfaxConnectionUI(type){
    if ( type == 1 )
        $("#testfaxPrimary").modal();
    else
        $("#testfaxSecondary").modal();
}

function testconnectionfaxprimary(){

    var s = './testconnection';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#testfaxPrimaryForm").serialize(),
        success: function(data) {
            if ( strcmpfax(data._result,"error") == 0 ) {
                $('#test-fax-primary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpfax(data._result,"success") == 0 ) {
                $('#test-fax-primary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function testconnectionfaxsecondary(){

    var s = './testconnection';

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testfaxSecondaryForm").serialize(),
        success: function(data) {
            if ( strcmpfax(data._result,"error") == 0 ) {
                $('#test-fax-secondary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpfax(data._result,"success") == 0 ) {
                $('#test-fax-secondary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function TestfaxConnection(type){
    if (type == 1) {
        testconnectionfaxprimary();
    } else if (type == 2 ){
        testconnectionfaxsecondary();
    }
}

