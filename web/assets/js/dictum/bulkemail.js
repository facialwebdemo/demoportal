function strcmpbulk(a, b)
{
    return (a<b?-1:(a>b?1:0));
}

function Alert4BulkEMAIL(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}

function ChangeEmailSpeed(value) {
    
    if ( value === 1) {
        $('#_speed').val("1");
        $('#_bulk_email-speed').html("SLOW");
    } 
    else if(value === 3){
        $('#_speed').val("3");
        $('#_bulk_email-speed').html("NORMAL");
            
    }else if(value === 10){
        $('#_speed').val("10");
        $('#_bulk_email-speed').html("FAST");
            
    }else if(value === 30){
        $('#_speed').val("30");
        $('#_bulk_email-speed').html("VERY FAST");
            
    }
    
}

function ChangeEmailFormat(value) {
    
    if(value === 1){
        $('#_Format').val("1");
        $('#_FileFormat').html("Email Id");
            
    }else if(value === 2){
        $('#_Format').val("2");
        $('#_FileFormat').html("Name,Email");
            
    }else if(value === 3){
        $('#_Format').val("3");
        $('#_FileFormat').html("Name,Email ID,Message");
            
    }else if(value === 4){
        $('#_Format').val("4");
        $('#_FileFormat').html("Email Id,Message");
            
    }
}
 
function LoadTemplateDataEMAIL(){
    var node = document.getElementById("_templateID");
    var index = node.selectedIndex;
    var value1 = node.options[index].value;
    CKEDITOR.instances['_messageemailbody'].setData(value1);
    $('#_messageemailbody').val(value1);
}


function LoadTemplateBody(){
    var node = document.getElementById("_templateID");
    var index = node.selectedIndex;
    var value1 = node.options[index].value;
    $.ajax({
        type: 'GET',
        url: "./loadtemplatebody?_tid="+value1,
        success: function(data) {           
            CKEDITOR.instances['_messageemailbody'].setData(data);
            $('#_messageemailbody').val(data);
            LoadTemplateSubject();
        }
    });
}



function LoadTemplateSubject(){
    var node = document.getElementById("_templateID");
    var index = node.selectedIndex;
    var value1 = node.options[index].value;
    $.ajax({
        type: 'GET',
        url: "./loadtemplatesubject?_tid="+value1,
        success: function(data) {           
            $('#_subject').val(data);
        }
    });
}


function emailblast(){
    var s = './bulkemail';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#bulkemail").serialize(),
        success: function(data) {
            if ( strcmpbulk(data._result,"error") === 0 ) {
                $('#bulk-email-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }else if ( strcmpbulk(data._result,"Blocked") === 0 ) {
                $('#bulk-email-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
            else if ( strcmpbulk(data._result,"success") === 0 ) {
                $('#bulk-email-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function AttachmentUpload() {

    $('#buttonUploadEAD').attr("disabled", true);

    var s = './uploadAttachment';
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileXMLToUploadEAD',
        url: s,
        dataType: 'json',
        success: function(data) {

            if (strcmpbulk(data._result, "error") == 0) {
                Alert4BulkEMAIL("<span><font color=blue>" + data._message + "</font></span>");


            }
            else if (strcmpbulk(data._result, "success") == 0) {
//                alert(data.filename);
                 $('#_filename').val(data.filename);
                Alert4BulkEMAIL("<span><font color=blue>" + data._message + "</font></span>");


            }

        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}


function AttachmentUpload1() {

    $('#buttonUploadEADS').attr("disabled", true);

    var s = './uploadAttachment';
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileXMLToUploadEADS',
        url: s,
        dataType: 'json',
        success: function(data) {

            if (strcmpbulk(data._result, "error") == 0) {
                Alert4BulkEMAIL("<span><font color=blue>" + data._message + "</font></span>");


            }
            else if (strcmpbulk(data._result, "success") == 0) {
//                alert(data.filename);
                 $('#_filenameS').val(data.filename);
                Alert4BulkEMAIL("<span><font color=blue>" + data._message + "</font></span>");


            }

        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}