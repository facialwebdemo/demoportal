function strcmpbulk(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Msg(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function CostReportpdf(val) {
    if (val == 1) {
        var val1 = encodeURIComponent(document.getElementById('startdate').value);
        var val2 = encodeURIComponent(document.getElementById('enddate').value);
        var s = './msgcostreport?_type=' + val + "&_reporttype=" + 0 + '&_startdate=' + val1 + "&_enddate=" + val2;
        window.location.href = s;
        return false;
    } else if (val == 2) {
        var val1 = encodeURIComponent(document.getElementById('startdate1').value);
        var val2 = encodeURIComponent(document.getElementById('enddate1').value);
        var s = './msgcostreport?_type=' + val + "&_reporttype=" + 0 + '&_startdate=' + val1 + "&_enddate=" + val2;
        window.location.href = s;
        return false;
    } else if (val == 3) {
        var val1 = encodeURIComponent(document.getElementById('startdate2').value);
        var val2 = encodeURIComponent(document.getElementById('enddate2').value);
        var s = './msgcostreport?_type=' + val + "&_reporttype=" + 0 + '&_startdate=' + val1 + "&_enddate=" + val2;
        window.location.href = s;
        return false;
    }
}



function ChangeSpeed(value) {

    if (value === 1) {
        $('#_speed').val("1");
        $('#_bulk_sms-speed').html("Slow");
    }
    else if (value === 3) {
        $('#_speed').val("3");
        $('#_bulk_sms-speed').html("NORMAL");

    } else if (value === 10) {
        $('#_speed').val("10");
        $('#_bulk_sms-speed').html("FAST");

    } else if (value === 30) {
        $('#_speed').val("30");
        $('#_bulk_sms-speed').html("VERY FAST");

    }
}

// function ChangeFormat(value) {
//    
//        if ( value === 1) {
//            $('#_Format').val("1");
//            $('#_FileFormat').html("Mobile no");
//        } 
//        else if(value === 2){
//            $('#_Format').val("2");
//            $('#_FileFormat').html("Name,Mobile NO");
//            
//        }else if(value === 3){
//            $('#_Format').val("3");
//            $('#_FileFormat').html("Name,Mobile NO,Message");
//            
//        }else if(value === 4){
//            $('#_Format').val("4");
//            $('#_FileFormat').html("Mobile NO,Message");
//            
//        }
// }

function smsblast() {

    var s = './bulkmsg';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#bulkmsg").serialize(),
        success: function(data) {
            if (strcmpbulk(data._result, "error") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Msg("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpbulk(data._result, "Blocked") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Msg("<span><font color=blue>" + data._message + "</font></span>");
            }
            else if (strcmpbulk(data._result, "success") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Msg("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function LoadTemplateData() {
    var node = document.getElementById("_templateID");
    var index = node.selectedIndex;
    var value1 = node.options[index].value;
    $('#_messagebody').val(value1);
}

function Alert4SMS(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function tempmobile() {
    Alert4Contacts("Bulk Mobile Messages is getting processed. You will get email when completed!!!");
}


function ChangeMediaType(value) {
    //1 for enabled
    //0 for disabled
    if (value === 1) {
        $('#_changeType').val("1");
        $('#_change-Type-Axiom').html("SMS");
    } else if (value === 2) {
        $('#_changeType').val("3");
        $('#_change-Type-Axiom').html("VOICE");
    } else if (value === 3) {
        $('#_changeType').val("2");
        $('#_change-Type-Axiom').html("USSD");
    } else if (value === 4) {
        $('#_changeType').val("4");
        $('#_change-Type-Axiom').html("EMAIL");
    } else if (value === 5) {
        $('#_changeType').val("5");
        $('#_change-Type-Axiom').html("FAX");
    } else if (value === 6) {
        $('#_socialchangeType').val("6");
        $('#_change-TypeSocial-Axiom').html("FACEBOOK");
    } else if (value === 7) {
        $('#_socialchangeType').val("7");
        $('#_change-TypeSocial-Axiom').html("LINKEDIN");
    } else if (value === 8) {
        $('#_socialchangeType').val("8");
        $('#_change-TypeSocial-Axiom').html("TWITTER");
    } else if (value === 18) {
        $('#_pushchangeType').val("18");
        $('#_change-TypePush-Axiom').html("Android Push");
    } else if (value === 19) {
        $('#_pushchangeType').val("19");
        $('#_change-TypePush-Axiom').html("I-Phone Push");
    }
}



//function searchRecords() {
////    var val = document.getElementById('_keyword').value;
//
//
//    var val1 = encodeURIComponent(document.getElementById('startdate').value);
//    var val2 = encodeURIComponent(document.getElementById('enddate').value);
//    var val = document.getElementById('_searchtext').value;
//    var val3 = document.getElementById('_changeType').value;
//  ///  alert('dataala' +val1+val2)
//
//    var s = './searchmsgtable.jsp?_startdate=' + val1 +"&_searchtext="+val+"&_enddate="+val2+"&_type="+val3;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#search_table_main').html(data);
//        }
//    });
//}




function searchRecords() {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val = document.getElementById('_searchtext').value;
    var val3 = document.getElementById('_changeType').value;


    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }

    if (strcmpbulk(val3, "0") === 0) {
        Alert4Msg("Please select message type!!!");
        return;
    }

    var s = './messagesreporttable.jsp?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2 + "&_type=" + val3;

    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            $("#MsgReportgraph").empty();
            $("#MsgReportgraph1").empty();
            //document.getElementById('graph11').html("");
            //document.getElementById('barchartid').html(""); 

            var day_data1 = null;
            day_data1 = BarChartDemo1(val1, val2, val3, val);
            Morris.Bar({
                element: 'MsgReportgraph1',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });


            var day_data = null;
            day_data = DonutChart1(val1, val2, val3, val);
            Morris.Donut({
                element: 'MsgReportgraph',
                data: day_data
            });
        }
    });





}
function BarChartDemo1(val1, val2, val3, val) {
    var s = './messagereportbar?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val3 + "&_searchtext=" + val;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function DonutChart1(val1, val2, val3, val) {
    var s = './messagereportdonut?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val3 + "&_searchtext=" + val;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function msgReportCSV() {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val = document.getElementById('_searchtext').value;
    var val3 = document.getElementById('_changeType').value;
    var s = './msgreportdown?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2 + "&_type=" + val3 + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function msgReportPDF() {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val = document.getElementById('_searchtext').value;
    var val3 = document.getElementById('_changeType').value;
    var s = './msgreportdown?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2 + "&_type=" + val3 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function ChangeSending(value) {

    if (value === 1) {
        $('#_sendingOption').val("1");
        $('#_bulk_sms-speed').html("Private");
    }
    else if (value === 3) {
        $('#_sendingOption').val("3");
        $('#_bulk_sms-speed').html("Public");

    }
}


function notificationblast() {

    var s = './bulkfbnotification';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#bulkmsg").serialize(),
        success: function(data) {
            if (strcmpbulk(data._result, "error") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Msg("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpbulk(data._result, "success") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Msg("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function twitterblast() {

    var s = './bulkfbnotification';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#bulkmsg").serialize(),
        success: function(data) {
            if (strcmpbulk(data._result, "error") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Msg("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpbulk(data._result, "success") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Msg("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}



function linkedinblast() {

    var s = './bulklinkedinnotification';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#bulkmsg").serialize(),
        success: function(data) {
            if (strcmpbulk(data._result, "error") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Msg("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpbulk(data._result, "success") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Msg("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function searchsocialRecords() {
    var val1 = encodeURIComponent(document.getElementById('socialstartdate').value);
    var val2 = encodeURIComponent(document.getElementById('socialenddate').value);
    var val = document.getElementById('_searchtext').value;
    var val3 = document.getElementById('_socialchangeType').value;


    if (document.getElementById('socialstartdate').value.length == 0 || document.getElementById('socialenddate').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }

    if (strcmpbulk(val3, "0") === 0) {
        Alert4Msg("Please select message type!!!");
        return;
    }

    var s = './socialmediareporttable.jsp?_startdate=' + val1 + "&_searchtext=" + encodeURIComponent(val) + "&_enddate=" + val2 + "&_type=" + val3;

    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            $("#MsgReportgraph").empty();
            $("#MsgReportgraph1").empty();

            var day_data1 = null;
            day_data1 = BarChartDemo1(val1, val2, val3, val);
            Morris.Bar({
                element: 'MsgReportgraph1',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });


            var day_data = null;
            day_data = DonutChart1(val1, val2, val3, val);
            Morris.Donut({
                element: 'MsgReportgraph',
                data: day_data
            });
        }
    });

}
function BarChartCostReport(val1, val2, val) {
//    alert(val)
    var s = './msgcostbar?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function DonutChartCostReport(val1, val2, val) {
    var s = './msgcostdonut?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function searchCostReport(val) {
//    alert(val)

    if (val == 1) {
        var ele = document.getElementById("costreport");
        ele.style.display = "block";
        var val1 = encodeURIComponent(document.getElementById('startdate').value);
        var val2 = encodeURIComponent(document.getElementById('enddate').value);
        if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
            Alert4Msg("Date Range is not selected!!!");
            return;
        }
        var day_data1 = null;
        day_data1 = BarChartCostReport(val1, val2, val);
        Morris.Bar({
            element: 'MsgCostReportgraph1',
            data: day_data1,
            xkey: 'label',
            ykeys: ['value'],
            labels: ['value'],
            barColors: function(type) {
                if (type === 'bar') {

                    return '#0066CC';
                }
                else {

                    return '#0066CC';
                }
            }
        });

        var day_data = null;
        day_data = DonutChartCostReport(val1, val2, val);
        Morris.Donut({
            element: 'MsgCostReportgraph',
            data: day_data
        });
    } else if (val == 2) {
        var ele = document.getElementById("costreportsocial");
        ele.style.display = "block";
        var val1 = encodeURIComponent(document.getElementById('startdate1').value);
        var val2 = encodeURIComponent(document.getElementById('enddate1').value);
        if (document.getElementById('startdate1').value.length == 0 || document.getElementById('enddate1').value.length == 0) {
            Alert4Msg("Date Range is not selected!!!");
            return;
        }
        var day_data1 = null;
        day_data1 = BarChartCostReport(val1, val2, val);
        Morris.Bar({
            element: 'MsgCostReportgraph21',
            data: day_data1,
            xkey: 'label',
            ykeys: ['value'],
            labels: ['value'],
            barColors: function(type) {
                if (type === 'bar') {

                    return '#0066CC';
                }
                else {

                    return '#0066CC';
                }
            }
        });

        var day_data = null;
        day_data = DonutChartCostReport(val1, val2, val);
        Morris.Donut({
            element: 'MsgCostReportgraph2',
            data: day_data
        });
    } else if (val == 3) {
        var ele = document.getElementById("costreportpush");
        ele.style.display = "block";
        var val1 = encodeURIComponent(document.getElementById('startdate2').value);
        var val2 = encodeURIComponent(document.getElementById('enddate2').value);
        if (document.getElementById('startdate2').value.length == 0 || document.getElementById('enddate2').value.length == 0) {
            Alert4Msg("Date Range is not selected!!!");
            return;
        }
        var day_data1 = null;
        day_data1 = BarChartCostReport(val1, val2, val);
        Morris.Bar({
            element: 'MsgCostReportgraph31',
            data: day_data1,
            xkey: 'label',
            ykeys: ['value'],
            labels: ['value'],
            barColors: function(type) {
                if (type === 'bar') {

                    return '#0066CC';
                }
                else {

                    return '#0066CC';
                }
            }
        });

        var day_data = null;
        day_data = DonutChartCostReport(val1, val2, val);
        Morris.Donut({
            element: 'MsgCostReportgraph3',
            data: day_data
        });
    }
}






function searchMSGReport(val) {
//    alert(val)

    if (val == 1) {
//          var ele = document.getElementById("costreport");
//          ele.style.display = "block";  

        var val1 = encodeURIComponent(document.getElementById('startdate').value);
        var val2 = encodeURIComponent(document.getElementById('enddate').value);
        var val4 = document.getElementById('_searchtext').value;
        var val3 = document.getElementById('_changeType').value;
        

        if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
            Alert4Msg("Date Range is not selected!!!");
            return;
        }
        
        var s = './messagesreporttable.jsp?_startdate=' + val1 + "&_searchtext=" + val4 + "&_enddate=" + val2 + "&_type=" + val3;
        $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {
                
//        $('#users_table_main_MSG').html(data);
                $('#licenses_data_table').html(data);
                 $("#licenses_data_table_Social").empty();
                  $("#licenses_data_table_push").empty();
                var day_data1 = null;
                day_data1 = BarChartDemo1(val1, val2, val3, val4);
//        day_data1 = BarChartCostReport(val1, val2,val);
                Morris.Bar({
                    element: 'MsgReportgraph1',
                    data: day_data1,
                    xkey: 'label',
                    ykeys: ['value'],
                    labels: ['value'],
                    barColors: function(type) {
                        if (type === 'bar') {

                            return '#0066CC';
                        }
                        else {

                            return '#0066CC';
                        }
                    }
                });

                var day_data = null;

//        day_data = DonutChartCostReport(val1, val2,val);
                day_data = DonutChart1(val1, val2, val3, val4);
                Morris.Donut({
                    element: 'MsgReportgraph',
                    data: day_data
                });
            }});

            
    } else if (val == 2) {


//        var ele = document.getElementById("costreportsocial");
//        ele.style.display = "block"; 
        var val1 = encodeURIComponent(document.getElementById('socialstartdate').value);

        var val2 = encodeURIComponent(document.getElementById('socialenddate').value);

        var val4 = document.getElementById('_socialsearchtext').value;

        var val3 = document.getElementById('_socialchangeType').value;


        if (document.getElementById('socialstartdate').value.length == 0 || document.getElementById('socialenddate').value.length == 0) {
            Alert4Msg("Date Range is not selected!!!");
            return;
        }

        var s = './socialmediareporttable.jsp?_startdate=' + val1 + "&_searchtext=" + encodeURIComponent(val4) + "&_enddate=" + val2 + "&_type=" + val3;
        $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {
                $('#licenses_data_table_Social').html(data);
                $("#licenses_data_table").empty();
                  $("#licenses_data_table_push").empty();
                var day_data1 = null;

//        day_data1 = BarChartCostReport(val1, val2,val);
                day_data1 = BarChartDemo1(val1, val2, val3, val4);
                Morris.Bar({
                    element: 'MsgReportgraphBSocial',
                    data: day_data1,
                    xkey: 'label',
                    ykeys: ['value'],
                    labels: ['value'],
                    barColors: function(type) {
                        if (type === 'bar') {

                            return '#0066CC';
                        }
                        else {

                            return '#0066CC';
                        }
                    }
                });

                var day_data = null;
//        day_data = DonutChartCostReport(val1, val2,val);
                day_data = DonutChart1(val1, val2, val3, val4);
                Morris.Donut({
                    element: 'MsgReportgraphDSocial',
                    data: day_data
                });
            }
        });
    } else if (val == 3) {

        var val1 = encodeURIComponent(document.getElementById('pushstartdate').value);
        var val2 = encodeURIComponent(document.getElementById('pushenddate').value);
        var val4 = document.getElementById('_pushsearchtext').value;
        var val3 = document.getElementById('_pushchangeType').value;

        if (document.getElementById('pushstartdate').value.length == 0 || document.getElementById('pushenddate').value.length == 0) {
            Alert4Msg("Date Range is not selected!!!");
            return;
        }
        var s = './pushreporttable.jsp?_startdate=' + val1 + "&_searchtext=" + encodeURIComponent(val4) + "&_enddate=" + val2 + "&_type=" + val3;
        $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {
                $('#licenses_data_table_push').html(data);
                 $("#licenses_data_table").empty();
                  $("#licenses_data_table_Social").empty();
                var day_data1 = null;
                day_data1 = BarChartDemo1(val1, val2, val3, val4);
//        day_data1 = BarChartCostReport(val1, val2,val);
                Morris.Bar({
                    element: 'MsgReportgraphPush1',
                    data: day_data1,
                    xkey: 'label',
                    ykeys: ['value'],
                    labels: ['value'],
                    barColors: function(type) {
                        if (type === 'bar') {

                            return '#0066CC';
                        }
                        else {

                            return '#0066CC';
                        }
                    }
                });

                var day_data = null;
                day_data = DonutChart1(val1, val2, val3, val4);
//        day_data = DonutChartCostReport(val1, val2,val);
                Morris.Donut({
                    element: 'MsgReportgraphPush',
                    data: day_data
                });
            }});
    }
}




function searchpushRecords() {
    var val1 = encodeURIComponent(document.getElementById('pushstartdate').value);
    var val2 = encodeURIComponent(document.getElementById('pushenddate').value);
//    alert(val2)
    var val = document.getElementById('_pushsearchtext').value;
    var val3 = document.getElementById('_pushchangeType').value;
//    var val3 = 18;


    if (document.getElementById('pushstartdate').value.length == 0 || document.getElementById('pushenddate').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }

//    if (strcmpbulk(val3, "0") === 0) {
//        Alert4Msg("Please select message type!!!");
//        return;
//    }

    var s = './pushreporttable.jsp?_startdate=' + val1 + "&_searchtext=" + encodeURIComponent(val) + "&_enddate=" + val2 + "&_type=" + val3;

    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table_push').html(data);
            $("#MsgReportgraphPush").empty();
            $("#MsgReportgraphPush1").empty();

            var day_data1 = null;
            day_data1 = BarChartDemo1(val1, val2, val3, val);
            Morris.Bar({
                element: 'MsgReportgraphPush1',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });


            var day_data = null;
            day_data = DonutChart1(val1, val2, val3, val);
            Morris.Donut({
                element: 'MsgReportgraphPush',
                data: day_data
            });
        }
    });
}


function pushReportCSV() {
    var val1 = encodeURIComponent(document.getElementById('pushstartdate').value);
    var val2 = encodeURIComponent(document.getElementById('pushenddate').value);
    var val = document.getElementById('_pushsearchtext').value;
    var val3 = document.getElementById('_pushchangeType').value;
//    var val3 = 18;
    var s = './msgreportdown?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2
            + "&_type=" + val3 + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function pushReportPDF() {
    var val1 = encodeURIComponent(document.getElementById('pushstartdate').value);
    var val2 = encodeURIComponent(document.getElementById('pushenddate').value);
    var val = document.getElementById('_pushsearchtext').value;
     var val3 = document.getElementById('_pushchangeType').value;
//    var val3 = 18;
    var s = './msgreportdown?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2
            + "&_type=" + val3 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function socialReportCSV() {
    var val1 = encodeURIComponent(document.getElementById('socialstartdate').value);
    var val2 = encodeURIComponent(document.getElementById('socialenddate').value);

    var val = document.getElementById('_socialsearchtext').value;
    var val3 = document.getElementById('_socialchangeType').value;

    var s = './msgreportdown?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2
            + "&_type=" + val3 + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function socialReportPDF() {
    var val1 = encodeURIComponent(document.getElementById('socialstartdate').value);
    var val2 = encodeURIComponent(document.getElementById('socialenddate').value);
    var val = document.getElementById('_socialsearchtext').value;
    var val3 = document.getElementById('_socialchangeType').value;
//    var s = './msgreportdown?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2 
//            + "&_type=" + val3 + "&_reporttype=" + 0;
    var s = './msgreportdown?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2
            + "&_type=" + val3 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function googlepushnotificationblast() {

    var s = './googlepushnotification?_type=10';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#bulkmsg").serialize(),
        success: function(data) {
            if (strcmpbulk(data._result, "error") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Msg("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpbulk(data._result, "success") === 0) {
                $('#bulk-sms-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Msg("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}