function strcmpfax(a, b)
{
    return (a<b?-1:(a>b?1:0));
}
function Alert4Fax(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}
function LoadFaxBody(){
    var node = document.getElementById("_templateID");
    var index = node.selectedIndex;
    var value1 = node.options[index].value;
    $.ajax({
        type: 'GET',
        url: "./loadtemplatebody?_tid="+value1,
        success: function(data) {           
            CKEDITOR.instances['_messageemailbody'].setData(data);
            $('#_messageemailbody').val(data);
            LoadTemplateSubject();
        }
    });
}

function toogleFAXEAD() {
    var ele = document.getElementById("uploadImageEAD");
    var text = document.getElementById("displayTextEAD");
    if(ele.style.display == "block") {
        ele.style.display = "none";
        text.innerHTML = "click here";
    }
    else {
        ele.style.display = "block";
        text.innerHTML = "hide";
    }
}

function UploadFAXFile()
{
    $.ajaxFileUpload
    (
    {
    
        url:'./fileuploadsubmit.jsp',
        //secureuri:false,
        fileElementId:'fileToUploadEAD',
        dataType: 'json',
        success: function (data, status)
        {
            if(typeof(data.error) != 'undefined')
            {
                if(data.error != '')
                {
                    Alert4Fax(data.error);
                }
                else
                {                               
                    var ads_image_ele = document.getElementById("ads_imageEAD");
                   
                    ads_image_ele.value=data.filename;
                                
                    var ele = document.getElementById("uploadImageEAD");
                    var text = document.getElementById("displayTextEAD");
                    if(ele.style.display == "block") {
                        ele.style.display = "none";
                     
                        text.innerHTML = data.msg + " file uploaded. Click for different file upload.";
                    }
                    else {
                        ele.style.display = "block";
                        text.innerHTML = "hide";
                    }
                    Alert4Fax(data.msg + " uploaded successfully");
              
                }
            }
        },
        error: function (data, status, e)
        {
            Alert4Contacts(e);
        }
    }
    )
    return false;
}

function UploadFAXFilev2()
{
    $.ajaxFileUpload
    (
    {
    
        url:'./fileuploadsubmit.jsp',
        //secureuri:false,
        fileElementId:'fileToUploadEAD',
        dataType: 'json',
        success: function (data, status)
        {
              alert(data.result);
            alert(data.message);
            
            if ( strcmpLicense(data.result,"error") == 0 ) {
                //$('#register-file-result').html("<span><font color=red>" + data.message + "</font></span></small>");
                Alert4License("<span><font color=red>" + data.message + "</font></span>");
                $('#buttonRegisterLicense').attr("disabled", false);            
            }
            else if ( strcmpLicense(data.result,"success") == 0 ) {
                //$('#register-file-result').html("<span><font color=blue>" + data.message + "</font></span>");
                Alert4License("<span><font color=blue>" + data.message + "</font></span>");
               
            }
        },
        error: function (data, status, e)
        {
            Alert4Fax(e);
        }
    }
    )
    return false;
}

function UploadFileForFAX(){
    $('#buttonRegisterLicense').attr("disabled", true);
    var s = './registerlicense';
    $.ajaxFileUpload({
        fileElementId:'licensefile',
        url: s,
        //enctype: 'application/x-www-form-urlencoded',
        dataType: 'json',
        //data: $("#RegisterLicenseFileForm").serialize(),
        success: function(data, status) {
            alert(data.result);
            alert(data.message);
            
            if ( strcmpLicense(data.result,"error") == 0 ) {
                //$('#register-file-result').html("<span><font color=red>" + data.message + "</font></span></small>");
                Alert4License("<span><font color=red>" + data.message + "</font></span>");
                $('#buttonRegisterLicense').attr("disabled", false);            
            }
            else if ( strcmpLicense(data.result,"success") == 0 ) {
                //$('#register-file-result').html("<span><font color=blue>" + data.message + "</font></span>");
                Alert4License("<span><font color=blue>" + data.message + "</font></span>");
                window.setTimeout(ShowLicenseDetails, 4000);
            }
        },
        error: function (data, status, e)
        {
            alert(e);
        }
    }); 
}

function UploadFAXNow(){
    $('#buttonRegisterLicense').attr("disabled", true);
    var s = './registerlicense';
    $.ajaxFileUpload({
        fileElementId:'licensefile',
        url: s,
        //enctype: 'application/x-www-form-urlencoded',
        dataType: 'json',
        //data: $("#RegisterLicenseFileForm").serialize(),
        success: function(data, status) {
            alert(data.result);
            alert(data.message);
            
            if ( strcmpLicense(data.result,"error") == 0 ) {
                //$('#register-file-result').html("<span><font color=red>" + data.message + "</font></span></small>");
                Alert4License("<span><font color=red>" + data.message + "</font></span>");
                $('#buttonRegisterLicense').attr("disabled", false);            
            }
            else if ( strcmpLicense(data.result,"success") == 0 ) {
                //$('#register-file-result').html("<span><font color=blue>" + data.message + "</font></span>");
                Alert4License("<span><font color=blue>" + data.message + "</font></span>");
                window.setTimeout(ShowLicenseDetails, 4000);
            }
        },
        error: function (data, status, e)
        {
            alert(e);
        }
    }); 
}


function sendfax(){
    var s = './sendfax';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#fax").serialize(),
        success: function(data) {
            if ( strcmpfax(data._result,"error") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Fax("<span><font color=red>" + data._message + "</font></span>");
            }else if ( strcmpfax(data._result,"Blocked") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Fax("<span><font color=blue>" + data._message + "</font></span>");
            }
            else if ( strcmpfax(data._result,"success") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Fax("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function sendfaxv2(val){
    var s = './sendfaxv2?_type='+encodeURI(val);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#fax").serialize(),
        success: function(data) {
           
            if ( strcmpfax(data._result,"error") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Fax("<span><font color=red>" + data._message + "</font></span>");
            }else if ( strcmpfax(data._result,"Blocked") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Fax("<span><font color=blue>" + data._message + "</font></span>");
            }
            else if ( strcmpfax(data._result,"success") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Fax("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function sendfaxv3(val){
    var s = './sendfaxbytag?_type='+encodeURI(val);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#fax").serialize(),
        success: function(data) {
            if ( strcmpfax(data._result,"error") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Fax("<span><font color=red>" + data._message + "</font></span>");
            }else if ( strcmpfax(data._result,"Blocked") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Fax("<span><font color=blue>" + data._message + "</font></span>");
            }
            else if ( strcmpfax(data._result,"success") === 0 ) {
                $('#fax-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Fax("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function sendfaxv3Attachment(val){
    var s = './sendfaxbytag?_type='+encodeURI(val);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#fax1").serialize(),
        success: function(data) {
            if ( strcmpfax(data._result,"error") === 0 ) {
                $('#fax-gateway-attachment-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Fax("<span><font color=red>" + data._message + "</font></span>");
            }else if ( strcmpfax(data._result,"Blocked") === 0 ) {
                $('#fax-gateway-attachment-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Fax("<span><font color=blue>" + data._message + "</font></span>");
            }
            else if ( strcmpfax(data._result,"success") === 0 ) {
                $('#fax-gateway-attachment-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Fax("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}