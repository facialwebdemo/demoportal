function Alert4Template(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}

function RefreshTemplates() {
    window.location.href = "./templates.jsp";
}

function strcmptemplate(a, b)
{
    return (a<b?-1:(a>b?1:0));
}

function ClearAddTemplateForm(){
    $('#add-new-template-result').html("<span><font color=blue>" + "" + "</font></span>");
//    $("#_oprname").val("");
//    $("#_opremail").val("");
//    $("#_oprphone").val("");
}

function ClearEditTemplateForm(){
    $('#edittemplate-result').html("<span><font color=blue>" + "" + "</font></span>");
//    $("#_oprnameE").val("");
//    $("#_opremailE").val("");
//    $("#_opremailE").val("");
}

function addTemplate(){
    $('#addnewTemplateSubmitBut').attr("disabled", true);
    var s = './addtemplate';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewTemplateForm").serialize(),
        success: function(data) {
            if ( strcmptemplate(data._result,"error") == 0 ) {
                $('#add-new-template-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewTemplateSubmitBut').attr("disabled", false);
            //ClearAddTemplateForm();
            }
            else if ( strcmptemplate(data._result,"success") == 0 ) {
                $('#add-new-template-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshTemplates, 3000);
            }
        }
    }); 
}



/*function loadEditTemplateDetails(_orpid){
    var s = './gettemplate?_id='+encodeURIComponent(_orpid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmptemplate(data._result,"success") == 0 ) {
                $('#idEditTemplateName').html(data._name);
                $("#_oprnameE").val(data._name);
                $("#_opremailE").val(data._email);
                $("#_oprphoneE").val(data._phone);
                $("#_oprroleidE").val(data._roleid);
                $("#_oprstatusE").val(data._status);
                $("#_opridE").val(data._id);
                $('#edittemplate-result').html("");
                $("#editTemplate").modal();
            } else {
                Alert4Template("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function edittemplate(){
    $('#buttonEditTemplateSubmit').attr("disabled", true);
    var s = './edittemplate';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editTemplateForm").serialize(),
        success: function(data) {
            if ( strcmptemplate(data._result,"error") == 0 ) {
                $('#edittemplate-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditTemplateSubmit').attr("disabled", false);
            //ClearEditTemplateForm();
            }
            else if ( strcmptemplate(data._result,"success") == 0 ) {
                $('#edittemplate-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditTemplateForm();
                window.setTimeout(RefreshTemplates, 3000);
            }
        }
    });
}*/


