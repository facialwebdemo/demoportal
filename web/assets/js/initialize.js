function Alert4Init(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}

function strcmpInit(a, b)
{
    return (a<b?-1:(a>b?1:0));
}

function ShowLicenseDetails() {
    window.location.href = "./showlicense.jsp";
}


function SaveOfficerCredential(_id){
    
    var formID = "#formOffice";
    
     if ( _id == 1 ) { 
         formID = formID + "1";
     } else  if ( _id == 2 ) {
         formID = formID + "2";
     } else  if ( _id == 3 ) {
         formID = formID + "3";
     } else  if ( _id == 0 ) {
        formID = "#formConfirm";
     }
         
    
    var s = './initializesc';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $(formID).serialize(),
        success: function(data) {
            
            if ( strcmpInit(data._result,"error") == 0 ) {
                Alert4Init("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpInit(data._result,"success") == 0 ) {
                
                if ( _id == 1 ) {
                    $('#officer1button').attr("disabled", true);            
                    $('#officer2button').attr("disabled", false);            
                    $('#officer3button').attr("disabled", true);            
                } 
                if ( _id == 2 ) {
                    $('#officer1button').attr("disabled", true);            
                    $('#officer2button').attr("disabled", true);            
                    $('#officer3button').attr("disabled", false);            
                }
                if ( _id == 3 ) {
                    $('#officer1button').attr("disabled", true);            
                    $('#officer2button').attr("disabled", true);            
                    $('#officer3button').attr("disabled", true);
                    $('#GenerateSecurityCredentialsButton').attr("disabled", false);   
                } 
                if ( _id == 0 ) {
                    Alert4Init("<span><font color=blue>" + data._message + "</font></span>");
                } 
            }
        }
    }); 
}


function StartState(){
    $('#officer1button').attr("disabled", false);            
    $('#officer2button').attr("disabled", true);            
    $('#officer3button').attr("disabled", true);   
    $('#GenerateSecurityCredentialsButton').attr("disabled", true);   
}