function strcmpPushMsg(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4PushMsgSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}


function ChangeActiveStatusPushMsg(perference,value) {
    
    if ( perference == 1) {
        if ( value == 1) {
            $('#_status').val("1");
            $('#_status-primary-PushMsg').html("Active");
        } else {
            $('#_status').val("0");
            $('#_status-primary-PushMsg').html("Suspended");
        }
    } else if ( perference == 2) {
        if ( value == 1) {
            $('#_statusS').val("1");
            
            $('#_statusS-secondary-PushMsg').html("Active");
        } else {
            $('#_statusS').val("0");
            $('#_statusS-secondary-PushMsg').html("Suspended");
        }
    }
}

function ChangeDelayWhileIdel(perference,value) {
    if ( perference == 1) {
        if ( value == 1) {
            $('#_delayWhileIdle').val("1");
            $('#_delayWhileIdle-primary-PushMsg').html("Active");
        } else {
            $('#_delayWhileIdle').val("0");
            $('#_delayWhileIdle-primary-PushMsg').html("Suspended");
        }
    } else if ( perference == 2) {
     
        if ( value == 1) {
            $('#_delayWhileIdleS').val("1");
            $('#_delayWhileIdleS-secondary-PushMsg').html("Production");
        } else {
            $('#_delayWhileIdleS').val("0");
            $('#_delayWhileIdleS-secondary-PushMsg').html("Sandbox");
        }
    }
}
function ChangeFailOverPushMsg(value) {
    //1 for enabled
    //0 for disabled
    if ( value == 1) {
        $('#_autofailover').val("1");
        $('#_autofailover-primary-PushMsg').html("Enabled");
    } else {
        $('#_autofailover').val("0");
        $('#_autofailover-primary-PushMsg').html("Disabled");
    }
}

function ChangeRetryPushMsg(preference,count) {
   
       if ( preference == 1) {
        if ( count == 2) {
            $('#_retries').val("2");
            $('#_retries-primary-PushMsg').html("2 retries");
        } else if ( count == 3) {
            $('#_retries').val("3");
            $('#_retries-primary-PushMsg').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retries').val("5");
            $('#_retries-primary-PushMsg').html("5 retries");
        }
    } else if ( preference == 2) {
         
        if (count == 2) {
            $('#_retriesS').val("2");
            $('#_retriesS-secondary-PushMsg').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesS').val("3");
            $('#_retriesS-secondary-PushMsg').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesS').val("5");
            $('#_retriesS-secondary-PushMsg').html("5 retries");
        }
    }
}

function ChangeRetryFromGoogle(preference,count) {
  
       if ( preference == 1) {
        if ( count == 2) {
            $('#_retriesFromGoogle').val("2");
            $('#_retriesFromGoogle-primary-PushMsg').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesFromGoogle').val("3");
            $('#_retriesFromGoogle-primary-PushMsg').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesFromGoogle').val("5");
            $('#_retriesFromGoogle-primary-PushMsg').html("5 retries");
        }
    } else if ( preference == 2) {
       
        if ( count == 2 || count == 0) {
            $('#_retriesFromGoogleS').val("2");
            $('#_retriesFromGoogleS-secondary-PushMsg').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesFromGoogleS').val("3");
            $('#_retriesFromGoogleS-secondary-PushMsg').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesFromGoogleS').val("5");
            $('#_retriesFromGoogleS-secondary-PushMsg').html("5 retries");
        }
    }
}

function ChangeTimeToLive(preference,count) {
       if ( preference == 1) {
        if ( count == 10) {
            $('#_timetolive').val("10");
            $('#_timetolive-primary-PushMsg').html("10 Seconds");
        } else if ( count == 20) {
            $('#_timetolive').val("20");
            $('#_timetolive-primary-PushMsg').html("20 Seconds");
        }
        else if ( count == 30) {
            $('#_timetolive').val("30");
            $('#_timetolive-primary-PushMsg').html("30 Seconds");
        }
    } else if ( preference == 2) {
        if ( count == 10) {
            $('#_timetoliveS').val("10");
            $('#_timetoliveS-secondary-PushMsg').html("10 Seconds");
        } else if ( count == 20) {
            $('#_timetoliveS').val("20");
            $('#_timetoliveS-secondary-PushMsg').html("20 Seconds");
        }
        else if ( count == 30) {
            $('#_timetoliveS').val("30");
            $('#_timetoliveS-secondary-PushMsg').html("30 Seconds");
        }
    }
}
function ChangeRetryDurationPushMsg(perference,duration) {
    //1 for enabled
    //0 for disabled
    if ( perference == 1) {
        if ( duration == 10) {
            $('#_retryduration').val("10");
            $('#_retryduration-primary-PushMsg').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retryduration').val("30");
            $('#_retryduration-primary-PushMsg').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retryduration').val("60");
            $('#_retryduration-primary-PushMsg').html("60 seconds");
        }
    } else if ( perference == 2) {
        if ( duration == 10) {
            $('#_retrydurationS').val("10");
            $('#_retrydurationS-secondary-PushMsg').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationS').val("30");
            $('#_retrydurationS-secondary-PushMsg').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationS').val("60");
            $('#_retrydurationS-secondary-PushMsg').html("60 seconds");
        }
    }
}



function PushMsgprimary(){
    var s = './loadpushnotificationsetting?_type=18&_preference=1';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            $('#_ip').val(data._ip);
            $('#_port').val(data._port);
            if (data._logConfirmation == true) {
                $('#_logConfirmation').attr("checked", true);
            }else{
                $('#_logConfirmation').attr("checked", false);
            }
            $('#_className').val(data._className);
            $('#_reserve1').val(data._reserve1);
            $('#_reserve2').val(data._reserve2);
            $('#_reserve3').val(data._reserve3);
            $('#_autofailover').val(data._autofailover);
            $('#_retries').val(data._retries);
            $('#_retryduration').val(data._retryduration);
            $('#_gcmurl').val(data._gcmurl);
            $('#_apikey').val(data._apikey);
            $('#_googleSenderKey').val(data._googleSenderKey);
             $('#_status').val(data._status);
            $('#_retriesFromGoogle').val(data._retriesFromGoogle);
            $('#_timetolive').val(data._timetolive);
            $('#_delayWhileIdle').val(data._delayWhileIdle);
            if (data._status == 1)
                ChangeActiveStatusPushMsg(1,1);
            else
                ChangeActiveStatusPushMsg(1,0);
            
            if (data._autofailover == 1)
                ChangeFailOverPushMsg(1);
            else
                ChangeFailOverPushMsg(0);
            
            if (data._retryduration == 10 )
                ChangeRetryDurationPushMsg(1,10);
            else if (data._retryduration == 30)
                ChangeRetryDurationPushMsg(1,30);
            else if (data._retryduration == 60)
                ChangeRetryDurationPushMsg(1,60);
            else
                ChangeRetryDurationPushMsg(1,10);
            
            if (data._retries == 2)
                ChangeRetryPushMsg(1,2);
            else if (data._retries == 3)
                ChangeRetryPushMsg(1,3);
            else if (data._retries == 5)
                ChangeRetryPushMsg(1,5);
            else
                ChangeRetryPushMsg(1,2);    
            
            
             if (data._retriesFromGoogle == 2 )
                ChangeRetryFromGoogle(1,2);
            else if (data._retriesFromGoogle == 3)
                ChangeRetryFromGoogle(1,3);
            else if (data._retriesFromGoogle == 5)
                ChangeRetryFromGoogle(1,5);
            
            
              if (data._timetolive == 10 )
                ChangeTimeToLive(1,10);
            else if (data._timetolive == 20)
                ChangeTimeToLive(1,20);
            else if (data._timetolive == 30)
                ChangeTimeToLive(1,30);
            
                 if (data._delayWhileIdle == 1 )
                ChangeDelayWhileIdel(1,1);
            else if (data._delayWhileIdle == 0)
                ChangeDelayWhileIdel(1,0);
        }
    });
}

function PushMsgsecondary(){
    var s = './loadiphonepushsettings?_type=19&_preference=1';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
          $('#_ipS').val(data._ipS);
            $('#_portS').val(data._portS);
            if (data._logConfirmationS == true) {
                $('#_logConfirmationS').attr("checked", true);
            }else{
                $('#_logConfirmationS').attr("checked", false);
            }
            $('#_classNameS').val(data._classNameS);
            $('#_reserve1S').val(data._reserve1S);
            $('#_reserve2S').val(data._reserve2S);
            $('#_reserve3S').val(data._reserve3S);
            $('#_retriesS').val(data._retriesS);
            $('#_retrydurationS').val(data._retrydurationS);
             $('#_statusS').val(data._statusS);
            $('#_bundleID').val(data._bundleID);
             $('#_certpassowrd').val(data._certpassowrd);
             
//             alert(data.uploadStatus);
             $('#uploadStatus').html(data.uploadStatus);
//             alert(data._retriesS);

                if (data._retriesS == 2)
                ChangeRetryPushMsg(2,2);
            else if (data._retriesS == 3)
                ChangeRetryPushMsg(2,3);
            else if (data._retriesS == 5)
                ChangeRetryPushMsg(2,5);
            else
                ChangeRetryPushMsg(1,2);    
    

            if (data._statusS == 1)
                ChangeActiveStatusPushMsg(2,1);
            else
                ChangeActiveStatusPushMsg(2,0);

            if (data._retrydurationS == 10)
                ChangeRetryDurationPushMsg(2,10);
            else if (data._retrydurationS == 30)
                ChangeRetryDurationPushMsg(2,30);
            else if (data._retrydurationS == 60)
                ChangeRetryDurationPushMsg(2,60);
            else
                ChangeRetryDurationPushMsg(2,10);

                 if (data._delayWhileIdleS == 1 )
                ChangeDelayWhileIdel(2,1);
            else if (data._delayWhileIdleS == 0)
                ChangeDelayWhileIdel(2,0);
        }
    }
    );
}

function LoadPushMsgSetting(type){
    if (type == 1) {
        PushMsgprimary();
    } else if (type == 2 ){
        PushMsgsecondary();
    }
}


function editPushMsgprimary(){
    var s = './editpushnotificationsetting';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#PushMsgprimaryform").serialize(),
        success: function(data) {
            if ( strcmpPushMsg(data._result,"error") == 0 ) {
                $('#save-PushMsg-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4PushMsgSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpPushMsg(data._result,"success") == 0 ) {
                $('#save-PushMsg-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function editPushMsgsecondary(){
    var s = './editiphonepushsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#PushMsgecondaryform").serialize(),
        success: function(data) {
            if ( strcmpPushMsg(data._result,"error") == 0 ) {
                $('#save-PushMsg-gateway-secondary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4PushMsgSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpPushMsg(data._result,"success") == 0 ) {
                $('#save-PushMsg-gateway-secondary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function EditPushMsgSetting(type){    
    if (type == 1) {
        editPushMsgprimary();
    } else if (type == 2 ){
        editPushMsgsecondary();
    }
}

function LoadTestPushMsgConnectionUI(type){
    if ( type == 1 )
        $("#testPushMsgPrimary").modal();
    else
        $("#testPushMsgSecondary").modal();
}

function testconnectionPushMsgprimary(){

    var s = './testconnection';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#testPushMsgPrimaryForm").serialize(),
        success: function(data) {
            if ( strcmpSMS(data._result,"error") == 0 ) {
                $('#test-PushMsg-primary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpSMS(data._result,"success") == 0 ) {
                $('#test-PushMsg-primary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function testconnectionPushMsgsecondary(){

    var s = './testconnection';

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testPushMsgSecondaryForm").serialize(),
        success: function(data) {
            if ( strcmpSMS(data._result,"error") == 0 ) {
                $('#test-PushMsg-secondary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpSMS(data._result,"success") == 0 ) {
                $('#test-PushMsg-secondary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function TestPushMsgConnection(type){
    if (type == 1) {
        testconnectionPushMsgprimary();
    } else if (type == 2 ){
        testconnectionPushMsgsecondary();
    }
}

function MessageLengthPushMsg(preference,count) {
    if ( preference == 1) {
        if ( count == 1) {
//              alert(count);
            $('#_messgeLength').val("1");
            $('#_messgeLength-primary-PushMsg').html("No");
        } else if ( count == 0) {
//            alert(count);
            $('#_messgeLength').val("0");
            $('#_messgeLength-primary-PushMsg').html("Yes");
        }
       
    } 
    else if ( preference == 2) {
        if ( count == 1) {
//              alert(count);
            $('#_messgeLengthS').val("1");
            $('#_messgeLengthS-secondary-PushMsg').html("No");
        } else if ( count == 0) {
//            alert(count);
            $('#_messgeLengthS').val("0");
            $('#_messgeLengthS-secondary-PushMsg').html("Yes");
        }
       
    } 
}

//function CertificateFileUpload() {
//
//    $('#buttonUploadEAD').attr("disabled", true);
//  var s = './editpushnotificationsetting?_password=' + document.getElementById('_certpassowrd').value;
//    $.ajaxFileUpload({
//        type: 'POST',
//        fileElementId: 'fileXMLToUploadEAD',
//        url: s,
//        dataType: 'json',
//        success: function(data,status) {
//            alert("hi");
//            if (strcmpPushMsg(data._result,"error") == 0) {
//                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
//            } else if (strcmpPushMsg(data._result, "success") == 0) {
//              Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
//            }
//        },
//        error: function(data, status, e)
//        {
//            alert(e);
//        }
//    });
//}


function CertificateFileUpload() {

    $('#buttonUploadEAD').attr("disabled", true);

    var s = './editiphonepushsettings?_password=' + document.getElementById('_certpassowrd').value;
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileXMLToUploadEAD',
        url: s,
        dataType: 'json',
        data: $("#PushMsgecondaryform").serialize(),
        success: function(data) {
            // alert(data);
            if (strcmpPushMsg(data._result, "error") == 0) {
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
            else if (strcmpPushMsg(data._result, "success") == 0) {
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}