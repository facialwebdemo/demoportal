

function testconnectionprimary(){
    
    var s = './testConnection?_type='+1+ '&_preference='+1;
  
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#primaryform").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") == 0 ) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmp(data._result,"success") == 0 ) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;                
            }
        }
    }); 
}

function testconnectionsecondary(){
    
    var s = './testConnection?_type='+1+ '&_preference='+1;
  
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#secondaryform").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") == 0 ) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmp(data._result,"success") == 0 ) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;                
            }
        }
    }); 
}
