function strcmpGeotracking(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Geotracking(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}
function SetRoamingSetting(value, _div, disp) {
    if (value == 1) {
        $('#_roamingID').val(value);
        $(_div).html(disp);
    }
    else if (value == 2) {
        $('#_roamingID').val(value);
        $(_div).html(disp);
    }
}
//function SethomecountrySetting(_div, disp) {
//    alert(_div);
//    alert(disp);
//    $('#_homeCountry').val(disp);
//    $(_div).html(disp);
//
//}
function RefreshGeotrackingList() {
    window.location.href = "./geotrackingmain.jsp";
}

//function ChangeRoamnigSetting(value) {
//
//    if (value == 1) {
//        $('#_isroaming').val("1");
//        $('#_status-Roaming').html("Yes");
//    } else {
//        $('#_isroaming').val("0");
//        $('#_status-Roaming').html("No");
//    }
//
//}




function loadRoamingDetails(_userId) {
    var s = './getgeotracking?_userId=' + encodeURIComponent(_userId);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpGeotracking(data._result, "success") == 0) {
                $("#_userID").val(data._userID);
                //$("#_userID1").val(data._userID1);
                $("#_homeCountry").val(data._homeCountry);
                //if ( strcmpGeotracking(data._foreignCountry,"") != 0 ) {
                    $("#_foreignCountry").val(data._foreignCountry);
                    $("#_eroaming").val(data._roamingAllowed);
                    $("#_startdate").val(data._Startdate);
                    $("#_enddate").val(data._Enddate);
                //}
                $("#changehome").modal();                
            } else {
                Alert4Geotracking("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}




function enableRoamingDetails(_userId) {
    var s = './getgeotracking?_userId=' + encodeURIComponent(_userId);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpGeotracking(data._result, "success") == 0) {
                $("#_userID1").val(data._userID);
                //$("#_userID").val(data._userID);
                //$("#_userID1").val(data._userID1);
                $("#_homeCountry").val(data._homeCountry);
                //if ( strcmpGeotracking(data._foreignCountry,"") != 0 ) {
                    $("#_foreignCountry").val(data._foreignCountry);
                    $("#_eroaming").val(data._roamingAllowed);
                    $("#_startdate").val(data._Startdate);
                    $("#_enddate").val(data._Enddate);
                //}
                $("#editroamingsetting").modal();                
            } else {
                Alert4Geotracking("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}



//function loadRDetails(_userId) {
//
//     var s = './getgeotracking?_userId=' + encodeURIComponent(_userId);
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            alert(data._result);
//            if (strcmpGeotracking(data._result, "success") == 0) {
//                alert(data._userID);
//                $('#_userID').html(data._userID);
//                 $('#_userID1').html(data._userID1);
//                            
//                   } else {
//                Alert4Geotracking("<span><font color=red>" + data._message + "</font></span>");
//            }
//        }
//    });
//}

function editgeotracking() {

    var s = './editgeotracking';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#edithomecountryForm").serialize(),
        success: function(data) {
            if (strcmpGeotracking(data._result, "error") == 0) {
                $('#editgeotrack-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Pushmessages(data._message);
            }
            else if (strcmpGeotracking(data._result, "success") == 0) {
                $('#editgeotrack-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonEditGeotrack').attr("disabled", true);
                $("#editroamingsetting").fadeToggle(); 
                window.setTimeout(searchRoaming, 1000);
                
            }
        }
    });
}
function editroamingsetting() {
    var s = './editgeotracking';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editRoamingSettingForm").serialize(),
        success: function(data) {
            if (strcmpGeotracking(data._result, "error") == 0) {
                $('#editroaming-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Pushmessages(data._message);
            }
            else if (strcmpGeotracking(data._result, "success") == 0) {
                $('#editroaming-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonEditRoaming').attr("disabled", true);
                window.setTimeout(searchRoaming, 2000);
                
            }
        }
    });
}

function searchRoaming() {
     
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
     if ( val.length <= 3 ){
        Alert4TrustedDevice("Keyword should be meaningful and/or more than 3 characters");
        return;
    }
   var s = './Geotracking.jsp?_searchtext='+val;
   pleaseWaitDiv.modal();
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
            $('#roaming_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}




function disableroamingsetting(_ruserId, isroaming) {

    var s = './editgeotracking?_ruserId=' + _ruserId + '&_eroaming=' + isroaming;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpGeotracking(data._result, "error") == 0) {
                Alert4Geotracking("<span><font color=red>" + data._message + "</font></span>");

            }
            else if (strcmpChannel(data._result, "success") == 0) {
                Alert4Geotracking("<span><font color=blue>" + data._message + "</font></span>");
                 window.setTimeout(searchRoaming, 2000);
            }
        }
    });
}


function TransactionMap(_userid,duration){
    
    
    var jsonData = $.ajax({
      url: './Geofencing.jsp?_searchtext='+encodeURIComponent(_userid)+'&_duration='+duration,
//        url: "./parkingmaps.jsp",
        dataType:"json",
        async: false
    }).responseText;

    var map = new google.maps.Map(document.getElementById('map_transaction'), {
        zoom:2,
        center: new google.maps.LatLng(3.15837,103.76141),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infoWindow = new google.maps.InfoWindow;

    var onMarkerClick = function() {
        var marker = this;
        var latLng = marker.getPosition();
//        infoWindow.setContent('Transaction Coordinates:' +
//            latLng.lat() + ', ' + latLng.lng());

    infoWindow.setContent('<h1>Hello World!</h1>');
        infoWindow.open(map, marker);
    };

    google.maps.event.addListener(map, 'click', function() {
        infoWindow.close();
    });
//    var infowindow = new google.maps.InfoWindow();
    var myJsonObj = jsonParse(jsonData);
    for (var k in myJsonObj) {
        var marker1 = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(myJsonObj[k]._lat, myJsonObj[k]._lng),
            txType:myJsonObj[k]._txtype
            
            });
//        google.maps.event.addListener(marker1, 'click', onMarkerClick);
//     makeInfoWindowEvent(map, infoWindow,'<h3>'+myJsonObj[k]._txtype+'<h3><h4>'+myJsonObj[k]._city+'</h4>', marker1);
//        alert(myJsonObj[k]._zipcode);
        makeInfoWindowEvent(map, infoWindow,'<p><h3>Action ::'+myJsonObj[k]._txtype+'</h3><h4>Address ::'
                                        +myJsonObj[k]._city+','+myJsonObj[k]._state
                                        +','+myJsonObj[k]._country+'<br>'+myJsonObj[k]._zipcode+'</h4>'
                                         +'<h5>Date ::'+myJsonObj[k]._date+'</h5></p>', marker1);
                                 
                                    function makeInfoWindowEvent(map, infowindow, contentString, marker1) {
    google.maps.event.addListener(marker1, 'click', function() {
    infowindow.setContent(contentString);
    infowindow.open(map, marker1);
  });
}   
    }
    
 
}


function geoTrackReport(_userId,_duration) {
  
    var s = './gettrackingreport?_userId=' + encodeURIComponent(_userId) + "&_duration=" + _duration + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function geoTrackpdf(_userId,_duration) {

    var s = './gettrackingreport?_userId=' + encodeURIComponent(_userId) + "&_duration=" + _duration + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}