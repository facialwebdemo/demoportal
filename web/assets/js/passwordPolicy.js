function strcmpPasswordPolicy(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4PasswordPolicySetting(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
        }
    });
}

function SetPasswordLength(value, _div, disp) {
   
        $('#_passwordLength').val(value);
        $(_div).html(disp);
   
}

function GeneratePassword(value, _div, disp) {
   
        $('#_passwordType').val(value);
        $(_div).html(disp);
   
}

function SetPasswordExpiryTime(value, _div, disp) {
   
        $('#_passwordExpiryTime').val(value);
        $(_div).html(disp);
   
}
function SetPasswordIssueLimit(value, _div, disp) {
   
        $('#_passwordIssueLimit').val(value);
        $(_div).html(disp);
   
}
function SetEnforceDormancyTime(value, _div, disp) {
   
        $('#_enforceDormancyTime').val(value);
        $(_div).html(disp);
   
}
function SetPasswordAttempts(value, _div, disp) {
   
        $('#_passwordInvalidAttempts').val(value);
        $(_div).html(disp);
   
}

function SetOldPasswordReuseTime(value, _div, disp) {
   
        $('#_oldPasswordReuseTime').val(value);
        $(_div).html(disp);
   
}
function SetChangePasswordAfterLogin(value, _div, disp) {
   
        $('#_changePasswordAfterLogin').val(value);
        $(_div).html(disp);
   
}
function SetEpinResetAndReissue(value, _div, disp) {
   
        $('#_epinResetReissue').val(value);
        $(_div).html(disp);
   
}
function SetCRResetReissue(value, _div, disp) {
   
        $('#_crResetReissue').val(value);
        $(_div).html(disp);
   
}
function LoadPasswordPolicySettings() {
    var s = './loadPasswordPolicySettings';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
             $('#_passwordLength').val(data._passwordLength);
            $('#_passwordExpiryTime').val(data._passwordExpiryTime);
            $('#_passwordIssueLimit').val(data._passwordIssueLimit);
            $('#_enforceDormancyTime').val(data._enforceDormancyTime);
            $('#_passwordInvalidAttempts').val(data._passwordInvalidAttempts);
            $('#_changePasswordAfterLogin').val(data._changePasswordAfterLogin);
            $('#_epinResetReissue').val(data._epinResetReissue);
            $('#_crResetReissue').val(data._crResetReissue);
            $('#_oldPasswordReuseTime').val(data._oldPasswordReuseTime);
            
              if(data._passwordType == 1)
                GeneratePassword(1, '#_passwordType_div', 'Numeric');
            else if(data._passwordType == 2)
                GeneratePassword(2, '#_passwordType_div', 'AlphaNumeric');
            else if(data._passwordType == 3)
                GeneratePassword(3, '#_passwordType_div', 'AlphaNumeric With Special Characters');
         
            
            if(data._passwordLength == 8)
                SetPasswordLength(8, '#_passwordLength_div', '8 Characters');
            else if(data._passwordLength == 10)
                SetPasswordLength(10, '#_passwordLength_div', '10 Characters');
            else if(data._passwordLength == 12)
                SetPasswordLength(12, '#_passwordLength_div', '12 Characters');
            else if(data._passwordLength == 14)
                SetPasswordLength(14, '#_passwordLength_div', '14 Characters');
            else if(data._passwordLength == 16)
                SetPasswordLength(16, '#_passwordLength_div', '16 Characters');
            
              if(data._passwordExpiryTime == 30)
                SetPasswordExpiryTime(30, '#_passwordExpiryTime_div', '30 Days');
            else  if(data._passwordExpiryTime == 60)
                SetPasswordExpiryTime(60, '#_passwordExpiryTime_div', '60 Days');
             else  if(data._passwordExpiryTime == 90)
                SetPasswordExpiryTime(90, '#_passwordExpiryTime_div', '90 Days');
             else  if(data._passwordExpiryTime == 120)
                SetPasswordExpiryTime(120, '#_passwordExpiryTime_div', '120 Days');
             else  if(data._passwordExpiryTime == 120)
                SetPasswordExpiryTime(180, '#_passwordExpiryTime_div', '180 Days');
             else  if(data._passwordExpiryTime == 99)
                SetPasswordExpiryTime(99, '#_passwordExpiryTime_div', 'Never');
            
             if(data._passwordIssueLimit == 3)
                SetPasswordIssueLimit(3, '#_passwordIssueLimit_div', '3 Passwords /month');
            else if(data._passwordIssueLimit == 5)
                SetPasswordIssueLimit(5, '#_passwordIssueLimit_div', '5 Passwords /month');
            else if(data._passwordIssueLimit == 7)
                SetPasswordIssueLimit(7, '#_passwordIssueLimit_div', '7 Passwords /month');
            else if(data._passwordIssueLimit == 99)
                SetPasswordIssueLimit(99, '#_passwordIssueLimit_div', 'Disabled');
            
             if(data._enforceDormancyTime == 1)
                SetEnforceDormancyTime(1, '#_enforceDormancyTime_div', '1 Month');
            else if(data._enforceDormancyTime == 2)
                SetEnforceDormancyTime(2, '#_enforceDormancyTime_div', '2 Months');
            else if(data._enforceDormancyTime == 3)
                SetEnforceDormancyTime(3, '#_enforceDormancyTime_div', '3 Months');
            
             if(data._passwordInvalidAttempts == 3)
                SetPasswordAttempts(3, '#_passwordInvalidAttempts_div', '3 Attempts');
            else  if(data._passwordInvalidAttempts == 5)
                SetPasswordAttempts(5, '#_passwordInvalidAttempts_div', '5 Attempts');
            else  if(data._passwordInvalidAttempts == 7)
                SetPasswordAttempts(7, '#_passwordInvalidAttempts_div', '7 Attempts');
            
            else  if(data._passwordInvalidAttempts == 99)
                SetPasswordAttempts(99, '#_passwordInvalidAttempts_div', 'Disabled');
            
             if(data._oldPasswordReuseTime == 3)
                SetOldPasswordReuseTime(3, '#_oldPasswordReuseTime_div', '3 Months');
            else if(data._oldPasswordReuseTime == 6)
                SetOldPasswordReuseTime(6, '#_oldPasswordReuseTime_div', '6 Months');
            else if(data._oldPasswordReuseTime == 12)
                SetOldPasswordReuseTime(12, '#_oldPasswordReuseTime_div', '12 Months');
      
              //alert(data._changePasswordAfterLogin);
              if (data._changePasswordAfterLogin == true)
                SetChangePasswordAfterLogin(1, '#_changePasswordAfterLogin_div', 'Enabled');
            else if (data._changePasswordAfterLogin == false)
                 SetChangePasswordAfterLogin(2, '#_changePasswordAfterLogin_div', 'Disabled');
            
            
             if (data._epinResetReissue == true)
                SetEpinResetAndReissue(1, '#_epinResetReissue_div', 'Enabled');
            else if (data._epinResetReissue == false)
                 SetEpinResetAndReissue(2, '#_epinResetReissue_div', 'Disabled');
             
             if (data._crResetReissue == true)
                SetCRResetReissue(1, '#_crResetReissue_div', 'Enabled');
            else if (data._crResetReissue == false)
                 SetCRResetReissue(2, '#_crResetReissue_div', 'Disabled');
            
        }
    });
}

function editPasswordPolicySettings() {
    var s = './editPassordPolicySettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#passwordPolicysettingsform").serialize(),
        success: function(data) {
            if (strcmpPasswordPolicy(data._result, "error") == 0) {
//                $('#save-password-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4PasswordPolicySetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpPasswordPolicy(data._result, "success") == 0) {
//                $('#save-password-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4PasswordPolicySetting("<span><font color=blue>" + data._message + "</font></span>");
              
            }
        }
    });
}