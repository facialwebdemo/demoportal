function Alert4Operator(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}

function RefreshOperators() {
    window.location.href = "./operators.jsp";
}

function strcmpOpr(a, b)
{
    return (a<b?-1:(a>b?1:0));
}

function OperatorAudits(_oprid,_duration){
    //var durationValue = document.getElementById ('durationValueHW').value;
    //window.location.href = './getopraudits?_oprid='+encodeURIComponent(_oprid) +"&_duration="+_duration + "&_format=pdf";
    //return false;
    var s = './getopraudits?_oprid='+encodeURIComponent(_oprid) +"&_duration="+_duration + "&_format=pdf";
    window.location.href = s;
    return false;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
}




function get_time_zone_offset( ) {
    var current_date = new Date();
    return -current_date.getTimezoneOffset() / 60;
}

function ClearAddOperatorForm(){
    $('#add-new-operator-result').html("<span><font color=blue>" + "" + "</font></span>");
    $("#_oprname").val("");
    $("#_opremail").val("");
    $("#_oprphone").val("");
}

function ClearEditOperatorForm(){
    $('#editoperator-result').html("<span><font color=blue>" + "" + "</font></span>");
    $("#_oprnameE").val("");
    $("#_opremailE").val("");
    $("#_opremailE").val("");
}

function addOperator(){
    $('#addnewOperatorSubmitBut').attr("disabled", true);
    var s = './addoperator';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewOperatorForm").serialize(),
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                $('#add-new-operator-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewOperatorSubmitBut').attr("disabled", false);
                //ClearAddOperatorForm();
                //window.setTimeout(RefreshOperators, 3000);
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                $('#add-new-operator-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                window.setTimeout(RefreshOperators, 3000);
            }
        }
    }); 
}



function loadEditOperatorDetails(_orpid){
    var s = './getoperator?_oprid='+encodeURIComponent(_orpid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpOpr(data._result,"success") == 0 ) {
                $('#idEditOperatorName').html(data._name);
                $("#_oprnameE").val(data._name);
                $("#_opremailE").val(data._email);
                $("#_oprphoneE").val(data._phone);
                $("#_oprroleidE").val(data._roleid);
                $("#_oprstatusE").val(data._status);
                $("#_opridE").val(data._id);
                $('#editoperator-result').html("");
                $("#editOperator").modal();
            } else {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function editoperator(){
    $('#buttonEditOperatorSubmit').attr("disabled", true);
    var s = './editoperator';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editOperatorForm").serialize(),
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                $('#editoperator-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditOperatorSubmit').attr("disabled", false);
            //ClearEditOperatorForm();
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                $('#editoperator-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshOperators, 3000);
            }
        }
    });
}




//function changerole(){
//    var s = './changeoprrole';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#changerole").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}


function ChangeOperatorStatus(oprid,status,uidiv){
    var s = './changeoprstatus?_op_status='+status+ '&_oprid='+encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#'+uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}


function ChangeOperatorRole(_roleid,_rolename,_oprid,uidiv){
    var s = './changeoprrole?_roleId='+_roleid+ '&_oprid='+encodeURIComponent(_oprid) + "&_roleName=" + encodeURIComponent(_rolename) ;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#'+uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}



//function changestatus(){
//    var s = './changeoprstatus';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#licenses_data_table").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}


//function getopraudits(){
//    var s = './getopraudits';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#addOperator").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}
//
//function listoperators(){
//    var s = './listoperators';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#addOperator").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}

//function resendpassword(){
//    var s = './resendoprpassword';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#passwordid").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}
//
//function setpassword(){
//    var s = './setoprpassword';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#passwordid").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}


function OperatorUnlockPassword(oprid,uidiv) {
    var s = './unlockoprpassword?_oprid='+encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#'+uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}
function OperatorResendPassword(oprid) {
    var s = './resendoprpassword?_oprid='+encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");                
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");                
            }
        }
    });

}
function OperatorSetPassword(oprid) {
    var s = './sendrandompassword?_oprid='+encodeURIComponent(oprid) + '&_send=false';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}
function OperatorSendRandomPassword(oprid) {
    var s = './sendrandompassword?_oprid='+encodeURIComponent(oprid) + '&_send=true';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}


function RefreshLogout() {
    window.location.href = "./signout.jsp";    
}


function changeoperatorpassword(){
    var s = './changeoprpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ChangePasswordform").serialize(),
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                $('#Password-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#_oldPassword').val("");
                $('#_newPassword').val("");
                $('#_confirmPassword').val("");
            //ClearAddOperatorForm();
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                $('#Password-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                $('#passwordChange').attr("disabled",true);
                window.setTimeout(RefreshLogout, 5000);
            }
        }
    }); 
}

function DownloadZippedLog(){
    var s = './downloadlogs';
    window.location.href = s;
    return false;
    
}
function DownloadCleanupLog(){
    var s = './downloadCleanuplogs';
    window.location.href = s;
    return false;
    
}






