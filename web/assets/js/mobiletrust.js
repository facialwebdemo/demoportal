function strcmpTrust(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4TrustSetting(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
        }
    });
}
function SetBackupSetting(value, _div, disp) {
    if (value == 1) {
        $('#_Backup').val(value);
        $(_div).html(disp);
    }
    else if (value == 2) {
        $('#_Backup').val(value);
        $(_div).html(disp);
    }
}
function SetExpiryMin(value, _div, disp) {

//    if (value === 1) {
//        $('#_ExpiryMin').val(value);
//        $(_div).html(disp);
//    }
//    else if (value === 2) {
//        $('#_ExpiryMin').val(value);
//        $(_div).html(disp);
//    } else if (value === 3) {
//        $('#_ExpiryMin').val(value);
//        $(_div).html(disp);
//    }

    $('#_ExpiryMin').val(value);
    $(_div).html(disp);
}
function SetSilentSetting(value, _div, disp) {

    if (value == 1) {
        $('#_SilentCall').val(value);
        $(_div).html(disp);
    }
    else if (value == 2) {
        $('#_SilentCall').val(value);
        $(_div).html(disp);
    }
}


function SetSelfDestructAlertSetting(value, _div, disp) {

    if (value == 1) {
        $('#_SelfDestructEnable').val(value);
        $(_div).html(disp);
    }
    else if (value == 2) {
        $('#_SelfDestructEnable').val(value);
        $(_div).html(disp);
    }
}

function SelfDestructAttempts(value, _div, disp) {
    
//    alert(value);
    if (value === 3) {
        $('#_SelfDestructAttempts').val(value);
        $(_div).html(disp);
    }
    else if (value === 5) {
        $('#_SelfDestructAttempts').val(value);
        $(_div).html(disp);
    } else if (value === 7) {
        $('#_SelfDestructAttempts').val(value);
        $(_div).html(disp);
    }
}

function SetCountrySetting(div,disp) {
  //  alert(disp);
    $('#_CountryName1').val(disp);
    $(div).html(disp);

}


function LoadTrustSettings() {
    var s = './loadmobiletrustsetting';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            $('#_CountryName').val(data._CountryName);
            $('#_ExpiryMin').val(data._ExpiryMin);
            $('#_Backup').val(data._Backup);
            $('#_SilentCall').val(data._SilentCall);
            $('#_SelfDestructEnable').val(data._SelfDestructEnable);
            $('#_SelfDestructAttempts').val(data._SelfDestructAttempts);
            $('#_selfDestructURl').val(data._selfDestructURl);
            $('#_LicenseKeyforAndroid').val(data._licenseforAndroid);
            $('#_LicenseKeyforIphone').val(data._licenseforIphone);
              $('#_timeStamp').val(data._timeStamp);
              $('#_deviceTracking').val(data._deviceTracking);
               $('#_geoFencing').val(data._geoFencing);
//             $('#_CountryName').val(data._CountryName);
            //alert(data._CountryName);
            if (data._ExpiryMin == 1)
                SetExpiryMin(1, '#_ExpiryMin_div', '1 minute');
            else if (data._ExpiryMin == 2)
                SetExpiryMin(2, '#_ExpiryMin_div', '2 minutes');
            else if (data._ExpiryMin == 3)
                SetExpiryMin(3, '#_ExpiryMin_div', '3 minutes');
            else if (data._ExpiryMin == 5)
                SetExpiryMin(5, '#_ExpiryMin_div', '5 minutes');
            else if (data._ExpiryMin == 7)
                SetExpiryMin(7, '#_ExpiryMin_div', '7 minutes');
            else if (data._ExpiryMin == 10)
                SetExpiryMin(10, '#_ExpiryMin_div', '10 minutes');

            if (data._Backup == true)
                SetBackupSetting(1, '#_backUp_div', 'Yes');
            else if (data._Backup == false)
                SetBackupSetting(2, '#_backUp_div', 'No');
            
            if (data._SilentCall == true)
                SetSilentSetting(1, '#_silentCall_div', 'Yes');
            else if (data._SilentCall == false)
                SetSilentSetting(2, '#_silentCall_div', 'No');
            
            if (data._SelfDestructEnable == true)
                SetSelfDestructAlertSetting(1, '#_selfDestructAlert_div', 'Yes');
            else if (data._SelfDestructEnable == false)
                SetSelfDestructAlertSetting(2, '#_selfDestructAlert_div', 'No');
            
            
            
              if (data._SelfDestructAttempts == 3)
               SelfDestructAttempts(3, '#_selfDestructAttempts_div', '3 Attempts');
            else if (data._SelfDestructAttempts == 5)
               SelfDestructAttempts(5, '#_selfDestructAttempts_div', '5 Attempts')
            else if (data._SelfDestructAttempts == 7)
               SelfDestructAttempts(7, '#_selfDestructAttempts_div', '7 Attempts');
           
             if (data._timeStamp == true)
                Timestamp(1, '#_timeStamp_div', 'Enable');
            else if (data._timeStamp == false)
                Timestamp(2, '#_timeStamp_div', 'Disable');
            
               if (data._deviceTracking == true)
                DeviceTracking(1, '#_deviceTracking_div', 'Enable');
            else if (data._deviceTracking == false)
                DeviceTracking(2, '#_deviceTracking_div', 'Disable');
            
               if (data._geoFencing == true)
                GeoFencing(1, '#_geoFencing_div', 'Enable');
            else if (data._geoFencing == false)
                GeoFencing(2, '#_geoFencing_div', 'Disable');

             //if(data._CountryName != null)
             //  SetCountrySetting(div ,data._CountryName);

        }
    });
}

function editTrustSettings() {
    var s = './editmobiletrustsetting';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#mobiletrustsettingsform").serialize(),
        success: function(data) {
            if (strcmpTrust(data._result, "error") == 0) {
                $('#save-global-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4TrustSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTrust(data._result, "success") == 0) {
//                $('#save-global-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4TrustSetting("<span><font color=blue>" + data._message + "</font></span>");
                //$('#mobile-trust-license').html("<span><font color=blue>" + data._license + "</font></span>");
                $('#_LicenseKeyforAndroid').val(data._licenseforAndroid);
                $('#_LicenseKeyforIphone').val(data._licenseforIphone);
            }
        }
    });
}

function Timestamp(value, _div, disp) {

    if (value == 1) {
        $('#_timeStamp').val(value);
        $(_div).html(disp);
    }
    else if (value == 2) {
        $('#_timeStamp').val(value);
        $(_div).html(disp);
    }
}
function DeviceTracking(value, _div, disp) {

    if (value == 1) {
        $('#_deviceTracking').val(value);
        $(_div).html(disp);
    }
    else if (value == 2) {
        $('#_deviceTracking').val(value);
        $(_div).html(disp);
    }
}
function GeoFencing(value, _div, disp) {

    if (value == 1) {
        $('#_geoFencing').val(value);
        $(_div).html(disp);
    }
    else if (value == 2) {
        $('#_geoFencing').val(value);
        $(_div).html(disp);
    }
}
