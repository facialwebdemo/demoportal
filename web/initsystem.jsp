<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    Date dFooter = new Date();
    SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
    String strYYYY = sdfFooter.format(dFooter);
    

    int port=request.getServerPort();
    if ( port != 8443 && port != 9443){
        out.println("<h1>Use Secure HTTP. Plain HTTP is not allowed!!!PORT invalid!!!</h1>");
        return;
    }
    
    String scheme = request.getScheme();
    if ( scheme.compareToIgnoreCase("https") != 0 ){
        out.println("<h1>Use Secure HTTP. Plain HTTP is not allowed!!!HTTPS missing!!!</h1>>");
        return;
    }
    
    boolean bSecure = request.isSecure();
    if ( bSecure == false){
        out.println("<h1>Use Secure HTTP. Plain HTTP is not allowed!!!Not Secure!!!</h1>>");
        return;
    }
    

//    ChannelManagement cm = new ChannelManagement();
//    Channels channelsObj = cm.getChannelByName("face");
//    if (channelsObj != null) {
////        TemplateManagement tm = new TemplateManagement();
////        Templates[] templates = tm.ListTypeTemplatesForInitCheck(channelsObj.getChannelid(), 1);
////
////        if (templates != null && templates.length != 0) {
////            String redirectURL = "../face";
////            response.sendRedirect(redirectURL);
////            return;
////        }
//    }
%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Channel Initialization</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin {
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="./assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/ico/favicon.png">
    </head>

    <body>

        <div class="container">

            <div class="hero-unit" align="center">
                <h1>Initializer Step </h1>
                <h2>Operator Creation And Initialization</h2>
                <br>
                <form class="form-signin" method="POST" action="#" id="InitFACEForm" name="InitFACEForm">
                    <p>
                    <div class="text" align="center" id="login-result" ></div>
                    <input type="text" id="_name"  name="_name"  class="input-block-level" placeholder="Set Administrator loginid">
                    <input type="text" id="_email"  name="_email"  class="input-block-level" placeholder="Enter Email id">
                    <input type="text" id="_phone"  name="_phone"  class="input-block-level" placeholder="Enter Phone">                    
                    <button class="btn btn-large btn-primary" onclick="ChannelInit()" type="button" id="INITFACEbutton">Initialize Now >></button>
                    </p>
                </form>
            </div>
        </div> <!-- /container -->

        <footer>
            <div align="center">
                <p>&copy; Molla Technologies 2009-<%=strYYYY%> (www.mollatech.com)</p>
            </div>
        </footer>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/initdb.js"></script>


    </body>
</html>
