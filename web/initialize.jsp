<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%
    Properties p = AxiomProtect.LicenseDetails();
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Initialize Security Credentials</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin {
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="./assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/ico/favicon.png">
    </head>

    <body>
        <div class="container-fluid">
            
            <h1>Initialize Security Credentials</h1>
            <hr>
            <div class="row-fluid">
                <div class="span4">
                    <form class="form-signin" id="formOffice1" name="formOffice1">
                        <h2 class="form-signin-heading">Officer 1</h2>
                        <input type="hidden" id="type" name="type" value="1">
                        <input type="password" id="officer1password" name="officer1password" class="input-block-level" placeholder="Password">
                        <input type="password" id="officer1passwordC" name="officer1passwordC" class="input-block-level" placeholder="Confirm Password">
                        <button id="officer1button" class="btn btn-primary" type="button" onclick="SaveOfficerCredential(1)">Save Now</button>
                    </form>
                </div>
                <div class="span4">
                    <form class="form-signin" id="formOffice2" name="formOffice2">
                        <h2 class="form-signin-heading">Officer 2</h2>
                        <input type="hidden" id="type" name="type" value="2">
                        <input type="password" id="officer2password" name="officer2password" class="input-block-level" placeholder="Password">
                        <input type="password" id="officer2passwordC" name="officer2passwordC" class="input-block-level" placeholder="Confirm Password">
                        <button id="officer2button" class="btn  btn-primary" type="button" onclick="SaveOfficerCredential(2)">Save Now</button>
                    </form>
                </div>
                <div class="span4">
                    <form class="form-signin" id="formOffice3" name="formOffice3">
                        <h2 class="form-signin-heading">Officer 3</h2>
                        <input type="hidden" id="type" name="type" value="3">
                        <input type="password" id="officer3password" name="officer3password" class="input-block-level" placeholder="Password">
                        <input type="password" id="officer3passwordC" name="officer3passwordC" class="input-block-level" placeholder="Confirm Password">
                        <button id="officer3button" class="btn  btn-primary" type="button" onclick="SaveOfficerCredential(3)">Save Now</button>
                    </form>
                </div>
            </div>
            <p class="lead">Click below button to generate security credential from above and bind it to this device.
                <br>   
            <form class="form-horizontal" id="formConfirm" name="formConfirm">
                <input type="hidden" id="type" name="type" value="0">
                <button id="GenerateSecurityCredentialsButton" class="btn btn-large btn-success" type="button" onclick="SaveOfficerCredential(0)">Initialize Security Credentials</button>                
            </form>
        </div>
        <p>

        </p>




        <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/initialize.js"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/ico/favicon.png">
        <script src="./assets/js/bootbox.min.js"></script>
        
        
        <script>
            StartState();
            </script>
            


    </body>
</html>


