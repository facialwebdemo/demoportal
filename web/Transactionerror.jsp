<%@include file="header.jsp" %>
<script src="./assets/js/login.js"></script>

<div class="container-fluid">
    <h1 class="text-success">Transaction Successful...!!! </h1>

    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id ="transactionauth" name="transactionauth">
            <fieldset> 

                <div class="control-group"> 
                     <div class="controls"> 
                        <h2>You are transfering amount from the specified account :1224569707 to the destination account : 345219707  and the amount you specified for transaction is :1000000.00.</h2>
                    </div> 
                </div>
                <div class="control-group"> 
                     <div class="controls"> 
                        <h2>The Hashcode you entered:ASSLDI12461123141213.Now please Enter your SOTP Below..</h2>
                    </div>
                </div> 

                <div class="control-group"> 
                    <div class="controls"> 
                        <input type="text" id="_OTP" name="_OTP" placeholder="123456" class="span3">
                    <button class="btn-info btn-large" onclick="testconnectionSMSsecondary()" id="testSMSSecondaryBut">OOB </button>
                    </div>
                </div> 


                <div class="control-group">
                    <div class="controls">
                        <div id="savecrtsettings-result"></div>
                        <button class="btn btn-primary" onclick="transauthentication()" type="button" id="savecrtsettings">Confirm Details Now >></button>                        
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

</div>

<%@include file="footer.jsp" %>