/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package axiom.web.service;

import com.mollatech.axiom.bridge.crypto.LoadSettings;

import com.mollatech.axiom.push.AxiomPush;
import com.mollatech.axiom.push.AxiomPushService;
import com.mollatech.axiom.push.PushNotificationStatus;

import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.namespace.QName;

/**
 *
 * @author mayuri
 */
public class AxiomPushWrapper {

    private AxiomPushService m_service = null;
    private AxiomPush m_port = null;
    private String m_channelid = null;

    //nilesh
    public static final int GET_USER_BY_USERID = 1;
    public static final int GET_USER_BY_PHONENUMBER = 2;
    public static final int GET_USER_BY_EMAILID = 3;
    public static final int GET_USER_BY_FULLNAME = 4;
    public static final int PKI_SOFTWARE_TOKEN = 8;

    public static final int PASSWORD = 1;
    public static final int SOFTWARE_TOKEN = 2;
    public static final int HARDWARE_TOKEN = 3;
    public static final int OUTOFBOUND_TOKEN = 4;
    public static final int CERTIFICATE = 5;
    public static final int CHALLAEGE_RESPONSE = 6;
    public static final int BIOMETRIX = 7;
    public static final int SECURE_PHRASE = 10;
    public static final int OTP_TOKEN_OUTOFBAND_SMS = 1;
    public static final int OTP_TOKEN_OUTOFBAND_VOICE = 3;
    public static final int OTP_TOKEN_OUTOFBAND_USSD = 2;
    public static final int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    public static final int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    public static final int OTP_TOKEN_SOFTWARE_PC = 3;
    public static final int OTP_TOKEN_SOFTWARE_WEB = 1;
    public static final int OTP_TOKEN_HARDWARE_MINI = 1;
    public static final int OTP_TOKEN_HARDWARE_CR = 2;

    public static final int TOKEN_STATUS_ACTIVE = 1;
    public static final int TOKEN_STATUS_LOCKEd = -1;
    public static final int TOKEN_STATUS_ASSIGNED = 0;
    public static final int TOKEN_STATUS_UNASSIGNED = -10;
    public static final int TOKEN_STATUS_SUSPENDED = -2;
    public static final int TOKEN_STATUS_ALL = 2;
    public static final int TOKEN_STATUS_LOST = -5;
    public static final int TOKEN_STATUS_FREE = -10;

    //subcategory of OOB
    public static final int OOB__SMS_TOKEN = 1;
    public static final int OOB__VOICE_TOKEN = 2;
    public static final int OOB__USSD_TOKEN = 3;
    public static final int OOB__EMAIL_TOKEN = 4;
    //subcategory of SOFTWARE
    public static final int SW_WEB_TOKEN = 1;
    public static final int SW_MOBILE_TOKEN = 2;
    public static final int SW_PC_TOKEN = 3;

    public static int OTP = 2;
    public static int SignatureOTP = 3;
    public static int ChallengeAndResponse = 4;
    public static int Password = 1;
    public static int BioMetrix = 5;

    public static final int REGISTER = 1;
    public static final int LOGIN = 2;
    public static final int TRANSACTION = 3;
    public static final int CHANGEINPROFILE = 4;

    public static final int BLUE = 1;
    public static final int GREEN = 2;
    public static final int RED = 3;
    public static final int GRAY = 4;
    public static final int ORANGE = 5;
    public static final int YELLOW = 6;
//    public static final int PINK = 7;
    public static final int MAGENTA = 7;

    private String m_sessionid = null;

    public AxiomPushWrapper() {
        try {

            String wsdlname = LoadSettings.g_sSettings.getProperty("axiom.wsdl.name");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
            m_channelid = channelid;
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String ipAddress = LoadSettings.g_sSettings.getProperty("axiom.ipaddress");
            String strPort = LoadSettings.g_sSettings.getProperty("axiom.port");
            String strSecured = LoadSettings.g_sSettings.getProperty("axiom.secured");

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    Date d = new Date();
                    System.out.println(d + ">>" + "channelid::" + channelid);
                    System.out.println(d + ">>" + "remotelogin::" + remotelogin);
                    System.out.println(d + ">>" + "password::" + password);
                    System.out.println(d + ">>" + "ipAddress::" + ipAddress);
                    System.out.println(d + ">>" + "strPort::" + strPort);
                    System.out.println(d + ">>" + "strSecured::" + strSecured);
                }
            } catch (Exception ex) {
            }

            SSLContext sslContext = null;
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
                try {
                    sslContext = SSLContext.getInstance("TLS");
                } catch (NoSuchAlgorithmException ex) {
                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            } catch (KeyManagementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            RssUserCerdentials userObj = null;
            java.lang.String sessionid = null;
            try { // Call Web Service Operation

                String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + "AxiomPushNotificationService" + "/AxiomPush?wsdl";
                if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
                    wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + "AxiomPushNotificationService" + "/AxiomPush?wsdl";
                }

                URL url = new URL(wsdlUrl);
                QName qName = new QName("http://push.bluebricks.com/", "AxiomPushService");
                //m_service = new MobileTrustInterfaceImplService(url, qName);
                //m_port = m_service.getMobileTrustInterfaceImplPort();
                m_service = new AxiomPushService(url, qName);
                m_port = m_service.getAxiomPushPort();
//                byte[] SHA1hash = UtilityFunctions.SHA1(channelid + remotelogin + password);
//                integritycheck = new String(Base64.encode(SHA1hash));
//                m_sessionid = m_port.openSession(channelid, remotelogin, password, integritycheck);

                //System.out.println("Result = " + sessionid);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

//    public AxiomWrapper(int webService) {
//        try {
//            String wsdlname = LoadSettings.g_sSettings.getProperty("axiom.wsdl.name");
//            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
//            m_channelid = channelid;
//            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
//            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
//            String ipAddress = LoadSettings.g_sSettings.getProperty("axiom.ipaddress");
//            String strPort = LoadSettings.g_sSettings.getProperty("axiom.port");
//            String strSecured = LoadSettings.g_sSettings.getProperty("axiom.secured");
//
//            String strDebug = null;
//            try {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    Date d = new Date();
//                    System.out.println(d + ">>" + "channelid::" + channelid);
//                    System.out.println(d + ">>" + "remotelogin::" + remotelogin);
//                    System.out.println(d + ">>" + "password::" + password);
//                    System.out.println(d + ">>" + "ipAddress::" + ipAddress);
//                    System.out.println(d + ">>" + "strPort::" + strPort);
//                    System.out.println(d + ">>" + "strSecured::" + strSecured);
//                }
//            } catch (Exception ex) {
//            }
//
//            SSLContext sslContext = null;
//            try {
//                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
//                try {
//                    sslContext = SSLContext.getInstance("TLS");
//                } catch (NoSuchAlgorithmException ex) {
//                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
//                    ex.printStackTrace();
//                }
//                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
//                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
//
//            } catch (KeyManagementException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
////            RssUserCerdentials userObj = null;
//            java.lang.String sessionid = null;
//            try { // Call Web Service Operation
//
//                String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomCoreInterfaceImpl?wsdl";
//                if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
//                    wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomCoreInterfaceImpl?wsdl";
//                }
//
//                URL url = new URL(wsdlUrl);
//                QName qName = new QName("http://core.v2.axiom.mollatech.com/", "AxiomCoreInterfaceImplService");
//                //m_service = new MobileTrustInterfaceImplService(url, qName);
//                //m_port = m_service.getMobileTrustInterfaceImplPort();
//                m_Aservice = new AxiomCoreInterfaceImplService(url, qName);
//                m_Aport = m_Aservice.getAxiomCoreInterfaceImplPort();
//                m_sessionid = m_Aport.openSession(channelid, remotelogin, password);
//                //System.out.println("Result = " + sessionid);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
    public String OpenSession(String channelId, String username, String password) {
        try {
            if (channelId != null) {
//                byte[] SHA1hash = UtilityFunctions.SHA1(channelId + username + password);
//                String integritycheck = new String(Base64.encode(SHA1hash));
                String sessionID = m_port.openSession(channelId, username, password);
                return sessionID;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public PushNotificationStatus UploadDeviceTokens(String sessionId, String userid,String groupid, String deviceToken,int devicetype) {
        try {
            if (sessionId != null) {
//             


PushNotificationStatus sessionID = m_port.uploadDeviceTokens(sessionId, userid, groupid, deviceToken, devicetype);
                return sessionID;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public PushNotificationStatus GetDeviceTokens(String sessionId,String groupid,int devicetype) {
        try {
            if (sessionId != null) {
             

                PushNotificationStatus astatus = m_port.getDeviceTokens(sessionId, groupid, devicetype);
                        return astatus;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public PushNotificationStatus CloseSession(String sessionId) {
        try {
            if (sessionId != null) {
             
                PushNotificationStatus status = m_port.closeSession(sessionId);
                return status;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
      public PushNotificationStatus sendPushMessageByGroup(String sessionId,   String groupid,  String type,String message) {
        PushNotificationStatus astatus = null;
        try {
         
            astatus = m_port.sendPushMessageByGroup(sessionId, groupid, type, message);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return astatus;

    }
      
        public PushNotificationStatus SendPushMessageByUserid(String sessionId,   String userid,  String type,String message) {
        PushNotificationStatus astatus = null;
        try {
         
            astatus = m_port.sendPushMessageByUserid(sessionId, userid, type, message);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return astatus;

    }


//      public AxiomStatus ChangeTokenStatus(String sessionId,RssUserCerdentials credentials) {
//        try {
//            if (sessionId != null) {
//                byte[] SHA1hash = UtilityFunctions.SHA1(sessionId);
//                integritycheck = new String(Base64.encode(SHA1hash));
//                AxiomStatus status = m_port.
//                return status;
//            } else {
//                return null;
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
    public PushNotificationStatus sendPushToDeviceTokens(String sessionId,  List<String> deviceTokens, int devicetype,String message) {
        PushNotificationStatus astatus = null;
        try {
         
            astatus = m_port.sendPushToDeviceTokens(sessionId, deviceTokens, devicetype, message);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return astatus;

    }

   
    
    
}

