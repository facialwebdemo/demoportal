/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.push;

import axiom.web.service.AxiomPushWrapper;
import com.mollatech.axiom.nucleus.db.Channels;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */

public class savePushWatchDetails extends HttpServlet {
 static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(savePushWatchDetails.class.getName());
 private final static int DEVICE_TYPE_FOR_WEB=33;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
         HttpSession session = request.getSession();
        response.setContentType("application/json");
//        String userid = request.getParameter("userid");
           String userid = (String) session.getAttribute("_userID");
           String sessionId= (String) session.getAttribute("_userSessinId");
        
        
           
        log.debug("userid :: "+userid);
        String groupid = "21";
        log.debug("groupid :: "+groupid);
        String deviceToken = request.getParameter("deviceToken");
        log.debug("deviceToken :: "+deviceToken);

         
//
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String result = null;
        String message = null;
         PushNotificationStatus retValue = null;
 
            if (deviceToken == null){
                           
                 result = "error";
                message = "Fill all Details!!";
                 try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }else{
                  if(deviceToken.isEmpty() == true) {
                    
                result = "error";
                message = "Fill all Details!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
                    }
            }
     
              

          AxiomPushWrapper obj=new AxiomPushWrapper();
        
          retValue = obj.UploadDeviceTokens(sessionId, userid, groupid, deviceToken, DEVICE_TYPE_FOR_WEB);
        String resultString = "Failure";
        if (retValue.errocode == 0) {
            resultString = "Success";
         result = "Success";
         message = "Web Token Details added successfuly..!!";
        }
  

       else {
            result = "error";
            message = "failed to add Web Token Details!!";

        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
