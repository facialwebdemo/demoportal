/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.push;

import axiom.web.service.AxiomWrapper;
import axiom.web.service.AxiomPushWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import static com.mollatech.axiom.push.savePushWatchDetails.log;

import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.InitTransactionPackage;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.TransactionStatus;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
import com.mollatech.web.token.SetAuthType;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class verifyOtp extends HttpServlet {

    public static final String TYPE_WEB_PUSH = "33";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, InterruptedException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json");
        AxiomWrapper axwrap = new AxiomWrapper();
        AxiomPushWrapper apwrap = new AxiomPushWrapper();

        String OTPTOVerify = null;
        String host = request.getContextPath().toLowerCase();

        OTPTOVerify = (String) request.getParameter("txtotp");    //cm.getString("_slientotp");

        String ip = request.getRemoteAddr();
        String beneficiaryBankConfirm = null;
        String beneficiaryNameConfirm = null;
        String amountConfirm = null;
        String paymentTypeConfirm = null;
        String fromAccountNoConfirm = null;
        String username = null;
        String sessionId = null;
        String _userID = null;
        if (request.getSession().getAttribute("_userSessinId") != null) {
            sessionId = request.getSession().getAttribute("_userSessinId").toString();
            request.getServletContext().setAttribute("sessionId", sessionId);

            _userID = request.getSession().getAttribute("_userID").toString();
            username = (String) request.getSession().getAttribute("_username");
            fromAccountNoConfirm = (String) request.getSession().getAttribute("fromAccountNo");
            beneficiaryBankConfirm = (String) request.getSession().getAttribute("beneficiaryBank");
            beneficiaryNameConfirm = (String) request.getSession().getAttribute("beneficiaryName");
            String otherAccountConfirm = (String) request.getSession().getAttribute("otherAccount");
            String emailConfirm = (String) request.getSession().getAttribute("email");
            amountConfirm = (String) request.getSession().getAttribute("amount");
            paymentTypeConfirm = (String) request.getSession().getAttribute("paymentType");
//                    String remarksConfirm = (String) request.getSession().getAttribute("remarks");
//                    String paymentDescriptionConfirm = (String) request.getSession().getAttribute("paymentDescription");
//                    String firstProcessDateConfirm = (String) request.getSession().getAttribute("firstProcessDate");
        }

        VerifyRequest vReq = new VerifyRequest();

        vReq.setCategory(2);
        vReq.setSubcategory(1);
        vReq.setCredential(OTPTOVerify);
        AxiomStatus astatus = axwrap.verifyCredential(sessionId, _userID, vReq, null);
//added for Init Transaction

//AxiomWrapper axWraper = new AxiomWrapper();
        
//            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
//           
//            Random randomGenerator = new Random();
//            int randomInt = randomGenerator.nextInt(10000000);
//                byte[] bytePayload = Base64.encode("SamplePayload".getBytes());
//            String strPayLoad = new String(bytePayload);
//           // RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, username, AxiomWrapper.GET_USER_BY_USERID, strPayLoad);
//            InitTransactionPackage package1 = new InitTransactionPackage();
//            byte[] authId = Base64.encode(("" + randomInt).getBytes());
//            String strauthId = new String(authId);
//            request.getSession().setAttribute("txid", strauthId);
//            request.getSession().setAttribute("_userID", _userID);
//            request.getServletContext().setAttribute("txid", strauthId);
//            request.getServletContext().setAttribute("_userID", _userID);
//   String txId = (String) request.getServletContext().getAttribute("txid");
//                      String strPayLoad = (String) request.getServletContext().getAttribute("strPayLoad");

//            package1.setAuthid(txId);
//            package1.setChannelid(channelid);
//            package1.setExpirytimeInMins(3);
//            package1.setFriendlyMsg("Login");
//              SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//        SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm:ss a");
////            package1.setMessage("you are trying to login to system from IP: " + request.getRemoteAddr() + " with Location: " + location + " browser Name: " + browser + " ,OS: " + baseOs + " At Time: " + new Date());
//            JSONObject json1 = new JSONObject();
//            json1.put("_servermsg", "Login for Trasaction from the Server : " + request.getRemoteAddr());
//            json1.put("_ip", request.getRemoteAddr());
//            json1.put("_location", "NA");
//            json1.put("_browser", "NA");
//            json1.put("_os", "NA");
//            String currentDate = dateformat.format(new Date());
//            String currentTime = timeformat.format(new Date());
//            json1.put("_time", "" + currentTime);
//            json1.put("_date", "" + currentDate);
//       
//            package1.setMessage(json1.toString());
//            package1.setSessionid(sessionId);
//            package1.setType(1);
//           // package1.setUserid(userObj.getRssUser().getUserId());
//            package1.setUserid(_userID);
//            AxiomStatus status = axWraper.initTransaction(package1, strPayLoad);
//          System.out.println("Intitiolization Transaction Status Regcode message" +status.getRegCodeMessage());
//      TransactionStatus Tstatus = axWraper.getStatus(sessionId, txId, strPayLoad);
//            int time = 0;
//            while (Tstatus.getTransactionStatus().equalsIgnoreCase("pending") && time < 180) {
//                Thread.sleep(5000);
//                time += 5;
//                Tstatus = axWraper.getStatus(sessionId, txId, strPayLoad);
//            }
//end of addition
        JSONObject json = new JSONObject();
        String result = null;
        String message = null;
        String resultString = "Failure";
        if (astatus.getErrorcode() == 0) {
            apwrap.SendPushMessageByUserid(sessionId, _userID, TYPE_WEB_PUSH, "Dear " + username + ", You have Successfully transfered Amount Of Rs. " + amountConfirm + " From Account Number " + fromAccountNoConfirm + ".  Do you approve this transaction?");
            response.sendRedirect("../2fa.jsp");

           request.getSession().setAttribute("pushmessage", "Dear " + username + ", You have Successfully transfered Amount Of Rs. " + amountConfirm + " From Account Number " + fromAccountNoConfirm + ".Do You approve these transaction?");
            resultString = "Confirmation successfull";
            result = "Success";
            message = "OTP verification successfull!!";

        } else {
            apwrap.SendPushMessageByUserid(sessionId, _userID, TYPE_WEB_PUSH, "Dear " + username + ", Your Transaction to transfer Amount Of Rs. " + amountConfirm + " From Account Number " + fromAccountNoConfirm + "is failed.");
            response.sendRedirect("../register_inlined.jsp");
            request.getSession().setAttribute("pushmessage", "Dear " + username + ", Your Transaction to transfer Amount Of Rs. " + amountConfirm + " From Account Number " + fromAccountNoConfirm + "is failed.Do You approve these transaction?");

            resultString = "Confirmation failed!!";
            result = "Failed";
            message = "OTP verification Failed!!";

        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ", e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (InterruptedException ex) {
            Logger.getLogger(verifyOtp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (InterruptedException ex) {
            Logger.getLogger(verifyOtp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
