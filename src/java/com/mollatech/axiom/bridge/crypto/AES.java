package com.mollatech.axiom.bridge.crypto;

public class AES extends HWSignature {
    
    private static String AES_ALGO="AES/CBC/NoPadding";    

//    public byte[] hex2Byte(String str) {
//        byte[] bytes = new byte[str.length() / 2];
//        for (int i = 0; i < bytes.length; i++) {
//            bytes[i] = (byte) Integer
//                    .parseInt(str.substring(2 * i, 2 * i + 2), 16);
//        }
//        return bytes;
//    }

//    public String asHex(byte buf[]) {
//        StringBuffer strbuf = new StringBuffer(buf.length * 2);
//        int i;
//
//        for (i = 0; i < buf.length; i++) {
//            if (((int) buf[i] & 0xff) < 0x10) {
//                strbuf.append("0");
//            }
//
//            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
//        }
//
//        return strbuf.toString();
//    }

    public byte[] hexToBytes(char[] hex) {
        int length = hex.length / 2;
        byte[] raw = new byte[length];
        for (int i = 0; i < length; i++) {
            int high = Character.digit(hex[i * 2], 16);
            int low = Character.digit(hex[i * 2 + 1], 16);
            int value = (high << 4) | low;
            if (value > 127) {
                value -= 256;
            }
            raw[i] = (byte) value;
        }
        return raw;
    }

    public String PINDecrypt1(String strEncryptedData) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(HWSignature.strHDDSignature.getBytes(), 128);
            byte [] encDataBytes = hex2Byte(strEncryptedData);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
            //return strData;
            return new String(byteEncryptedData);
            
            
            //return strEncryptedData;

//            byte[] raw = HWSignature.strHDDSignature.getBytes();
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//            byte[] original = cipher.doFinal(hexBytes);
//            String originalString = new String(original);
//            return originalString;
        } catch (Exception e) {
            return null;
        }
    }

    public String PINEncrypt1(String strData) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(HWSignature.strHDDSignature.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData.getBytes());
            return asHex(byteEncryptedData);
            //return strData;
            
//            byte[] raw = HWSignature.strHDDSignature.getBytes();
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
//            byte[] encrypted = cipher.doFinal(strData.getBytes());
//            String strEncryptedData = asHex(encrypted);
//            return strEncryptedData;
        } catch (Exception e) {
            return null;
        }
    }

    public String PINEncrypt1(String strData, String password) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData.getBytes());
            return asHex(byteEncryptedData);
            //return strData;
            
            
            //Cipher cipher = Cipher.getInstance(AES_ALGO);

//            //byte[] raw = HWSignature.strHDDSignature.getBytes();
//             //String key = "1234567812345678";
//            String iv = "1234567812345678";
//            
//            
//            byte[] raw = password.getBytes();
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            int blockSize = cipher.getBlockSize();
//            byte[] dataBytes = strData.getBytes();
//            int plaintextLength = dataBytes.length;
//            if (plaintextLength % blockSize != 0) {
//                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
//            }
//            byte[] plaintext = new byte[plaintextLength];
//            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
//
//            //SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
//            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
//            
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec,ivspec);
//            
//            byte[] encrypted = cipher.doFinal(plaintext);
            
//            byte[] encrypted = cipher.doFinal(strData.getBytes());
//            String strEncryptedData = asHex(encrypted);
//            return strEncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String PINDecrypt1(String strEncryptedData, String password) {
        try {            
            //return strEncryptedData;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte [] encDataBytes = hex2Byte(strEncryptedData);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
            //return strData;
            return new String(byteEncryptedData);
            //return strEncryptedData;
            
            
            
//            byte[] hexBytes = hexToBytes(strEncryptedData.toCharArray());
//
//            //byte[] raw = HWSignature.strHDDSignature.getBytes();
//            byte[] raw = password.getBytes();
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//            byte[] original = cipher.doFinal(hexBytes);
//            String originalString = new String(original);
//            return originalString;
        } catch (Exception e) {
            return null;
        }
    }
    
    public String PINEncryptBytes(byte[] strData, String password) {
        try {            
            //return strData;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData);
            return asHex(byteEncryptedData);
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
        
    }
    
    public String PINEncrypt(String strData, String password) {
        try {            
            //return strData;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData.getBytes());
            return asHex(byteEncryptedData);
            
//            int iKeyLength=128;
//            Crypto _c = new Crypto();
//            Crypto.AES aes = _c.new AES();
//            int keySizeInBits = iKeyLength; //256 / 128
//            int keySizeInBytes = keySizeInBits / 8;
//
//            //String salt = Crypto.Utils.byteArrayToHexString(Crypto.Utils.getRandomBytes(8));
//            //String password = "1234567890";
//            String salt = Crypto.Utils.byteArrayToHexString(password.substring(0, 8).getBytes());
//            String key = Crypto.Utils.pbkdf2(password, salt, 1000, keySizeInBytes);
//
//            // generate IV
//            //byte[] ivBytes = aes.generateIV();
//            aes.setIV(password.substring(0, 8).getBytes());
//            
//            /*** encrypt */
//            String ciphertext = aes.encrypt(strData, key);
//            //System.out.println(ciphertext);
//            //System.out.println(key);
//            return new String(Base64.decode(ciphertext));

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public byte[]  PINDecryptBytes(String strEncryptedData, String password) {
        try {            
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte [] encDataBytes = hex2Byte(strEncryptedData);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
            //return strData;
            return byteEncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String PINDecrypt(String strEncryptedData, String password) {
        try {
            
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte [] encDataBytes = hex2Byte(strEncryptedData);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
            //return strData;
            return new String(byteEncryptedData);
            
            //return strEncryptedData;
//            int iKeyLength=128;
//
//            int keySizeInBits = iKeyLength; //256 / 128
//            int keySizeInBytes = keySizeInBits / 8;
//            Crypto _c = new Crypto();
//            Crypto.AES aes1 = _c.new AES();
//            String salt = Crypto.Utils.byteArrayToHexString(password.substring(0, 8).getBytes());
//            aes1.setIV(password.substring(0, 8).getBytes());
//            String key1 = Crypto.Utils.pbkdf2(password, salt, 1000, keySizeInBytes);                        
//            String plaintext = aes1.decrypt(new String(Base64.encode(strEncryptedData.getBytes())), key1);
//            //System.out.println(plaintext);
//            return plaintext;

        } catch (Exception e) {
            return null;
        }
    }
    
//    public static void main(String[] args)  {
//        
//        
//        
//                
//        //AES aesObj = new AES();
//        //String hexData = aesObj.PINEncrypt("rainbow5",strHDDSignature);
//        //System.out.println(hexData);
//        //String mydata = aesObj.PINDecrypt("ed51098cb7e71b4f2796ec1c3c402c58",getSignature());
//        //System.out.println(mydata);   
//        
//        //String mydata = new String("这是一条测试消息");
//        //System.out.println(mydata);   
//        
//    }
}



