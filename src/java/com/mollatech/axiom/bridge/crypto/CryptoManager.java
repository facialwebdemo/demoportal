/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.bridge.crypto;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.KeySpec;
import java.util.Date;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author vikramsareen
 */
public class CryptoManager {

//    public byte[] encryptRSATempECB(byte[] plainData, PublicKey pubKey) {
//        try {
//            byte[] encryptionByte = null;
//            //Cipher cipher = Cipher.getInstance("RSA");
//            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
//            //encryptionByte = cipher.doFinal(plainData);
//            byte[] someData = cipher.update(plainData);
//            byte[] moreData = cipher.doFinal();
//            byte[] encrypted = new byte[someData.length + moreData.length];
//            System.arraycopy(someData, 0, encrypted, 0, someData.length);
//            System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);
//
//            encryptionByte = encrypted;
//            return encryptionByte;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    private byte[] encryptRSATempNONE(byte[] plainData, PublicKey pubKey) {
//        try {
//            byte[] encryptionByte = null;
//            Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
//            encryptionByte = cipher.doFinal(plainData);
//            return encryptionByte;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    private byte[] decryptRSATempECB(byte[] encrptdByte, PrivateKey privateKey) {
//        try {
//            byte[] encryptionByte = null;
//            //Cipher cipher = Cipher.getInstance("RSA");
//            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//            cipher.init(Cipher.DECRYPT_MODE, privateKey);
//            //encryptionByte = cipher.doFinal(encrptdByte);
//            //System.out.println("Recovered String     :::  " + new String(encryptionByte));
//            byte[] someDecrypted = cipher.update(encrptdByte);
//            byte[] moreDecrypted = cipher.doFinal();
//            byte[] decrypted = new byte[someDecrypted.length + moreDecrypted.length];
//            System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
//            System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);
//
//            encryptionByte = decrypted;
//
//            return encryptionByte;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    private byte[] decryptRSATempNONE(byte[] encrptdByte, PrivateKey privateKey) {
//        try {
//            byte[] encryptionByte = null;
//            Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");
//            cipher.init(Cipher.DECRYPT_MODE, privateKey);
//            encryptionByte = cipher.doFinal(encrptdByte);
//            //System.out.println("Recovered String     :::  " + new String(encryptionByte));
//            return encryptionByte;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }

    private static String AES_ALGO = "AES/CBC/PKCS5Padding";
    //private static String AES_ALGO = "AES/CBC";

    //private static String AES_ALGO = "AES";
    final private static byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    public static final int RSA = 1;
    public static final int SHA1WithRSA = 2;
    public static final int ANDROID = 1;
    public static final int IOS = 2;
//     static {
//     System.out.println("before adding BC");
//     Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
//     System.out.println("after adding BC");
// }

    private static byte[] v = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    static {
        Provider p = Security.getProvider("BC");
        if (p == null) {
            Security.addProvider(new BouncyCastleProvider()); // add it
        }
    }

    public static void main(String args[]) throws Exception {

//        CryptoManager cm = new CryptoManager();
//        AxiomRSAKeyPair akeypair = cm.generateAxiomRSAKeyPair(512);
//        PublicKey pubkey = akeypair.visiblekey;
//        PrivateKey prvkey = akeypair.privatekey;
//
//        //PublicKey pubkey = cm.getPublicKey("MIGJAoGBAMtuarPi14Z3YghXUDrB+c3qA83vaTxxKkzgBFYxYQXlqAylmZs5vylgSz9TZiiHTJHrN2BhDNE16Vu9HqVZa5kh3Cc/L7A434mJzzbXmYaPashZvptBfTXOw5/Kl+0Q/pS6YCQpqn0Fb+vDF1zjMtbk4b3BOBdfyd7t0xt6BJvLAgMBAAE=");
//        String data = "manoj";
//        byte[] encryptedDataUsingECB = cm.encryptRSATempECB(data.getBytes(), pubkey);
//        byte[] encryptedDataUsingNONE = cm.encryptRSATempNONE(data.getBytes(), pubkey);
//
//        System.out.println("Using ECB::" + new String(Base64.encode(encryptedDataUsingECB)));
//        System.out.println("Using NONE::" + new String(Base64.encode(encryptedDataUsingNONE)));
//
//        byte[] decryptedDataUsingECB = cm.decryptRSATempECB(encryptedDataUsingECB, prvkey);
//        byte[] decryptedDataUsingNONE = cm.decryptRSATempNONE(encryptedDataUsingNONE, prvkey);
//
//        System.out.println("Using ECB (decrypted)::" + new String(decryptedDataUsingECB));
//        System.out.println("Using NONE (decrypted)::" + new String(decryptedDataUsingNONE));
//
//        decryptedDataUsingNONE = null;
//        decryptedDataUsingECB = null;
//
//        decryptedDataUsingNONE = cm.decryptRSATempECB(encryptedDataUsingECB, prvkey);
//        decryptedDataUsingECB = cm.decryptRSATempNONE(encryptedDataUsingNONE, prvkey);
//
//        System.out.println("Using ECB (decrypted reversed)::" + new String(decryptedDataUsingECB));
//        System.out.println("Using NONE (decrypted reversed)::" + new String(decryptedDataUsingNONE));
        CryptoManager cmObj = new CryptoManager();
        byte[] b = cmObj.generateKeyAESForAndroid("test password".toCharArray(), 128);
        System.out.println("encryption key 1 is ::" + new String(Base64.encode(b)));

        b = cmObj.generateKeyAESForAndroid("test password".toCharArray(), 128);
        System.out.println("encrypted key 2 is ::" + new String(Base64.encode(b)));

//        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
//        
//        System.out.println("Secure Random without Crypto" + sr.generateSeed(10));
//        
//        SecureRandom sr1 =  SecureRandom.getInstance("SHA1PRNG", "Crypto");
//        
//        System.out.println("Secure Random with Crypto" + sr1.generateSeed(10));
//        String otptokenSecret = "BaEf9F1s8FGB5okFFjcdZsOQjdzPYqlls6geCGNWu0HXBhJkNlOeaG0tshl1oHDI";
//        CryptoManager c = new CryptoManager();
//        byte[] secByte = Base64.decode(otptokenSecret);
//        String filepath = "/Users/vikramsareen/Desktop/all/acleda/axiom protect/FPT/IdeaLink Seed Code/AES128 Format/20140320-IDEA001-1554.bin";
//        String strDecryptedSecret = c.decryptSecreteAES128(secByte, filepath);
//        System.out.println(strDecryptedSecret);
//        System.out.println(strDecryptedSecret);
//        String secrete = "URFIYzgCiBP1Ncxg345oLvMmDrDhbxGSbikFdlfQ7ACxNwguOJbbQJu1Jcd4B3nW";
//        byte[] secByte = Base64.decode(secrete);
//        String filepath = "D:\\_nk\\IdeaLink Seed Code\\IdeaLink Seed Code\\AES128 Format\\20140320-IDEA001-1554.bin";
//        c.decryptSecreteAES128(secByte, filepath);
        //String sec256 = "5/JISSXJdZLYBmXBrHT3Nseps6T8r9a2Sqtp86/j4F5rRY1FuSTh/qQ5T9JyEytD";
        //String file256 = "/Users/nileshkamble/Downloads/IdeaLinkSeedCode/AES256Format/20140320-IDEA001-1554.bin";
        //byte[] secByte256 = Base64.decode(sec256);
        //c.decryptSecreteAES256(secByte256, file256);
//         CheckAESEncrypt();
    }

//    public static void main(String args[]) throws UnsupportedEncodingException {
//        CryptoManager cm = new CryptoManager();
////        PublicKey key = cm.getPublicKey("MIGJAoGBAMtuarPi14Z3YghXUDrB+c3qA83vaTxxKkzgBFYxYQXlqAylmZs5vylgSz9TZiiHTJHrN2BhDNE16Vu9HqVZa5kh3Cc/L7A434mJzzbXmYaPashZvptBfTXOw5/Kl+0Q/pS6YCQpqn0Fb+vDF1zjMtbk4b3BOBdfyd7t0xt6BJvLAgMBAAE=");
////        String data = "manoj";
////
////        byte[] da = cm.encryptRSA(data.getBytes(), key);
////        //System.out.println(new String(da));
////
////        byte[] s = Base64.encode(key.getEncoded());
////        String vKey = new String(s);
////        //System.out.println("Public Key   ::  " + vKey);
//
//        String mydata = new String("这是一条测试消息 this is sample");
//        
//        for(int i=0;i<mydata.length();i++) { 
//            char ch = mydata.charAt(i);
//            System.out.print(String.format("%04x", (int) ch));
//        }
//        
//        System.out.println();
//        
//        //System.out.println(cm.asHex(mydata.getBytes("UTF8")));
//        
//        String [] _numbers = "60103663542;610333333;605955959;".split(";");
//        //String [] _numbers = "60103663542".split(";");
//        for (int i = 0; i < _numbers.length; i++) {
//            System.out.println(_numbers[i]);
//        }
//        
//        
//        
//        //CryptoManager cm = new CryptoManager();
//        //cm.CheckAESEncrypt();
//    }
    public String generateTokenSecret(String regcode, String deviceid) {
        return generateTokenSecretInner(regcode, deviceid);
    }

    private String generateTokenSecretInner(String regcode, String deviceid) {

        try {
            String strRegCodeSecret = deviceid;
            strRegCodeSecret += regcode;
            Date d = new Date();
            strRegCodeSecret += d.getTime();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(strRegCodeSecret.getBytes());
            byte[] output = md.digest();
            String strHexSecret = asHex(output);
            return strHexSecret;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

   
  
  
    public AxiomRSAKeyPair generateAxiomRSAKeyPair(int iLength) throws NoSuchAlgorithmException {
        return generateAxiomRSAKeyPairInner(iLength);
    }

    public boolean verifyDataRSA(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        return verifyDataRSAInner(data, sigBytes, publicKey, type);
    }

    public byte[] signDataRSA(byte[] data, PrivateKey privateKey, int type) {
        return signDataRSAInner(data, privateKey, type);
    }

    public byte[] encryptRSA(byte[] plainData, PublicKey pubKey) {
        return encryptRSAInner(plainData, pubKey);
    }

    public byte[] encryptRSAForAndroid(byte[] plainData, PublicKey pubKey) {
        return encryptRSAForAndroidInner(plainData, pubKey);
    }

    public byte[] decryptRSA(byte[] encrptdByte, PrivateKey privateKey) {
        return decryptRSAInner(encrptdByte, privateKey);
    }

    public byte[] decryptRSAForAndroid(byte[] encrptdByte, PrivateKey privateKey) {
        return decryptRSAForAndroidInner(encrptdByte, privateKey);
    }

    public byte[] generateKeyAES(byte[] keyStart, int iLength) {
        return generateKeyAESInner(keyStart, iLength);
    }

    public byte[] generateKeyAESForAndroid(char[] keyStart, int iLength) {
        return generateKeyAESForAndroidInner(keyStart, iLength);
    }

    public byte[] encryptAES(byte[] key, byte[] clear) {
        //String strKey = new String(Base64.encode(key));
        //String data = new String(Base64.encode(clear));
        //System.out.println("encryptAES Key::" + strKey);
        //System.out.println("encryptAES Data::" + data);

        return encryptAESInner(key, clear);
        //return clear;
    }

    public byte[] decryptAES(byte[] key, byte[] encrypted) {
        //String strKey = new String(Base64.encode(key));
        //String data = new String(Base64.encode(encrypted));
        //System.out.println("Key     ::   " + strKey);
        //System.out.println("Data     ::   " + data);
        return decryptAESInner(key, encrypted);
        //return encrypted;
    }

    private AxiomRSAKeyPair generateAxiomRSAKeyPairInner(int iLength) throws NoSuchAlgorithmException {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(iLength);
            KeyPair keyPair = kpg.genKeyPair();
            //byte[] pri = keyPair.getPrivate().getEncoded();
            //byte[] pub = keyPair.getPublic().getEncoded();
            AxiomRSAKeyPair arsakp = new AxiomRSAKeyPair();
            arsakp.visiblekey = keyPair.getPublic();
            arsakp.privatekey = keyPair.getPrivate();
            return arsakp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] encryptRSAInner(byte[] plainData, PublicKey pubKey) {
        try {
            byte[] encryptionByte = null;
            //Cipher cipher = Cipher.getInstance("RSA");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            //encryptionByte = cipher.doFinal(plainData);
            byte[] someData = cipher.update(plainData);
            byte[] moreData = cipher.doFinal();
            byte[] encrypted = new byte[someData.length + moreData.length];
            System.arraycopy(someData, 0, encrypted, 0, someData.length);
            System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);

            encryptionByte = encrypted;
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] encryptRSAForAndroidInner(byte[] plainData, PublicKey pubKey) {
        try {
            byte[] encryptionByte = null;
            //Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            encryptionByte = cipher.doFinal(plainData);
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] decryptRSAInner(byte[] encrptdByte, PrivateKey privateKey) {
        try {
            byte[] encryptionByte = null;
            //Cipher cipher = Cipher.getInstance("RSA");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            //encryptionByte = cipher.doFinal(encrptdByte);
            //System.out.println("Recovered String     :::  " + new String(encryptionByte));
            byte[] someDecrypted = cipher.update(encrptdByte);
            byte[] moreDecrypted = cipher.doFinal();
            byte[] decrypted = new byte[someDecrypted.length + moreDecrypted.length];
            System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
            System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);

            encryptionByte = decrypted;

            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] decryptRSAForAndroidInner(byte[] encrptdByte, PrivateKey privateKey) {

        try {
            byte[] encryptionByte = null;
            //Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            encryptionByte = cipher.doFinal(encrptdByte);
            //System.out.println("Recovered String     :::  " + new String(encryptionByte));
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    private byte[] signDataRSAInner(byte[] data, PrivateKey privateKey, int type) {
        try {
            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA1WithRSA");
            }
            signature.initSign(privateKey);

            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);

            byte[] sig = signature.sign();

            //String b64Hash = new String(Base64.encode(hashedData));
            //System.out.println("Hash To be Signed is:" + b64Hash);
            //String b64Str = new String(Base64.encode(sig));
            //System.out.println("Signature is:" + b64Str);
            //String b64Str1 = new String(Base64.encode(data));
            //System.out.println("Data is:" + b64Str1);
            //return signature.sign();
            return sig;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private boolean verifyDataRSAInner(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        try {

            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA1WithRSA");
            }
            signature.initVerify(publicKey);

            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);

            return signature.verify(sigBytes);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private byte[] generateKeyAESInner(byte[] keyStart, int iLength) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(keyStart);
            // 128, 192 and 256 bits may not be available
            kgen.init(iLength, sr);
            SecretKey skey = kgen.generateKey();
            byte[] key = skey.getEncoded();
            return key;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    private byte[] generateKeyAESForAndroidInner(char[] password, int iLength) {
        try {
            int iterationCount = 1000;
            int keyLength = iLength;
    
            KeySpec keySpec = new PBEKeySpec(password, iv,
                    iterationCount, keyLength);
            SecretKeyFactory keyFactory = SecretKeyFactory
                    .getInstance("PBKDF2WithHmacSHA1");
            byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
            SecretKey skey = new SecretKeySpec(keyBytes, "AES");
            byte[] key = skey.getEncoded();

            System.out.println("The AES encryption key is " + new String(Base64.encode(key)));

//            byte[] encrypted = null;
//            try {
//                SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
//                Cipher cipher = Cipher.getInstance(AES_ALGO);
//                IvParameterSpec ivspec = new IvParameterSpec(iv);
//                cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
//                String data = "test data to be encrypt and decrypt";
//                byte[] someData = cipher.update(data.getBytes());
//                byte[] moreData = cipher.doFinal();
//                encrypted = new byte[someData.length + moreData.length];
//                System.arraycopy(someData, 0, encrypted, 0, someData.length);
//                System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);
//                //return encrypted;
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                return null;
//            }
//            
//            byte[] decrypted =null;
//
//            try {
//                SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
//                Cipher cipher = Cipher.getInstance(AES_ALGO);
//                IvParameterSpec ivspec = new IvParameterSpec(iv);
//                cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
//                byte[] someDecrypted = cipher.update(encrypted);
//                byte[] moreDecrypted = cipher.doFinal();
//                decrypted = new byte[someDecrypted.length + moreDecrypted.length];
//                System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
//                System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);                
//                
//                String dataDecrypted = new String(decrypted);
//                System.out.println(dataDecrypted);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                return null;
//            }
//            
//            KeyGenerator kgen = KeyGenerator.getInstance("AES");
//            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
//            sr.setSeed(keyStart);
//            // 128, 192 and 256 bits may not be available
//            kgen.init(iLength, sr);
//            SecretKey skey = kgen.generateKey();
//            byte[] key = skey.getEncoded();
            return key;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    private byte[] encryptAESInner(byte[] key, byte[] clear) {
        try {
            //String key1 = ")!@(#*$&%^123098";
            //key = key1.getBytes();

            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance(AES_ALGO);
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
            byte[] encrypted = cipher.doFinal(clear);
            
//            byte[] someData = cipher.update(clear);
//            byte[] moreData = cipher.doFinal();
//                        
//            byte[] encrypted = new byte[someData.length + moreData.length];
//            System.arraycopy(someData, 0, encrypted, 0, someData.length);
//            System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);

            //System.out.println("Encrypted Data::" + new String(Base64.encode(encrypted)));
            return encrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] decryptAESInner(byte[] key, byte[] encrypted) {
        try {
            //String key1 = ")!@(#*$&%^123098";
            //key = key1.getBytes();

            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance(AES_ALGO);
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
//cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] decrypted = cipher.doFinal(encrypted);
//            byte[] someDecrypted = cipher.update(encrypted);
//            byte[] moreDecrypted = cipher.doFinal();
//            byte[] decrypted = new byte[someDecrypted.length + moreDecrypted.length];
//            System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
//            System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);
            return decrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

 

 public String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public byte[] read(File file) throws IOException {
        byte[] buffer = new byte[(int) file.length()];
        InputStream ios = null;
        try {
            ios = new FileInputStream(file);
            if (ios.read(buffer) == -1) {
                throw new IOException("EOF reached while trying to read the whole file");
            }
        } finally {
            try {
                if (ios != null) {
                    ios.close();
                }
            } catch (IOException e) {
            }
        }

        return buffer;
    }

    //end of addition
    public byte[] DecryptAES_CBC(byte[] data, byte[] key) throws Exception {

        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        Cipher cipher = Cipher.getInstance(AES_ALGO);
        //Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS7Padding");

        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);

        return cipher.doFinal(data);
    }
    
    

  
    
}
