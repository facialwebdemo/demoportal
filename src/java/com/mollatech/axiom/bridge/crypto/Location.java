/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.bridge.crypto;

/**
 *
 * @author mollatech2
 */
public class Location {

    @Override
    public String toString() {
        return "Location{" + "area=" + area + ", city=" + city + ", state=" + state + ", country=" + country + ", zipcode=" + zipcode + '}';
    }

    public String area;
    public String city;
    public String state;
    public String country;
    public String zipcode;
    public String lattitude;
    public String longitude;
    

}
