/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.bridge.crypto;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 * @author vikramsareen
 */
public class AxiomRSAKeyPair {
    public PublicKey visiblekey;
    public PrivateKey privatekey;    
}
