package com.mollatech.axiom.bridge.crypto;

import java.util.Date;
import java.util.Properties;

public class LoadSettings {

    public static Properties g_sSettings = null;
    public static String g_strPath = null;
    public static Properties g_templateSettings = null;
    public static String g_templatestrPath = null;
    public static Properties g_subjecttemplateSettings = null;
    public static String g_subjecttemplatestrPath = null;
   

    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
            usrhome += sep + "axiomv2-settings";
      

        g_subjecttemplatestrPath = usrhome + sep;
        String filepath = usrhome + sep + "subject.templates.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_subjecttemplateSettings = p1.properties;
            //System.out.println("subject template  file loaded >>" + filepath);
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "subject template file failed to load >> " + filepath);
        }
    }

    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
      
      
            usrhome += sep + "axiomv2-settings";
        
        
        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "demoportal.conf";

     
        PropsFileUtil p = new PropsFileUtil();

        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
//            System.out.println("dbsetting setting file loaded >>" + filepath);

        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "dbsetting setting file failed to load >> " + filepath);
        }
    }

    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
       
            usrhome += sep + "axiomv2-settings";
        
        g_templatestrPath = usrhome + sep;
        String filepath = usrhome + sep + "templates.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_templateSettings = p1.properties;
            //System.out.println("template  file loaded >>" + filepath);
        } else {
            System.out.println(new Date() + ">>" + "template  file failed to load >> " + filepath);
        }
    }

    public static void LoadManually(String path) {
        DBSettingManual(path);
        TemplateSettingManual(path);
        SubjectTemplateSettingManual(path);
    }

    private static void DBSettingManual(String path) {
        String sep = System.getProperty("file.separator");
        String usrhome = path;
       
            usrhome += sep + "axiomv2-settings";
        
        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "demoportal.conf";
        PropsFileUtil p = new PropsFileUtil();

        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
//            System.out.println("manually loaded dbsetting setting file loaded >>" + filepath);

        } else {
            System.out.println(new Date() + ">>" + "manually loaded dbsetting setting file failed to load >> " + filepath);
        }
    }

    private static void TemplateSettingManual(String path) {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
        
            usrhome += sep + "axiomv2-settings";
       
        g_templatestrPath = usrhome + sep;
        String filepath = usrhome + sep + "templates.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_templateSettings = p1.properties;
//            System.out.println("manually loaded  template  file loaded >>" + filepath);
        } else {
            System.out.println(new Date() + ">>" + "manually loaded  template  file failed to load >> " + filepath);
        }
    }

    private static void SubjectTemplateSettingManual(String path) {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            //    System.out.println("filepath   :" +usrhome);
        }
       
            usrhome += sep + "axiomv2-settings";
       
        g_subjecttemplatestrPath = usrhome + sep;
        String filepath = usrhome + sep + "subject.templates.conf";
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_subjecttemplateSettings = p1.properties;
            //System.out.println("manually loaded  subject template  file loaded >>" + filepath);
        } else {
            System.out.println(new Date() + ">>" + "manually loaded  subject template  file failed to load >> " + filepath);
        }
    }


   

}
