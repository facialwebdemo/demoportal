package com.mollatech.axiom.bridge.crypto;

//import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import java.io.BufferedWriter;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Random;
import org.bouncycastle.util.encoders.Base64;

public class UtilityFunctions {

//    public static void main(String[] args) {
//        try {
////            File sourceFile = new File("E:\\RiskBasedAuthentication\\AxiomAuthanticationAcleda\\dist\\AxiomAuthanticationAcleda.jar");
////            File destinationFile = new File("E:\\" + sourceFile.getName());
////            CopyFile copyFileExample = new CopyFile();
////            copyFileExample.copyFile(sourceFile, destinationFile);
//            
//            UtilityFunctions u = new UtilityFunctions();
//            System.out.println(u.masking("1234567890abcd"));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    public String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String Bas64SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return new String(Base64.encode(array));
        } catch (Exception e) {
            return null;
        }
    }

    public String HexSHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return convertToHex(array);
        } catch (Exception e) {
            return null;
        }
    }

    public void copyFile(File sourceFile, File destinationFile) {
        try {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            FileOutputStream fileOutputStream = new FileOutputStream(
                    destinationFile);

            int bufferSize;
            byte[] bufffer = new byte[512];
            while ((bufferSize = fileInputStream.read(bufffer)) > 0) {
                fileOutputStream.write(bufffer, 0, bufferSize);
            }
            fileInputStream.close();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int delete(File file) {
        try {
            if (file.isDirectory()) {

                //directory is empty, then delete it
                if (file.list().length == 0) {

                    file.delete();
                    return 0;

                } else {

                    //list all the directory contents
                    String files[] = file.list();

                    for (String temp : files) {
                        //construct the file structure
                        File fileDelete = new File(file, temp);

                        //recursive delete
                        delete(fileDelete);
                    }

                    //check the directory again, if empty then delete it
                    if (file.list().length == 0) {
                        file.delete();
                        return 0;
                    }
                }

            } else {
                //if file, then delete it
                file.delete();
                return -1;
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return -1;
    }

    public static Object deserializeFromObject(ByteArrayInputStream binaryObject) {
        ObjectInputStream ins;
        try {
            ins = new ObjectInputStream(binaryObject);
            Object object = (Object) ins.readObject();
            ins.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String MaskData(String data) {
        String set = data.substring(data.length() - 4, data.length());
        String retData = "";
        for (int i = 0; i < data.length(); i++) {
            if (i < data.length() - 4) {
                retData = "*" + retData;
            }
        }
        retData = retData + set;
        return retData;

    }

    public int CopyPlugins(File source, String contextPath) {
        try {

            String sep = System.getProperty("file.separator");

            String filename = source.getName();
            File destination = new File(LoadSettings.g_sSettings.getProperty("axiom.dir") + System.getProperty("file.separator")
                    + contextPath
                    + sep + "WEB-INF" + sep + "lib" + sep
                    + filename);
            // String coreFilename = new File(filepath).getName();
            File coreDestination = new File(LoadSettings.g_sSettings.getProperty("axiom.dir") + System.getProperty("file.separator")
                    + contextPath + "core"
                    + sep + "WEB-INF" + sep + "lib" + sep
                    + filename);
            int result = -1;
            UtilityFunctions utilFun = new UtilityFunctions();
            utilFun.copyFile(source, destination);
            utilFun.copyFile(source, coreDestination);
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public int deletePlugins(String name, String contextPath) {
        try {

            String sep = System.getProperty("file.separator");

            File destination = new File(LoadSettings.g_sSettings.getProperty("axiom.dir") + System.getProperty("file.separator")
                    + contextPath
                    + sep + "WEB-INF" + sep + "lib" + sep
                    + name);
            // String coreFilename = new File(filepath).getName();
            File coreDestination = new File(LoadSettings.g_sSettings.getProperty("axiom.dir") + System.getProperty("file.separator")
                    + contextPath + "Core"
                    + sep + "WEB-INF" + sep + "lib" + sep
                    + name);
            UtilityFunctions utilFun = new UtilityFunctions();
            utilFun.delete(destination);
            utilFun.delete(coreDestination);
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public static boolean isValidEmail(String emailAddress) {
        return emailAddress.contains(" ") == false && emailAddress.matches(".+@.+\\.[a-z]+");
    }

    public static boolean isValidPhoneNumber(String stringNumber) {
        if (stringNumber.equals("")) {
            return false;
        }
        if (stringNumber.startsWith("+")) {
            stringNumber = stringNumber.replaceAll(" ", "");

            char[] charNumber = stringNumber.toCharArray();
            for (int i = 1; i < charNumber.length; i++) {
                if (!Character.isDigit(charNumber[i])) {
                    return false;
                }
            }
            return true;
        }

        stringNumber = stringNumber.replaceAll(" ", "");

        char[] charNumber = stringNumber.toCharArray();
        for (int i = 0; i < charNumber.length; i++) {
            if (!Character.isDigit(charNumber[i])) {
                return false;
            }
        }
        return true;
    }

    public static byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    public String writeTofile(String filename, String data) {
        try {
            Writer output = null;
            //    String filepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive");
            File file = new File(filename);
            output = new BufferedWriter(new FileWriter(file));
            output.write(data);
            output.close();
            return file.getAbsolutePath();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static int calculatePercentage(int value, int total) {
        int percentage = (value * 100) / total;
        return percentage;
    }

    public static int getRandomNumber(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

    // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static void main(String []args){
        String reg = Bas64SHA1("18728281");
        
        //System.out.println("Registartion code   :   " + reg);
        
        Date d = new Date(1401460868L);
        
        //System.out.println(d.toString());
    }
    
    
}
