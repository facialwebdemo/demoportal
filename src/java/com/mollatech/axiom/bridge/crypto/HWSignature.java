package com.mollatech.axiom.bridge.crypto;


import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HWSignature {
    //static 
    //static String strHDDSignature = null;

    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

    public static byte[] hex2Byte(String str) {
        byte[] bytes = new byte[str.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer
                    .parseInt(str.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    protected static String strHDDSignature = null;

   
 
    public static String getSignature() {
        return getSignatureInner();
    }

    private static String getSignatureInner() {
        try {
            String strHA = LoadSettings.g_sSettings.getProperty("ha.enabled");

            if (strHA == null) {
                return strHDDSignature;
            }
            if (strHA.compareToIgnoreCase("yes") == 0) {
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA1");
                    md.update("22ec3f11d-bd3a-3f23-a5bb-273f707bcd41".getBytes());
                    byte[] output = md.digest();
                    String strHDDSignatureInner = asHex(output);
                    strHDDSignatureInner = strHDDSignatureInner.substring(0, 16);
                    //int iLen = strHDDSignatureInner.length();
                    return strHDDSignatureInner;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return null;
                }
            } else {
                return strHDDSignature;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
