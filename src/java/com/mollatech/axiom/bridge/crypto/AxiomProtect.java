package com.mollatech.axiom.bridge.crypto;

import com.mollatech.axiom.bridge.crypto.CryptoManager;


import static com.mollatech.axiom.bridge.crypto.HWSignature.asHex;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AxiomProtect {

    private static String licfile = null;
    private static String publickey = null;
    private static String aeskey = null;
    //private static String hardwareid = null;
    private static Properties g_sSettings = null;
    private static byte[] aes_password = null;
    private static String g_filepath = null;

    private static Date g_sCheckDateForLicense = null;

    public static String USER_PASSWORD = "user.password";
    public static String USER_CHALLENGERESPONSE = "user.channelresponse";
    public static String OTP_OOB = "otp.oob";
    public static String OTP_SOFTWARE = "otp.software";
    public static String OTP_HARDWARE = "otp.hardware";
    public static String PKI_CERTIFICATE = "pki.certificate";
    public static String PKI_HARDWARE = "pki.hardware";
    public static String PKI_SOFTWARE = "pki.software";

    public static String MOBILETRUST_DEVICEPROFILE = "mobile.device.profile";
    public static String MOBILETRUST_GEOFENCE = "mobile.trust.geo";
    public static String MOBILETRUST_PUSH = "mobile.trust.push";
    public static String MOBILETRUST_E2E = "mobile.trust.e2e";
    public static String MOBILETRUST_TIMESTAMP = "mobile.trust.timestamp";
    public static String MOBILETRUST_TYPE = "mobiletrust.licensetype";
    public static String MOBILETRUST = "mobile.trust";

    public static String REMOTESIGNSERVICEWITH_PASSWORD = "remote.signing.service.password";
    public static String REMOTESIGNSERVICEWITH_OTP = "remote.signing.service.otp";
    public static String REMOTESIGNSERVICEWITH_CHALLENGERESPONSE = "remote.signing.service.channelresponse";

    public static String EPIN = "epin";

    public static final String CHANNELS = "channels";
    public static final String OPERATORS = "operators";
    public static final String GATEWAY_SMS = "gateway.sms";
    public static final String GATEWAY_EMAIL = "gateway.email";
    public static final String GATEWAY_VOICE = "gateway.voice";
    public static final String GATEWAY_USSD = "gateway.ussd";
    public static final String GATEWAY_FAX = "gateway.fax";

    public static final String INTERACTIONS = "interactions";
    public static final String GATEWAY_PUSH_APPLE = "gateway.push.apple";
    public static final String GATEWAY_PUSH_GOOGLE = "gateway.push.google";    
    public static final String THIRD_PARTY_CALLERS_COUNT = "callers.count";
    public static final String THIRD_PARTY_CALLERS = "callers";
    public static final String SOCIAL_MESSAGING = "social.messaging";

    static {
        LoadAxiomProtect();
       

    }

    private static void LoadAxiomProtect() {
        try {

            String sep = System.getProperty("file.separator");
            String usrhome = System.getProperty("catalina.home");

            if (usrhome == null) {
                usrhome = System.getenv("catalina.home");
            }

            if (usrhome == null) {
                usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            }

            usrhome += sep + "bridge-settings";

            g_filepath = usrhome + sep + "bridge-settings.conf";

        
            PropsFileUtil p = new PropsFileUtil();

            if (p.LoadFile(g_filepath) == true) {
                g_sSettings = p.properties;
            } else {
                System.out.println("license  file failed to load >> " + g_filepath);
            }
            aeskey = g_sSettings.getProperty("reserved.1");
            String strHashKey = g_sSettings.getProperty("reserved.7");
//            publickey = g_sSettings.getProperty("reserved.2");

            licfile = usrhome + sep + "license.enc";
            
            if (aeskey != null && aeskey.isEmpty() == false) {
                AES aesObj = new AES();
                //System.out.println("reserved.7" + strHashKey);
                //System.out.println("key to decrypt" + AES.getSignature());
                byte[] byteHashKey = aesObj.PINDecryptBytes(strHashKey, AES.getSignature());
                //System.out.println("decrypted key " + asHex(byteHashKey));
                //System.out.println("encrypted random key " + aeskey);
                aes_password = aesObj.PINDecryptBytes(aeskey, asHex(byteHashKey));
                //System.out.println("decrypted random key " + aes_password);
            } else {
                //System.out.println("ERROR: KEY is NULL... installation is tempered or corrupted!!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int SetPasswordsInner(String[] passwords) {
        try {
            if (passwords == null || passwords.length == 0) {
                return -3;
            }

            MessageDigest md = MessageDigest.getInstance("SHA1");
            for (int i = 0; i < passwords.length; i++) {
                md.update(passwords[i].getBytes());
            }
            byte[] outputHashOFCredentials = md.digest();

            md.reset();

            AES aesObj = new AES();
            String password = AES.getSignature();
            String strEncryptedKEY = aesObj.PINEncryptBytes(outputHashOFCredentials, password);

            String password2 = asHex(outputHashOFCredentials);
            byte[] dbPassword = GenerateRandomNumber().getBytes();
            String strEndDBPassword = aesObj.PINEncryptBytes(dbPassword, password2);

            PropsFileUtil pfUtilsObj = new PropsFileUtil();
            boolean bResult = false;
            bResult = pfUtilsObj.LoadFile(g_filepath);
            HashMap map = new HashMap();

            pfUtilsObj.properties.setProperty("reserved.7", strEncryptedKEY);
            pfUtilsObj.properties.setProperty("reserved.1", strEndDBPassword);

            Enumeration enamObj = pfUtilsObj.properties.propertyNames();
            while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) pfUtilsObj.properties.getProperty(key);
                map.put(key, value);
            }

            bResult = pfUtilsObj.ReplaceProperties(map, g_filepath);
            if (bResult == true) {
                LoadAxiomProtect();
                return 0;
            } else {
                return -2;
            }

            //return strEncryptedData;
            //return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    private static int ReplacePasswordsInner(String[] curpasswords, String[] newpasswords) {
        try {

            if (curpasswords == null || curpasswords.length == 0) {
                return -4;
            }

            if (newpasswords == null || newpasswords.length == 0) {
                return -3;
            }

            MessageDigest md = MessageDigest.getInstance("SHA1");
            for (int i = 0; i < curpasswords.length; i++) {
                md.update(curpasswords[i].getBytes());
            }
            byte[] outputCurrentAESKEY = md.digest();

            PropsFileUtil pfUtilsObj = new PropsFileUtil();
            boolean bResult = pfUtilsObj.LoadFile(g_filepath);
            HashMap map = new HashMap();

            String strCurPassInHex = pfUtilsObj.properties.getProperty("reserved.7");

            AES aesObj = new AES();
            byte[] bytesDecryptedCurPassword = aesObj.PINDecryptBytes(strCurPassInHex, AES.getSignature());

            String CurPasswordFromFile = asHex(bytesDecryptedCurPassword);
            String CurPasswordFromDashbd = asHex(outputCurrentAESKEY);

            if (CurPasswordFromDashbd.compareTo(CurPasswordFromFile) != 0) {
                //password did not match
                Date d = new Date();

                System.out.println(d + ">>" + "Failed to match");
                return -9;
            }

            md.reset();
            for (int i = 0; i < newpasswords.length; i++) {
                md.update(newpasswords[i].getBytes());
            }
            byte[] outputNewHashFromOfficers = md.digest();
            String strEncryptedKEY = aesObj.PINEncryptBytes(outputNewHashFromOfficers, AES.getSignature());

            pfUtilsObj.properties.setProperty("reserved.7", strEncryptedKEY);

            //replace the r.1 also 
            String strEncRandomKey = pfUtilsObj.properties.getProperty("reserved.1");
            //decrypt the r.1 using old hash key 
            //byte[] strPlainRandomKey = aesObj.PINDecryptBytes(strEncRandomKey, new String(bytesDecryptedCurPassword));
            byte[] strPlainRandomKey = aesObj.PINDecryptBytes(strEncRandomKey, asHex(bytesDecryptedCurPassword));
            //encrypt the r.1 using new hash key 
            //String strEncNewRandomKey = aesObj.PINEncryptBytes(strPlainRandomKey, new String(outputNewHashFromOfficers));
            String strEncNewRandomKey = aesObj.PINEncryptBytes(strPlainRandomKey, asHex(outputNewHashFromOfficers));

            pfUtilsObj.properties.setProperty("reserved.1", strEncNewRandomKey);

            Enumeration enamObj = pfUtilsObj.properties.propertyNames();
            while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) pfUtilsObj.properties.getProperty(key);
                map.put(key, value);
            }

            bResult = pfUtilsObj.ReplaceProperties(map, g_filepath);
            if (bResult == true) {
                LoadAxiomProtect();
                return 0;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            Logger.getLogger(AxiomProtect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    private static int g_sLicenseResult = -1;

//    public static int ValidateLicense() {
//        try {
//            
//            if ( licfile == null ) 
//                return -100;
//
//            if (g_sCheckDateForLicense == null) {
//                LicenseFile license = LicenseValidator.validateLicenseFile(
//                        licfile,
//                        publickey,
//                        null,
//                        null,
//                        null,
//                        null,
//                        null);
//
//                g_sCheckDateForLicense = new Date();                
//                
//                g_sLicenseResult = license.getStatusCode();
//                
//                System.out.println("if (g_sCheckDateForLicense == null)  "  + g_sLicenseResult);
//                System.out.println("if (g_sCheckDateForLicense == null)  "  + g_sCheckDateForLicense);
//                
//                return g_sLicenseResult;
//                
//            } else {
//                Date d = new Date();
//                Calendar cal = null;
//                cal = Calendar.getInstance();
//                cal.setTime(d);
//                int date = cal.get(Calendar.DATE);
//
//                cal = Calendar.getInstance();
//                cal.setTime(g_sCheckDateForLicense);
//                int date1 = cal.get(Calendar.DATE);
//
//                if (date1 == date) {
//                    
//                    System.out.println("if (date1 == date)  "  + g_sLicenseResult);
//                    System.out.println("if (date1 == date)  "  + g_sCheckDateForLicense);
//
//                    
//                    return g_sLicenseResult; //stored result
//                } else {                    
//                    LicenseFile license = LicenseValidator.validateLicenseFile(
//                            licfile,
//                            publickey,
//                            null,
//                            null,
//                            null,
//                            null,
//                            null);
//
//                    g_sCheckDateForLicense = new Date();
//                    g_sLicenseResult = license.getStatusCode();
//                    
//                    System.out.println("if (date1 != date)  "  + g_sLicenseResult);
//                    System.out.println("if (date1 != date)  "  + g_sCheckDateForLicense);
//                    
//                    return g_sLicenseResult;
//                }
//
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return -99;
//        }
//        //return 0;
//    }


   
 
    public static boolean IsSecurityInitialized() {
        if (aeskey == null || aeskey.isEmpty() == true) {
            return false;
        } else {
            return true;
        }
    }

    public static int SetPasswords(String[] passwords) {
        return SetPasswordsInner(passwords);
    }

    public static int ReplacePasswords(String[] curpasswords, String[] newpasswords) {
        return ReplacePasswordsInner(curpasswords, newpasswords);
    }

    public static byte[] AccessDataBytes(byte[] bytesEncryptedData) {
        try {
            if (bytesEncryptedData == null) {
                return null;
            }

            //System.out.println("AccessDataBytes data size is " + bytesEncryptedData.length);
            //return bytesEncryptedData;
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.decryptAES(aeskey1, bytesEncryptedData);
            return original;
//            raw = Arrays.copyOf(aes_password, 16);
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            //Cipher cipher = Cipher.getInstance("AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//            byte[] original = cipher.doFinal(bytesEncryptedData);
//            return original;
            //return bytesEncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //return bytesEncryptedData;
        }
    }

    public static String AccessData(String strEncryptedData) {
        try {
            if (strEncryptedData == null || strEncryptedData.isEmpty() == true) {
                return null;
            }

            byte[] hexBytes = HWSignature.hex2Byte(strEncryptedData);

            //return strEncryptedData;
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.decryptAES(aeskey1, hexBytes);
            return new String(original);

//            byte[] hexBytes = HWSignature.hex2Byte(strEncryptedData);
//            byte[] raw = aes_password;
//            raw = Arrays.copyOf(aes_password, 16);
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//            byte[] original = cipher.doFinal(hexBytes);
//            String originalString = new String(original);
//            return originalString;
            //return strEncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //return strEncryptedData;
        }
    }

    public static String ProtectData(String strData) {
        try {

            if (strData == null || strData.isEmpty() == true) {
                return null;
            }

            //return strData;
            byte[] raw = aes_password;
            if (aes_password == null) {
                Date d = new Date();
                System.out.println(d + ">>" + "Protect Data aes_password is NULL ");
                return null;
            } else {
//                System.out.println("Protect Data aes_password is NOT NULL with legth " + aes_password.length);
            }
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.encryptAES(aeskey1, strData.getBytes());

            String strEncryptedData = HWSignature.asHex(original);
            return strEncryptedData;
            //return new String(original);

            //byte[] raw = HWSignature.strHDDSignature.getBytes();
//            byte[] raw = aes_password;
//            raw = Arrays.copyOf(aes_password, 16);
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
//            byte[] encrypted = cipher.doFinal(strData.getBytes());
//            String strEncryptedData = HWSignature.asHex(encrypted);
//            return strEncryptedData;
            //return strData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //return strData;
        }
    }

    public static byte[] ProtectDataBytes(byte[] bytesData) {
        try {

            if (bytesData == null) {
                return null;
            }

            //System.out.println("ProtectDataBytes data size is " + bytesData.length);
            //return bytesData;
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.encryptAES(aeskey1, bytesData);
            return original;

//            byte[] raw = aes_password;
//            raw = Arrays.copyOf(aes_password, 16);
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
//            byte[] encrypted = cipher.doFinal(bytesData);
//            return encrypted;
            //return bytesData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //return bytesData;
        }
    }

    private static String GenerateRandomNumber() {
        Date d = new Date();
        return "" + d.getTime();
    }

    private static Properties g_LicenseProperties = null;

}
