/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.operations;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
//@WebServlet(name = "secondfactorauth", urlPatterns = {"/secondfactorauth"})
public class secondfactorauth extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssUserCerdentials");
        String sessionId = (String) request.getSession().getAttribute("_userSessinId");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        try {
            String _otp = request.getParameter("_otp");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.OTP);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }

            String result = "success";
            String message = "successful credential verification....";
            String url = "transaction.jsp";

            if (_otp == null || _lattitude == null) {
                result = "error";
                message = "Invalid Credentials!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");

            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            AxiomCredentialDetails axiomCredentialDetails = null;
            if (userObj != null) {

                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.OTP) {
                                if (axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                    retValue = 0;
                                    break;
                                } else {
                                    result = "error";
                                    message = "User status is not Active...!!!";
                                    url = "index.jsp";
                                    json.put("_result", result);
                                    json.put("_message", message);
                                    json.put("_url", url);
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }

                    }
                } else {
                    result = "error";
                    message = "User not found...!!!";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    return;
                }
            } else {
                result = "error";
                message = "User not found...!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            VerifyRequest vReq = new VerifyRequest();
            vReq.setCategory(axWraper.OTP);
            vReq.setCredential(_otp);

            AxiomStatus aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);

            if (aStatus.getErrorcode() == 0) {
                HttpSession session = request.getSession(true);
                session.setAttribute("_apOprAuth", "yes");
                session.setAttribute("_rssUserCerdentials", userObj);
                session.setAttribute("_userSessinId", sessionId);

                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            } else {
                HttpSession session = request.getSession(true);
                session.setAttribute("_apOprAuth", "yes");
                session.setAttribute("_rssUserCerdentials", userObj);
                session.setAttribute("_userSessinId", sessionId);
                url = "index.jsp";
                result = "error";
                message = "Invalid credentials!!!";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
