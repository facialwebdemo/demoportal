/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.operations;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
//@WebServlet(name = "transactiondetails", urlPatterns = {"/transactiondetails"})
public class transactiondetails extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json");
        RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssUserCerdentials");
        String sessionId = (String) request.getSession().getAttribute("_userSessinId");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        try {

            String _source = request.getParameter("_source");
            String _destination = request.getParameter("_destination");
            String _amount = request.getParameter("_amount");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.OTP);
           
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }
            
            String result = "success";
            String message = "successful credential verification....";
            String url = "transaction.jsp";

            if (_source == null || _source.isEmpty()
                    || _destination == null || _destination.isEmpty()
                    || _amount == null || _amount.isEmpty()) {
                result = "error";
                message = "Please fill all details!!!";
                url = "transaction.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            if (_longitude == null || _lattitude == null) {
                strPayLoad = null;
            }

            HttpSession session = request.getSession(true);
            session.setAttribute("_ammount", _amount);
            session.setAttribute("_source", _source);
            session.setAttribute("_dest", _destination);
            session.setAttribute("_signaturekey", _source + _destination + _amount);
                 result = "success";
                message = "Transaction is procesing!!!";
                url = "transactionvalidation.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
          
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
