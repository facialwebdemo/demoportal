/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.operations;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.AxiomLocationImpl;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.bridge.crypto.Location;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
//@WebServlet(name = "sendOOB", urlPatterns = {"/sendOOB"})
public class sendSignatureOOB extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
         *********************************************
         added by manoj
        
         */
        response.setContentType("application/json");
        System.out.println("inside transactionauth()");

        Enumeration parameterList = request.getParameterNames();
        while (parameterList.hasMoreElements()) {
            String sName = parameterList.nextElement().toString();

            String[] sMultiple = request.getParameterValues(sName);
            if (1 >= sMultiple.length) // parameter has a single value. print it.
            {
                System.out.println(sName + " = " + request.getParameter(sName));
            } else {
                for (int i = 0; i < sMultiple.length; i++) // if a paramater contains multiple values, print all of them
                {
                    System.out.println(sName + "[" + i + "] = " + sMultiple[i]);
                }
            }
        }
        System.out.println("after printing all params!!!!");

        String fromAccountNoConfirm = (String) request.getSession().getAttribute("fromAccountNo");
        String beneficiaryBankConfirm = (String) request.getSession().getAttribute("beneficiaryBank");
        String beneficiaryNameConfirm = (String) request.getSession().getAttribute("beneficiaryName");
        String otherAccountConfirm = (String) request.getSession().getAttribute("otherAccount");
        String emailConfirm = (String) request.getSession().getAttribute("email");
        String amountConfirm = (String) request.getSession().getAttribute("amount");
        String paymentTypeConfirm = (String) request.getSession().getAttribute("paymentType");
        String remarksConfirm = (String) request.getSession().getAttribute("remarks");
        String paymentDescriptionConfirm = (String) request.getSession().getAttribute("paymentDescription");
        String firstProcessDateConfirm = (String) request.getSession().getAttribute("firstProcessDate");
        String channelid = (String) request.getSession().getAttribute("_channelid");
        String location = (String) request.getSession().getAttribute("location");
        String browser = (String) request.getSession().getAttribute("browser");
        String agent = (String) request.getSession().getAttribute("agent");
        String authtype = (String) request.getSession().getAttribute("authtype");
        String _locationChunk = (String) request.getSession().getAttribute("_locationChunk");
        int locationType = (Integer) request.getSession().getAttribute("locationType");
        String baseOs = "NA";
        if (agent.contains("Linux")) {
            baseOs = "Linux";
        } else if (agent.contains("Mac")) {
            baseOs = "Linux";
        } else if (agent.contains("Windows")) {
            baseOs = "Windows";
        }

        //response.setContentType("application/json");
        RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssUserCerdentials");
        String sessionId = (String) request.getSession().getAttribute("_userSessinId");

//        String sotp = request.getParameter("sotp");
        String dataToSign = request.getParameter("dataToSign");

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        try {

            //String _sotp = request.getParameter("_otp");
            //String dataToSign = "I here by confirm that, I'm doing transaction of Amount : " + _ammount + " From Account No. : " + _source + " To Account No. : " + _dest;
            // String _datatosign = request.getParameter("_dataToSign");
            //String _lattitude = request.getParameter("_lattitude");
            //String _longitude = request.getParameter("_longitude");
            String _lattitude = (String) request.getSession().getAttribute("_lattitude");
            String _longitude = (String) request.getSession().getAttribute("_longitude");

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.SignatureOTP);
            payload.put("locationChunk", _locationChunk);
            payload.put("locationType", locationType);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
//            if (_lattitude.equalsIgnoreCase("latitude")) {
//                _lattitude = null;
//                _longitude = null;
//                strPayLoad = null;
//            }

            HttpSession session = request.getSession(true);
            AxiomLocationImpl axiomLoc = new AxiomLocationImpl();
            Location loc = axiomLoc.getLocation(_lattitude, _longitude);

            if (loc == null) {
                loc = axiomLoc.getLocationByIp(request.getRemoteAddr());
            }
            if (loc != null) {
                session.setAttribute("_Location", loc);
            }

            session.setAttribute("_errorGeoFence", "Not Checked");
            session.setAttribute("_errorOTPToken", "Not Checked");
            session.setAttribute("_errorRSS", "Not Checked");
            session.setAttribute("_errorGeneral", "Failed");

            String result = "success";
            String message = "Signature OTP Send Successfully";
            String url = "transactionvalidation.jsp";
//            String result = "success";
//            String message = "successful verified....";
//            String url = "transactionconfirmation.jsp";

            if (_longitude == null || _lattitude == null) {
                strPayLoad = null;
            }

            int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();

            AxiomCredentialDetails axiomCredentialDetails = null;
            if (userObj != null) {

                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.OUTOFBOUND_TOKEN) {
                                if (axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                    retValue = 0;
                                    break;
                                } else {
//
//                                    session.setAttribute("_errorGeneral", "User status is not Active...!!!");
//                                    response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer.jsp");
//                                    return;
                                }
                            }
                        }

                    }
                } else {
                    result = "error";
                    message = "No Tokens assigned to user...!!!";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
//                    json.put("_url", url);
                    out.print(json);
                    out.flush();
//                    session.setAttribute("_errorGeneral", "Invalid User!!!");
//                    response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer.jsp");
//                    return;
                }

            } else {
                result = "error";
                message = "User not found...!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
//                return;
//                session.setAttribute("_errorOTPToken", "User not found!!!");
//                response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer.jsp");
//                return;
            }

            String[] data1 = new String[3];
            data1[0] = fromAccountNoConfirm;
            data1[1] = otherAccountConfirm;
            data1[2] = amountConfirm;

            List<String> dataString = new ArrayList<String>(Arrays.asList(data1));

            AxiomStatus as = axWraper.sendNotification(sessionId, userObj.getRssUser().getUserId(), AxiomWrapper.OUTOFBOUND_TOKEN, dataString, null);
            
//AxiomStatus as1=axWraper.sendNotification(sessionId, userObj.getRssUser().getUserId(),AxiomWrapper. , dataString, null);
            if (as.getErrorcode() == 0) {
                json.put("_result", "success");
                json.put("_message", "One Time Password has been sent successfully");
                out.print(json);
                out.flush();
//                session.setAttribute("_errorOTPToken", "One Time Password has been sent successfully");
//                response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer.jsp");
                return;
            } else {
                json.put("_result", "error");
                json.put("_message", "Could not send One Time Password");
                out.print(json);
                out.flush();
//                    session.setAttribute("_errorOTPToken", "Could not send One Time Password");
//                response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer.jsp");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
