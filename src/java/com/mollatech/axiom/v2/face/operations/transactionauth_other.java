/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.operations;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.AxiomLocationImpl;
import com.mollatech.axiom.bridge.crypto.Location;
//import com.mollatech.axiom.connector.mobiletrust.Location;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomData;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.InitTransactionPackage;
import com.mollatech.axiom.v2.core.rss.RemoteSigningInfo;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.TransactionStatus;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
//import com.mollatech.internal.handler.geolocation.AxiomLocationImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Random;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
//@WebServlet(name = "transactionauth", urlPatterns = {"/transactionauth"})
public class transactionauth_other extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("inside transactionauth()");
        Enumeration parameterList = request.getParameterNames();
        while (parameterList.hasMoreElements()) {
            String sName = parameterList.nextElement().toString();

            String[] sMultiple = request.getParameterValues(sName);
            if (1 >= sMultiple.length) // parameter has a single value. print it.
            {
                System.out.println(sName + " = " + request.getParameter(sName));
            } else {
                for (int i = 0; i < sMultiple.length; i++) // if a paramater contains multiple values, print all of them
                {
                    System.out.println(sName + "[" + i + "] = " + sMultiple[i]);
                }
            }
        }
        System.out.println("after printing all params!!!!");

        String fromAccountNoConfirm = (String) request.getSession().getAttribute("fromAccountNo");
        String beneficiaryBankConfirm = (String) request.getSession().getAttribute("beneficiaryBank");
        String beneficiaryNameConfirm = (String) request.getSession().getAttribute("beneficiaryName");
        String otherAccountConfirm = (String) request.getSession().getAttribute("otherAccount");
        String emailConfirm = (String) request.getSession().getAttribute("email");
        String amountConfirm = (String) request.getSession().getAttribute("amount");
        String paymentTypeConfirm = (String) request.getSession().getAttribute("paymentType");
        String remarksConfirm = (String) request.getSession().getAttribute("remarks");
        String paymentDescriptionConfirm = (String) request.getSession().getAttribute("paymentDescription");
        String firstProcessDateConfirm = (String) request.getSession().getAttribute("firstProcessDate");
        String channelid = (String) request.getSession().getAttribute("_channelid");
        String location = (String) request.getSession().getAttribute("location");
        String browser = (String) request.getSession().getAttribute("browser");
        String agent = (String) request.getSession().getAttribute("agent");
        String authtype = (String) request.getSession().getAttribute("authtype");
        String _locationChunk = (String) request.getSession().getAttribute("_locationChunk");
        int locationType = (Integer) request.getSession().getAttribute("locationType");
        String baseOs = "NA";
        if (agent.contains("Linux")) {
            baseOs = "Linux";
        } else if (agent.contains("Mac")) {
            baseOs = "Linux";
        } else if (agent.contains("Windows")) {
            baseOs = "Windows";
        }

        //response.setContentType("application/json");
        RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
        String sessionId = (String) request.getSession().getAttribute("_userSessinId");

        String sotp = request.getParameter("sotp");
        String dataToSign = request.getParameter("dataToSign");

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        try {
            String _lattitude = (String) request.getSession().getAttribute("_lattitude");
            String _longitude = (String) request.getSession().getAttribute("_longitude");

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.SignatureOTP);
            payload.put("locationChunk", _locationChunk);
            payload.put("locationType", locationType);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            HttpSession session = request.getSession(true);
            AxiomLocationImpl axiomLoc = new AxiomLocationImpl();
            Location loc = axiomLoc.getLocation(_lattitude, _longitude);

            if (loc == null) {
                loc = axiomLoc.getLocationByIp(request.getRemoteAddr());
            }
            if (loc != null) {
                session.setAttribute("_Location", loc);
            }

            session.setAttribute("_errorGeoFence", "Not Checked");
            session.setAttribute("_errorOTPToken", "Not Checked");
            session.setAttribute("_errorRSS", "Not Checked");
            session.setAttribute("_errorGeneral", "Failed");

            String result = "success";
            String message = "successful verified....";
            String url = "transactionconfirmation.jsp";
            if (authtype.equals("OTP")) {
                if (sotp == null || sotp.isEmpty()) {
                    session.setAttribute("_errorGeneral", "Signature OTP was not provided!!!");
                    response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                    return;
                }
            }
            if (_longitude == null || _lattitude == null) {
                strPayLoad = null;
            }

            int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();

//            if (sessionId == null) {
//                result = "error";
//                message = "Please Login again your session has expired";
//                url = "index.jsp";
//                json.put("_result", result);
//                json.put("_message", message);
//                json.put("_url", url);
//                out.print(json);
//                out.flush();
//                return;
//            }
            AxiomCredentialDetails axiomCredentialDetails = null;
            if (userObj != null) {

                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.PASSWORD) {
                                if (axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                    retValue = 0;
                                    break;
                                } else {
                                    //result = "error";
                                    //message = "User Status is not Active...!!!";
//                                    url = "index.jsp";
//                                    json.put("_result", result);
//                                    json.put("_message", message);
//                                    json.put("_url", url);
//                                    out.print(json);
//                                    out.flush();
//                                    return;
                                    session.setAttribute("_errorGeneral", "Token status is invalid!!!");
                                    response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                                    return;
                                }
                            }
                        }

                    }
                } else {
//                    result = "error";
//                    message = "User not found...!!!";
//                    url = "index.jsp";
//                    json.put("_result", result);
//                    json.put("_message", message);
//                    json.put("_url", url);
//                    out.print(json);
//                    out.flush();
                    session.setAttribute("_errorGeneral", "Invalid User!!!");
                    response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                    return;
                }
                if (authtype.equals("PUSH")) {
                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                    SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm:ss a");
                    Random randomGenerator = new Random();
                    int randomInt = randomGenerator.nextInt(10000000);
                    InitTransactionPackage package1 = new InitTransactionPackage();
                    byte[] authId = Base64.encode(("" + randomInt).getBytes());
                    String strauthId = new String(authId);
                    package1.setAuthid(strauthId);
                    package1.setChannelid(channelid);
                    package1.setExpirytimeInMins(3);
                    package1.setFriendlyMsg("Transaction");
                    JSONObject json1 = new JSONObject();
//                    package1.setMessage("you are transfer " + amountConfirm + " USD to " + otherAccountConfirm + " from IP: " + request.getRemoteAddr() + " with Location: " + location + " browser Name: " + browser + " ,OS: " + baseOs + " At Time: " + new Date());
                    json1.put("_servermsg", "Trasaction of " +amountConfirm+" USD from your Account number "+fromAccountNoConfirm+" to "+otherAccountConfirm);
                    json1.put("_ip", request.getRemoteAddr());
                    json1.put("_location", location);
                    json1.put("_browser", browser);
                    json1.put("_os", baseOs);
                    String currentDate = dateformat.format(new Date());
                    String currentTime = timeformat.format(new Date());
                    json1.put("_time", "" + currentTime);
                    json1.put("_date", "" + currentDate);
                    package1.setMessage(json1.toString());
                    package1.setSessionid(sessionId);
                    package1.setType(1);
                    package1.setUserid(userObj.getRssUser().getUserId());
                    AxiomStatus status = axWraper.initTransaction(package1, strPayLoad);
                    if (status.getErrorcode() != 0) {
                        session.setAttribute("_errorGeneral", status.getError());
                        response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                        return;
                    }
                    TransactionStatus Tstatus = axWraper.getStatus(sessionId, strauthId, strPayLoad);
                    int time = 0;
                    String txStatus = "Not Approved";
                    String QAStatus = "Not Answered";
                    while (time < 300) {
                        Thread.sleep(5000);
                        time += 5;
                        Tstatus = axWraper.getStatus(sessionId, strauthId, null);
                        if (package1.getQuestion() != null) {
                            if (Tstatus.getTransactionStatus().equalsIgnoreCase("DENIED")) {
                                break;
                            }
                            if (!Tstatus.getQuestionAndAnswerstatus().equalsIgnoreCase("pending") && Tstatus.getTransactionStatus().equalsIgnoreCase("APPROVED")) {
                                break;
                            }
                        } else {
                            if (!Tstatus.getTransactionStatus().equalsIgnoreCase("pending")) {
                                break;
                            }
                        }

                    }
                    txStatus = Tstatus.getTransactionStatus();
                    if (Tstatus.getQuestionAndAnswerstatus().equalsIgnoreCase("RESPONDED")) {
                        QAStatus = Tstatus.getResponse();
                    }
                    if (!Tstatus.getTransactionStatus().equalsIgnoreCase("APPROVED")) {
                        session.setAttribute("_errorGeneral", "You dont approved the Transaction");
                        response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                        return;
                    }
                    if (package1.getQuestion() != null) {
                        if (!Tstatus.getQuestionAndAnswerstatus().equalsIgnoreCase("RESPONDED") || Tstatus.getResponse() == null) {
                            session.setAttribute("_errorGeneral", "You dont responded the Transaction");
                            response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                            return;
                        }
                    }
                    session.setAttribute("txStatus", txStatus);
                    session.setAttribute("QAStatus", QAStatus);
//                    session.setAttribute("_errorGeoFence", "Passed");
//                    session.setAttribute("_errorOTPToken", "Passed");
//                    session.setAttribute("_errorRSS", "Passed. Your Digital Signature is <i><b>" + aData.getSignature() + "</b></i>");
                    session.setAttribute("_errorGeneral", "Transaction was successfully executed!!!");
                    response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                    return;
                }
            } else {
//                result = "error";
//                message = "User not found...!!!";
//                url = "index.jsp";
//                json.put("_result", result);
//                json.put("_message", message);
//                json.put("_url", url);
//                out.print(json);
//                out.flush();
//                return;
                session.setAttribute("_errorGeneral", "User not found!!!");
                response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                return;
            }

            String[] data1 = new String[3];
            data1[0] = fromAccountNoConfirm;
            data1[1] = otherAccountConfirm;
            data1[2] = amountConfirm;

//            String hashCodeForSigningNew = axWraper.GenerateSignatureCode(sessionId, data1, null);
//            String hashCodeForSigning = request.getParameter("hashCodeForSigning");
//            if (hashCodeForSigningNew.compareTo(hashCodeForSigning) != 0) {
//                session.setAttribute("_errorGeneral", "Signing Data did not match!!!");
//                session.setAttribute("_errorOTPToken", "Signing Data did not match!!!");
//                response.sendRedirect("/mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
//                return;
//            }
            VerifyRequest vReq = new VerifyRequest();
            AxiomStatus aStatus = null;
            if (authtype.equals("OTP")) {
                vReq.setCategory(axWraper.OTP_TOKEN_SOFTWARE_MOBILE);
                vReq.setCredential(sotp);
                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
            }
            if (authtype.equals("PKI")) {
                VerifyRequest vReq1 = new VerifyRequest();
                String siggenerated = (String) request.getParameter("siggenerated");
                String verifyData = (String) request.getParameter("dataToSign");
                vReq1.setCategory(6);
                vReq1.setSubcategory(1);
                vReq1.setPkisignData(siggenerated);
                vReq1.setPkiplainData(verifyData);
                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq1, null);
                session.setAttribute("_errorGeoFence", "Passed");
                session.setAttribute("_errorOTPToken", "Passed");
                session.setAttribute("_errorGeneral", "Transaction was successfully executed!!!");
                response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
            }

            if (aStatus.getErrorcode() == 0) {

                session.setAttribute("_errorGeoFence", "Passed");
                session.setAttribute("_errorOTPToken", "Passed");
                session.setAttribute("_errorGeneral", "Transaction was successfully executed!!!");
                response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");

            } else {

                vReq.setCategory(AxiomWrapper.SignatureOTP);
                vReq.setCredential(sotp);
                for (int i = 0; i < data1.length; i++) {
                    vReq.getSigningData().add(data1[i]);
                }

                RemoteSigningInfo rInfo = new RemoteSigningInfo();
                rInfo.setBAddTimestamp(true);
                rInfo.setBNotifyUser(true);
                rInfo.setBReturnSignedDocument(true);
                rInfo.setDataToSign(dataToSign);
                rInfo.setDesignation("");
                rInfo.setName(userObj.getRssUser().getUserName());
                rInfo.setReason("Transaction");
                rInfo.setReferenceId(null);
                rInfo.setType(1);
                rInfo.setClientLocation("pune");

                AxiomData aData = axWraper.ValidateUserAndSignTransaction(sessionId,
                        userObj.getRssUser().getUserId(),
                        vReq, rInfo, true,
                        null, strPayLoad);

                if (aData.getIErrorCode() != 0) {

                    /* GEO FENCING */
                    if (aData.getIErrorCode() == -14
                            || aData.getIErrorCode() == -7
                            || aData.getIErrorCode() == -10
                            || aData.getIErrorCode() == -11
                            || aData.getIErrorCode() == -12
                            || aData.getIErrorCode() == -13) {
                        session.setAttribute("_errorGeoFence", "Failed. You are either roaming without prior approval? Please contact us to enable roaming.");
                    }

                    /* OTP */
                    if (aData.getIErrorCode() == -9
                            || aData.getIErrorCode() == -16
                            || aData.getIErrorCode() == -17
                            || aData.getIErrorCode() == -22) {
                        session.setAttribute("_errorOTPToken", "Failed. Singautre OTP is invalid!!!");
                    }

                    System.out.println(" The error code is " + aData.getIErrorCode());

                    if (aData.getIErrorCode() == -27) {
                        session.setAttribute("_errorOTPToken", "Failed. Your digital certificate is not present!!!");
                    } else {
                        session.setAttribute("_errorOTPToken", "Failed. Your digital certificate is not invalid, either it is expired or revoked!!!");
                    }

                    session.setAttribute("_errorGeneral", "Failed");

                    response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
                    return;
                } else {
                    session.setAttribute("_errorGeoFence", "Passed");
                    session.setAttribute("_errorOTPToken", "Passed");
                    session.setAttribute("_errorRSS", "Passed. Your Digital Signature is <i><b>" + aData.getSignature() + "</b></i>");
                    session.setAttribute("_errorGeneral", "Transaction was successfully executed!!!");
                    response.sendRedirect("mybankonline/confirm_with_otp_files/ibg_fund_transfer_ack.jsp");
//                url = "index.jsp";
//                result = "error";
//                message = aData.getSErrorMessage();
//                json.put("_result", result);
//                json.put("_message", message);
//                json.put("_url", url);
//                out.print(json);
//                out.flush();
//                return;
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
