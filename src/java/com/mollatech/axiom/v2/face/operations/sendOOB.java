/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.operations;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
//@WebServlet(name = "sendOOB", urlPatterns = {"/sendOOB"})
public class sendOOB extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       /*
         *********************************************
         added by manoj
        
         */
        response.setContentType("application/json");
        RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssUserCerdentials");
        String sessionId = (String) request.getSession().getAttribute("_userSessinId");
       String _signaturekey = (String) request.getSession().getAttribute("_signaturekey");
       String _ammount = (String) request.getSession().getAttribute("_ammount");
       String _source = (String) request.getSession().getAttribute("_source");
       String _dest = (String) request.getSession().getAttribute("_dest");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        try {

            String _sotp = request.getParameter("_otp");
            
            String dataToSign = "I here by confirm that, I'm doing transaction of Amount : "+_ammount +" From Account No. : "+_source +" To Account No. : "+_dest;
           // String _datatosign = request.getParameter("_dataToSign");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.OTP); 
            
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }
           
            String result = "success";
            String message = "Signature OTP Send Successfully";
            String url = "transactionvalidation.jsp";

//           

            if (_longitude == null || _lattitude == null) {
                strPayLoad = null;
            }

             int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
            sessionId = axWraper.OpenSession(channelid, remotelogin, password);
            
           

            if (sessionId == null) {
                result = "error";
                message = "Please Login again your session has expired";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            
             AxiomCredentialDetails axiomCredentialDetails = null;
             if (userObj != null) {

                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.OTP) {
                                if (axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                    retValue = 0;
                                    break;
                                } else {
                                    result = "error";
                                    message = "User status is not Active...!!!";
                                    url = "index.jsp";
                                    json.put("_result", result);
                                    json.put("_message", message);
                                    json.put("_url", url);
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }

                    }
                } else {
                    result = "error";
                    message = "User not found...!!!";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    return;
                }
            } else {
                result = "error";
                message = "User not found...!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

           
            
            AxiomWrapper aAWrapper  = new AxiomWrapper();
            
            String []data = new String[3];
            data[0]= _source;
            data[1] = _dest;
            data[2] = _ammount;
            int res = -1;
//            int res = aAWrapper.SendSignatureOneTimePassword(sessionId, url, data);
         
           
           
           

            
            if (res == 0) {
               
               url = "transactionvalidation.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            } else {
                url = "transactionvalidation.jsp";
                result = "error";
                message = "Failed to send otp contact your bank.";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
