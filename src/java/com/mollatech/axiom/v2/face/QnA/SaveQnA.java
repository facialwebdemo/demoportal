/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.QnA;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomChallengeResponse;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomQuestionAndAnswer;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.RssUserDetails;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ash
 */
public class SaveQnA extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AxiomChallengeResponse challengeResponse = (AxiomChallengeResponse) request.getSession().getAttribute("challengeResponse");
        String sessionId = request.getSession().getAttribute("_userSessinId").toString();
        RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
        JSONObject json = new JSONObject();
        AxiomWrapper wrapper = new AxiomWrapper();
        JSONObject payload = new JSONObject();
        String _locationChunk = "";
        HttpSession session = request.getSession();
        String channelid = session.getAttribute("_channelid").toString();
        String location = session.getAttribute("location").toString();
        String browser = session.getAttribute("browser").toString();
        String agent = session.getAttribute("agent").toString();
        String _lattitude = session.getAttribute("_lattitude").toString();
        String _longitude = session.getAttribute("_longitude").toString();
       _locationChunk= session.getAttribute("_locationChunk").toString();
        PrintWriter out = response.getWriter();
        int locationType = 2;
        payload.put("ip", request.getRemoteAddr());
        payload.put("longi", _longitude);
        payload.put("latti", _lattitude);
        payload.put("txType", AxiomWrapper.REGISTERUSER);
        payload.put("locationChunk", _locationChunk);
        payload.put("locationType", locationType);
        byte[] bytePayload = Base64.encode(payload.toString().getBytes());
        String strPayLoad = new String(bytePayload);
        if (_lattitude.equalsIgnoreCase("latitude")) {
            _lattitude = null;
            _longitude = null;
            strPayLoad = null;
        }
        try {
            AxiomChallengeResponse acr = new AxiomChallengeResponse();
            RssUserCerdentials cerdentials = new RssUserCerdentials();
            RssUserDetails details = new RssUserDetails();
            cerdentials.setRssUser(userObj.getRssUser());
            cerdentials.setGeoCredentials(userObj.getGeoCredentials());
            cerdentials.setTwoWayAuthStatus(userObj.getTwoWayAuthStatus());
            AxiomCredentialDetails acd = new AxiomCredentialDetails();
            AxiomChallengeResponse axiomChallengeResponse = new AxiomChallengeResponse();
            for (int i = 0; i < challengeResponse.getWebQAndA().size(); i++) {
                AxiomQuestionAndAnswer questionAndAnswer = challengeResponse.getWebQAndA().get(i);
                String ans = request.getParameter("ans" + questionAndAnswer.getQuestionid());
                questionAndAnswer.setAnswerByUser(ans);
                axiomChallengeResponse.getWebQAndA().add(questionAndAnswer);
            }
            acd.setQas(axiomChallengeResponse);
            acd.setCategory(AxiomWrapper.CHALLAEGE_RESPONSE);
            cerdentials.getTokenDetails().add(acd);
            AxiomStatus status = wrapper.AssignCredential(sessionId, cerdentials, null, strPayLoad);
            if (status.getErrorcode() == 0) {
                json.put("result", "success");
                json.put("message", "Questions And Answers Are Submitted Successfully.");
            } else {
                json.put("result", "error");
                json.put("message", "Error In Submitting Questions And Answers.");
            }
        } catch (Exception ex) {
            try {
                json.put("result", "error");
                json.put("message", "Error In Submitting Questions And Answers.");
                ex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
