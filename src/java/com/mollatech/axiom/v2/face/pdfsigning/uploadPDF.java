/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.pdfsigning;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.Operators;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.File;
import java.io.IOException;
//import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import java.io.PrintWriter;
import java.nio.channels.Channels;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;


public class uploadPDF extends HttpServlet {
 final String itemTypeOp = "UPLOADPDF";
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
//         String hwserialno = request.getParameter("_emailId");
//         String _otp = request.getParameter("_otp");
//              String filename = request.getParameter("fileXMLToUploadEAD");
//          
//             
//             System.out.println(hwserialno+" "+_otp+" "+filename);
//        String _password = request.getParameter("_password");
        String strError = "";
        String sep = System.getProperty("file.separator");
        //String saveFile = "";
        String savepath = "";
       
        String result = "success";

        String message = "File Upload sucessfully";
        //  String counter  = "0";

        JSONObject json = new JSONObject();
        //AxiomWrapper ax=new AxiomWrapper();
        
        savepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive");
savepath=savepath+sep;

        String strUniqueID = new String(request.getSession().getId());
        strUniqueID = strUniqueID.substring(0, 8);

        String optionalFileName = "";
        
        String strfilename = "";
        FileItem fileItem = null;
        String[] files = new String[1];	 // file names
        String dirName = savepath;
        int retValue = 0;

        int i = 0;

//        AuditManagement audit = new AuditManagement();

        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator it = fileItemsList.iterator();

                while (it.hasNext()) {
                    FileItem fileItemTemp = (FileItem) it.next();
                    if (fileItemTemp.isFormField()) {
                        if (fileItemTemp.getFieldName().equals("filename")) {
                            optionalFileName = fileItemTemp.getString();
//                            request.setAttribute("filename", optionalFileName);
                             
                        } else {
                            System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                        } 
                    } else {
                        fileItem = fileItemTemp;    
                    } 
                    if (fileItem != null) {
                        String fileName = fileItem.getName().trim(); 
                        fileName=fileName.replace(" ", "");
                        request.getSession().setAttribute("filename", fileName.trim());
                        
                         if(fileItem.getSize() == 0){
                            strError = "Please Select File To Upload...!!!";
                            result = "error";
                            try { json.put("result", result);
                            json.put("message", strError);
                            }catch(Exception e){e.printStackTrace();}
                            //out.print(json);
                            out.print("{result:'"+result+"',message:'"+strError+"'}");
                            out.flush();
                            return;
                        }
                        if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000*5) { // size cannot be more than 65Kb. We want it light.
                            if (optionalFileName.trim().equals("")) {
                                fileName = FilenameUtils.getName(fileName);
                            } else {
                                fileName = optionalFileName;
                            }
                            files[i++] = dirName + fileName;
                            File saveTo = new File(dirName + fileName.trim());

                            //saveFile = fileName;
                            //String secret = null;
                            AxiomStatus axiom[] = null;
                            try {
                                fileItem.write(saveTo);
                               strfilename = saveTo.getName();
                                
                               //upload document to server
//                                byte[] doc=fileItem.get();
//                                AxiomWrapper ax=new AxiomWrapper();
//                                ax.uploadPDFDoc(sessionId,doc);
                                
                                
                               

//                                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                                        request.getRemoteAddr(),
//                                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                                        "Upload PDF File", result, retValue, "PDF Signing Management",
//                                        "","",
//                                        itemTypeOp, operatorS.getOperatorid());

//                              
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            strError = "Error: " + fileName + " size is more than 5MB. Please upload lighter files.";
                            result = "error";
                            json.put("_result", result);
                            json.put("_message", strError);
//                            json.put("_failed", failed);
//                            json.put("_success", success);
                            out.print(json);
                            out.flush();
                            return;
                        }
                    } else {
                        result = "error";
                        message = "Fail To Upload !!";
                        json.put("_result", result);
                        json.put("_message", message);
//                        json.put("_failed", failed);
//                        json.put("_success", success);
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            } catch (Exception ex) {
                //Logger.getLogger(uploadHardwareOTPTokens.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        } else {
            result = "error";
            message = "Fail To Upload!!";
            try {
            json.put("_result", result);
            json.put("_message", message);
            } catch(Exception e){
            
        }
//            json.put("_failed", failed);
//            json.put("_success", success);
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("filename", strfilename);
//            json.put("_failed", failed);
//            json.put("_success", success);


        } catch(Exception e){
            
        }finally {
            out.print(json);
            out.flush();
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
