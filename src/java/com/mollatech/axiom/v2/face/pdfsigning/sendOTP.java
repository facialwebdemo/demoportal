/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.pdfsigning;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.Operators;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.Channels;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author parimal
 */
public class sendOTP extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    
    
    response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String resultString = "Failed";
        String result = "success";
        String message = "One Time Passowrd Send Successfully....";
        BufferedWriter bufferedWriter = null;
        try {
                  Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
      
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
//            String sessionId = (String) request.getSession().getAttribute("_apSessionID");

            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String _type = request.getParameter("type");
            String email = userObj.getRssUser().getEmailid();
            int tokentype=-1;
            if(_type==null && _type.equals("undefined")==true){
                
                
                
            }else{
                tokentype=Integer.parseInt(_type);
            }
             AxiomWrapper axWrapper = new AxiomWrapper();
          String sessionId=null;
        if(request.getSession().getAttribute("sessionid")==null){
        String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
             sessionId = axWrapper.OpenSession(channelid, remotelogin, password);
        }else{
              sessionId = (String )request.getSession().getAttribute("sessionid");
        }
        
            RssUserCerdentials rssUser= axWrapper.getRSSUser(sessionId, email, 3, null, null);
            if(rssUser!=null){
            
                AxiomStatus as = axWrapper.sendNotification(sessionId, rssUser.getRssUser().getUserId(), tokentype, null, null);
                
            if (as.getErrorcode() == 0) {
                    json.put("_result", "success");
                    json.put("_message", "One Time Password has been sent successfully");
                } else {
                    json.put("_result", "error");
                    json.put("_message", "Could not send One Time Password");
                } 
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            out.print(json);
            out.flush();
        }

    
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
