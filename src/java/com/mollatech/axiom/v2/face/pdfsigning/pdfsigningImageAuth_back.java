/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.pdfsigning;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.common.AxiomProtectConnector;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomException;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.AxiomTokenData;
import com.mollatech.axiom.v2.core.rss.Operators;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.web.token.SetAuthType;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.nio.channels.Channels;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author parimal
 */
public class pdfsigningImageAuth_back extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, AxiomException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String resultString = "Failed";
        String result = "success";
        String message = "Document signed Successfully....";
        BufferedWriter bufferedWriter = null;

        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
//        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
//        String sessionId = (String) request.getSession().getAttribute("_userSessinId");
        
        

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
         String sep = System.getProperty("file.separator");
        String _fileName = request.getParameter("_filename");
        String _emailid = request.getParameter("_emailId");
        String _otp = request.getParameter("_otp");
        String _referneceid = request.getParameter("_referneceid");
        String _location=request.getParameter("loc");
        String _allowedPhone=request.getParameter("_allowedphones");
        String _allowedCountry=request.getParameter("_allowedCountry");
         String _checkEmail=request.getParameter("_checkEmail");
        String _allowedPhoneCountry=_allowedPhone+_allowedCountry+_checkEmail;
        
        
        
           String _user_name = request.getParameter("savemepost");
            String user_name = _user_name.substring(_user_name.indexOf("src"), _user_name.length());
            String finalstring = user_name.substring(user_name.indexOf(",") + 1, user_name.length() - 2);
            //String imagepath = "E:\\imagefile.png";
            String imagepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") +sep+ "imagefile.png";
            byte[] data = Base64.decode(finalstring);
            try {
                FileOutputStream fos = new FileOutputStream(imagepath);
                fos.write(data);
                fos.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            String city=null;
            String country=null;
            if(_location.contains("undefined")){
                 city="Unknown";
            country="Unknown";
            }else{
            
            String[] temp=_location.split(",");
            
            if(temp!=null){
            city=temp[3];
            country=temp[4];
            }
            }
               
            
            
        AxiomWrapper axWrapper = new AxiomWrapper();
        String sessionId=null;
         String qrDataUrl="";
        if(request.getSession().getAttribute("sessionid")==null){
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
             qrDataUrl=LoadSettings.g_sSettings.getProperty("axiom.qrcodeurl");
        
             sessionId = axWrapper.OpenSession(channelid, remotelogin, password);
        }else{
              sessionId = (String )request.getSession().getAttribute("sessionid");
              
        }
//        AxiomStatus as = axWrapper.signPdfWithQR(sessionId, _fileName, _emailid, _otp, _referneceid, city, country,qrDataUrl,_allowedPhoneCountry);
//        if(as.getErrorcode()==0){
//            
//            json.put("_result", "success");
//            json.put("_message", "PDF Document signing Success!!");
//            out.print(json);
//            
//        }else{
//            
//            json.put("_result", "error");
//            json.put("_message", as.getRegCodeMessage());
//            out.print(json);
//        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (AxiomException ex) {
            Logger.getLogger(pdfsigningImageAuth_back.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (AxiomException ex) {
            Logger.getLogger(pdfsigningImageAuth_back.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
