/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.pdfsigning;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.common.AxiomProtectConnector;
import com.mollatech.axiom.v2.core.rss.AxiomException;
import com.mollatech.axiom.v2.core.rss.Remotesignature;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class upDateAutherizedUsers extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, AxiomException {
        
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String resultString = "Failed";
        String result = "success";
        String message = "Document signed Successfully....";
        String deviceid = request.getParameter("deviceid");
        AxiomWrapper axWrapper = new AxiomWrapper();
        String sessionId = null;
        String qrDataUrl = "";
        String refid = request.getParameter("referenceId");
        
        String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
        String password = LoadSettings.g_sSettings.getProperty("axiom.password");
        String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
        qrDataUrl = LoadSettings.g_sSettings.getProperty("axiom.qrcodeurl");
        sessionId = axWrapper.OpenSession(channelid, remotelogin, password);
        
        Remotesignature[] signInfoByUser = axWrapper.getAllSignatureByUserid(sessionId, refid, 2);
        Remotesignature remSign = signInfoByUser[0];
        if (remSign.getAllowedUsers() != null) {
            HashMap deviceH = (HashMap) AxiomProtectConnector.deserializeFromObject(new ByteArrayInputStream(remSign.getAllowedUsers()));
            if (deviceH != null) {
                String authpdf = (String) deviceH.get(deviceid);
                String authupdate = authpdf.replaceAll("NO", "YES");
                int res = axWrapper.updateRemoteSignaturesByRequest(sessionId, deviceid,refid, authupdate);
                
                if (res == 0) {
                    result = "success";
                    message = "Device Authenticated for this emailid";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    
                } else {
                    
                    result = "error";
                    message = "Device is not Authenticated for this emailid";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                }
                
            }
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (AxiomException ex) {
            Logger.getLogger(upDateAutherizedUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (AxiomException ex) {
            Logger.getLogger(upDateAutherizedUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
