/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.pdfsigning;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.common.AxiomProtectConnector;
import com.mollatech.axiom.v2.core.rss.Remotesignature;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ideasventure
 */

public class pdfdownload extends HttpServlet {
   private static final int BUFSIZE = 4096;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception  {
        response.setContentType("text/html;charset=UTF-8");
        String _locationChunk=request.getParameter("_locationChunk");
         String userAgent = request.getHeader("User-Agent");
		  String _longitude = request.getParameter("_longitude");
        String _lattitude = request.getParameter("_longitude");
         String deviceDetailsFromRequest = "";
		  String ipAddress=request.getRemoteAddr();
        String email=request.getParameter("txtEmailId");
        String  emailChecked=(String)request.getSession().getAttribute("emailChecked");
            
        String filename = (String)request.getSession().getAttribute("pdfPath");
        Remotesignature remSign=(Remotesignature)request.getSession().getAttribute("remSignObj");
        String deviceId=(String) request.getSession().getAttribute("deviceid");
        
        if(emailChecked==null)
        {
            return;
        }
        
         AxiomWrapper ax = (AxiomWrapper) request.getSession().getAttribute("axWrapper");
        if (remSign.getDownloadrequests() != null) {
            HashMap scannedDetails = (HashMap) AxiomProtectConnector.deserializeFromObject(new ByteArrayInputStream(remSign.getDownloadrequests()));
            if (scannedDetails != null) {
                deviceDetailsFromRequest = (String) scannedDetails.get(deviceId);
                if (deviceDetailsFromRequest != null && !deviceDetailsFromRequest.equals("")) {
                     int count = Integer.parseInt(deviceDetailsFromRequest.split(",")[6]);
                    deviceDetailsFromRequest = deviceDetailsFromRequest.split(",")[0] + "," + deviceDetailsFromRequest.split(",")[1] + "," + deviceDetailsFromRequest.split(",")[2] + "," + deviceDetailsFromRequest.split(",")[3] + "," + deviceDetailsFromRequest.split(",")[4] +","+deviceDetailsFromRequest.split(",")[5]+","+(count + 1);
                    int o = ax.addDownloadRequest(request.getSession().getAttribute("sessionId").toString(), (String) request.getSession().getAttribute("deviceid"), (String) request.getSession().getAttribute("refid"), deviceDetailsFromRequest);
                } else {
                    

                     deviceDetailsFromRequest = _longitude + "," + _lattitude + "," + email + "," +_locationChunk+"," +new Date().toString() +","+ipAddress+ "," + 1;
                    int o = ax.addDownloadRequest(request.getSession().getAttribute("sessionId").toString(), (String) request.getSession().getAttribute("deviceid"), (String) request.getSession().getAttribute("refid"), deviceDetailsFromRequest);
                }

            }
        } else {

             deviceDetailsFromRequest = _longitude + "," + _lattitude + "," + email +","+_locationChunk+"," +new Date().toString() +","+ipAddress+ "," + 1;
            int o = ax.addDownloadRequest(request.getSession().getAttribute("sessionId").toString(), (String) request.getSession().getAttribute("deviceid"), (String) request.getSession().getAttribute("refid"), deviceDetailsFromRequest);
        }



        
        
  try{
      if(_locationChunk!=null)
        {
           if(!remSign.getAllowedPhones().contains(_locationChunk))
          {
             response.sendRedirect("./AxiomPdfAuth.jsp&CountryO="+"LOCATIONFAILED");
             return;
          }
            
        }     
      if(emailChecked.equals("NO") ||emailChecked.equals("NOTRECIEVED") )
      {
      
          if(email!=null)
          {
             
//                AuthorisationDetailsforPdf authReq= new AuthorisationDetailsforPdf();
//                authReq.setEmailId(email);
//                authReq.setIsEmailVerified("NO");
//                authReq.setOs(request.getSession().getAttribute("_phoneos").toString());
//                GregorianCalendar c = new GregorianCalendar();
//                 c.setTime(new Date());
//                  XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
          String  authReq=email+"+"+"NO"+"="+request.getSession().getAttribute("_phoneos").toString()+"="+new Date().toString();
             int o=  ax.updateRemoteSignaturesByRequest(request.getSession().getAttribute("sessionId").toString(),(String)request.getSession().getAttribute("deviceid"), (String)request.getSession().getAttribute("refid"), authReq);
          }

      }   
       else
      {
          
            String filepath = filename;
            File file = new File(filepath);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(filepath);
            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(filepath)).getName();
           // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }
            in.close();
            outStream.close();
      }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        finally {
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           processRequest(request, response);
       } catch (Exception ex) {
           Logger.getLogger(pdfdownload.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           processRequest(request, response);
       } catch (Exception ex) {
           Logger.getLogger(pdfdownload.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
