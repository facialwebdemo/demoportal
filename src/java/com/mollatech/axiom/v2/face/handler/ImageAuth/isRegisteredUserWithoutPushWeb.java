/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.ImageAuth;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.EasyCheckInSessions;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.web.token.SetAuthType;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class isRegisteredUserWithoutPushWeb extends HttpServlet {

    public final int ACTIVE_STATUS = 0;
    public final int COMPLETED_STATUS = 1;
    public final int SUSPENDED_STATUS = -1;
    public final int EXPIRED_STATUS = -2;
    public final int PENDING_STATUS = 0;

//      easylogin session 
    public static final int EEXPIRED_STATUS = -2;
    public final int EREJECTED_STATUS = -1;
    public final int ENA_STATUS = 0;
    public static final int EAPPROVED_STATUS = 1;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        String ServerResponse = "";
        String _locationChunk = "";
        int type = (Integer) request.getSession().getAttribute("type");
        try {
         
            
            String _userName = request.getParameter("username");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");
            String location = request.getParameter("location");
            String browser = request.getParameter("browser");
            String agent = request.getParameter("agent");
            String ip = request.getRemoteAddr();
            _locationChunk = request.getParameter("_locationChunk");
            String googleAddress = request.getParameter("_googleAddress");
            int locationType = 2;
            String baseOs = "NA";
            if (agent.contains("Linux")) {
                baseOs = "Linux";
            } else if (agent.contains("Mac")) {
                baseOs = "Linux";
            } else if (agent.contains("Windows")) {
                baseOs = "Windows";
            }
            System.out.println(_userName);
            System.out.println(_lattitude);
            System.out.println(_longitude);
            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.REGISTERUSER);
            payload.put("locationChunk", _locationChunk);
            payload.put("locationType", locationType);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }

            String result = "success";
            String message = "successful credential verification....";
            //String url = "validation.jsp";
            String url = "";

            if (_userName == null) {
                result = "error";
                message = "Invalid Credentials!! 1st";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");

            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
            String sessionId = axWraper.OpenSession(channelid, remotelogin, password);

            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_USERID, strPayLoad);
            AxiomCredentialDetails axiomCredentialDetails = null;
            AxiomCredentialDetails axiomCredentalSecurePhrase = null;
            if (userObj != null) {
                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SECURE_PHRASE) {
                                axiomCredentalSecurePhrase = axiomCredentialDetails;
                            }
                        }
                    }
                }
            }
            HttpSession session = request.getSession(true);
            if (axiomCredentalSecurePhrase != null) {
                session.setAttribute("_rssDemoUserCerdentials", userObj);
                session.setAttribute("_userSessinId", sessionId);
                session.setAttribute("_username", userObj.getRssUser().getUserName());
                session.setAttribute("_userID", userObj.getRssUser().getUserId());
                boolean flag = false;
                for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                    axiomCredentialDetails = userObj.getTokenDetails().get(i);
                    if (axiomCredentialDetails != null) {
                        if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN
                                && axiomCredentialDetails.getSubcategory() == AxiomWrapper.SW_WEB_TOKEN) {
                            request.getSession().setAttribute("_apWebTokenStatus", axiomCredentialDetails.getStatus());
                            if (axiomCredentialDetails.getStatus() == 1) {
                                flag = true;
                            } else {
                                flag = false;
                            }

                            break;

                        }
                        if (type == SetAuthType.SIGN_TX) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.CERTIFICATE) {
                                flag = true;
                                break;
                            }
                        }

                        if (type == SetAuthType.QnA) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.CHALLAEGE_RESPONSE) {
                                flag = true;
                                break;
                            }
                        }

                        if (type == SetAuthType.EPIN) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.CHALLAEGE_RESPONSE) {
                                flag = true;
                                break;
                            }
                        }
                    }
                }
                if (type == SetAuthType.PUSH) {

                    url = "./2fa.jsp";
                } else if (type == SetAuthType.securePhrase) {

                    url = "./2fa.jsp";

                } else if (type == SetAuthType.WEBTOKEN) {
                    if (flag == true) {
                        url = "./2faa.jsp";
                    } else {
                        url = "./register_inlined.jsp";
                    }
                } //                else if (type == SetAuthType.PKI){
                //                    if (flag == true) {
                //                        url = "./2faa.jsp";
                //                    }
                //                }
                else if (type == SetAuthType.PDF_SIGNING) {
//                    if (flag == true) {
                    url = "./pdfsigningab.jsp";
//                    }
                } else if (type == SetAuthType.QnA) {

                    url = "./2fa.jsp";

                } else if (type == SetAuthType.esigner) {

                    url = "/face";

                } else if (type == SetAuthType.Tokens) {

                    url = "./allTokens.jsp";

                } else if (type == SetAuthType.SIGN_TX) {

                    url = "./2farss.jsp";

                } else if (type == SetAuthType.PDF_AUTH) {

                    url = "./pdfAuthNew.jsp";

                } else if (type == SetAuthType.LOC_AWARE_EASY_LOGIN) {
                    EasyCheckInSessions alogin = new EasyCheckInSessions();
//                    ApEasyloginsession alogin = new ApEasyloginsession();
                    alogin.setAppChannelid(channelid);
                    alogin.setAppIp(request.getRemoteAddr());
                    alogin.setAppSesssionId(sessionId);
                    alogin.setAppUserid(userObj.getRssUser().getUserId());
                    alogin.setRequesterIndex(0);
                    alogin.setStatus(ENA_STATUS);
                    alogin.setErrormsg("User not found");
                    alogin.setEasyloginId("");
                    alogin.setLocation(googleAddress);
                    alogin.setAgent(agent);
                    alogin.setLattitude(_lattitude);
                    alogin.setLongitude(_longitude);
                    alogin.setBrowser(browser);
                    GregorianCalendar c = new GregorianCalendar();
                    c.setTime(new Date());
                    XMLGregorianCalendar created = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                    alogin.setCreatedon(created);
                    EasyCheckInSessions easycheckSession= axWraper.addEasyLoginRequest(sessionId, alogin);
                    if(easycheckSession!=null){
                        System.out.println("easycheckSession added for user:"+easycheckSession.getAppUserid());
                    }else{
                        System.out.println("Unable to add easycheckin session");
                    }
                    AxiomStatus astatus = axWraper.verifyEasyLoginRequest(sessionId, userObj.getRssUser().getUserId(), _lattitude, _longitude);
                    if (astatus.getErrorcode() == 0) {
//                            com.mollatech.axiom.v2.core.rss.ApEasylogin easylogin = axWraper.getEasyLogin(sessionId, easysession.getEasyloginId());
                        url = "./home.jsp";
                        result = "success";
                        message = "The Easy login request approved!!!";
//                            ServerResponse = " Your Request is approved from " + easylogin.getDeviceinfo() + " with device id :" + easylogin.getDeviceid() + " from location Lattitude :" + easylogin.getLattitude() + " Longitude :" + easylogin.getLongitude() + " from IP address :" + easylogin.getIp();
                    } else {
                        url = "Easylogin.jsp";
                        result = "error";
                        message = astatus.getError();
                    }
                } else if (type == SetAuthType.EPIN) {
                    url = "./EPIN.jsp";
                }else if(type==SetAuthType.WEBPUSH)
                {
                    url = "./RegisterWebPush.jsp";
                }else if(type == SetAuthType.FBIO){
                    url = "./fBIOReg.jsp";
                }
                session.setAttribute("_channelid", channelid);
                session.setAttribute("location", location);
                session.setAttribute("browser", browser);
                session.setAttribute("agent", agent);
                session.setAttribute("_locationChunk", _locationChunk);
                session.setAttribute("locationType", locationType);
                session.setAttribute("_lattitude", _lattitude);
                session.setAttribute("_longitude", _longitude);
                json.put("_result", result);
                json.put("_message", message);
                json.put("_serverResponse", ServerResponse);
                json.put("_url", url);
                json.put("_cookieString", axiomCredentalSecurePhrase.getSecurePhraseID());
                out.print(json);
                out.flush();
                return;
            } else {
                result = "error";
                message = "User not found...!!!";
                json.put("_result", result);
                json.put("_serverResponse", ServerResponse);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
