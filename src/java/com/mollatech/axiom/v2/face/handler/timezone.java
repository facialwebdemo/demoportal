package com.mollatech.axiom.v2.face.handler;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

public class timezone extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String _gmt = request.getParameter("_gmt");
        //System.out.println("timezone() >> _gmt >> " + _gmt);

        String message = "timezone is null or invalid...";
        String result = "error";
        if(_gmt==null) {
            return;
        } else {
            HttpSession session = request.getSession(true);
            session.setAttribute("_apOprTimeZone",Float.valueOf(_gmt));
            response.setContentType("application/json");
            result = "success";
            message = "timezone defined successfully...";
        }
        
        try { json.put("_result", result);
        json.put("_message", message);
        }catch(Exception e){e.printStackTrace();}
        out.print(json);
        out.flush();
        return;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
