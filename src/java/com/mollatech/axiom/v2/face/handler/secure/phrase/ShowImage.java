package com.mollatech.axiom.v2.face.handler.secure.phrase;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jmimemagic.*;
import java.io.OutputStream;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

public class ShowImage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            response.setContentType("image/gif");

            RssUserCerdentials rssUserObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String sessionId = (String) request.getSession().getAttribute("_userSessinId");
            
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");
            
            JSONObject payload = new JSONObject();
            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.GETIMAGE);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }
            
            AxiomWrapper axWraper = new AxiomWrapper();
          
            AxiomCredentialDetails axiomCredentialDetails = null;
           if (rssUserObj.getTokenDetails() != null) {
                for (int i = 0; i < rssUserObj.getTokenDetails().size(); i++) {
                    axiomCredentialDetails = rssUserObj.getTokenDetails().get(i);

                    if (axiomCredentialDetails != null) {
                        if (AxiomWrapper.SECURE_PHRASE == axiomCredentialDetails.getCategory()) {
//                              axiomCredentialDetails.setStatus(istatus);
//                               rssUserObj.getTokenDetails().set(i, axiomCredentialDetails);
                            
                            byte[] base64Image = axiomCredentialDetails.getSecurePhraseImage();
                            if(base64Image == null){
                                return;
                            }
                            byte[] plainImage = Base64.decode(base64Image);
                            MagicMatch match = Magic.getMagicMatch(plainImage, true);

                            String mimeType = match.getMimeType();
                            if (mimeType.startsWith("image")) {
                                response.setContentType(mimeType);
                                response.setContentLength(plainImage.length);
                                OutputStream out = response.getOutputStream();
                                out.write(plainImage, 0, plainImage.length);
                            } else {
                                return;
                            }

                        } 
                    }
                }
            }

//            byte[] imageInBytes = QuickLZ.decompress(imageInBytesComp);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
//package com.ideasventure.mreach.manage;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//public class ShowImage extends HttpServlet {
//
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//        //PrintWriter out = response.getWriter();
//        OutputStream out = response.getOutputStream();
//        try {
//            String savepath = "";
//
//            savepath = System.getProperty("java.home");
//            if (savepath == null) {
//                savepath = System.getProperty("user.home");
//            }
//            savepath += System.getProperty("file.separator");
//            savepath += "mreach";
//            savepath += System.getProperty("file.separator");
//
//            String filename = request.getParameter("name");
//            if (filename == null || filename.length() == 0) {
//                return;
//            }
//            String filepath = savepath + filename;
//
//            ServletContext sc = getServletContext();
//            String mimeType = sc.getMimeType(filepath);
//            if (mimeType == null) {
//                sc.log("Could not get MIME type of " + filename);
//                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//                return;
//            }
//            response.setContentType(mimeType);
//
//            File file = new File(filepath);
//            FileInputStream fileStream = new FileInputStream(file);
//            try {
//                int fileSize = (int) file.length();
//                response.setContentLength(fileSize);
//                byte[] data = new byte[fileSize];
//                int bytesRead = 0;
//                while (bytesRead < fileSize) {
//                    bytesRead += fileStream.read(data, bytesRead, fileSize - bytesRead);
//                }
//                out.write(data, 0, fileSize);
//                //out.clearBuffer();
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                fileStream.close();
//                out.close();
//            }
//
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//
//
//
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//}

