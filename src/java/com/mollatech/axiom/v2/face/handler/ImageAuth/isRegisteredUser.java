/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.ImageAuth;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.InitTransactionPackage;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.TransactionStatus;
import com.mollatech.web.token.SetAuthType;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class isRegisteredUser extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm:ss a");
        String _locationChunk = "";
        int type = (Integer) request.getSession().getAttribute("type");
        try {
            String _userName = request.getParameter("username");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");
            String location = request.getParameter("location");
            String browser = request.getParameter("browser");
            String agent = request.getParameter("agent");
            _locationChunk = request.getParameter("_locationChunk");
            int locationType = 2;
            String baseOs = "NA";
            if (agent.contains("Linux")) {
                baseOs = "Linux";
            } else if (agent.contains("Mac")) {
                baseOs = "Linux";
            } else if (agent.contains("Windows")) {
                baseOs = "Windows";
            }
            System.out.println(_userName);
//            System.out.println(_userpassword);
            System.out.println(_lattitude);
            System.out.println(_longitude);
//            byte[] locationChunk = Base64.encode(locations.getBytes());
//            String strlocationChunk = new String(locationChunk);
            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.REGISTERUSER);
            payload.put("locationChunk", _locationChunk);
            payload.put("locationType", locationType);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }

            String result = "success";
            String message = "successful credential verification....";
            //String url = "validation.jsp";
            String url = "";

            if (_userName == null) {
                result = "error";
                message = "Invalid Credentials!! 1st";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
            String sessionId = axWraper.OpenSession(channelid, remotelogin, password);

            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(10000000);
            RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_USERID, strPayLoad);
            InitTransactionPackage package1 = new InitTransactionPackage();
            byte[] authId = Base64.encode(("" + randomInt).getBytes());
            String strauthId = new String(authId);
            package1.setAuthid(strauthId);
            package1.setChannelid(channelid);
            package1.setExpirytimeInMins(3);
            package1.setFriendlyMsg("Login");
//            package1.setMessage("you are trying to login to system from IP: " + request.getRemoteAddr() + " with Location: " + location + " browser Name: " + browser + " ,OS: " + baseOs + " At Time: " + new Date());
            JSONObject json1 = new JSONObject();
            json1.put("_servermsg", "Login for Trasaction from the Server : " + request.getRemoteAddr());
            json1.put("_ip", request.getRemoteAddr());
            json1.put("_location", location);
            json1.put("_browser", browser);
            json1.put("_os", baseOs);
            String currentDate = dateformat.format(new Date());
            String currentTime = timeformat.format(new Date());
            json1.put("_time", "" + currentTime);
            json1.put("_date", "" + currentDate);
            package1.setMessage(json1.toString());
            package1.setSessionid(sessionId);
            package1.setType(1);
            package1.setUserid(userObj.getRssUser().getUserId());
            AxiomStatus status = axWraper.initTransaction(package1, strPayLoad);
            if (status.getErrorcode() != 0) {
                result = "error";
                message = status.getError();
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            TransactionStatus Tstatus = axWraper.getStatus(sessionId, strauthId, strPayLoad);
            int time = 0;
            while (Tstatus.getTransactionStatus().equalsIgnoreCase("pending") && time < 180) {
                Thread.sleep(5000);
                time += 5;
                Tstatus = axWraper.getStatus(sessionId, strauthId, strPayLoad);
            }
            if (!Tstatus.getTransactionStatus().equalsIgnoreCase("APPROVED")) {
                result = "error";
                message = "You don't approved the transaction ";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            AxiomCredentialDetails axiomCredentialDetails = null;
            AxiomCredentialDetails axiomCredentalSecurePhrase = null;
            if (userObj != null) {
                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SECURE_PHRASE) {
                                axiomCredentalSecurePhrase = axiomCredentialDetails;
                            }
                        }
                    }
                }
            }
            HttpSession session = request.getSession();
            if (axiomCredentalSecurePhrase != null) {
                session.setAttribute("_rssDemoUserCerdentials", userObj);
                session.setAttribute("_userSessinId", sessionId);
                session.setAttribute("_username", userObj.getRssUser().getUserName());
                  session.setAttribute("_userID", userObj.getRssUser().getUserId());
                boolean flag = false;
                for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                    axiomCredentialDetails = userObj.getTokenDetails().get(i);
                    if (axiomCredentialDetails != null) {
                        if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN
                                && axiomCredentialDetails.getSubcategory() == AxiomWrapper.SW_WEB_TOKEN) {
                            flag = true;
                            break;
                        }
                    }
                }
//                if(type == SetAuthType.PKI){
//                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
//                    axiomCredentialDetails = userObj.getTokenDetails().get(i);
//                    if (axiomCredentialDetails != null) {
//                        if (axiomCredentialDetails.getCategory() == AxiomWrapper.PKI_SOFTWARE_TOKEN
//                               && axiomCredentialDetails.getStatus() != AxiomWrapper.TOKEN_STATUS_ACTIVE) {
//                            flag = true;
//                            break;
//                        }
//                    }
//                }
//                }

                if (type == SetAuthType.NOPASSWORD) {

                    url = "./home.jsp";
                } else if (type == SetAuthType.WITHOUTPUSH) {

                    url = "./2fa.jsp";

                } else if (type == SetAuthType.WEBTOKEN) {
                    if (flag == true) {
                        url = "./2faa.jsp";
                    }

//                } else if (type == SetAuthType.PKI) {
//                    if (flag == true) {
//                        url = "./2faa.jsp";
//                    }
                }
                session.setAttribute("_channelid", channelid);
                session.setAttribute("location", location);
                session.setAttribute("browser", browser);
                session.setAttribute("agent", agent);
                session.setAttribute("_locationChunk", _locationChunk);
                session.setAttribute("locationType", locationType);
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                json.put("_cookieString", axiomCredentalSecurePhrase.getSecurePhraseID());
                out.print(json);
                out.flush();
                return;
            } else {
                result = "error";
                message = "User not found...!!!";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
//            HttpSession session = request.getSession(true);
//            if (userObj != null) {
//
//                session.setAttribute("_apOprAuth", "yes");
//                session.setAttribute("_rssDemoUserCerdentials", userObj);
//                session.setAttribute("_userSessinId", sessionId);
//                session.setAttribute("_lattitude", _lattitude);
//                session.setAttribute("_longitude", _longitude);
//                session.setAttribute("_username", _userName);
//                json.put("_result", result);
//                json.put("_message", message);
//                json.put("_url", url);
//                out.print(json);
//                out.flush();
//                return;
//
//            } else {
//                result = "error";
//                message = "User not found...!!!";
////                    url = "index.jsp";
//                session.setAttribute("_username", _userName);
//                session.setAttribute("_rssDemoUserCerdentials", null);
////                session.setAttribute("_userSessinId", sessionId);
//                json.put("_result", result);
//                json.put("_message", message);
//                json.put("_url", url);
//                out.print(json);
//                out.flush();
//                return;
//
//            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
