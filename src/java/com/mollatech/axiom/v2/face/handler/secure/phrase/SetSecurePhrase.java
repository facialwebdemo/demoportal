/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.secure.phrase;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.AxiomLocationImpl;
import com.mollatech.axiom.bridge.crypto.Location;
import com.mollatech.axiom.bridge.crypto.UtilityFunctions;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class SetSecurePhrase extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            response.setContentType("application/json");

            PrintWriter out = response.getWriter();
            String result = "success";
            String message = "Secure phrase updated successfully!!!";

            JSONObject json = new JSONObject();
            int retValue = 0;

            String _select_color = request.getParameter("_select_color");
            String _securePhrase = request.getParameter("_securePhrase");
            String _xCoordinate = request.getParameter("_xCoordinate");
            String _yCoordinate = request.getParameter("_yCoordinate");
            int ixCoordinate = -1;
             int iyCoordinate = -1;
            if(_xCoordinate != null){
                ixCoordinate = Integer.parseInt(_xCoordinate);
            }
            if(_yCoordinate != null){
                iyCoordinate = Integer.parseInt(_yCoordinate);
            }
            
            String _category = request.getParameter("_category");
            int icategory = 0;
            if (_category != null) {
                icategory = Integer.parseInt(_category);
            }

            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");
            AxiomLocationImpl axiomLoc = new AxiomLocationImpl();
            JSONObject payload = new JSONObject();
            String strPayLoad = null;
            if (_lattitude.equals("latitude")) {
                Location location = axiomLoc.getLocationByIp(request.getRemoteAddr());

                if (location != null) {
                    payload.put("ip", request.getRemoteAddr());
                    payload.put("longi", location.longitude);
                    payload.put("latti", location.lattitude);
                    payload.put("txType", AxiomWrapper.LOGIN);
                    byte[] bytePayload = Base64.encode(payload.toString().getBytes());
                    strPayLoad = new String(bytePayload);
                }
            } else {

                payload.put("ip", request.getRemoteAddr());
                payload.put("longi", _longitude);
                payload.put("latti", _lattitude);
                payload.put("txType", AxiomWrapper.LOGIN);
                byte[] bytePayload = Base64.encode(payload.toString().getBytes());
                strPayLoad = new String(bytePayload);
            }

            RssUserCerdentials rssUserObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String sessionId = (String) request.getSession().getAttribute("_userSessinId");
            String sampleText = _securePhrase;
//            String sampleText1 = "";
//            String fileName = "Image";
            Font font = new Font("Arial", Font.PLAIN, 48);
            FontRenderContext frc = new FontRenderContext(null, true, true);
            java.awt.geom.Rectangle2D bounds = font.getStringBounds(sampleText, frc);
            int w = (int) bounds.getWidth();
            int h = (int) bounds.getHeight();
//            BufferedImage image = new BufferedImage(w, h + 100, BufferedImage.TYPE_INT_RGB);
            BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            int icolor = 0;
            if (_select_color != null) {
//                icolor = Integer.parseInt(_select_color);
                  g.setColor( new Color(
            Integer.valueOf( _select_color.substring( 1, 3 ), 16 ),
            Integer.valueOf( _select_color.substring( 3, 5 ), 16 ),
            Integer.valueOf( _select_color.substring( 5, 7 ), 16 ) ));
            }
//            if (icolor == AxiomWrapper.BLUE) {
//                g.setColor(Color.BLUE);
////                g.setColor(Color.red);
//            } else if (icolor == AxiomWrapper.GREEN) { 
//                g.setColor(Color.GREEN);
//            } else if (icolor == AxiomWrapper.RED) {
//                g.setColor(Color.RED);
//            } else if (icolor == AxiomWrapper.GRAY) {
//                g.setColor(Color.GRAY);
//            } else if (icolor == AxiomWrapper.ORANGE) {
//                g.setColor(Color.ORANGE);
//            } else if (icolor == AxiomWrapper.YELLOW) {
//                g.setColor(Color.YELLOW);
//            } else if (icolor == AxiomWrapper.MAGENTA) {
//                g.setColor(Color.MAGENTA);
//            }
//            g.fillRect(0, 0, w, h + 100);
            g.fillRect(0, 0, w, h);
            g.setColor(Color.WHITE);
            g.setFont(font);
//            g.drawString(sampleText, (float) bounds.getX(), (float) -bounds.getY() + 50);
            g.drawString(sampleText, (float) bounds.getX(), (float) -bounds.getY());
            g.dispose();
            String savepath = System.getProperty("catalina.home");
            if (savepath == null) {
                savepath = System.getenv("catalina.home");
            }
            savepath += System.getProperty("file.separator");
            savepath += "axiomv2-settings";
            savepath += System.getProperty("file.separator");
            savepath += "user-phrase";
            savepath += System.getProperty("file.separator");
            File outputfile = new File(savepath + rssUserObj.getRssUser().getUserName() + ".png");
            ImageIO.write(image, "png", outputfile);
String url = "./2fa.jsp";
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            BufferedImage img = ImageIO.read(new File(savepath + rssUserObj.getRssUser().getUserName() + ".png"));
            ImageIO.write(img, "png", baos);
            baos.flush();

            byte[] base64String = Base64.encode(baos.toByteArray());
            baos.close();

            AxiomCredentialDetails axiomCredentialDetails = null;
             String securephraseID = null;
            if (rssUserObj.getTokenDetails() != null) {
                for (int i = 0; i < rssUserObj.getTokenDetails().size(); i++) {
                    axiomCredentialDetails = rssUserObj.getTokenDetails().get(i);
                    if (axiomCredentialDetails != null) {
                        if (icategory == axiomCredentialDetails.getCategory()) {
                            axiomCredentialDetails.setSecurePhraseImage(base64String);
                            axiomCredentialDetails.setXCoordinate(ixCoordinate);
                            axiomCredentialDetails.setYCoordinate(iyCoordinate);
                            byte[] _securephraseID = UtilityFunctions.SHA1("" + base64String + ixCoordinate+ iyCoordinate);
                             securephraseID = new String(Base64.encode(_securephraseID));
                             axiomCredentialDetails.setSecurePhraseID(securephraseID);
                        } else {
                            rssUserObj.getTokenDetails().set(i, null);
                        }
                    }
                }
            }
            File f = new File(savepath + rssUserObj.getRssUser().getUserName() + ".png");
            f.delete();

            AxiomWrapper axWrapper = new AxiomWrapper();

            AxiomStatus axStatus = axWrapper.ChangeUserCredentials(sessionId, rssUserObj, null);
            if(axStatus.getErrorcode() == 0 ){
//                       Cookie userCookie = new Cookie(securephraseID, securephraseID);
//                      userCookie.setMaxAge(60*60*24*365); //Store cookie for 1 year
//                      response.addCookie(userCookie);
            }
            
            if (axStatus.getErrorcode() == 0) {
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url",url);
                out.print(json);
                out.flush();
                return;
            } else {
                result = "error";
                message = "Secure phrase updation failed!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
