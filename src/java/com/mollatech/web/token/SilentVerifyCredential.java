/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.web.token;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ashish
 */
public class SilentVerifyCredential extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        AxiomWrapper axwrap = new AxiomWrapper();

        String OTPTOVerify = null;
        String host = request.getContextPath().toLowerCase();

        OTPTOVerify = (String) request.getParameter("_slientotp");    //cm.getString("_slientotp");

        String ip = request.getRemoteAddr();

        String sessionId = null;
        if (request.getSession().getAttribute("_userSessinId") != null) {
            sessionId = request.getSession().getAttribute("_userSessinId").toString();
        }

        RssUserCerdentials _userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssWebUserCerdentials");
        int type = (Integer) request.getSession().getAttribute("type");
        VerifyRequest vReq = new VerifyRequest();
        if (type == SetAuthType.WEBTOKEN) {
            vReq.setCategory(2);
            vReq.setSubcategory(1);
            vReq.setCredential(OTPTOVerify);
        } 
//        else if (type == SetAuthType.PKI) {
//            String siggenerated = (String) request.getParameter("_dataToSign");
//            String verifyData = (String) request.getParameter("_signData");
//            vReq.setCategory(6);
//            vReq.setSubcategory(1);
//            vReq.setCredential(OTPTOVerify);
//            vReq.setPkisignData(verifyData);
//            vReq.setPkiplainData(siggenerated);
//        }

        AxiomStatus astatus = axwrap.verifyCredential(sessionId, _userObj.getRssUser().getUserId(), vReq, null);

        ////Response.Redirect(Session["_destinationUrl"].ToString());
        request.getSession().setAttribute("_APWebTokenResult", astatus.getErrorcode());
        request.getSession().setAttribute("_APWebTokenResultMessage", astatus.getError());

        if (astatus.getErrorcode() == 0) {
//           s String urltoredirect = request.getSession().getAttribute("./2fa.jsp").toString();

            response.sendRedirect("./2fa.jsp");
            // Server.Transfer(urltoredirect);
            return;
        } else {

            response.sendRedirect("./register_inlined.jsp");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
