/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.web.token;

import axiom.web.service.AxiomWrapper;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class VerifyActCode extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
         try {
        AxiomWrapper axWrapper = new AxiomWrapper();
        String host = request.getContextPath().toLowerCase();

        String result = "success";
        String message = "going for web token authentication!!!";

        String _apActCode = (String) request.getParameter("_apregid");
        request.getSession().setAttribute("_apregid", _apActCode);
        //cm.getString("_apregid");

        String _apActCodeMessage = (String) request.getSession().getAttribute("_apActivationMessage");// cm.getString("_apActivationMessage"); 
        JSONObject json = new JSONObject();
        if (_apActCodeMessage == null || _apActCode == null) {
            json.put("_result", "error");
            json.put("_message", "Data is invalid/empty!!!");
            response.setContentType("application/json");
            out.print(json);
            return;
        } else {
            if (_apActCodeMessage.equals(_apActCode)) {
                json.put("_result", "success");
                json.put("_message", "Activation Code is valid and proceed with setting your PIN to safegaurd Web PIN token!!!");
                response.setContentType("application/json");
                out.print(json);

                request.getSession().setAttribute("_apActivationMessage", null);

                return;

            } else {
                json.put("_result", "error");
                json.put("_message", "Activation Code is invalid!!!");
                response.setContentType("application/json");
                out.print(json);
                return;
            }

        }

    } catch (Exception ex) {
        ex.printStackTrace();
    }


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
