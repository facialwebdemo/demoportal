/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.web.token;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class AssignAndSendRegCodePKI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AxiomStatus aStatus = null;
        PrintWriter out = response.getWriter();
        AxiomWrapper axWrapper = new AxiomWrapper();
        //  AxiomWrapper axWrapper = new AxiomWrapper();
        String host = request.getContextPath().toLowerCase();
         String tokenType=request.getParameter("tokenType");
        response.setContentType("application/json");
        String result = "success";
        String message = "Web Token updated successfully!!!";
        JSONObject json = new JSONObject();
        int retValue = 0;

        String sessionId = null;
        if (request.getSession().getAttribute("_userSessinId") == null) {
            json.put("_result", "error");
            json.put("_message", "Invalid Session!!!");
            response.setContentType("application/json");
            out.print(json);
            return;
        }
        sessionId = request.getSession().getAttribute("_userSessinId").toString();

        RssUserCerdentials _userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");// (RssUserCerdentials)request.getSession().getAttribute("_apuserObj");

        if (_userObj == null) {

            json.put("_result", "error");
            json.put("_message", "User not found!!!");

            response.setContentType("application/json");
            out.print(json);
            return;

        }

        String iStatusWebToken = null;
        if (request.getSession().getAttribute("_apWebTokenStatus") != null) {
            iStatusWebToken = request.getSession().getAttribute("_apWebTokenStatus").toString();

        }
        RssUserCerdentials cerdentials = new RssUserCerdentials();
        if (iStatusWebToken == null
                || iStatusWebToken.equals("null")
                || iStatusWebToken.equals("-10")) {  // unassigned

//        Array.Clear(_userObj.tokenDetails, 0, _userObj.tokenDetails.Length);
            int i_category = 2; // software token
            int i_subcategory = 1; // web token
             int type = (Integer) request.getSession().getAttribute("type");
             if(tokenType!=null)
             {
             type=Integer.parseInt(tokenType);
             }
            AxiomCredentialDetails axiomCredentialDetails = new AxiomCredentialDetails();
            if(type == SetAuthType.WEBTOKEN){
//            axiomCredentialDetails.setCategory(i_category); // 2 - sw token
//            axiomCredentialDetails.setSubcategory(i_subcategory);//1 - web token
//            }
//            else if(type == SetAuthType.PKI){
                i_category = 5;
                i_subcategory =1 ;
            axiomCredentialDetails.setCategory(i_category); // 2 - sw token
            axiomCredentialDetails.setSubcategory(i_subcategory);//1 - web token
            }
            else if(type==8)
            {
                i_category = 2;
                i_subcategory =2;
            axiomCredentialDetails.setCategory(i_category); // 2 - sw token
            axiomCredentialDetails.setSubcategory(i_subcategory);//1 - web token
                
            }
             _userObj.getTokenDetails().add(axiomCredentialDetails);
            RssUserCerdentials rssCred= new RssUserCerdentials();
            rssCred.getTokenDetails().add(axiomCredentialDetails);
            rssCred.setRssUser(_userObj.getRssUser());
            
            aStatus = axWrapper.AssignCredential(sessionId,rssCred , null, null);
            
            if(aStatus.getErrorcode()==0)
            {
            
                aStatus = axWrapper.sendNotification(sessionId, _userObj.getRssUser().getUserId(), i_category, null, null);
           
            }
            if (aStatus != null && aStatus.getErrorcode() != 0) { // success for assign

                json.put("_result", "error");
                json.put("_message", "Web Token Assignment Failed!!!");

                response.setContentType("application/json");
                out.print(json);
                return;
            }
            System.out.println("aStatus.getRegcode() :"+aStatus.getRegcode());
        } else {
            
            aStatus = axWrapper.sendNotification(sessionId, _userObj.getRssUser().getUserId(),1, null, null);

        }
         String msgToSend = aStatus.getRegcode();
         String userIDToSend = request.getSession().getAttribute("_username").toString();
         boolean bActCodeSendStatus = false;
           if(aStatus.getErrorcode()==0)
           {
            bActCodeSendStatus=true;
           }
              if (bActCodeSendStatus == true) { // success for send reg code
            request.getSession().setAttribute("_apActivationMessage", aStatus.getRegcode());
            request.getSession().setAttribute("ApmessageStatus", "Sent");
            request.getSession().setAttribute("_rssWebUserCerdentials", _userObj);
            request.getSession().setAttribute("_rssDemoUserCerdentials", _userObj);
            json.put("_result", "success");
            json.put("_message", "Registration Code is send to your registered mobile phone number!!!");
//        var keybytes = Encoding.UTF8.GetBytes(Session["SessionIdentity"].ToString().Substring(0, 16));
//        var iv = Encoding.UTF8.GetBytes(Session["SessionIdentity"].ToString().Substring(7, 16));
//
//        byte[] enc = AxiomWrapper.EncryptStringToBytes(json.ToString(), keybytes, iv);
//        string op = Convert.ToBase64String(enc);
            JSONObject jnew = new JSONObject();
//        jnew.Add("ResultData", op);
            response.setContentType("application/json");
            out.print(json);

            return;
        } else {
            result = "error";
            message = "Failed to send registration code!!!";

            json.put("_result", result);
            json.put("_message", message);

            response.setContentType("application/json");
            out.print(json);
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
