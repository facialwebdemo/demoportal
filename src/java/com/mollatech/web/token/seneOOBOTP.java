/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.web.token;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class seneOOBOTP extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        AxiomStatus aStatus = null;
        PrintWriter out = response.getWriter();
        AxiomWrapper axWrapper = new AxiomWrapper();
        
        //  AxiomWrapper axWrapper = new AxiomWrapper();
        String host = request.getContextPath().toLowerCase();
         String tokenType=request.getParameter("tokenType");
        response.setContentType("application/json");
        String result = "success";
        String message = "OTP Sent Successfully!!!";
        JSONObject json = new JSONObject();
        int retValue = 0;
     
        String sessionId = null;
         sessionId = request.getSession().getAttribute("_userSessinId").toString();
         
         
        if (request.getSession().getAttribute("_userSessinId") == null) {
            json.put("_result", "error");
            json.put("_message", "Invalid Session!!!");
            response.setContentType("application/json");
            out.print(json);
            return;
        }
        
        JSONObject payload = new JSONObject();
        HttpSession session = request.getSession();
        String _locationChunk = "";
         String _lattitude = session.getAttribute("_lattitude").toString();
        String _longitude = session.getAttribute("_longitude").toString();
       _locationChunk= session.getAttribute("_locationChunk").toString();
//        PrintWriter out = response.getWriter();
        int locationType = 2;
        payload.put("ip", request.getRemoteAddr());
        payload.put("longi", _longitude);
        payload.put("latti", _lattitude);
        payload.put("txType", AxiomWrapper.REGISTERUSER);
        payload.put("locationChunk", _locationChunk);
        payload.put("locationType", locationType);
        
        byte[] bytePayload = Base64.encode(payload.toString().getBytes());
        String strPayLoad = new String(bytePayload);
//          strPayLoad = new String(bytePayload);
        if (_lattitude.equalsIgnoreCase("latitude")) {
            _lattitude = null;
            _longitude = null;
            strPayLoad = null;
        }
       RssUserCerdentials _userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");// (RssUserCerdentials)request.getSession().getAttribute("_apuserObj");
   AxiomStatus astatus=axWrapper.sendNotification(sessionId, _userObj.getRssUser().getUserId(),3, null, null);
       if(astatus.getErrorcode()==0)
       {
         json.put("_result", "success");
            json.put("_message", "OTP Send Successfully");
            response.setContentType("application/json");
            out.print(json);
            return;   
       }
       else
       {
           json.put("_result", "failed");
            json.put("_message", "failed");
            response.setContentType("application/json");
            out.print(json);
            return;   
       }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
