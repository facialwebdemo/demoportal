/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.web.token;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.common.AxiomProtectConnector;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomTokenData;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class ActivateWebToken extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {

            AxiomWrapper axWrapper = new AxiomWrapper();
            String host = request.getContextPath().toLowerCase();

            String result = "success";
            String message = "going for web token authentication!!!";
            JSONObject json = new JSONObject();

            //String regcode =(string)Request.Params["_apregid"];
            String regcode = request.getSession().getAttribute("_apregid").toString();   // cm.getString("_apregid");     
JSONObject payload = new JSONObject();
        HttpSession session = request.getSession();
        String _locationChunk = "";
         String _lattitude = session.getAttribute("_lattitude").toString();
        String _longitude = session.getAttribute("_longitude").toString();
       _locationChunk= session.getAttribute("_locationChunk").toString();
//        PrintWriter out = response.getWriter();
        int locationType = 2;
        payload.put("ip", request.getRemoteAddr());
        payload.put("longi", _longitude);
        payload.put("latti", _lattitude);
        payload.put("txType", AxiomWrapper.REGISTERUSER);
        payload.put("locationChunk", _locationChunk);
        payload.put("locationType", locationType);
        
        byte[] bytePayload = Base64.encode(payload.toString().getBytes());
        String strPayLoad = new String(bytePayload);
//          strPayLoad = new String(bytePayload);
        if (_lattitude.equalsIgnoreCase("latitude")) {
            _lattitude = null;
            _longitude = null;
            strPayLoad = null;
        }
            if (regcode != null
                    && request.getSession().getAttribute("_userSessinId") != null
                    && request.getSession().getAttribute("_rssWebUserCerdentials") != null) {

                String ip = request.getRemoteAddr();

                //strPayLoad = MakeMyPayLoad(Request.QueryString["_lattitude"].ToString(), Request.QueryString["_longitude"].ToString(), ip
                String sessionId = request.getSession().getAttribute("_userSessinId").toString();
                RssUserCerdentials _userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssWebUserCerdentials");

                response.setContentType("application/json");

                int icategory = 2;
                int isubcategory = 1;
                int type = (Integer) request.getSession().getAttribute("type");
                RssUserCerdentials rssWb= new RssUserCerdentials();
                if (_userObj.getTokenDetails() != null) {
                    for (int i = 0; i < _userObj.getTokenDetails().size(); i++) {
                        AxiomCredentialDetails axiomCredentialDetails = _userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (icategory == axiomCredentialDetails.getCategory()) {
                                if (type == SetAuthType.WEBTOKEN) {
                                    axiomCredentialDetails.setStatus(1); //mark as active
                                      rssWb.getTokenDetails().add(axiomCredentialDetails);
                                }
//                                else if (type == SetAuthType.PKI) {
//                                    axiomCredentialDetails.setStatus(1); //mark as active
//                                    axiomCredentialDetails.setCategory(8); //mark as active
//                                 rssWb.getTokenDetails().add(axiomCredentialDetails);
//                                }
                               
                            }

                            
                        }
                       
                    }
                     rssWb.setRssUser(_userObj.getRssUser());
                }

                //2 - get token data
                AxiomTokenData secret = axWrapper.ActivateToken(sessionId, rssWb, regcode, strPayLoad);

                if (secret != null) { // success

                    String UtcTime = (String) request.getParameter("_apusertime");
                    String deviceType = (String) request.getParameter("_apdevicetype");

                    // String UtcTime = cm.getString("_apusertime");
                    //String deviceType = cm.getString("_apdevicetype");
                    if (deviceType == null || UtcTime == null) {

                        json.put("_result", "error");
                        json.put("_message", "Device Type OR Time is not defined!!!");
                        response.setContentType("application/json");
                        out.print(json);

                        return;
                    }

                    long pubExpiretime = 0;

                    if (deviceType.equals("private")) {
                        long expirytime = secret.getPublicWebTokenExpiryTime() * 60 * 1000 * 365 * 2;  // 2 years
                        pubExpiretime = Long.parseLong(UtcTime) + expirytime;
                        //DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                        //DateTime date = start.AddMilliseconds(pubExpiretime).ToLocalTime();
                    } else if (deviceType.equals("public")) {
                        long expirytime = secret.getPublicWebTokenExpiryTime() * 60 * 1000;     // 10 minutes
                        pubExpiretime = Long.parseLong(UtcTime) + expirytime;
                        // DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                        // DateTime date = start.AddMilliseconds(pubExpiretime).ToLocalTime();
                    }

                    // axWrapper.PrintLogs("Client PC UtcTime is " + date);
                    json.put("_result", "success");
                    Date d = new Date();

                    byte[] data = AxiomProtectConnector.Bas64SHA1Inner(_userObj.getRssUser().getUserId());
                    String wtidq = new String(data);
                    String hex = new String(data).replace("", "-");
                    String wtid = hex;
                    if (type == SetAuthType.WEBTOKEN) {
                        json.put("_pubTokenExpirytime", pubExpiretime);
                        json.put("_identifier", wtid);
                        json.put("_secret", secret.getOTPSecret());
                        json.put("_otplength", secret.getOTPLength());
                        json.put("_otpduration", secret.getOTPduration());
                        json.put("_certPrivateKey", secret.getCertificatePrivateKey());
                        json.put("_pinAttemptCount", secret.getInvalidPinAttempt());
                        json.put("_type", (Integer) request.getSession().getAttribute("type"));
                        response.setContentType("application/json");
                        out.print(json);
                        return;
                    } 
                } else {
                    result = "error";
                    message = "Failed to activate your token!!!";
                    json.put("_result", result);
                    json.put("_message", message);

                    //String callbackstrr = Request.Params["jsoncallback"];
                    //String strresponser = callbackstrr + "(" + json.ToString() + ")";
                    response.setContentType("application/json");
                    out.print(json);

                    return;

                }
            } else {
                result = "error";
                message = "Insufficient Parameters!!!";

                json.put("_result", result);
                json.put("_message", message);
                //String callbackstrr = Request.Params["jsoncallback"];
                //String strresponser = callbackstrr + "(" + json.ToString() + ")";
                response.setContentType("application/json");
                out.print(json);

                return;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
